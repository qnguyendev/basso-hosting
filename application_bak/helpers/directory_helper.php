<?php if (!defined('BASEPATH')) exit('No direct access script allowed');
if (!function_exists('create_folder')) {
    function create_folder()
    {
        if (!is_dir('./uploads/' . date('Y')))
            mkdir('./uploads/' . date('Y'));

        if (!is_dir('./uploads/' . date('Y/m')))
            mkdir('./uploads/' . date('Y/m'));

        if (!is_dir('./uploads/' . date('Y/m/d')))
            mkdir('./uploads/' . date('Y/m/d'));
    }
}