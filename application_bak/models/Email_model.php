<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Email
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 */
class Email_model extends CI_Model
{
    public function insert(string $subject, string $content, $send_to, string $bcc = null)
    {
        if (!is_array($send_to)) {
            $data = [
                'subject' => $subject,
                'content' => $content,
                'created_time' => time(),
                'to' => $send_to
            ];

            if ($bcc != null)
                $data['bcc'] = $bcc;

            $this->db->insert('email_jobs', $data);
            return $this->db->insert_id();
        }

        $data = [];
        foreach ($send_to as $to) {
            $item = [
                'subject' => $subject,
                'content' => $content,
                'created_time' => time(),
                'to' => $to
            ];

            if ($bcc != null)
                $item['bcc'] = $bcc;
            array_push($data, $item);
        }

        $this->db->insert_batch('email_jobs', $data);
        return null;
    }

    public function get(int $id = null)
    {
        if ($id != null)
            $this->db->where('id', $id);
        else {
            $this->db->where('sent', 0);
            $this->db->order_by('created_time', 'desc');
            $this->db->limit(1);
        }
        return $this->db->get('email_jobs')->row();
    }

    /**
     * Đánh dấu email đã được gửi
     * @param $id
     */
    public function set_sent($id)
    {
        $this->db->where('id', $id);
        $this->db->update('email_jobs', ['sent' => 1]);
    }

    /**
     * Ghi log email bị lỗi
     * @param $id
     * @param $log
     */
    public function set_log($id, $log)
    {
        $this->db->where('id', $id);
        $this->db->update('email_jobs', ['log' => $log]);
    }

    /**
     * Xóa những email từ 30 ngày trước
     * @param int $day
     */
    public function clean(int $day = 30)
    {
        $this->db->where('created_time <=', time() - $day * 3600);
        $this->db->delete('email_jobs');
    }

    public function delete(int $id)
    {
        $this->db->where('id', $id);
        $this->db->delete('email_jobs');
    }
}