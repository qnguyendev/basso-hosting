<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Widget_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Widget_model extends CI_Model
{
    public function get($area_id, $type = null, $limit = 0)
    {
        $this->db->select('t.*, o.area_id as area');
        $this->db->from('widgets t');
        $this->db->join('widgets_display o', 't.id = o.widget_id', 'left');
        $this->db->where('o.area_id', $area_id);

        if ($type != null) {
            if (!is_array($type))
                $this->db->where('t.type', $type);
            else
                $this->db->where_in('t.type', $type);
        }

        $this->db->where('t.active', 1);
        if ($limit > 0) {
            $this->db->limit($limit);
            return $this->db->get()->row();
        }

        $this->db->order_by('t.order', 'asc');
        return $this->db->get()->result();
    }

    public function get_by_id(int $id)
    {
        $this->db->where('id', $id);
        return $this->db->get('widgets')->row();
    }
}