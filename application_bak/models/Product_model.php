<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Product_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 */
class Product_model extends CI_Model
{
    /**
     * Danh sách bài viết cho sitemap
     */
    public function get_sitemap()
    {
        $this->db->select('name, slug, created_time, updated_time');
        $this->db->where('active', 1);
        return $this->db->get('products')->result();
    }

    /**
     * Lấy sản phẩm theo danh mục
     * @param $term_id
     * @param int $page
     * @param int $limit
     * @param null $order
     * @param $filter
     * @return array
     */
    public function get_by_term($term_id = 0, $page = 1, $limit = 15, $order = null, $filter = null)
    {
        $offset = ($page - 1) * $limit;
        $offset = $offset < 0 ? 0 : $offset;

        $this->db->select('t.*');
        $this->db->select('(select path from products_images where product_id = t.id limit 1) as image');
        $this->db->from('products t');
        $this->db->join('products_terms o', 't.id = o.product_id', 'left');

        if ($filter != null) {
            $this->db->join('products_attribute_values d', 'd.product_id = t.id', 'left');
            $this->db->where_in('d.attribute_value_id', $filter);
        }

        if ($term_id != 0)
            $this->db->where('o.term_id', $term_id);

        $this->db->where('t.active', 1);
        $this->db->limit($limit);
        $this->db->offset($offset);

        if ($order != null) {
            switch ($order) {
                case 'name':
                    $this->db->order_by('t.name', 'asc');
                    break;
                case 'price-asc':
                    $this->db->order_by('t.price', 'asc');
                    break;
                case 'price-desc':
                    $this->db->order_by('t.price', 'desc');
                    break;
                case 'new':
                    $this->db->order_by('t.created_time', 'desc');
                    break;
            }
        }

        $this->db->group_by('t.id');
        return $this->db->get()->result();
    }

    /**
     * @param $term_id
     * @param array|null $filter
     * @return int
     */
    public function count_by_term($term_id = 0, array $filter = null)
    {
        $this->db->from('products t');
        $this->db->join('products_terms o', 't.id = o.product_id', 'left');

        if ($filter != null) {
            $this->db->join('products_attribute_values d', 'd.product_id = t.id', 'left');
            $this->db->where_in('d.attribute_value_id', $filter);
        }

        if ($term_id != 0)
            $this->db->where('o.term_id', $term_id);

        $this->db->where('t.active', 1);

        return $this->db->count_all_results();
    }

    public function get_images($product_id)
    {
        $this->db->select('path');
        if (is_array($product_id))
            $this->db->where_in('product_id', $product_id);
        else
            $this->db->where('product_id', $product_id);

        return $this->db->get('products_images')->result();
    }

    /**
     * Lấy sản phẩm theo slug
     * @param $slug
     * @return array
     */
    public function get_by_slug($slug)
    {
        $this->db->where('slug', $slug);
        return $this->db->get('products')->row();
    }

    public function get_by_id($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('products')->row();
    }

    /**
     * Lấy tags theo id sản phẩm
     * @param $id
     * @return array
     */
    public function get_tags($id)
    {
        $this->db->select('o.*');
        $this->db->from('products_tags t');
        $this->db->join('tags o', 't.tag_id = o.id', 'right');
        $this->db->where('t.product_id', $id);
        return $this->db->get()->result();
    }

    /**
     * Lấy danh sách thuộc tính sản phẩm
     * @param int|null $product_id
     * @return array
     */
    public function get_attributes($product_id = null)
    {
        if ($product_id == null) {
            $this->db->select('o.name as attribute, o.id as attribute_id, o.type, d.id, d.name, d.value');
            $this->db->from('attributes o');
            $this->db->join('attribute_values d', 'd.attribute_id = o.id', 'left');
            $this->db->where('o.active', 1);
            $this->db->group_by('d.id');
            return $this->db->get()->result();
        }

        $this->db->select('t.attribute_id, o.name as attribute, d.name, d.value, d.id, o.type');
        $this->db->from('products_attribute_values t');
        $this->db->join('attributes o', 't.attribute_id = o.id', 'left');
        $this->db->join('attribute_values d', 't.attribute_value_id = d.id', 'left');
        $this->db->where('t.product_id', $product_id);
        return $this->db->get()->result();
    }

    public function get_attribute_values($value_id)
    {
        $this->db->select('t.id, o.name as attribute, t.name as value, t.attribute_id');
        $this->db->from('attribute_values t');
        $this->db->join('attributes o', 't.attribute_id = o.id', 'left');

        if (!is_array($value_id))
            $this->db->where('t.id', $value_id);
        else
            $this->db->where_in('t.id', $value_id);

        return $this->db->get()->result();
    }

    /**
     * Lấy ID danh mục của sản phẩm
     * @param $id
     * @return array
     */
    public function get_terms($id)
    {
        $this->db->select('t.*');
        $this->db->from('terms t');
        $this->db->join('products_terms o', 't.id = o.term_id', 'left');
        $this->db->where('o.product_id', $id);
        return $this->db->get()->result();
    }

    /**
     * Lấy sản phẩm liên quan
     * @param $term_id
     * @param int $limit
     * @return array
     */
    public function get_related($term_id, $limit = 8)
    {
        $this->db->select('t.*');
        $this->db->select('(select path from products_images where product_id = t.id limit 1) as image');
        $this->db->from('products t');
        $this->db->join('products_terms o', 't.id = o.product_id', 'left');

        if (!is_array($term_id))
            $this->db->where('o.term_id', $term_id);
        else if (count($term_id) > 0)
            $this->db->where_in('o.term_id', $term_id);

        $this->db->where('t.active', 1);
        $this->db->limit($limit);
        $this->db->group_by('t.id');

        return $this->db->get()->result();
    }

    /**
     * @param $tag_id
     * @return array
     */
    public function get_by_tags($tag_id)
    {
        $this->db->select('t.*');
        $this->db->select('(select path from products_images where product_id = t.id limit 1) as image');
        $this->db->from('products t');
        $this->db->join('products_tags o', 'o.product_id = t.id', 'left');
        $this->db->where('t.active', 1);

        if (is_array($tag_id))
            $this->db->where_in('o.tag_id', $tag_id);
        else
            $this->db->where_in('o.tag_id', $tag_id);

        return $this->db->get()->result();
    }

    /**
     * Lấy sản phẩm hiển thị widget
     * @param $order
     * @param $limit
     * @return array
     */
    public function get_widget($limit, $order = 'newest')
    {
        $this->db->select('t.*');
        $this->db->select('(select path from products_images where product_id = t.id limit 1) as image');
        $this->db->from('products t');

        if ($order == 'newest')
            $this->db->order_by('t.created_time', 'desc');
        else if ($order == 'featured')
            $this->db->where('t.featured', 1);
        else if ($order == 'random')
            $this->db->order_by('rand()');
        else if ($order == 'sale') {
            $now = time();
            $this->db->where("(t.sale_price > 0 and (t.sale_from is null or t.sale_from <= $now) and (st.ale_to is null or t.sale_to >= $now))");
        }

        $this->db->where('t.active', 1);
        $this->db->limit($limit);

        return $this->db->get()->result();
    }

    /**
     * Tìm kiếm sản phẩm
     * @param string $key
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function search($key, $page = 1, $limit)
    {
        $this->db->select('t.*');
        $this->db->select('(select path from products_images where product_id = t.id limit 1) as image');
        $this->db->like('t.name', $key, 'both');
        $this->db->or_where('t.sku', strtoupper($key));
        $this->db->limit($limit);
        $this->db->offset(($page - 1) * $limit);

        return $this->db->get('products t')->result();
    }

    /**
     * Đếm số lượng sp tìm được
     * @param string $key
     * @return int
     */
    public function search_count($key)
    {
        $this->db->like('name', $key, 'both');
        $this->db->or_where('sku', strtoupper($key));
        return $this->db->count_all_results('products');
    }

    public function get_upsell_product(int $product_id)
    {
        $select = 't.id, t.name, t.price, t.sale_price, t.sale_from, t.sale_to, t.sku,
            (select path from products_images where product_id = t.id limit 1) as image';
        $this->db->select($select);
        $this->db->from('products t');
        $this->db->join('products_upsell o', 't.id = o.related_id', 'left');
        $this->db->where_in('o.product_id', $product_id);
        $this->db->group_by('t.id');
        return $this->db->get()->result();
    }

    public function get_related_articles(int $product_id)
    {
        $this->db->select('t.*');
        $this->db->from('articles t');
        $this->db->join('products_articles o', 't.id = o.article_id', 'left');
        $this->db->where('o.product_id', $product_id);
        $this->db->group_by('t.id');
        return $this->db->get()->result();
    }
}