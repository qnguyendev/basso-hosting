<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Project_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Project_model extends \CI_Model
{
    public function get_by_term($term_id, $page = 1, $limit = PAGING_SIZE)
    {
        $this->db->select('t.*, (select image_path from projects_images where project_id = t.id limit 1) as image');
        $this->db->from('projects t');
        $this->db->join('projects_terms o', 'o.project_id = t.id', 'left');
        $this->db->where('o.term_id', $term_id);
        $this->db->limit($limit);
        $this->db->offset(($page - 1) * $limit);
        return $this->db->get()->result();
    }

    public function count_by_term($term_id)
    {
        $this->db->where('term_id', $term_id);
        return $this->db->count_all_results('projects_terms');
    }

    public function get_by_slug($slug)
    {
        $this->db->where('slug', $slug);
        return $this->db->get('projects')->row();
    }

    public function get_terms($project_id)
    {
        $this->db->select('o.*');
        $this->db->from('projects_terms t');
        $this->db->join('terms o', 't.term_id = o.id', 'left');
        $this->db->where('project_id', $project_id);
        return $this->db->get()->result();
    }

    public function get_images($project_id)
    {
        $this->db->select('image_path as path');
        $this->db->where('project_id', $project_id);
        return $this->db->get('projects_images')->result();
    }
}