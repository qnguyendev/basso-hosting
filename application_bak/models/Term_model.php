<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Term_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property CI_Loader $load
 */
class Term_model extends CI_Model
{
    public function get($active = -1)
    {
        $this->db->select('id, parent_id, name, slug, new_tab, type, external_url');

        if (in_array($active, [0, 1]))
            $this->db->where('active', $active);

        $this->db->where_in('type', get_terms());
        $this->db->order_by('order', 'asc');
        return $this->db->get('terms')->result();
    }

    public function get_by_slug($slug)
    {
        $this->db->where('slug', $slug);
        return $this->db->get('terms')->row();
    }

    public function get_by_type($type, $sort = 'asc', $count = false)
    {
        if ($count) {
            $this->db->select('t.name, t.id, t.slug, t.type, ifnull(t.parent_id, 0) as parent_id, t.new_tab, t.type, count(o.term_id) as count');
            $this->db->from('terms t');

            if ($type == 'product')
                $this->db->join('products_terms o', 'o.term_id = t.id', 'left');
            else if ($type == 'post')
                $this->db->join('articles_terms o', 'o.term_id = t.id', 'left');

            $this->db->group_by('t.id');
            $this->db->order_by('t.name', $sort);

        } else {
            $this->db->select('t.*');
            $this->db->from('terms t');
            $this->db->where('t.type', $type);
        }

        $this->db->where('t.type', $type);
        if ($type != 'home')
            $this->db->where('t.active', 1);
        return $this->db->get()->result();
    }
}