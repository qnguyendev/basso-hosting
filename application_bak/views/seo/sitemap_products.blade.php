<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach($data as $item)
        <url>
            <loc>{{base_url(product_url($item->slug))}}</loc>
            <lastmod>{{$item->updated_time == null ? date('Y-m-d', $item->created_time) : date('Y-m-d', $item->updated_time)}}</lastmod>
        </url>
    @endforeach
</urlset>