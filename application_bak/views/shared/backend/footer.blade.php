<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<script src="/assets/vendor/modernizr/modernizr.custom.js"></script>
<script src="/assets/vendor/js-storage/js.storage.js"></script>
<script src="/assets/vendor/jquery/dist/jquery.js"></script>
<script src="/assets/vendor/popper.js/dist/umd/popper.js"></script>
<script src="/assets/vendor/bootstrap/dist/js/bootstrap.js"></script>
<script src="/assets/vendor/hullabaloo.js"></script>
<script src="/assets/vendor/moment/min/moment-with-locales.js"></script>
<script src='/assets/js/core/knockout-3.4.2.js'></script>
<script src='/assets/js/core/knockout.validation.min.js'></script>
<script src='/assets/js/core/knockout.mapping-latest.js'></script>
<script src='/assets/js/core/knockstrap.min.js'></script>
<script src="/assets/vendor/sweetalert/dist/sweetalert.min.js"></script>
<script src='/assets/vendor/datetimepicker-master/jquery.datetimepicker.full.min.js'></script>
<script src='/assets/vendor/select2/dist/js/select2.full.js'></script>
<script src='/assets/js/resources/string.js'></script>
<script src='/assets/js/resources/functions.js'></script>
<script src="/assets/js/core/underscore.min.js"></script>
<script src="/assets/js/core/underscore.ko.min.js"></script>
<script src='/assets/js/core/ko.extends.js?v={{time()}}'></script>
<script src="/assets/js/core/app.js"></script>
@if(isset($image_uploader))
    <script src="/assets/js/image_upload.js"></script>
    <!--script-- src="/assets/js/seo_editor.js"></script-->
@endif
@yield('script')
@yield('footer')