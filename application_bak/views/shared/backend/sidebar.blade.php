<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
global $current_user, $user_roles, $theme_config;
require './theme/cpanel.php';

$controller = strtolower($this->router->fetch_class());
$action = strtolower($this->router->fetch_method());
$module = strtolower($this->router->fetch_module());
?>
<!-- sidebar-->
<aside class="aside-container">
    <!-- START Sidebar (left)-->
    <div class="aside-inner">
        <nav class="sidebar" data-sidebar-anyclick-close="">
            <!-- START sidebar nav-->
            <ul class="sidebar-nav">
                <?php
                foreach ($sidebars as $parent) :
                    if (!has_role($parent)) continue;

                    $has_child = isset($parent['items']);
                    if (!$has_child) :
                        $output = '<li';
                        if ($controller == $parent['controller'])
                            $output .= ' class="active"';

                        $url = $parent['module'] . '/' . $parent['controller'];
                        if (isset($parent['action'])) {
                            $_action = (!is_array($parent['action']) ? $parent['action'] : $parent['action'][0]);
                            $_action = $_action == 'index' ? null : $_action;
                            $url .= '/' . $_action;
                        }
                        $output .= '><a href="' . base_url($url) . '">';

                        if (isset($parent['icon']))
                            $output .= '<em class="' . $parent['icon'] . '"></em>';
                        $output .= '<span>' . $parent['name'] . '</span>';
                        $output .= '</a></li>';
                        echo $output;
                    else:
                        $child_controllers = array_column($parent['items'], 'controller');
                        $child_actions = array_column($parent['items'], 'action');

                        $output = '<li';
                        if (in_array($controller, $child_controllers))
                            $output .= ' class="active"';

                        $div_id = uniqid();
                        $url = '#collapsed_' . $div_id;
                        if (isset($parent['module'])) {
                            $url = "/{$parent['module']}";
                            if (isset($parent['controller'])) {
                                $url .= "/{$parent['controller']}";
                                if (isset($parent['action'])) {
                                    if (!is_array($parent['action']))
                                        $url .= "/{$parent['action']}";
                                }
                            }
                            $output .= '><a href="' . $url . '">';
                        } else
                            $output .= '><a data-toggle="collapse" href="' . $url . '">';

                        if (isset($parent['icon']))
                            $output .= '<em class="' . $parent['icon'] . '"></em>';
                        $output .= '<span>' . $parent['name'] . '</span></a>';

                        $output .= '<ul class="sidebar-nav sidebar-subnav collapse"';
                        $output .= ' id="collapsed_' . $div_id . '">';

                        foreach ($parent['items'] as $item):
                            if (!has_role($item)) continue;

                            $output .= '<li';
                            if ($controller == $item['controller']) {
                                if (is_array($item['action'])) {
                                    if (in_array($action, $item['action']))
                                        $output .= ' class="active"';
                                } else if ($action == $item['action'])
                                    $output .= ' class="active"';
                            }

                            $url = $item['module'] . '/' . $item['controller'];
                            if (isset($item['action'])) {
                                $_action = (!is_array($item['action']) ? $item['action'] : $item['action'][0]);
                                $_action = $_action == 'index' ? null : $_action;
                                $url .= '/' . $_action;
                            }

                            $output .= '><a href="' . base_url($url) . '">';
                            $output .= '<span class="sidebar-normal">';
                            $output .= $item['name'] . '</span>';
                            $output .= '</a></li>';
                        endforeach;

                        $output .= '</ul></li>';
                        echo $output;
                    endif;
                endforeach;
                ?>
            </ul>
            <!-- END sidebar nav-->
        </nav>
    </div>
    <!-- END Sidebar (left)-->
</aside>