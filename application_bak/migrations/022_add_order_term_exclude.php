<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Migration_add_order_term_exclude * @property CI_DB_forge $dbforge
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 */
class Migration_add_order_term_exclude extends CI_Migration
{
    protected $_table_name = "order_term_exclude";

    public function up()
    {

    }

    public function down()
    {
        $this->dbforge->drop_table($this->_table_name, TRUE);
    }
}