<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Migration_alter_articles * @property CI_DB_forge $dbforge
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 */
class Migration_alter_articles extends CI_Migration
{
    protected $_table_name = "articles";

    public function up()
    {
        $this->dbforge->add_column($this->_table_name, [
            'views' => ['type' => 'int', 'null' => true, 'default' => 0]
        ]);
    }

    public function down()
    {
        $this->dbforge->drop_table($this->_table_name, TRUE);
    }
}