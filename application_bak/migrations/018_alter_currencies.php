<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Migration_alter_currencies * @property CI_DB_forge $dbforge
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 */
class Migration_alter_currencies extends CI_Migration
{
    protected $_table_name = "currencies";

    public function up()
    {
        $this->dbforge->add_field([
            'id' => ['type' => 'int', 'auto_increment' => true],
            'created_time' => ['type' => 'int']
        ]);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->_table_name, TRUE);
    }

    public function down()
    {
        $this->dbforge->drop_table($this->_table_name, TRUE);
    }
}