<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Migration_alter_crawled_amz_products * @property CI_DB_forge $dbforge
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 */
class Migration_alter_crawled_amz_products extends CI_Migration
{
    protected $_table_name = "crawled_amz_products";

    public function up()
    {
        $this->dbforge->modify_column($this->_table_name, [
            'brand' => ['type' => 'varchar', 'constraint' => 1024, 'null' => true]
        ]);
    }

    public function down()
    {
        $this->dbforge->drop_table($this->_table_name, TRUE);
    }
}