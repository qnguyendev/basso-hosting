<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Migration_alter_order_term_list * @property CI_DB_forge $dbforge
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 */
class Migration_alter_order_term_list extends CI_Migration
{
    protected $_table_name = "order_term_list";

    public function up()
    {
        $this->dbforge->add_column($this->_table_name, [
            'type' => ['type' => 'varchar', 'constraint' => 32, 'default' => 'include']
        ]);
        $this->db->query(add_foreign_key($this->_table_name, 'order_term_id', 'order_terms(id)',
            'CASCADE', 'NO ACTION'));
    }

    public function down()
    {
        $this->dbforge->drop_table($this->_table_name, TRUE);
    }
}