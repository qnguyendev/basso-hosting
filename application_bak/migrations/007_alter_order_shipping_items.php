<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Migration_alter_order_shipping_items * @property CI_DB_forge $dbforge
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 */
class Migration_alter_order_shipping_items extends CI_Migration
{
    protected $_table_name = "order_shipping_items";

    public function up()
    {
        $this->dbforge->modify_column($this->_table_name,
            [
                'name' => ['type' => 'varchar', 'constraint' => 1024]
            ]);
    }

    public function down()
    {
        $this->dbforge->drop_table($this->_table_name, TRUE);
    }
}