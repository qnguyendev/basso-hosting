<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Migration_alter_customer_order_items * @property CI_DB_forge $dbforge
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 */
class Migration_alter_customer_order_items extends CI_Migration
{
    protected $_table_name = "customer_order_items";

    public function up()
    {
        $this->dbforge->add_column($this->_table_name, [
            'web_shipping_fee' => ['type' => 'float', 'default' => 0, 'null' => true]
        ]);
    }

    public function down()
    {
        $this->dbforge->drop_table($this->_table_name, TRUE);
    }
}