<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Cart
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Option_model $option_model
 * @property Image_model $image_model
 * @property CI_Cart $cart
 * @property Payment_model $payment_model
 * @property Payment_limit_model $payment_limit_model
 * @property Common_model $common_model
 * @property Customer_model $customer_model
 * @property Voucher_model $voucher_model
 * @property Customer_order_model $customer_order_model
 * @property Payment_history_model $payment_history_model
 * @property Country_model $country_model
 * @property Order_term_model $order_term_model
 * @property Email_model $email_model
 * @property Term_model $term_model
 */
class Cart extends Site_Controller
{
    private $_cart;
    private $_vn_pay;

    public function __construct()
    {
        parent::__construct();
        global $cart, $theme_config;
        $this->_cart = $cart;
        $this->_vn_pay = $theme_config['vn_pay'];

        $this->load->model('admin/image_model');
        $this->load->model('admin/voucher_model');
        $this->load->model('payment_model');
        $this->load->model('basso/payment_limit_model');
        $this->load->model('basso/customer_order_model');
        $this->load->model('basso/payment_history_model');
        $this->load->model('basso/country_model');
        $this->load->model('basso/order_term_model');
        $this->load->model('common_model');
        $this->load->model('management/customer_model');
        $this->load->model('admin/voucher_model');
        $this->load->helper('truemoney');
        $this->load->helper('basso/crawler');
        $this->load->model('email_model');
    }

    public function index()
    {
        set_head_index(false);
        set_head_title('Giỏ hàng');

        $this->blade->set('payments', $this->payment_model->get_active());
        $this->blade->set('payment_limits', $this->payment_limit_model->get());
        $this->blade->set('term', $this->term_model->get_by_slug('cac-dieu-khoan-va-dieu-kien'));
        return $this->blade->render();
    }

    public function GET_index()
    {
        $action = $this->input->get('action');
        if ($action == null)
            json_error('Yêu cầu không hợp lệ');

        switch ($action) {
            case 'init':
                $user_info = [
                    'name' => null,
                    'phone' => null,
                    'city_id' => null,
                    'district_id' => null,
                    'email' => null,
                    'address' => null
                ];

                $total = array_sum(array_column($this->_cart, 'total'));
                $curency_rate = $total >= 500 ? 26000 : 26000;
                for ($i = 0; $i < count($this->_cart); $i++) {
                    $this->_cart[$i]->sub_total = ($this->_cart[$i]->origin_price * $this->_cart[$i]->qty + $this->_cart[$i]->web_shipping_fee) * $curency_rate;
                    $this->_cart[$i]->sub_total += $this->_cart[$i]->qty * $this->_cart[$i]->origin_term_fee * $curency_rate;

                    if ($this->_cart[$i]->weight > 0.2)
                        $this->_cart[$i]->sub_total += $this->_cart[$i]->qty * $this->_cart[$i]->weight * 260000;
                }

                $data = [
                    'data' => $this->_cart,
                    'cities' => $this->common_model->get_cities(),
                    'districts' => []
                ];

                if ($this->ion_auth->logged_in()) {
                    $_user = $this->ion_auth->user()->row();
                    $user = $this->customer_model->get_by_id(intval($_user->id));

                    $user_info['name'] = $user->name;
                    $user_info['phone'] = $user->phone;
                    $user_info['email'] = $user->email;
                    $user_info['city_id'] = $user->city_id;
                    $user_info['district_id'] = $user->district_id;
                    $user_info['address'] = $user->address;

                    $data['districts'] = $this->common_model->get_districts($user->city_id);
                    $data['user'] = $user_info;
                }

                if (count($data['districts']) == 0)
                    $data['districts'] = $this->common_model->get_districts($data['cities'][0]->id);

                json_success(null, $data);
                break;
            case 'change-city':
                $id = $this->input->get('city_id');
                json_success(null, ['data' => $this->common_model->get_districts($id)]);
            default:
                json_error('Yêu cầu không hợp lệ');
                break;
        }
    }

    public function POST_index()
    {
        $this->form_validation->set_rules('action', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $action = $this->input->post('action');
        switch ($action) {
            case 'change-qty':
                $qty = $this->input->post('qty');
                $id = $this->input->post('id');
                $total = 0;
                for ($i = 0; $i < count($this->_cart); $i++)
                    $total = $this->_cart[$i]->origin_price * $qty;

                $this->cart->update(['rowid' => $id, 'qty' => $qty, 'total' => $total]);
                break;
            case 'update-item':
                $color = $this->input->post('color');
                $size = $this->input->post('size');
                $note = $this->input->post('note');
                $id = $this->input->post('id');
                $this->cart->update([
                    'rowid' => $id,
                    'size' => $size,
                    'color' => $color,
                    'note' => $note
                ]);
                json_success(null);
                break;
            case 'update-cart':

                $cart_detail = $this->_update_index();
                $cart_detail['total_discount'] = 0;


                $voucher = $this->input->post('voucher');
                $voucher = $this->voucher_model->get_by_code($voucher);
                if ($voucher != null) {
                    if ($voucher->active && $voucher->valid_from <= time() && $voucher->valid_to >= time()) {
                        $cart_detail['voucher'] = $voucher;
                    }
                }

                json_success(null, $cart_detail);
                break;
            case 'update-voucher':
                json_error('Mã giảm giá không tồn tại', $this->_update_index());
                break;
            case 'create':
                $this->_create_order();
                break;
            case 'apply-voucher':
                $voucher = $this->input->post('voucher');

                if ($voucher != null) {
                    $voucher = strtoupper(trim($voucher));
                    $voucher = $this->voucher_model->get_by_code($voucher);
                    if ($voucher == null)
                        json_error('Không tìm thấy mã giảm giá');

                    if (!$voucher->active || $voucher->valid_from > time())
                        json_error('Mã giảm giá không hợp lệ');

                    if ($voucher->valid_to < time())
                        json_error('Mã giảm giá hết hạn');

                    if ($voucher != null) {
                        if ($voucher->active && $voucher->valid_from <= time() && $voucher->valid_to >= time()) {
                            unset($voucher->valid_from);
                            unset($voucher->valid_to);
                            unset($voucher->active);
                            unset($voucher->created_time);
                            unset($voucher->limit);
                            unset($voucher->remaining);
                            unset($voucher->note);
                        }
                    }

                    json_success(null, ['voucher' => $voucher]);
                }

                json_error(null);
                break;
            default:
                json_error('Yêu cầu không hợp lệ');
                break;
        }
    }

    private function _create_order()
    {
        if (count($this->_cart) == 0)
            json_error('Không có sản phẩm trong đơn hàng');

        $this->form_validation->set_rules('name', null, 'required');
        $this->form_validation->set_rules('phone', null, 'required');
        $this->form_validation->set_rules('email', null, 'required');
        $this->form_validation->set_rules('address', null, 'required');
        $this->form_validation->set_rules('payment_method', null, 'required');
        $this->form_validation->set_rules('payment_limit', null, 'required|numeric');
        $this->form_validation->set_rules('city_id', null, 'required|numeric');
        $this->form_validation->set_rules('district_id', null, 'required|numeric');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $name = $this->input->post('name');
        $phone = $this->input->post('phone');
        $email = $this->input->post('email');
        $address = $this->input->post('address');
        $city_id = $this->input->post('city_id');
        $district_id = $this->input->post('district_id');
        $_payment_method = $this->input->post('payment_method');
        $payment_limit = $this->input->post('payment_limit');
        $note = $this->input->post('note');
        $voucher = $this->input->post('voucher');

        $payment_method = $this->payment_model->get_by_type($_payment_method);
        if ($payment_method == null)
            json_error('Không tìm thấy phương thức thanh toán');

        $payment_limit = $this->payment_limit_model->get($payment_limit);
        if ($payment_limit == null)
            json_error('Hạn mức thanh toán không hợp lệ');

        $cart_info = $this->_update_index();
        $items = [];

        $customer = [
            'id' => null,
            'name' => $name,
            'phone' => $phone,
            'email' => $email,
            'shipping_address' => $address,
            'order_place_id' => null,
            'city_id' => $city_id,
            'district_id' => $district_id
        ];

        if ($this->ion_auth->logged_in()) {
            $user = $this->ion_auth->user()->row();
            $customer['id'] = $user->id;
        }

        $countries = $this->country_model->get();
        $order = [
            'created_time' => time(),
            'status' => CustomerOrderStatus::PENDING,
            'web_shipping_fee' => 0,
            'sub_total' => 0,
            'total' => $cart_info['origin_total'],
            'country_id' => $countries[0]->id,
            'currency_rate' => 0,
            'currency_symbol' => $countries[0]->currency_symbol,
            'discount_code' => null,
            'discount_total' => 0,
            'discount_type' => null,
            'brand' => 'basso',
            'order_code' => $this->customer_order_model->create_order_code(time(), 'basso'),
            'branch' => null,
            'user_id' => null,
            'note' => $note,
            'website' => $this->_cart[0]->website,
            'approve_user_id' => null,
            'world_shipping_fee' => 0,
            'total_weight' => 0,
            'payment_method' => $_payment_method,
            'quantity' => array_sum(array_column($this->_cart, 'qty')),
            'discount_code' => strtoupper(trim($this->input->post('voucher'))),
            'fee_percent' => $payment_limit->fee
        ];

        $currency_rate = 26000;
        $order['currency_rate'] = $currency_rate;

        foreach ($this->_cart as $item) {
            $image_id = $this->image_model->insert($item->image);
            $this->image_model->set_active($image_id, 1);

            $_item = [
                'name' => $item->name,
                'link' => $item->link,
                'website' => $item->website,
                'requirement' => CustomerOrderItemRequirement::PENDING,
                'status' => CustomerOrderItemStatus::PENDING,
                'variations' => json_encode($item->variations),
                'note' => $item->note,
                'price' => $item->origin_price,
                'quantity' => $item->qty,
                'term_id' => null,
                'term_fee' => 0,
                'weight' => 0,
                'image_id' => $image_id,
                'term_id' => $item->term_id,
                'term_fee' => 0,
                'weight' => round(floatval($item->weight), 2),
                'web_shipping_fee' => $item->web_shipping_fee
            ];

            if ($_item['weight'] > 0.2) {
                $order['world_shipping_fee'] += 260000 * $_item['weight'] * $_item['quantity'];
                $order['total_weight'] += $_item['weight'] * $_item['quantity'];
            } else
                $_item['weight'] = 0;

            $order['web_shipping_fee'] += $item->web_shipping_fee;

            #region Danh mục phụ thu và số tiền phụ thu
            if ($item->term_id != null) {
                $order_term = $this->order_term_model->get($item->term_id);
                if ($order_term != null) {
                    $steps = $this->order_term_model->get_steps($item->term_id);
                    usort($steps, function ($a, $b) {
                        return $a->step >= $b->step;
                    });

                    for ($i = 0; $i < count($steps); $i++) {
                        if (floatval($_item['price']) >= $steps[$i]->step) {
                            if ($steps[$i]->type == OrderTermType::AMOUNT)
                                $_item['term_fee'] = $steps[$i]->amount;
                            elseif ($steps[$i]->type == OrderTermType::PERCENT) {
                                $fee = $_item['price'] * intval($steps[$i]->amount) / 100;
                                if ($fee < $order_term->min_fee) {
                                    $fee = $order_term->min_fee;
                                    $_item['term_fee'] = $fee * $currency_rate;
                                } else {
                                    $_item['term_fee'] = $currency_rate * $_item['price'] * intval($steps[$i]->amount) / 100;
                                }
                            }
                        }
                    }
                }
            }
            #endregion

            $order['sub_total'] += $_item['term_fee'] * $_item['quantity'];
            array_push($items, $_item);
        }

        $order['sub_total'] += ($order['web_shipping_fee'] + $order['total']) * $currency_rate + $order['world_shipping_fee'];
        $order['sub_total'] = $order['sub_total'] * ($payment_limit->fee + 100) / 100;

        $voucher = $this->voucher_model->get_by_code($voucher);
        if ($voucher != null) {
            if ($voucher->active && $voucher->valid_from <= time() && $voucher->valid_to >= time()) {
                $order['discount_total'] = $voucher->discount_value;
                $order['discount_type'] = $voucher->discount_type;

                if ($voucher->discount_type == DiscountType::AMOUNT) {
                    $order['sub_total'] -= $voucher->discount_value;
                    $order['discount_amount'] = $voucher->discount_value;
                } else if ($voucher->discount_type == DiscountType::PERCENT) {
                    $order['discount_max'] = $voucher->max_value;
                    $discount = $order['sub_total'] * $voucher->discount_value / 100;
                    if ($discount > $voucher->max_value && $voucher->max_value > 0)
                        $discount = $voucher->max_value;

                    $order['discount_amount'] = $discount;
                    $order['sub_total'] -= $discount;
                }
            }
        }

        $total_deposit = $cart_info['sub_total'] * $payment_limit->limit / 100;
        $order_id = $this->customer_order_model->insert($customer, $order, $items);
        //$this->customer_order_model->update_order_total($order_id);

        if ($order_id) {
            //$this->customer_order_model->update_order_total($order_id);
            $payment_type = PaymentType::COD;

            switch ($payment_method->type) {
                case 'bank-transfer':
                    $payment_type = PaymentType::TRANSFER;
                    break;
                case 'banking':
                    $payment_type = PaymentType::ONLINE;
                    break;
                case 'visa':
                    $payment_type = PaymentType::ONLINE;
                    break;
            };

            #region gửi email
            $order = $this->customer_order_model->get_by_id($order_id);
            $items = $this->customer_order_model->get_items($order_id);
            $order->payment_method = $payment_method != null ? $payment_method->name : 'Chuyển khoản';
            $order->total_deposit = $total_deposit;

            $content = $this->blade->render('confirm_email_template',
                [
                    'order' => $order,
                    'items' => $items,
                    'payment_limit' => $payment_limit->limit
                ], true);
            $subject = "[Basso] - Xác nhận đơn hàng #$order->order_code";
            $this->email_model->insert($subject, $content, $order->email);
            #endregion

            #region tạo tài khoản nếu chưa có trong hệ thống
            $email = strtolower($email);
            if (!$this->ion_auth->email_check($email)) {
                $pass = time();
                $this->ion_auth->register($email, $pass, $email, [
                    'first_name' => $name,
                    'phone' => $phone
                ], [6]);

                $content = $this->blade->render('created_account_email_template', [
                    'email' => $email,
                    'name' => $name,
                    'pass' => $pass
                ], true);
                $subject = "[Basso] - Xác nhận tạo tài khoản";
                $this->email_model->insert($subject, $content, $order->email);
            }
            #endregion

            $this->cart->destroy();
            if (in_array($payment_method->type, ['cod', 'bank-transfer'])) {
                $redirect_url = base_url('don-hang?id=' . $order->order_code);
                if ($this->ion_auth->logged_in())
                    $redirect_url = base_url('tai-khoan/don-hang?id=' . $order->order_code);

                json_success(null, ['redirect_url' => $redirect_url]);
            }

            #region Thanh toán online
            if (in_array($payment_method->type, ['banking', 'visa'])) {
                $payment_id = $this->payment_history_model->insert(time(), $total_deposit, $payment_type, "Thanh toán đơn hàng #$order->order_code",
                    $order_id, PaymentHistoryStatus::PENDING, $customer['id']);

                $req_data = [
                    "vnp_Version" => "2.0.0",
                    "vnp_TmnCode" => $this->_vn_pay['vnp_TmnCode'],
                    "vnp_Amount" => intval($total_deposit) * 100,
                    "vnp_Command" => "pay",
                    "vnp_CreateDate" => date('YmdHis'),
                    "vnp_CurrCode" => "VND",
                    "vnp_IpAddr" => $this->input->ip_address(),
                    "vnp_Locale" => 'vn',
                    "vnp_OrderInfo" => "Thanh toan don hang #$order->order_code",
                    "vnp_OrderType" => 'other',
                    "vnp_ReturnUrl" => base_url('/confirm_payment'),
                    "vnp_TxnRef" => $payment_id,
                ];

                ksort($req_data);
                $query = "";
                $i = 0;
                $hashdata = "";

                foreach ($req_data as $key => $value) {
                    if ($i == 1) {
                        $hashdata .= '&' . $key . "=" . $value;
                    } else {
                        $hashdata .= $key . "=" . $value;
                        $i = 1;
                    }
                    $query .= urlencode($key) . "=" . urlencode($value) . '&';
                }

                $vnp_Url = $this->_vn_pay['vnp_Url'] . "?" . $query;
                if (isset($this->_vn_pay['vnp_HashSecret'])) {
                    // $vnpSecureHash = md5($vnp_HashSecret . $hashdata);
                    $vnpSecureHash = hash('sha256', $this->_vn_pay['vnp_HashSecret'] . $hashdata);

                    $this->payment_history_model->update_trans_ref($payment_id, $vnpSecureHash);
                    $vnp_Url .= 'vnp_SecureHashType=SHA256&vnp_SecureHash=' . $vnpSecureHash;

                    json_success(null, ['redirect_url' => $vnp_Url]);
                }

                json_error('Có lỗi thanh toán, vui lòng liên hệ CSKH');
            }
            #endregion
        }

        json_error('Có lỗi, vui lòng F5 thử lại');
    }

    private function _update_index()
    {
        $cart_total = array_sum(array_column($this->_cart, 'total'));
        $currency_rate = $cart_total >= 500 ? 26000 : 26000;

        for ($i = 0; $i < count($this->_cart); $i++) {
            $this->_cart[$i]->sub_total = ($this->_cart[$i]->origin_price * $this->_cart[$i]->qty + $this->_cart[$i]->web_shipping_fee) * $currency_rate;
            if ($this->_cart[$i]->origin_term_fee != $this->_cart[$i]->term_fee)
                $this->_cart[$i]->sub_total += $this->_cart[$i]->qty * $this->_cart[$i]->origin_term_fee * $currency_rate;
            else
                $this->_cart[$i]->sub_total += $this->_cart[$i]->qty * $this->_cart[$i]->term_fee;

            if ($this->_cart[$i]->weight > 0.2)
                $this->_cart[$i]->sub_total += $this->_cart[$i]->qty * $this->_cart[$i]->weight * 260000;
        }

        $total = array_sum(array_column($this->_cart, 'sub_total'));
        return ['data' => $this->_cart, 'total' => $total, 'sub_total' => $total, 'origin_total' => $cart_total];
    }

    public function DELETE_index()
    {
        $this->form_validation->set_data($this->input->input_stream());
        $this->form_validation->set_rules('id', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $id = $this->input->input_stream('id');
        $this->cart->remove($id);
        json_success(null);
    }

    #region Xác nhận thanh toán
    public function confirm_trans()
    {
        $payment_id = $this->input->get('order_id');
        $amount = $this->input->get('amount');
        $res_code = $this->input->get('response_code');
        $trans_ref = $this->input->get('trans_ref');

        if ($payment_id != null && $amount != null && $res_code != null && $trans_ref != null) {
            if (is_numeric($payment_id)) {
                $payment_id = intval($payment_id);
                $payment = $this->payment_history_model->get_by_id($payment_id);

                //  Nếu đơn hàng tồn tại
                if ($payment != null) {
                    $status = $res_code == '00' ? PaymentHistoryStatus::COMPLETED : PaymentHistoryStatus::CANCELLED;

                    $order = $this->customer_order_model->get_by_id($payment->order_id);
                    $this->payment_history_model->update_status($payment_id, $status, $order);

                    return redirect(base_url('don-hang?id=' . $order->order_code));
                }
            }
        }

        return redirect(base_url());
    }

    #endregion

    public function tracking_order()
    {
        $id = $this->input->get('id');
        if ($id == null)
            return redirect(base_url());

        set_head_index(false);
        set_head_title('Thông tin đơn hàng');

        $this->load->model('basso/order_shipping_model');

        $order = $this->customer_order_model->get_by_code($id);
        if ($order == null) {
            $this->blade->set('error_msg', '');
            return $this->blade->render();
        }

        $payment_method = $this->payment_model->get_by_type($order->payment_method);
        $items = $this->customer_order_model->get_items($order->id);
        if ($payment_method == null)
            $order->payment_method = 'Đặt cọc trực tiếp';
        else
            $order->payment_method = $payment_method->name;

        $this->blade->set('order', $order);
        $this->blade->set('items', $items);

        $country = $this->country_model->get($order->country_id);
        if ($country != null) {
            $order->world_shipping_fee = $country->shipping_fee * $order->total_weight;
        }
        $order_info = [
            'payment_date' => null,
            'in_warehouse_date' => null,
            'in_inventory_date' => null,
            'shipped_time' => null
        ];

        #region Ngày về kho Mỹ
        $in_warehouse_items = array_filter($items, function ($t) {
            return $t->delivered_date != NULL;
        });

        if (count($in_warehouse_items) == count($items)) {
            usort($in_warehouse_items, function ($a, $b) {
                return $a->delivered_date < $b->delivered_date;
            });

            $order_info['in_warehouse_date'] = date('d/m/Y', $in_warehouse_items[0]->delivered_date);
        }
        #endregion

        #region Ngày về VN
        $in_inventory_items = array_filter($items, function ($t) {
            return $t->imported_time != NULL;
        });

        if (count($in_inventory_items) == count($items)) {
            usort($in_inventory_items, function ($a, $b) {
                return $a->imported_time < $b->imported_time;
            });

            $order_info['in_inventory_date'] = date('d/m/Y', $in_inventory_items[0]->imported_time);
        }
        #endregion

        #region ngày giao hàng
        $shipped_items = array_filter($items, functioN ($t) {
            return $t->shipped == 1;
        });

        if (count($shipped_items) == count($items)) {
            $shipping_histories = $this->order_shipping_model->get_by_order($order->id);
            if (count($shipping_histories) > 0) {
                $shipping_histories = array_filter($shipping_histories, function ($t) {
                    return $t->shipped_time != null;
                });
                usort($shipping_histories, function ($a, $b) {
                    return $a->shipped_time < $b->shipped_time;
                });

                if (isset($shipping_histories[0]))
                    $order_info['shipped_time'] = date('d/m/Y', $shipping_histories[0]->shipped_time);
            }
        }
        #endregion

        $this->blade->set('order_info', $order_info);
        $this->blade->set('payments', $this->payment_history_model->get_by_order($order->id));
        return $this->blade->render();
    }

    public function confirm_email_template()
    {
        return $this->blade->render();
    }
}