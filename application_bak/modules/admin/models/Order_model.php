<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Order_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 */
class Order_model extends CI_Model
{
    /**
     * Thêm mới đơn hàng thủ công
     * /admin/order
     * @param $customer
     * @param $payment_type
     * @param $discount_total tổng tiền giảm trừ
     * @param $products
     * @return int
     */
    public function insert($customer, $discount_total, $payment_type, $products)
    {
        $data = array(
            'created_time' => time(),
            'name' => $customer['name'],
            'phone' => $customer['phone'],
            'email' => $customer['email'],
            'address' => $customer['address'],
            'city_id' => $customer['city_id'],
            'district_id' => $customer['district_id'],
            'payment_id' => $customer['payment_id'],
            'total' => 0,
            'sub_total' => 0,
            'shipping_status' => 'waiting',
            'payment_status' => $payment_type != 'cod' ? 'waiting' : 'success'
        );

        foreach ($products as $item)
            $data['total'] += intval($item['quantity']) * intval($item['price']);

        $data['sub_total'] = $data['total'] - $discount_total;
        $this->db->insert('orders', $data);
        $order_id = $this->db->insert_id();

        #region Chi tiết đơn hàng
        foreach ($products as $item) {

            $order_detail = [
                'order_id' => $order_id,
                'product_id' => $item['id'],
                'quantity' => intval($item['quantity']),
                'total' => intval($item['quantity']) * intval($item['price']),
                'sub_total' => intval($item['quantity']) * intval($item['price'])
            ];

            $this->db->insert('order_detail', $order_detail);
            $order_detail_id = $this->db->insert_id();

            #region Thuộc tính sản phẩm
            $attributes = [];
            foreach ($item['data'] as $t) {

                //  Xóa những mảng không liên quan
                foreach ($t as $key => $value) {
                    if (!in_array($key, ['order_detail_id', 'attribute_value_id', 'attribute_id', 'order_id']))
                        unset($t[$key]);
                }

                $t['order_id'] = $order_id;
                $t['order_detail_id'] = $order_detail_id;
                array_push($attributes, $t);
            }
            if (count($attributes) > 0)
                $this->db->insert_batch('order_detail_product', $attributes);
            #endregion
        }
        #endregion
        return $order_id;
    }

    /**
     * Lấy danh sách đơn hàng trong hệ thống
     * @param int $page
     * @param $start
     * @param $end
     * @param string $status
     * @param null $key
     * @param null $labels
     * @return array
     */
    public function get($page = 1, $start, $end, $status = 'all', $key = null, $labels = null)
    {
        $this->db->select('t.id, t.name, t.phone, t.sub_total, t.email, t.created_time, t.shipping_status, 
        o.name as payment, t.payment_status, o.type as payment_type');
        $this->db->from('orders t');
        $this->db->join('payments o', 't.payment_id = o.id', 'left');
        $this->db->join('orders_labels d', 't.id = d.order_id', 'left');

        $this->db->where('t.created_time >= ', $start);
        $this->db->where('t.created_time <= ', $end);

        if ($status != 'all')
            $this->db->where('t.shipping_status', $status);

        if ($key != null) {
            if (!empty($key)) {
                $this->db->where("(t.id = $key or t.phone = '$key' or t.name like '%$key%')");
            }
        }

        if ($labels != null) {
            if (is_array($labels)) {
                $this->db->where_in('d.label_id', $labels);
            }
        }

        $limit = $this->config->item('result_per_page');
        $this->db->limit($limit);
        $this->db->offset(($page - 1) * $limit);
        $this->db->order_by('t.created_time', 'desc');
        $this->db->group_by('t.id');
        return $this->db->get()->result();
    }

    public function count($start, $end, $status = 'all', $key = null, $labels = null)
    {
        $this->db->from('orders t');
        $this->db->join('payments o', 't.payment_id = o.id', 'left');
        $this->db->join('orders_labels d', 't.id = d.order_id', 'left');

        $this->db->where('t.created_time >= ', $start);
        $this->db->where('t.created_time <= ', $end);

        if ($status != 'all')
            $this->db->where('t.shipping_status', $status);

        if ($key != null) {
            if (!empty($key)) {
                $this->db->where("(t.id = $key or t.phone = '$key' or t.name like '%$key%')");
            }
        }

        if ($labels != null) {
            if (is_array($labels)) {
                $this->db->where_in('d.label_id', $labels);
            }
        }
        return $this->db->count_all_results();
    }

    /**
     * Chi tiết đơn hàng
     * @param $id
     * @return mixed
     */
    public function get_by_id($id)
    {
        $this->db->select('t.*, o.name as payment, o.type as payment_type, d.name as district, k.name as city');
        $this->db->from('orders t');
        $this->db->join('payments o', 't.payment_id = o.id', 'left');
        $this->db->join('districts d', 't.district_id = d.id', 'left');
        $this->db->join('cities k', 't.city_id = k.id', 'left');
        $this->db->where('t.id', $id);

        return $this->db->get()->row();
    }

    /**
     * Lấy nhãn đơn hàng
     * @param $order_id
     * @return array
     */
    public function get_labels($order_id)
    {
        $this->db->select('t.*, o.name, o.color');
        $this->db->from('orders_labels t');
        $this->db->join('order_labels o', 't.label_id = o.id', 'right');
        $this->db->where('t.order_id', $order_id);
        return $this->db->get()->result();
    }

    /**
     * Danh sách sản phẩm của đơn hàng
     * @param $order_id
     * @return  array
     */
    public function get_products($order_id)
    {
        $this->db->select('t.id as order_detail_id, o.id, o.price, o.sale_price, o.sale_from, o.sale_to, o.name, t.quantity, 
        t.total, t.sub_total, t.discount_total,
        (select path from products_images where product_id = t.product_id limit 1) as image');
        $this->db->from('order_detail t');
        $this->db->join('products o', 't.product_id = o.id', 'right');
        $this->db->where('t.order_id', $order_id);
        return $this->db->get()->result();
    }

    /**
     * Lấy thuộc tính sản phẩm đặt mua trong đơn hàng
     * @param $order_detail_id
     * @return array
     */
    public function get_attributes($order_detail_id)
    {
        $this->db->select('d.name, o.name as value, t.attribute_id, o.id');
        $this->db->from('order_detail_product t');
        $this->db->join('attribute_values o', 't.attribute_value_id = o.id', 'left');
        $this->db->join('attributes d', 'o.attribute_id = d.id', 'left');
        $this->db->where('t.order_detail_id', $order_detail_id);

        return $this->db->get()->result();
    }

    /**
     * Tìm kiếm sản phẩm để thêm vào giỏ hàng
     * @param $key
     * @return array
     */
    public function search_products($key)
    {
        $this->db->select('id, price, sale_price, sale_from, sale_to, name, 
        (select path from products_images where product_id = id limit 1) as image');
        $this->db->from('products o');
        $this->db->where('sku', strtoupper($key));
        $this->db->or_like('name', $key, 'both');
        $this->db->limit(5);
        return $this->db->get()->result();
    }

    public function insert_city($id, $name)
    {
        $data = array('city_id' => $id, 'name' => $name);
        $this->db->insert('districts', $data);
    }

    /**
     * Các hình thức vận chuyển
     */
    public function shipping_method()
    {
        $this->db->order_by('name', 'asc');
        return $this->db->get('shipping_methods')->result();
    }

    /**
     * xác nhận thanh toán đơn hàng
     * @param $id
     * @param $status
     */
    public function confirm_payment($id, $status)
    {
        $this->db->where('id', $id);
        $this->db->update('orders', array('payment_status' => $status));
    }

    /**
     * Cập nhật trạng thái giao hàng
     * @param $id
     * @param $data
     */
    public function update($id, $data = null)
    {
        if ($data != null) {
            if (is_array($data)) {
                $this->db->where('id', $id);
                $this->db->update('orders', $data);
            }
        }
    }

    /**
     * cập nhật nhãn
     * @param $order_id
     * @param $labels_id
     */
    public function update_labels($order_id, $labels_id)
    {
        $this->db->where('order_id', $order_id);
        $this->db->delete('orders_labels');

        $data = array();
        foreach ($labels_id as $label_id)
            array_push($data, array('order_id' => $order_id, 'label_id' => $label_id));

        $this->db->insert_batch('orders_labels', $data);
    }

    /**
     * Cập nhật danh sách sản phẩm trong đơn hàng
     * @param $order_id
     * @param $products
     */
    public function update_detail($order_id, $products)
    {

        $discount_total = array_sum(array_column($products, 'discount_total'));
        $total = array_sum(array_column($products, 'total'));
        $sub_total = array_sum(array_column($products, 'sub_total'));

        $order = $this->get_by_id($order_id);
        $discount_total += $order->discount_total;
        $sub_total -= $order->discount_total;

        $update_order = [
            'discount_total' => $discount_total,
            'sub_total' => $sub_total,
            'total' => $total
        ];
        $this->db->where('id', $order_id);
        $this->db->update('orders', $update_order);

        #region Chi tiết đơn hàng
        $this->db->where('order_id', $order_id);
        $this->db->delete('order_detail');

        foreach ($products as $item) {
            //  Tạo chi tiết đơn hàng: sản phẩm, số lượng, tổng tiền, tổng giảm trừ...
            $order_detail = [
                'order_id' => $order_id,
                'product_id' => $item['product_id'],
                'quantity' => intval($item['quantity']),
                'total' => intval($item['total']),
                'sub_total' => intval($item['sub_total']),
                'discount_total' => intval($item['discount_total'])
            ];

            $this->db->insert('order_detail', $order_detail);
            $order_detail_id = $this->db->insert_id();

            //  Tạo thuộc tính sản phẩm
            $attributes = [];
            foreach ($item['data'] as $t) {

                //  Xóa những mảng không liên quan
                foreach ($t as $key => $value) {
                    if (!in_array($key, ['order_detail_id', 'attribute_value_id', 'attribute_id', 'order_id']))
                        unset($t[$key]);
                }

                $t['order_id'] = $order_id;
                $t['order_detail_id'] = $order_detail_id;
                array_push($attributes, $t);
            }
            if (count($attributes) > 0)
                $this->db->insert_batch('order_detail_product', $attributes);
            #endregion
        }
        #endregion
    }

    /**
     * Danh sách tỉnh/thành
     * @return array
     */
    public function get_cities()
    {
        $this->db->order_by('name', 'asc');
        return $this->db->get('cities')->result();
    }

    /**
     * Danh sách quận/huyện theo tỉnh thành
     * @param $city_id
     * @return array
     */
    public function get_districts($city_id)
    {
        $this->db->where('city_id', $city_id);
        $this->db->order_by('name', 'asc');
        return $this->db->get('districts')->result();
    }
}