<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Attribute_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 */
class Attribute_model extends CI_Model
{
    public function get($page = 1)
    {
        if ($page > 0) {
            $this->db->limit($this->config->item('result_per_page'));
            $this->db->offset(($page - 1) * $this->config->item('result_per_page'));
        } else
            $this->db->select('id, name');

        $this->db->order_by('name', 'asc');
        return $this->db->get('attributes')->result();
    }

    public function get_by_id($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('attributes')->row();
    }

    public function count()
    {
        return $this->db->count_all_results('attributes');
    }

    public function insert($id, $name, $des, $type, $active)
    {
        $data['name'] = $name;
        $data['description'] = $des;
        $data['active'] = $active;

        if ($id == 0) {
            $data['type'] = $type;
            $this->db->insert('attributes', $data);
        } else {
            $this->db->where('id', $id);
            $this->db->update('attributes', $data);
        }
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('attributes');
    }

    public function get_values($attribute_id, $page = 1)
    {
        if (is_array($attribute_id)) {
            $this->db->where_in('attribute_id', $attribute_id);
            return $this->db->get('attribute_values')->result();
        }

        if ($attribute_id > 0)
            $this->db->where('attribute_id', $attribute_id);
        if ($page >= 1) {
            $this->db->limit($this->config->item('result_per_page'));
            $this->db->offset(($page - 1) * $this->config->item('result_per_page'));
        }
        return $this->db->get('attribute_values')->result();
    }

    public function count_values($attribute_id)
    {
        $this->db->where('attribute_id', $attribute_id);
        return $this->db->count_all_results('attribute_values');
    }

    public function insert_value($attribute_id, $id, $name, $type, $value, $active)
    {
        if ($id == 0) {
            $data = array(
                'name' => $name,
                'value' => $value,
                'active' => $active,
                'attribute_id' => $attribute_id,
                'type' => $type
            );

            $this->db->insert('attribute_values', $data);
        } else {
            $data = array(
                'name' => $name,
                'value' => $value,
                'active' => $active
            );

            $this->db->where('id', $id);
            $this->db->update('attribute_values', $data);
        }
    }

    public function delete_value($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('attribute_values');
    }
}