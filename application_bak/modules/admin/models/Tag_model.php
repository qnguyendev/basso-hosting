<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Tag_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 */
class Tag_model extends CI_Model
{
    public function get_id($tags)
    {
        if (is_array($tags)) {
            $slugs = array();
            foreach ($tags as $tag)
                array_push($slugs, url_title($tag));

            $this->db->where_in('slug', $slugs);
            $query = $this->db->get('tags')->result();
            $result = array_column($query, 'id');

            if (count($tags) > count($query)) {
                $temp_tags = array();
                foreach ($tags as $tag) {
                    if (!in_array(url_title($tag), array_column($query, 'slug')))
                        array_push($temp_tags, $tag);
                }

                array_merge($result, $this->insert($temp_tags));
            }

            return $result;
        }

        return null;
    }

    private function insert($tags)
    {
        $result = array();
        if (is_array($tags)) {
            foreach ($tags as $tag) {
                $slug = url_title($tag);
                $this->db->insert('tags', array('name' => $tag, 'slug' => $slug));
                array_push($result, $this->db->insert_id());
            }
        }

        return $result;
    }

    public function search($tag)
    {
        $slug = url_title($tag);
        $this->db->like('slug', $slug, 'both');
        return $this->db->get('tags')->result();
    }
}