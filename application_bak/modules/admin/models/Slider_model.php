<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Slider_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 */
class Slider_model extends CI_Model
{
    public function insert($id, $data)
    {
        if ($id == 0) {
            $data['active'] = 1;
            $this->db->insert('sliders', $data);
        } else {
            if (array_key_exists('type', $data))
                unset($data['type']);

            $this->db->where('id', $id);
            $this->db->update('sliders', $data);
        }
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('sliders');
    }

    public function get_by_id($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('sliders')->row();
    }

    public function set_active($id, $active = 1)
    {
        $this->db->where('id', $id);
        $this->db->update('sliders', array('active' => $active));
    }

    public function change_order($id, $order)
    {
        $this->db->where('id', $id);
        $this->db->update('sliders', array('order' => $order));
    }

    public function get(int $active = -1)
    {
        if ($active != -1) {
            if (in_array($active, [0, 1]))
                $this->db->where('active', $active);
        }

        $this->db->order_by('order', 'asc');
        return $this->db->get('sliders')->result();
    }
}