<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Member
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Image_model $image_model
 * @property admin/Term_model $term_model
 * @property Member_model $member_model
 */
class Member extends \Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('term_model');
        $this->load->model('member_model');
    }

    public function index()
    {
        $breadcrumbs = [['text' => 'Danh sách thành viên']];

        return $this->blade
            ->set('breadcrumbs', $breadcrumbs)
            ->render();
    }

    public function GET_index()
    {
        $page = $this->input->get('page');
        if (!is_numeric($page))
            $page = 1;
        $page = $page < 1 ? 1 : $page;
        $result = $this->member_model->get($page);
        $pagination = Pagination::calc($this->member_model->count(), $page, PAGING_SIZE);
        json_success(null, ['data' => $result, 'pagination' => $pagination]);
    }

    public function DELETE_index()
    {
        $id = $this->input->input_stream('id');
        if ($id == null)
            json_error('Không tìm thấy ID thành viên');

        $member = $this->member_model->get_by_id(intval($id));
        if ($member == null)
            json_error('Không tìm thấy thành viên');

        $this->member_model->delete($id);
        $this->image_model->set_active($member->image_id, 0);

        success_msg('Xóa thành viên thành công');
        json_success('Xóa thành viên thành công');
    }

    public function create()
    {
        $breadcrumbs = [
            ['text' => 'Danh sách thành viên', 'url' => base_url('content/member')],
            ['text' => 'Thêm thành viên']
        ];

        return $this->blade
            ->set('breadcrumbs', $breadcrumbs)
            ->render('edit');
    }

    public function edit($id)
    {
        if (!isset($id)) {
            error_msg('Đường dẫn không hợp lệ');
            return redirect(base_url('content/member'));
        }

        $member = $this->member_model->get_by_id($id);
        if ($member == null) {
            error_msg('Không tìm thấy thành viên');
            return redirect(base_url('admin/member'));
        }

        $breadcrumbs = [
            ['text' => 'Danh sách thành viên', 'url' => base_url('admin/member')],
            ['text' => 'Cập nhật thành viên']
        ];

        return $this->blade
            ->set('breadcrumbs', $breadcrumbs)
            ->set('member', $member)
            ->render('edit');
    }

    public function POST_save()
    {
        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $des = $this->input->post('des');
        $content = $this->input->post('content');
        $position = $this->input->post('position');
        $email = $this->input->post('email');
        $phone = $this->input->post('phone');
        $facebook = $this->input->post('facebook');
        $twitter = $this->input->post('twitter');
        $google_plus = $this->input->post('google_plus');
        $image_id = $this->input->post('image_id');

        if ($id == null || $name == null || $position == null)
            json_error('Vui lòng nhập đủ thông tin');

        if ($id > 0) {
            $member = $this->member_model->get_by_id($id);
            if ($member->image_id != null)
                $this->image_model->set_active($member->image_id, 0);
        }

        $data = [
            'description' => $des,
            'content' => $content,
            'email' => $email,
            'phone' => $phone,
            'image_id' => null,
            'image_path' => null,
            'facebook' => $facebook,
            'twitter' => $twitter,
            'google_plus' => $google_plus
        ];

        #region Hình ảnh upload
        if ($image_id != null) {
            $this->image_model->set_active($image_id);     //  Đánh dấu ảnh được sử dụng
            $img = $this->image_model->get($image_id);     //  Lấy thông tin ảnh
            if ($img != null) {
                $data['image_path'] = $img->path;
                $data['image_id'] = $img->id;
            }
        }
        #endregion

        $this->member_model->insert($id, $name, $position, $data);
        success_msg($id == 0 ? 'Thêm thành viên thành công' : 'Cập nhật thành công');
        json_success(null);
    }
}