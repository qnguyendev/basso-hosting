<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Product
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Brand_model $brand_model
 * @property Term_model $term_model
 * @property Attribute_model $attribute_model
 * @property Product_model $product_model
 * @property Image_model $image_model
 * @property Tag_model $tag_model
 * @property Article_model $article_model
 */
class Product extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('brand_model');
        $this->load->model('term_model');
        $this->load->model('attribute_model');
        $this->load->model('image_model');
        $this->load->model('product_model');
        $this->load->model('article_model');
        $this->load->model('tag_model');
    }

    public function index()
    {
        $breadcrumbs = [['text' => 'Quản lý sản phẩm']];

        return $this->blade
            ->set('breadcrumbs', $breadcrumbs)
            ->set('products', $this->product_model->get())
            ->set('page_title', 'Quản lý sản phẩm')->render();
    }

    public function GET_index()
    {
        $page = $this->input->get('page');
        $filter = $this->input->get('filter');
        $page = intval($page) < 1 ? 1 : $page;

        $result['data'] = $this->product_model->get($page, $filter);
        $result['pagination'] = Pagination::calc($this->product_model->count($filter), $page);

        json_success(null, $result);
    }

    public function DELETE_index()
    {
        $id = $this->input->input_stream('id');
        if ($id != null) {
            $prod = $this->product_model->get_by_id($id);
            if ($prod != null) {
                $images = $this->product_model->get_images($id);
                if (count($images) > 0)
                    $this->image_model->set_active(array_column($images, 'id'), 0);
                $this->product_model->delete($id);

                json_success('Xóa sản phẩm thành công');
            }
            json_error('Không tìm thấy sản phẩm yêu cầu');
        }
        json_error('Không tìm thấy sản phẩm yêu cầu');
    }

    public function GET_search()
    {
        $action = $this->input->get('action');
        if ($action == null)
            json_error('Yêu cầu không hợp lệ');

        switch ($action) {
            case 'find-article':
                $key = $this->input->get('key');
                $selected = $this->input->get('selected');
                $page = $this->input->get('page');

                $result = $this->article_model->get($page, $key, 0, $selected);
                $total_item = $this->article_model->count($key, 0, $selected);
                json_success(null, [
                    'data' => $result,
                    'pagination' => Pagination::calc($total_item, $page)
                ]);
                break;
            case 'find-product':
                $key = $this->input->get('key');
                $selected = $this->input->get('selected');
                $page = $this->input->get('page');

                $result = $this->product_model->get($page, $key, $selected);
                $total_item = $this->product_model->count($key, $selected);
                json_success(null, [
                    'data' => $result,
                    'pagination' => Pagination::calc($total_item, $page)
                ]);
                break;
            default:
                json_error('Yêu cầu không hợp lệ');
                break;
        }
    }

    public function create()
    {
        $this->load->helper('file');
        $brands = $this->brand_model->get(0);
        $terms = $this->term_model->get(0, 'product');
        $attributes = $this->attribute_model->get(0);
        $attribute_values = $this->attribute_model->get_values(0, 0);
        $countries = json_decode(read_file('./assets/admin/countries.json'));

        $breadcrumbs = [['url' => base_url('admin/product'), 'text' => 'Quản lý sản phẩm'],
            ['text' => 'Thêm sản phẩm']];
        $this->blade->set('image_uploader', true);
        return $this->blade->set('page_title', 'Thêm sản phẩm')
            ->set('brands', $brands)
            ->set('terms', $terms)
            ->set('attributes', $attributes)
            ->set('attribute_values', $attribute_values)
            ->set('country', $countries)
            ->set('breadcrumbs', $breadcrumbs)
            ->render('editor');
    }

    public function edit($id)
    {
        if (!isset($id))
            return redirect('/admin/product');

        if (!is_numeric($id))
            return redirect('/admin/product');

        $product = $this->product_model->get_by_id($id);
        if ($product == null) {
            $this->session->set_flashdata('error', 'Sản phẩm không tồn tại');
            return redirect('/admin/product');
        }

        $this->load->helper('file');
        $brands = $this->brand_model->get(0);
        $terms = $this->term_model->get(0, 'product');
        $attributes = $this->attribute_model->get(0);
        $attribute_values = $this->attribute_model->get_values(0, 0);
        $countries = json_decode(read_file('./assets/admin/countries.json'));

        $breadcrumbs = [['url' => '/admin/product', 'text' => 'Quản lý sản phẩm'],
            ['text' => 'Sửa sản phẩm']];
        $this->blade->set('image_uploader', true);
        return $this->blade->set('page_title', 'Sửa sản phẩm')
            ->set('brands', $brands)
            ->set('terms', $terms)
            ->set('attributes', $attributes)
            ->set('attribute_values', $attribute_values)
            ->set('country', $countries)
            ->set('breadcrumbs', $breadcrumbs)
            ->render('editor');
    }

    public function GET_edit($id)
    {
        $product = $this->product_model->get_by_id($id);
        $product->upsell = $this->product_model->get_upsell_product($product->id);
        $product->articles = $this->product_model->get_related_articles($product->id);

        $images = $this->product_model->get_images($id);
        $attributes = $this->product_model->get_attributes($id);
        $result = ['product' => $product,
            'terms' => array_column($this->product_model->get_terms($product->id), 'term_id'),
            'images' => $images,
            'tags' => array_column($this->product_model->get_tags($product->id), 'name'),
            'attributes' => array_column($attributes, 'attribute_id')
        ];

        $attribute_id = array_unique($result['attributes']);
        $result['attribute_values'] = [];
        foreach ($attribute_id as $id) {
            $item = ['attribute_id' => $id, 'value_id' => [], 'values' => [], 'attribute_name' => null];
            $values = array_filter($attributes, function ($t) use ($id) {
                return $t->attribute_id == $id;
            });

            $item['value_id'] = array_column($values, 'attribute_value_id');
            $item['values'] = $this->attribute_model->get_values($id);

            $attribute = $this->attribute_model->get_by_id($id);
            if ($attribute != null)
                $item['attribute_name'] = $attribute->name;

            array_push($result['attribute_values'], $item);
        }

        json_success(null, $result);
    }

    public function POST_save()
    {
        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $slug = $this->input->post('slug');
        $des = $this->input->post('des');
        $content = $this->input->post('content');
        $images_id = $this->input->post('images_id');
        $sku = $this->input->post('sku');
        $active = $this->input->post('active');
        $show_price = $this->input->post('show_price');
        $available = $this->input->post('available');
        $out_stock = $this->input->post('out_stock');
        $price = $this->input->post('price');
        $sale_price = $this->input->post('sale_price');
        $sale_from = $this->input->post('sale_from');
        $sale_to = $this->input->post('sale_to');
        $terms = $this->input->post('terms');
        $brand_id = $this->input->post('brand_id');
        $country_code = $this->input->post('country_code');
        $tags = $this->input->post('tags');
        $seo_title = $this->input->post('seo_title');
        $seo_des = $this->input->post('seo_des');
        $seo_index = $this->input->post('seo_index');
        $attributes = $this->input->post('attributes');
        $related_products = $this->input->post('related_products');
        $related_articles = $this->input->post('related_articles');

        if ($id == null || $name == null)
            json_error('Vui lòng nhập tên sản phẩm');

        if ($sku != null) {
            $sku = strtoupper($sku);
            if ($this->product_model->get_by_sku($sku) != null && $id == 0)
                json_error('Mã sản phẩm đã tồn tại');
        }

        $slug = $slug == null
            ? url_title($name, '-', true)
            : url_title($slug, '-', true);
        $slug = $this->product_model->create_slug($id, $slug);

        #region Khuyến mãi

        if ($sale_from != null)
            $sale_from = strtotime($sale_from);
        if ($sale_to != null)
            $sale_to = strtotime("$sale_to 23:59:59");

        if ($sale_from != null && $sale_to != null) {
            if ($sale_from > $sale_to)
                json_error('Thời gian khuyến mãi không hợp lệ');
        }
        #endregion

        #region xử lý ảnh sản phẩm

        //  Set các ảnh cũ chưa active
        if ($id > 0) {
            $product_images = $this->product_model->get_images($id);
            if (count($product_images) > 0)
                $this->image_model->set_active(array_column($product_images, 'id'), 0);
        }

        //  Lấy ảnh mới
        $images = null;
        if ($images_id != null) {
            if (is_array($images_id)) {
                if (count($images_id) > 0) {

                    $this->image_model->set_active($images_id);     //  Đánh dấu ảnh được sử dụng
                    $img = $this->image_model->get($images_id);     //  Lấy thông tin ảnh

                    $images = array();
                    foreach ($img as $item) {
                        array_push($images, ['product_id' => null,
                            'image_id' => $item->id,
                            'path' => $item->path]);
                    }
                }
            }
        }
        #endregion

        $data = ['name' => $name,
            'slug' => $slug,
            'description' => $des,
            'content' => $content,
            'description' => $des,
            'active' => is_numeric($active) ? (in_array($active, [0, 1]) ? $active : 1) : 1,
            'out_stock' => is_numeric($out_stock) ? (in_array($out_stock, [0, 1]) ? $out_stock : 1) : 1,
            'sale_from' => $sale_from,
            'sale_to' => $sale_to,
            'country_code' => $country_code,
            'show_price' => is_numeric($show_price) ? (in_array($show_price, [0, 1]) ? $show_price : 1) : 1,
            'seo_title' => $seo_title,
            'seo_description' => $seo_des,
            'seo_index' => is_numeric($seo_index) ? (in_array($seo_index, [0, 1]) ? $seo_index : 1) : 1,
            'sku' => $sku,
            'price' => null,
            'sale_price' => null,
            'sale_from' => $sale_from,
            'sale_to' => $sale_to,
            'available' => is_numeric($available) ? $available : null];

        if ($id == 0) {
            $user = $this->ion_auth->user()->row();
            $data['author_id'] = $user->id;
        }

        if ($price != null) {
            if (is_numeric($price))
                $data['price'] = intval($price);
            else
                $data['price'] = intval(preg_replace('/[^0-9]/', '', $price));
        }

        if ($sale_price != null) {
            if (is_numeric($sale_price)) {
                if ($sale_price > 0)
                    $data['sale_price'] = $sale_price;
            } else {
                $sale_price = intval(preg_replace('/[^0-9]/', '', $sale_price));
                if ($sale_price > 0)
                    $data['sale_price'] = $sale_price;
            }
        }

        if (is_numeric($brand_id)) {
            if ($brand_id > 0)
                $data['brand_id'] = $brand_id;
        }

        $tag_id = array();
        if ($tags != null) {
            $tags = explode(',', $tags);
            $tag_id = $this->tag_model->get_id($tags);
        }

        $id = $this->product_model->insert($id, $data, $terms, $images, $tag_id, $attributes, $related_products, $related_articles);
        if (!$id)
            json_error('Có lỗi, vui lòng F5 thử lại');

        $msg = $id == 0 ? 'Thêm sản phẩm thành công' : 'Cập nhật sản phẩm thành công';
        $this->session->set_flashdata('success', $msg);
        json_success(null, ['id' => $id]);
    }
}