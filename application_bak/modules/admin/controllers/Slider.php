<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Slider
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Slider_model $slider_model
 * @property Image_model $image_model
 * @property CI_Form_validation $form_validation
 */
class Slider extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('slider_model');
        $this->load->model('image_model');
    }

    public function index()
    {
        $this->blade->set('image_uploader', true);
        $this->blade->set('breadcrumbs', [['text' => 'Slider trang chủ']])->render();
    }

    public function GET_index()
    {
        json_success(null, ['data' => $this->slider_model->get()]);
    }

    public function POST_index()
    {
        $this->form_validation->set_rules('id', null, 'required|numeric');
        $this->form_validation->set_rules('name', null, 'required');
        if (!$this->form_validation->run())
            json_error('Vui lòng nhập các thông tin bắt buộc');

        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $title = $this->input->post('title');
        $content = $this->input->post('content');
        $redirect_url = $this->input->post('redirect_url');
        $youtube_id = $this->input->post('youtube_id');
        $image_id = $this->input->post('image_id');
        $image_path = $this->input->post('image_path');
        $button_text = $this->input->post('button_text');

        if ($image_id != null || $youtube_id != null) {
            $data = ['name' => $name,
                'title' => $title,
                'content' => $content,
                'redirect_url' => $redirect_url,
                'image_id' => $image_id,
                'image_path' => $image_path,
                'youtube_id' => $youtube_id,
                'button_text' => $button_text
            ];

            if ($image_id != null)
                $this->image_model->set_active($image_id, 1);

            $this->slider_model->insert($id, $data);
            json_success('Thêm slide thành công');
        }
        json_error('Vui lòng nhập đủ thông tin');
    }

    public function DELETE_index()
    {
        $id = $this->input->input_stream('id');
        if ($id != null) {
            $slide = $this->slider_model->get_by_id($id);
            if ($slide == null)
                json_error('Không tìm thấy slide');
            else {
                $this->slider_model->delete($id);
                if ($slide->image_id != null)
                    $this->image_model->set_active($id, 0);

                json_success('Xóa slide thành công');
            }
        }
        json_error('Không tìm thấy slide');
    }

    public function POST_active()
    {
        $id = $this->input->post('id');
        $active = $this->input->post('active');
        $this->slider_model->set_active($id, $active);

        json_success(null);
    }

    public function POST_order()
    {
        $id = $this->input->post('id');
        $order = $this->input->post('order');
        $this->slider_model->change_order($id, $order);

        json_success(null);
    }
}