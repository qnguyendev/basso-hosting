<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Image_upload
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Upload $upload
 * @property Image_model $image_model
 */
class Image_upload extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $config['upload_path'] = './uploads/' . date('Y/m/d');
        $config['allowed_types'] = 'jpg|jpeg|png';
        $this->load->library('upload', $config);
        $this->load->model('image_model');
    }

    public function POST_index()
    {
        create_folder();
        $result['data'] = [];
        $result['id'] = [];

        if (!empty($_FILES)) {
            $filesCount = count($_FILES['files']['name']);
            for ($i = 0; $i < $filesCount; $i++) {
                $_FILES['file']['name'] = $_FILES['files']['name'][$i];
                $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                $_FILES['file']['size'] = $_FILES['files']['size'][$i];

                // Upload file to server
                if ($this->upload->do_upload('file')) {

                    // Uploaded file data
                    $fileData = $this->upload->data();
                    $path = '/uploads/' . date('Y/m/d') . '/' . $fileData['file_name'];
                    $id = $this->image_model->insert($path);

                    array_push($result['id'], $id);
                    array_push($result['data'], ['id' => $id, 'path' => $path]);
                }
            }
        } else {
            $url = $this->input->post('url');
            if (isset($url)) {
                $_url = !is_array($url) ? [$url] : $url;
                foreach ($_url as $t) {
                    $id = $this->image_model->insert($t);

                    array_push($result['id'], $id);
                    array_push($result['data'], ['id' => $id, 'path' => $t]);
                }
            }
        }

        json_success(null, $result);
    }

    public function POST_change()
    {
        $id = $this->input->post('id');
        if (isset($id)) {
            if ($id != null) {
                $active = $this->input->post('active');
                $this->image_model->set_active($id, $active);
                json_error(null);
            }
        }

        json_error(null);
    }
}