<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Voucher
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Voucher_model $voucher_model
 */
class Voucher extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('voucher_model');
    }

    public function index()
    {
        $breadcrumbs = [['text' => 'Quản lý mã giảm giá']];
        $this->blade
            ->set('breadcrumbs', $breadcrumbs)
            ->render();

    }

    public function GET_index()
    {
        json_success(null, ['data' => $this->voucher_model->get()]);
    }

    public function DELETE_index()
    {
        $id = $this->input->input_stream('id');
        if ($id == null)
            json_error('Không tìm thấy mã giảm giá');

        if (!is_numeric($id))
            json_error('Không tìm thấy mã giảm giá');

        $store = $this->voucher_model->get($id);
        if ($store == null)
            json_error('Không tìm thấy mã giảm giá');

        $this->voucher_model->delete($id);
        json_success('Xóa cửa mã giảm giá thành công');
    }

    public function POST_index()
    {
        $id = $this->input->post('id');
        $code = $this->input->post('code');
        $valid_from = $this->input->post('valid_from');
        $valid_to = $this->input->post('valid_to');
        $discount_type = $this->input->post('discount_type');
        $discount_value = $this->input->post('discount_value');
        $max_value = $this->input->post('max_value');

        $id = check_null_data($id);
        $code = check_null_data($code);
        $valid_from = check_null_data($valid_from);
        $valid_to = check_null_data($valid_to);
        $discount_type = check_null_data($discount_type);
        $discount_value = check_null_data($discount_value);

        if ($id == null || $code == null || $valid_from == null || $valid_to == null
            || $discount_type == null || $discount_value == null)
            json_error('Vui lòng nhập thông tin bắt buộc');

        $valid_from = strtotime($valid_from . ' 00:00:00');
        $valid_to = strtotime($valid_to . ' 23:59:59');
        if ($valid_from > $valid_to)
            json_error('Thời gian hiệu lực không hợp lệ');

        $voucher = $this->voucher_model->get_by_code($code);
        if ($voucher != null) {
            if ($voucher->id != $id)
                json_error('Mã giảm giá đã tồn tại trong hệ thống');
        }

        $data = [
            'valid_from' => $valid_from,
            'valid_to' => $valid_to,
            'discount_type' => $discount_type,
            'discount_value' => $discount_value,
            'max_value' => $max_value,
            'active' => 1,
            'created_time' => time()
        ];

        $data = check_null_data($data);
        $this->voucher_model->insert($id, strtoupper(trim($code)), $data);
        json_success($id == 0 ? 'Thêm mã giảm giá thành công' : 'Cập nhật mã giảm giá thành công');
    }

    public function POST_active()
    {
        $id = $this->input->post('id');
        $active = $this->input->post('active');
        $this->voucher_model->change_active($id, $active == 1 ? 0 : 1);
    }
}