<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Config
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Option_model $option_model
 * @property CI_Upload $upload
 */
class Config extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data = ['logo' => null, 'fav_icon' => null, 'site_name' => null,
            'site_info' => null, 'contact_email' => null, 'facebook' => null,
            'twitter' => null, 'instagram' => null, 'google_plus' => null,
            'pinterest' => null, 'products_per_page' => 15, 'articles_per_page' => 15,
            'related_products' => 6, 'related_articles' => 6, 'redirect_to_cart' => false,
            'youtube' => null
        ];

        foreach ($data as $key => $value) {
            $option = $this->option_model->get($key);
            if ($option != null)
                $data[$key] = $option->value;
        }

        $breadcrumbs = [['text' => 'Cấu hình website']];
        return $this->blade
            ->set('breadcrumbs', $breadcrumbs)
            ->render($data);
    }

    public function POST_index()
    {
        $valid_keys = ['site_name', 'site_info', 'contact_email', 'facebook', 'twitter',
            'related_articles', 'redirect_to_cart', 'instagram', 'google_plus',
            'pinterest', 'products_per_page', 'articles_per_page', 'related_products',
            'youtube'
        ];

        if ($this->input->post('redirect_to_cart') == null) {
            $this->option_model->save('redirect_to_cart', false);
        }

        foreach ($_POST as $key => $value) {
            if (in_array($key, $valid_keys))
                $this->option_model->save($key, $value);
        }

        if (isset($_FILES)) {
            if (count($_FILES) > 0) {
                $config['upload_path'] = './uploads/';
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['overwrite'] = true;
                $this->load->library('upload', $config);

                if (isset($_FILES['logo'])) {
                    if (file_exists($_FILES['logo']['tmp_name'])) {
                        $item = $this->option_model->get('logo');
                        if ($item != null) {
                            if (is_file($item->value) || is_file('.' . $item->value))
                                unlink($item->value);
                        }

                        $this->upload->do_upload('logo');
                        $data = $this->upload->data();
                        $file_path = '/uploads/' . $data['file_name'];

                        $this->option_model->save('logo', $file_path);
                    }
                }

                if (isset($_FILES['fav_icon'])) {
                    if (file_exists($_FILES['fav_icon']['tmp_name'])) {
                        $item = $this->option_model->get('fav_icon');
                        if ($item != null) {
                            if (is_file($item->value) || is_file('./' . $item->value))
                                unlink($item->value);
                        }

                        $this->upload->do_upload('fav_icon');
                        $data = $this->upload->data();
                        $file_path = '/uploads/' . $data['file_name'];

                        $this->option_model->save('fav_icon', $file_path);
                    }
                }
            }
        }

        $this->session->set_flashdata('success', 'Cập nhật thành công');
        return redirect(current_url());
    }

    public function seo()
    {
        $breadcrumbs = [['text' => 'Cấu hình SEO']];
        $data = [
            'robots_follow' => false,
            'post_sitemap' => false,
            'product_sitemap' => false,
            'page_sitemap' => false
        ];

        foreach ($data as $key => $value) {
            $option = $this->option_model->get($key);
            if ($option != null)
                $data[$key] = $option->value == 1;
        }

        $file = fopen('./robots.txt', "r") or exit("Unable to open file!");
        $data['robots_content'] = [];
        while (!feof($file)) {
            array_push($data['robots_content'], fgets($file));
        }
        fclose($file);

        return $this->blade
            ->set('breadcrumbs', $breadcrumbs)
            ->set('data', $data)
            ->render();
    }

    public function POST_seo()
    {
        $keys = ['robots_follow', 'post_sitemap', 'product_sitemap', 'page_sitemap'];
        foreach ($_POST as $key => $value) {
            if (in_array($key, $keys)) {
                $value = $this->input->post($key);
                $this->option_model->save($key, $value);
            }
        }

        $robots_content = $this->input->post('robots_content');
        $fp = fopen('./robots.txt', 'w');
        fwrite($fp, $robots_content);
        fclose($fp);

        $this->session->set_flashdata('success', 'Cập nhật thông tin SEO thành công');
        return redirect(current_url(), 'location', 301);
    }
}