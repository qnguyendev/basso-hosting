<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="editor_modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    <span data-bind="text: id() == 0 ? 'Thêm voucher' : 'Cập nhật mã giảm giá'"></span>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>
                        Mã giảm giá
                        <span class="text-danger">*</span>
                        <span class="validationMessage" data-bind="validationMessage: code"></span>
                    </label>
                    <input type="text" class="form-control text-uppercase"
                           data-bind="value: code, enable: id() == 0"/>
                </div>
                <div class="row">
                    <div class="form-group col-sm-12 col-md-6">
                        <label>Từ ngày</label>
                        <input type="text" class="form-control" data-bind="datePicker: valid_from"/>
                    </div>
                    <div class="form-group col-sm-12 col-md-6">
                        <label>đến ngày</label>
                        <input type="text" class="form-control" data-bind="datePicker: valid_to"/>
                    </div>

                    <div class="form-group col-sm-6">
                        <label>Kiểu giảm giá</label>
                        <select class="form-control" data-bind="value: discount_type, enable: id() === 0">
                            <option value="percent">% đơn hàng</option>
                            <option value="amount">Số tiền</option>
                        </select>
                    </div>
                    <div class="form-group col-sm-6">
                        <label>
                            <span data-bind="text: discount_type() == 'percent' ? 'Phần trăm' : 'Số tiền'"></span>
                            giảm
                        </label>
                        <input type="number" class="form-control" data-bind="value: discount_value"/>
                    </div>
                    <div class="form-group col-sm-12" data-bind="visible: discount_type() == 'percent'">
                        <label>Số tiền giảm tối đa</label>
                        <input type="number" class="form-control" data-bind="value: max_value"/>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success" data-bind="click: $root.save">
                    <i class="fa fa-check"></i>
                    Lưu lại
                </button>
            </div>
        </div>
    </div>
</div>
