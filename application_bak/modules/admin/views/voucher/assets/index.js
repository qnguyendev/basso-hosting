function viewModel() {
    let self = this;
    let today = new Date();
    self.id = ko.observable(0);
    self.code = ko.observable().extend({
        required: {message: LABEL.required}
    });
    self.valid_from = ko.observable(moment(today).format('YYYY-MM-DD'));
    self.valid_to = ko.observable(moment(today.setDate(today.getDate() + 7)).format('YYYY-MM-DD'));
    self.discount_type = ko.observable('amount');
    self.discount_value = ko.observable(0);
    self.max_value = ko.observable(0);
    self.result = ko.mapping.fromJS([]);

    self.init = function () {
        AJAX.get(window.location, null, true, (res) => {
            ko.mapping.fromJS(res.data, self.result);
        });
    };

    self.add_voucher = function () {
        self.id(0);
        self.code(undefined);
        self.discount_type('amount');
        self.discount_value(0);
        self.max_value(0);

        self.errors.showAllMessages(false);
        MODAL.show('#editor_modal');
    };

    self.edit = function (item) {
        self.id(item.id());
        self.code(item.code());
        self.valid_from(moment.unix(item.valid_from()).format('YYYY-MM-DD'));
        self.valid_to(moment.unix(item.valid_to()).format('YYYY-MM-DD'));
        self.discount_type(item.discount_type());
        self.discount_value(item.discount_value());
        self.max_value(item.max_value());

        MODAL.show('#editor_modal');
    };

    self.delete = function (item) {
        ALERT.confirm('Xác nhận xóa mã giảm giá', null, function () {
            AJAX.delete(window.location, {id: item.id()}, true, (res) => {
                if (res.error)
                    NOTI.danger(res.message);
                else {
                    NOTI.success(res.message);
                    self.result.remove(item);
                }
            });
        });
    };

    self.save = function () {
        if (!self.isValid())
            self.errors.showAllMessages();
        else {
            let data = {
                id: self.id(),
                code: self.code(),
                valid_from: self.valid_from(),
                valid_to: self.valid_to(),
                discount_type: self.discount_type(),
                discount_value: self.discount_value(),
                max_value: self.max_value()
            };

            AJAX.post(window.location, data, true, (res) => {
                if (res.error)
                    NOTI.danger(res.message);
                else {
                    NOTI.success(res.message);
                    self.init();
                    MODAL.hide('#editor_modal');
                }
            });
        }
    };

    self.change_active = function (item) {
        AJAX.post('/admin/voucher/active', {id: item.id(), active: item.active()}, true, () => {
            NOTI.success('Cập nhật thành công');
            item.active(item.active() == 1 ? 0 : 1);
        });
    };
}

let model = new viewModel();
model.init();
ko.validatedObservable(model);
ko.applyBindings(model, document.getElementById('main-content'));