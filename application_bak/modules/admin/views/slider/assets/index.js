function viewModel() {
    let self = this;
    ko.utils.extend(self, new imageUploadModel());

    self.id = ko.observable(0);
    self.name = ko.observable();
    self.title = ko.observable();
    self.content = ko.observable();
    self.type = ko.observable('image');
    self.image_id = ko.observable();
    self.image_path = ko.observable();
    self.youtube_id = ko.observable().extend(({
        required: {
            message: LABEL.required,
            onlyIf: function () {
                return self.type() == 'youtube';
            }
        }
    }));
    self.redirect_url = ko.observable();
    self.result = ko.mapping.fromJS([]);
    self.button_text = ko.observable();

    self.init = function () {
        AJAX.get(window.location, null, true, (res) => {
            ko.mapping.fromJS(res.data, self.result);
        });
    };

    self.create = function () {
        self.errors.showAllMessages(false);
        self.id(0);
        self.name(undefined);
        self.title(undefined);
        self.content(undefined);
        self.image_id(undefined);
        self.image_path(undefined);
        self.youtube_id(undefined);
        self.redirect_url(undefined);
        MODAL.show('#editor_modal');
    };

    self.edit = function (item) {
        self.id(item.id());
        self.name(item.name());

        if (item.title() != null)
            self.title(item.title());

        if (item.content() != null)
            self.content(item.content());

        if (item.image_id() !== null) {
            self.type('image');
            self.image_id(item.image_id());
            self.image_path(item.image_path());
        }

        if (item.button_text() !== null) {
            self.button_text(item.button_text());
        }

        if (item.youtube_id() != null) {
            self.type('youtube');
            self.youtube_id(item.youtube_id());
        }

        MODAL.show('#editor_modal');
    };

    //#region Xem trước ảnh upload
    self.image_upload = function () {
        self.image_uploader.show(true);
        self.image_uploader.save = function () {
            self.image_id(self.image_uploader.selected_images[0].id);
            self.image_path(self.image_uploader.selected_images[0].path);
            self.image_uploader.close();
        };
    };

    self.delete_image = function () {
        self.image_id(undefined);
        self.image_path(undefined);
    };
    //#endregion

    self.save = function () {
        if (!self.isValid())
            self.errors.showAllMessages();
        else {
            let data = {
                id: self.id(),
                name: self.name()
            };

            if (self.title() !== undefined) {
                if (self.title().length > 0)
                    data.title = self.title();
            }

            if (self.content() !== undefined) {
                if (self.content().length > 0)
                    data.content = self.content();
            }

            if (self.redirect_url() !== undefined) {
                if (self.redirect_url().length > 0)
                    data.redirect_url = self.redirect_url();
            }

            if (self.type() === 'image') {
                if (self.image_id() === undefined) {
                    NOTI.danger('Bạn chưa chọn ảnh');
                    return;
                }

                data.image_id = self.image_id();
                data.image_path = self.image_path();
            } else if (self.type() === 'youtube') {
                data.youtube_id = self.youtube_id();
            }

            if (self.button_text() !== undefined) {
                if (self.button_text().length > 0) {
                    data.button_text = self.button_text();
                }
            }

            AJAX.post(window.location, data, true, (res) => {
                if (res.error)
                    NOTI.danger(res.message);
                else {
                    NOTI.success(res.message);
                    MODAL.hide('#editor_modal');
                    self.init();
                }
            });
        }
    };

    self.delete = function (item) {
        ALERT.confirm('Xác nhận xóa slide', null, function () {
            AJAX.delete(window.location, {id: item.id()}, true, (res) => {
                if (!res.error) {
                    NOTI.success('Đã xóa slide');
                    self.result.remove(item);
                } else
                    ALERT.error(res.message);
            });
        });
    };

    self.change_active = function (item) {
        AJAX.post('/admin/slider/active', {id: item.id(), active: item.active() == 0 ? 1 : 0}, true, () => {
            item.active(item.active() == 1 ? 0 : 1);
        });
    };

    self.change_order = function (item) {
        AJAX.post('/admin/slider/order', {id: item.id(), order: item.order()}, true, () => {
            self.init();
        });
    }
}

let model = new viewModel();
model.init();
ko.validatedObservable(model);
ko.applyBindings(model, document.getElementById('main-content'));