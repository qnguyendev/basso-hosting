function viewModel() {
    let self = this;
    self.result = ko.mapping.fromJS([]);
    self.id = ko.observable(0);
    self.name = ko.observable().extend({
        required: {message: LABEL.required}
    });
    self.slug = ko.observable();
    self.website = ko.observable();
    self.content = ko.observable();
    self.image_id = ko.observable();
    self.image_path = ko.observable();

    self.seo_title = ko.observable();
    self.seo_keywords = ko.observable();
    self.seo_description = ko.observable();
    self.seo_index = ko.observable(1);
    self.seo_detail = ko.observable({
        title: 'Tiêu đề page website không vượt quá 70 kí tự (tốt nhất từ 60-70 kí tự)',
        description: 'Mô tả page website không vượt quá 155 kí tự (tốt nhất từ 100-155 kí tự). Là những đoạn mô tả ngắn gọn về website, bài viết...',
        slug: ''
    });

    self.init = function () {
        if (window.location.toString().indexOf('/edit/') > 0) {
            AJAX.get(window.location, null, true, (res) => {
                if (res.error) {
                    ALERT.error(res.message, function () {
                        window.location = res.url;
                    });
                } else {
                    self.id(res.data.id);
                    self.name(res.data.name);
                    self.slug(res.data.slug);

                    if (res.data.website != null)
                        self.website(res.data.website);
                    if (res.data.content != null)

                        self.content(res.data.content);

                    self.seo_index(res.data.seo_index);

                    if (res.data.seo_title !== null)
                        self.seo_title(res.data.seo_title);
                    if (res.data.seo_description !== null)
                        self.seo_description(res.data.seo_description);
                    if (res.data.image_path != null)
                        self.image_path(res.data.image_path);
                    if (res.data.image_id != null)
                        self.image_id(res.data.image_id);

                    self.preview_seo();
                }
            });
        }
    };

    //#region Xem trước ảnh upload
    self.images_preview = function (data, event) {
        if (event.target.files.length === 0)
            return;

        let file = event.target.files[0];
        if (file.size < 2048 * 1024) {
            if (file.type == "image/jpeg" || file.type == "image/jpg" || file.type == "image/png") {
                let formData = new FormData();
                formData.append('files[]', file);

                AJAX.upload(URL.upload_image, formData, true, (res) => {
                    if (res.data.length == 1) {
                        self.image_id(res.data[0].id);
                        self.image_path(res.data[0].path);
                    }
                });
            } else {
                ALERT.error('Định dạng ảnh không hợp lệ.');
            }
        } else {
            ALERT.error('Ảnh phải nhỏ hơn 2MB');
        }
    };

    self.delete_image = function () {
        self.image_id(undefined);
        self.image_path(undefined);
    };
    //#endregion

    //#region Xem trước nội dung SEO
    self.preview_seo = function () {
        let title = LABEL.default_seo_title, des = LABEL.default_seo_des;

        if (self.name() !== undefined) {
            if (self.name() !== null)
                if (self.name().length > 0)
                    title = self.name();
        }

        if (self.seo_title() !== undefined) {
            if (self.seo_title() !== null)
                if (self.seo_title().length > 0)
                    title = self.seo_title();
        }

        if (self.seo_description() !== undefined) {
            if (self.seo_description() !== null)
                if (self.seo_description().length > 0)
                    des = self.seo_description();
        }

        self.seo_detail({
            title: title,
            description: des,
            slug: self.slug()
        });

        if (self.slug() === undefined && self.name() !== undefined) {
            if (self.name() !== null)
                if (self.name().length > 0)
                    self.slug(create_slug(self.name()));
        }
    };

    self.create_slug = function () {
        if (self.slug() !== undefined)
            if (self.slug().length > 0)
                self.slug(create_slug(self.slug()));
    };
    //#endregion

    self.save = function () {
        if (!self.isValid())
            self.errors.showAllMessages();
        else {
            let data = {
                id: self.id(),
                name: self.name(),
                slug: self.slug(),
                seo_index: self.seo_index()
            };

            if (self.website() !== undefined)
                if (self.website().length > 0)
                    data.website = self.website();

            if (self.content() !== undefined)
                if (self.content().length > 0)
                    data.content = self.content();

            if (self.seo_title() !== undefined)
                if (self.seo_title().length > 0)
                    data.seo_title = self.seo_title();

            if (self.seo_description() !== undefined)
                if (self.seo_description().length > 0)
                    data.des = self.seo_description();

            if (self.image_id() != undefined)
                data.image = self.image_id();

            AJAX.post('/admin/brand/save', data, true, (res) => {
                if (res.error)
                    NOTI.danger(res.message);
                else
                    window.location = '/admin/brand';
            });
        }
    }
}

let model = new viewModel();
model.init();
ko.validatedObservable(model);
ko.applyBindings(model, document.getElementById('main-content'))