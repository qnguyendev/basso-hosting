<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-sm-10 offset-sm-1">
            <div class="card card-default">
                <div class="card-header no-border">
                    <div class="card-title">
                        <a href="{{base_url('admin/member/create')}}" class="float-right">
                            <i class="fa fa-plus"></i>
                            Thêm thành viên
                        </a>
                        Danh sách thành viên
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th width="30">ID</th>
                            <th width="100"></th>
                            <th>Tên</th>
                            <th>Vị trí</th>
                            <th>Email</th>
                            <th>Điện thoại</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody data-bind="foreach: result" class="text-center">
                        <tr>
                            <th data-bind="text: id"></th>
                            <td>
                                <!--ko if: image_path() != null -->
                                <img data-bind="attr: {src: '/timthumb.php?src=' + image_path() + '&w=70&h=70'}, visible: image_path() != null"/>
                                <!--/ko-->
                                <!--ko if: image_path() == null-->
                                <img src="/timthumb.php?src=/assets/img/no-image-available.jpg&w=50&h=50"/>
                                <!--/ko-->
                            </td>
                            <td>
                                <a data-bind="attr: {href: '{{base_url('admin/member/edit/')}}' + id()}, text: name"
                                   class="font-weight-bold">
                                </a>
                            </td>
                            <td data-bind="text: position"></td>
                            <td data-bind="text: email"></td>
                            <td data-bind="text: phone"></td>
                            <td>
                                <a data-bind="attr: {href: '{{base_url('admin/member/edit/')}}' + id()}"
                                   class="btn btn-xs btn-info">
                                    <i class="fa fa-edit"></i>
                                    Sửa
                                </a>
                                <a href="javascript:" class="btn btn-xs btn-danger" data-bind="click: $root.delete">
                                    <i class="fa fa-trash"></i>
                                    Xóa
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    @include('pagination')
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{load_js('index')}}"></script>
@endsection