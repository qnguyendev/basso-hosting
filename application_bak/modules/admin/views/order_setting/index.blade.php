<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <form method="post" autocomplete="off">
       <div class="row">
           <div class="col-md-12 col-lg-10 offset-lg-1 col-xl-8 offset-xl-2">
               <div class="card card-default">
                   <div class="card-header">
                       <div class="card-title">Cấu hình đặt hàng</div>
                   </div>
                   <div class="card-body">
                       <div class="form-group">
                           <label>Email nhận thông báo có đơn hàng (phân cách dấu phẩy)
                           </label>
                           <input type="text" class="form-control" placeholder="nguyenvana@gmail.com"
                                  name="order_send_list" value="{{$order_send_list}}"/>
                       </div>
                       <div class="form-group">
                           <label>Trang hoàn thành thanh toán</label>
                           <textarea id="order_thank_you"
                                     name="order_thank_you_content">{{$order_thank_you_content}}</textarea>
                       </div>
                       <div class="form-group">
                           <label>Footer email xác nhận đơn hàng</label>
                           <textarea id="order_email_footer"
                                     name="order_email_footer_content">{{$order_email_footer_content}}</textarea>
                       </div>
                   </div>
                   <div class="card-footer text-center">
                       <button class="btn btn-success" type="submit">
                           <i class="fa fa-check"></i>
                           Lưu lại
                       </button>
                   </div>
               </div>
           </div>
       </div>
    </form>
@endsection
@section('script')
    <script src="//cdn.ckeditor.com/4.10.1/basic/ckeditor.js"></script>
    <script>
        window.onload = function () {
            CKEDITOR.replace(document.getElementById('order_thank_you'), {
                height: '200px'
            });

            CKEDITOR.replace(document.getElementById('order_email_footer'), {
                height: '212px'
            });
        }
        function submitForm() {
            $('form').submit();
        }
    </script>
@endsection