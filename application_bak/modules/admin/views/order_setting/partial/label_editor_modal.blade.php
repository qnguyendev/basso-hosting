<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="label-editor-modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" data-bind="text: id() == 0 ? 'Thêm nhãn đơn hàng' : 'Cập nhật nhãn'"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form autocomplete="off" class="modal-body">
                <div class="form-group">
                    <label>
                        Tên nhãn
                        <span class="text-danger">*</span>
                        <span class="validationMessage" data-bind="validationMessage: name"></span>
                    </label>
                    <input type="text" class="form-control" data-bind="value: name"/>
                </div>
                <div class="form-group">
                    <label>Mô tả</label>
                    <input type="text" class="form-control" data-bind="value: des"/>
                </div>
                <div class="form-group">
                    <label>Màu sắc nhãn</label>
                    <input type="text" class="form-control"
                           data-bind="colorPicker: color, style: {background: color}"/>
                </div>
            </form>
            <div class="modal-footer text-center">
                <button class="btn btn-success btn-sm" data-bind="click: $root.save">
                    <i class="fa fa-check"></i>
                    Lưu lại
                </button>
            </div>
        </div>
    </div>
</div>