ko.bindingHandlers.colorPicker = {
    init: function (element, valueAccessor) {
        let value = valueAccessor();
        $(element).val(ko.utils.unwrapObservable(value));
        $(element).ColorPicker({
            onChange: function (hsb, hex, rgb) {
                let observable = valueAccessor();
                observable('#' + hex);
            }
        })
    },
    update: function (element, valueAccessor) {
        $(element).val(ko.utils.unwrapObservable(valueAccessor()));
    }
};

function viewModel() {
    let self = this;
    self.id = ko.observable(0);
    self.name = ko.observable().extend({
        required: LABEL.required
    });
    self.des = ko.observable();
    self.color = ko.observable();
    self.result = ko.mapping.fromJS([]);

    self.init = function () {
        AJAX.get(window.location, null, true, (res) => {
            ko.mapping.fromJS(res.data, self.result);
        });
    };

    self.create = function () {
        self.id(0);
        self.name(undefined);
        self.des(undefined);
        self.color(undefined);
        self.errors.showAllMessages(false);
        MODAL.show('#label-editor-modal');
    };

    self.edit = function (item) {
        self.id(item.id());
        self.name(item.name());
        self.des(item.description());
        self.color(item.color());

        self.errors.showAllMessages(false);
        MODAL.show('#label-editor-modal');
    };

    self.save = function () {
        if (!self.isValid())
            self.errors.showAllMessages();
        else {
            let data = {
                id: self.id(),
                name: self.name(),
                des: self.des(),
                color: self.color()
            };

            AJAX.post(window.location, data, true, (res) => {
                if (res.error)
                    NOTI.danger(res.message);
                else {
                    NOTI.success(res.message);
                    MODAL.hide('#label-editor-modal');
                    self.init();
                }
            });
        }
    };

    self.delete = function (item) {
        ALERT.confirm('Xác nhận xóa nhãn ' + item.name(), null, () => {
            AJAX.post('/admin/order_setting/delete_label', {id: item.id()}, true, (res) => {
                if (res.error)
                    NOTI.danger(res.message);
                else {
                    self.result.remove(item);
                    NOTI.success('Đã xóa nhãn');
                }
            });
        });
    }
}

let model = new viewModel();
model.init();
ko.validatedObservable(model);
ko.applyBindings(model, document.getElementById('main-content'));