<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('top')
    <button class="btn btn-primary" data-bind="click: create">
        <i class="now-ui-icons ui-1_simple-add"></i>
        Thêm tài khoản
    </button>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-7 col-xl-8">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">
                        <a href="#" class="float-right" data-bind="click: create">
                            <i class="fa fa-plus"></i>
                            Thêm thành viên
                        </a>
                        Danh sách thành viên
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Tài khoản</th>
                            <th>Email</th>
                            <th>Phân quyền</th>
                            <th width="140">Trạng thái</th>
                            <th width="180"></th>
                        </tr>
                        </thead>
                        <tbody data-bind="foreach: result" class="text-center">
                        <tr>
                            <td data-bind="text: name"></td>
                            <td data-bind="text: email"></td>
                            <td data-bind="text: roles"></td>
                            <td>
                                <!--ko if: active() == 1-->
                                <span class="badge badge-success">Kích hoạt</span>
                                <!--/ko-->
                                <!--ko if: active() == 0-->
                                <span class="badge badge-warning">Không kích hoạt</span>
                                <!--/ko-->
                            </td>
                            <td>
                                <button class="btn btn-info btn-xs btn-link" data-bind="click: $root.edit">
                                    <i class="fa fa-edit"></i>
                                    Cập nhật
                                </button>
                                <button class="btn btn-danger btn-xs" data-bind="click: $root.delete">
                                    <i class="fa fa-trash"></i>
                                    Xóa
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-lg-5 col-xl-4">
            @include('partial/guide')
        </div>
    </div>
@endsection
@section('modal')
    @include('editor_modal')
@endsection
@section('script')
    <script src="{{load_js('index')}}"></script>
@endsection