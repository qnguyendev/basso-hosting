<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')

@section('content')
    <form autocomplete="off" class="row">
        <div class="col-md-12 col-lg-7 col-xl-8">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Thông tin sản phẩm</div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label>
                            Tên sản phẩm
                            <span class="text-danger">*</span>
                            <span class="validationMessage" data-bind="validationMessage: name"
                                  style="display: none;"></span>
                        </label>
                        <input type="text" class="form-control"
                               data-bind="value: name, event: {change: seo_preview.change}"/>
                    </div>
                    <div class="form-group">
                        <label> Đường dẫn tĩnh </label>
                        <input type="text" class="form-control"
                               data-bind="value: slug, event: {change: $root.create_slug}" title="Bắt buộc">
                    </div>
                    <div class="form-group">
                        <label>Mô tả ngắn</label>
                        <textarea data-bind="value: des" class="form-control" rows="5" cols="10"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Chi tiết sản phẩm</label>
                        <textarea data-bind="ckeditor: content" class="form-control" rows="6"></textarea>
                    </div>
                </div>
            </div>

            @include('partial/editor/images')
            @include('partial/editor/attributes')
            @include('partial/editor/related_products')
            @include('partial/editor/related_articles')
        </div>

        <div class="col-md-12 col-lg-5 col-xl-4">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">Hiển thị</div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <div class="row">
                            <!-- hiển thị sản phẩm -->
                            <div class="col-md-6">
                                <div class="checkbox c-checkbox">
                                    <label>
                                        <input class="form-check-input" type="checkbox" data-bind="checked: active"/>
                                        <span class="fa fa-check"></span>
                                        Hiển thị trên website
                                    </label>
                                </div>
                            </div>

                            <!-- còn hàng/hết hàng -->
                            <div class="col-md-6">
                                <div class="checkbox c-checkbox">
                                    <label>
                                        <input class="form-check-input" type="checkbox"
                                               data-bind="checked: out_stock"/>
                                        <span class="fa fa-check"></span>
                                        Sản phẩm hết hàng
                                    </label>
                                </div>
                            </div>

                            <!-- Hiển thị giá -->
                            <div class="col-md-6">
                                <div class="checkbox c-checkbox">
                                    <label>
                                        <input class="form-check-input" type="checkbox"
                                               data-bind="checked: show_price"/>
                                        <span class="fa fa-check"></span>
                                        Hiển thị giá
                                    </label>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="checkbox c-checkbox">
                                    <label>
                                        <input class="form-check-input" type="checkbox"
                                               data-bind="checked: discount_schedule"/>
                                        <span class="fa fa-check"></span>
                                        Đặt lịch giảm giá
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <!-- mã sản phẩm -->
                        <div class="form-group col-12 col-sm-6">
                            <label>Mã sản phẩm</label>
                            <input type="text" class="form-control" data-bind="value: sku" placeholder="Mã tự động"/>
                        </div>

                        <!-- trong kho -->
                        <div class="form-group col-12 col-sm-6">
                            <label>Số lượng</label>
                            <input type="number" class="form-control" data-bind="value: available"/>
                        </div>

                        <div class="form-group col-12 col-sm-6">
                            <label>Giá sản phẩm</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        đ
                                    </div>
                                </div>
                                <input type="text" class="form-control"
                                       data-bind="moneyMask: price"/>
                            </div>
                        </div>

                        <div class="form-group col-12 col-sm-6">
                            <label>Giá khuyến mãi</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        đ
                                    </div>
                                </div>
                                <input type="text" class="form-control"
                                       data-bind="moneyMask: sale_price"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group" data-bind="visible: discount_schedule">
                        <div class="row">
                            <div class="col-sm-6 col-12">
                                <label>Từ ngày</label>
                                <input type="text" class="form-control" data-bind="datePicker: sale_from"/>
                            </div>
                            <div class="col-sm-6 col-12">
                                <label>đến ngày</label>
                                <input type="text" class="form-control" data-bind="datePicker: sale_to"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-center">
                    <a class="btn btn-secondary btn-sm" href="/admin/product">
                        <i class="fa fa-arrow-left"></i>
                        Quay lại
                    </a>
                    <button class="btn btn-success btn-sm" data-bind="click: $root.save">
                        <i class="fa fa-check"></i>
                        Lưu
                    </button>
                </div>
            </div>

            <!-- Danh mục, nhãn hiệu -->
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">Danh mục sản phẩm</div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <div id="term-list">
                            <div class="row">
                                @foreach($terms as $term)
                                    <div class="col-12 col-md-6">
                                        <div class="checkbox c-checkbox w-100 mt-1 ml-2">
                                            <label>
                                                <input class="form-check-input" type="checkbox" value="{{$term->id}}"
                                                       data-bind="checked: terms"/>
                                                <span class="fa fa-check"></span>
                                                {{$term->name}}
                                            </label>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-12 col-sm-6">
                            <label>Thương hiệu</label>
                            <select class="form-control select2" data-bind="value: brand_id">
                                <option value="0">- - - - -</option>
                                @foreach($brands as $brand)
                                    <option value="{{$brand->id}}">{{$brand->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-12 col-sm-6">
                            <label>Xuất xứ</label>
                            <select class="form-control select2" data-bind="value: country_code">
                                <option value="">- - - - - -</option>
                                @foreach($country as $item)
                                    <option value="{{$item->code}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Nhóm sản phẩm</label>
                        <input type="text" data-role="tagsinput"
                               data-bind="value: tags" placeholder="Nhập tag nhóm"/>
                    </div>
                </div>
            </div>

            <!-- SEO -->
        @include('partial/seo_editor')
        <!--//end seo-->
        </div>
    </form>
@endsection

@section('modal')
    @include('partial/editor/modal/find_product_modal')
    @include('partial/editor/modal/find_article_modal')
@endsection

@section ('script')
    {{ckeditor()}}
    <script src="/assets/js/core/underscore.min.js"></script>
    <script src="/assets/vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.js"></script>
    <script>
        let attributes = {{json_encode($attributes, JSON_UNESCAPED_UNICODE )}};
        let attribute_values = {{json_encode($attribute_values, JSON_UNESCAPED_UNICODE)}};
    </script>
    <script src="{{load_js('edit')}}"></script>
@endsection

@section('css')
    <link rel="stylesheet" href="/assets/vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.css"/>
@endsection