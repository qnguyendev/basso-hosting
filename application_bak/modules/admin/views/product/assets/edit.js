function viewModel() {
    let self = this;
    ko.utils.extend(self, new imageUploadModel());
    self.id = ko.observable(0);
    self.name = ko.observable().extend({
        required: {message: LABEL.required}
    });

    self.slug = ko.observable();
    self.des = ko.observable();
    self.content = ko.observable();

    self.created_time = ko.observable(new Date()).extend({
        required: {message: LABEL.required}
    });

    self.active = ko.observable(true);
    self.discount_schedule = ko.observable(false);  //  Có lịch giảm giá

    self.price = ko.observable();
    self.out_stock = ko.observable(false);  //  Hết hàng
    self.show_price = ko.observable(true); //  Hiển thị giá
    self.available = ko.observable();   //  Số lượng trong kho

    self.sku = ko.observable(); //  mã sản phẩm
    self.sale_price = ko.observable();
    self.sale_from = ko.observable();
    self.sale_to = ko.observable();

    self.terms = ko.observableArray([]);
    self.brand_id = ko.observable(0);
    self.country_code = ko.observable('');

    self.tags = ko.observable(); //  Sản phẩm cùng nhóm

    self.seo_title = ko.observable();
    self.seo_description = ko.observable();
    self.seo_index = ko.observable(1);
    self.seo_preview = {
        title: ko.observable('Tiêu đề page website không vượt quá 70 kí tự (tốt nhất từ 60-70 kí tự)'),
        des: ko.observable('Mô tả page website không vượt quá 155 kí tự (tốt nhất từ 100-155 kí tự). Là những đoạn mô tả ngắn gọn về website, bài viết...'),
        slug: ko.observable(undefined),
        change: function () {
            let title = LABEL.default_seo_title, des = LABEL.default_seo_des;

            if (self.name() !== undefined) {
                if (self.name() !== null)
                    if (self.name().length > 0)
                        title = self.name();
            }

            if (self.seo_title() !== undefined) {
                if (self.seo_title() !== null)
                    if (self.seo_title().length > 0)
                        title = self.seo_title();
            }

            if (self.seo_description() !== undefined) {
                if (self.seo_description() !== null)
                    if (self.seo_description().length > 0)
                        des = self.seo_description();
            }

            self.seo_preview.title(title);
            self.seo_preview.slug(self.slug());
            self.seo_preview.des(des);

            if (self.slug() === undefined && self.name() !== undefined) {
                if (self.name() !== null)
                    if (self.name().length > 0)
                        self.slug(create_slug(self.name()));
            }
        }
    };

    //  Hình ảnh
    self.images_id = ko.observableArray([]);
    self.images_url = ko.observableArray([]);

    self.related_products = {
        list: ko.mapping.fromJS([]),
        selected: ko.observableArray([]),
        key: ko.observable(),
        result: ko.mapping.fromJS([]),
        pagination: ko.observable(),
        show: function () {
            ko.mapping.fromJS([], self.related_products.result);
            self.related_products.key(undefined);
            MODAL.show('#find-product-modal');
        },
        search: function (page) {
            let data = {
                key: self.related_products.key(),
                action: 'find-product',
                selected: self.related_products.selected(),
                page: page
            };
            AJAX.get('/admin/product/search', data, true, (res) => {
                if (!res.error) {
                    ko.mapping.fromJS(res.data, self.related_products.result);
                    self.related_products.pagination(res.pagination);
                } else
                    NOTI.danger(res.message);
            });
        },
        select: function (item) {
            if (!self.related_products.selected().includes(item.id()))
                self.related_products.selected.push(item.id());
        },
        save: function () {
            let items = self.related_products.result.filter((x) => {
                return self.related_products.selected().indexOf(x.id()) >= 0;
            });

            ko.utils.arrayPushAll(self.related_products.list, items);
            MODAL.hide('#find-product-modal');
        },
        remove: function (item) {
            self.related_products.list.remove(item);
            self.related_products.selected.remove(item);
        }
    };

    self.related_articles = {
        list: ko.mapping.fromJS([]),
        selected: ko.observableArray([]),
        key: ko.observable(),
        result: ko.mapping.fromJS([]),
        pagination: ko.observable(),
        show: function () {
            ko.mapping.fromJS([], self.related_articles.result);
            self.related_articles.key(undefined);
            MODAL.show('#find-article-modal');
        },
        search: function (page) {
            let data = {
                key: self.related_articles.key(),
                action: 'find-article',
                selected: self.related_articles.selected(),
                page: page
            };
            AJAX.get('/admin/product/search', data, true, (res) => {
                if (!res.error) {
                    ko.mapping.fromJS(res.data, self.related_articles.result);
                    self.related_articles.pagination(res.pagination);
                } else
                    NOTI.danger(res.message);
            });
        },
        select: function (item) {
            if (!self.related_articles.selected().includes(item.id()))
                self.related_articles.selected.push(item.id());
        },
        save: function () {
            let items = self.related_articles.result.filter((x) => {
                return self.related_articles.selected().indexOf(x.id()) >= 0;
            });

            ko.utils.arrayPushAll(self.related_articles.list, items);
            MODAL.hide('#find-article-modal');
        },
        remove: function (item) {
            self.related_articles.list.remove(item);
            self.related_articles.selected.remove(item);
        }
    };

    //#region params thuộc tính
    self.attributes = ko.observableArray(attributes);   //  Danh sách thuộc tính và giá trị thuộc tính
    self.attribute_id = ko.observable();
    self.is_show_attribute = ko.observable(false);

    self.selected_attribute_id = ko.observableArray([]);    // Danh sách id thuộc tính được lựa chọn
    self.selected_attributes = ko.mapping.fromJS([]);   //  Danh sách thuộc tính được lựa
    //#endregion

    self.init = function (show_loader) {
        if (window.location.toString().indexOf('/edit/') > 0) {
            AJAX.get(window.location, null, show_loader, (res) => {
                self.terms(res.terms);

                ko.utils.arrayPushAll(self.images_url, res.images);
                ko.utils.arrayPushAll(self.images_id, res.images.map(a => a.id));

                self.id(res.product.id);
                self.name(res.product.name);
                self.slug(res.product.slug);
                self.active(res.product.active == 1);
                self.show_price(res.product.show_price == 1);
                self.out_stock(res.product.out_stock == 1);
                self.seo_index(res.product.seo_index);
                self.brand_id(res.product.brand_id);
                self.country_code(res.product.country_code);

                ko.mapping.fromJS(res.product.upsell, self.related_products.list);
                ko.mapping.fromJS(res.product.articles, self.related_articles.list);
                self.related_articles.list.each((x) => {
                    self.related_articles.selected.push(x.id());
                });
                self.related_products.list.each((x) => {
                    self.related_products.selected.push(x.id());
                });

                if (res.product.available !== null)
                    self.available(res.product.available);

                if (res.product.price != null)
                    self.price(moneyFormat(res.product.price));

                if (res.product.sale_price != null) {
                    self.sale_price(moneyFormat(res.product.sale_price));

                    if (res.product.sale_from != null) {
                        self.discount_schedule(true);
                        self.sale_from(moment.unix(res.product.sale_from).format('YYYY-MM-DD'));
                    }

                    if (res.product.sale_to != null) {
                        self.discount_schedule(true);
                        self.sale_to(moment.unix(res.product.sale_to).format('YYYY-MM-DD'));
                    }
                }

                if (res.product.seo_title != null)
                    self.seo_title(res.product.seo_title);

                if (res.product.seo_description != null)
                    self.seo_description(res.product.seo_description);

                if (res.product.content != null)
                    self.content(res.product.content);

                if (res.product.description != null)
                    self.des(res.product.description);

                if (res.product.sku != null)
                    self.sku(res.product.sku);

                if (res.tags != null) {
                    if (res.tags.length > 0) {
                        self.tags(res.tags.join(','));
                    }
                }

                if (res.attributes.length > 0) {
                    self.is_show_attribute(true);
                    self.selected_attribute_id(res.attributes);
                    ko.mapping.fromJS(res.attribute_values, self.selected_attributes);
                    $('select[multiple]').select2();
                }

                self.seo_preview.change();
            });
        }
    };

    //#region Ảnh/upload
    self.upload_images = function () {
        self.image_uploader.show(false);
        self.image_uploader.save = () => {
            for (let i = 0; i < self.image_uploader.selected_images.length; i++) {
                let item = self.image_uploader.selected_images[i];
                self.images_id.push(item.id);
            }

            ko.utils.arrayPushAll(self.images_url, self.image_uploader.selected_images);
            self.image_uploader.close();
        };
    };

    self.delete_image = function (item) {
        self.images_id.remove(item.id);
        self.images_url.remove(item);
    };
    //#endregion

    //#region Thuộc tính sản phẩm
    self.show_attribute = function () {
        self.is_show_attribute(true);
    };

    //  Thêm thuộc tính
    self.add_attribute = function () {
        if (self.attribute_id() === undefined || self.attribute_id().length === 0) {
            if (self.attributes().length > 0)
                self.attribute_id(self.attributes()[0].id);
        }

        if (!self.selected_attribute_id().includes(self.attribute_id())) {
            let item = {
                attribute_id: ko.observable(self.attribute_id()),
                attribute_name: ko.observable(),
                values: ko.mapping.fromJS([]),
                value_id: ko.observableArray([])
            };

            _.each(self.attributes(), function (attr) {
                if (attr.id === self.attribute_id())
                    item.attribute_name(attr.name);
            });

            self.selected_attribute_id.push(self.attribute_id());
            self.selected_attributes.push(item);
            self.change_attribute(item);

            $('select.select2').select2();
        }
    };

    //  Thay đổi lựa chọn thuộc tính
    self.change_attribute = function (item) {
        item.value_id([]);
        ko.mapping.fromJS(_.where(attribute_values, {attribute_id: item.attribute_id()}), item.values);
    };

    //  Xóa thuộc tính
    self.delete_attribute = function (item) {
        self.selected_attributes.remove(item);
        self.selected_attribute_id.remove(item.attribute_id());
    };
    //#endregion

    //#region Mẫu mã sản phẩm
    self.add_variant = function () {
        let item = {
            value_id: ko.observableArray([]),
            selected: ko.observableArray([]),
            price: ko.observable(),
            sku: ko.observable(),
            quantity: ko.observable()
        };

        self.variants.push(item);
        $('table select.select2').select2();
    };

    self.change_variant_value = function (item) {
        if (item.value_id().length > 0) {
            //  List giá trị theo id
            let values = _.filter(attribute_values, function (t) {
                return item.value_id().includes(t.id);
            });

            if (values.length == 1) {
                item.selected.push({
                    value_id: ko.observable(values[0].id),
                    attribute_id: ko.observable(values[0].attribute_id)
                })
            } else {
                let attrs_id = [];
                _.each(values, function (t) {
                    attrs_id.push(t.attribute_id);
                });

                for (let i = 0; i < item.selected().length; i++) {
                    if (attrs_id.includes(item.selected()[i].attribute_id())) {
                        values = _.filter(values, function (t) {
                            return t.attribute_id == item.selected()[i].attribute_id() && t.id != item.selected()[i].value_id;
                        });

                        console.log(values);
                    }
                }
            }

            console.log(ko.toJSON(item));
        } else {
            item.selected_value_id([]);
            item.attribute_id([]);
        }
    };

    self.remove_variant = function (item) {
        self.variants.remove(item);
    };
    //#endregion

    self.create_slug = function () {
        if (self.slug() !== undefined)
            if (self.slug().length > 0)
                self.slug(create_slug(self.slug()));
    };

    //#region Lưu sản phẩm
    self.save = function () {
        if (!self.isValid())
            self.errors.showAllMessages();
        else {
            //#region Dữ liệu đẩy về server
            let data = {
                id: self.id(),
                name: self.name(),
                slug: self.slug(),
                related_products: self.related_products.selected(),
                related_articles: self.related_articles.selected()
            };

            if (self.des() !== undefined)
                if (self.des().length > 0)
                    data.des = self.des();

            if (self.content() !== undefined)
                if (self.content().length > 0)
                    data.content = self.content();

            data.active = self.active() ? 1 : 0;
            data.out_stock = self.out_stock() ? 1 : 0;
            data.show_price = self.show_price() ? 1 : 0;

            if (self.sku() !== undefined) {
                if (self.sku().length > 0)
                    data.sku = self.sku();
            }

            if (self.available() !== undefined) {
                data.available = self.available();
            }

            if (self.price() !== undefined) {
                if (self.price().length > 0) {
                    data.price = toNumber(self.price());
                }
            }

            //  Giá sale
            if (self.sale_price() !== undefined) {
                if (self.sale_price().length > 0) {
                    data.sale_price = toNumber(self.sale_price());
                }
            }

            //  Nếu đặt lịch giảm giá
            if (self.discount_schedule()) {
                if (self.sale_from() !== undefined)
                    if (self.sale_from().length > 0)
                        data.sale_from = self.sale_from();

                if (self.sale_to() !== undefined)
                    if (self.sale_to().length > 0)
                        data.sale_to = self.sale_to();
            }

            if (self.terms().length > 0)
                data.terms = self.terms();

            if (self.brand_id() > 0)
                data.brand_id = self.brand_id();
            if (self.country_code().length > 0)
                data.country_code = self.country_code();

            if (self.images_id().length > 0)
                data.images_id = self.images_id();

            data.seo_index = self.seo_index();

            if (self.seo_title() !== undefined)
                if (self.seo_title().length > 0)
                    data.seo_title = self.seo_title();

            if (self.seo_description() !== undefined)
                if (self.seo_description().length > 0)
                    data.seo_des = self.seo_description();

            if (self.tags() !== undefined) {
                if (self.tags().length > 0)
                    data.tags = self.tags();
            }

            //  Thuộc tính sản phẩm
            if (self.selected_attributes().length > 0) {
                let value_id = [];
                _.each(self.selected_attributes(), function (item) {
                    _.each(item.value_id(), function (id) {
                        value_id.push(id);
                    });
                });

                data.attributes = value_id;
            }

            /*
            let attributes = ko.observableArray([]);
            _.each(self.attributes(), function (item) {
                _.each(item.value_id(), function (id) {
                    attributes.push(id);
                });
            });

            if (attributes().length > 0)
                data.append('attribute', attributes());
                */
            //#endregion

            AJAX.post(window.location, data, true, (res) => {
                if (res.error)
                    NOTI.danger(res.message);
                else {
                    window.location = '/admin/product/';
                }
            });
        }
    }
    //#endregion
}

let model = new viewModel();
model.init(true);
ko.validatedObservable(model);
ko.applyBindings(model, document.getElementById('main-content'))