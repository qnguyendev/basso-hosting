<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="find-article-modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Tìm bài viết
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form autocomplete="off" class="modal-body">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" data-bind="value: related_articles.key"
                               placeholder="Tìm theo tên bài viết"/>
                        <div class="input-group-append" data-bind="click: function(){$root.related_articles.search(1)}">
                            <button class="btn btn-primary">
                                Tìm bài viết
                            </button>
                        </div>
                    </div>
                </div>

                <table class="table table-stripe table-bordered">
                    <thead>
                    <tr>
                        <th width="60"></th>
                        <th></th>
                        <th>Tiêu đề</th>
                        <th>Ngày đăng</th>
                    </tr>
                    </thead>
                    <tbody data-bind="foreach: related_articles.result" class="text-center">
                    <tr>
                        <td>
                            <div class="checkbox c-checkbox w-100 mt-1 ml-2">
                                <label>
                                    <input class="form-check-input" type="checkbox"
                                           data-bind="checked: $root.related_articles.selected, value: id"/>
                                    <span class="fa fa-check"></span>
                                </label>
                            </div>
                        </td>
                        <td>
                            <!--ko if: image_path() == null-->
                            <img src="/timthumb.php?src=/assets/img/no-image-available.jpg&w=50&h=50"/>
                            <!--/ko-->
                            <!--ko if: image_path() != null-->
                            <img data-bind="attr: {src: '/timthumb.php?src=' + image_path() + '&w=50&h=50'}"/>
                            <!--/ko-->
                        </td>
                        <td data-bind="text: name"></td>
                        <td data-bind="text: moment.unix(created_time()).format('DD/MM/YYYY')"></td>
                    </tr>
                    </tbody>
                </table>

                @include('partial/editor/modal/related_articles_pagination')
            </form>
            <div class="modal-footer">
                <button class="btn btn-success" data-bind="click: related_articles.save">
                    <i class="fa fa-check"></i>
                    Thêm bài viết
                </button>
            </div>
        </div>
    </div>
</div>