<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="find-product-modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Tìm sản phẩm
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form autocomplete="off" class="modal-body">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" data-bind="value: related_products.key"
                               placeholder="Tìm theo tên, mã sản phẩm"/>
                        <div class="input-group-append" data-bind="click: function(){$root.related_products.search(1)}">
                            <button class="btn btn-primary">
                                Tìm sản phẩm
                            </button>
                        </div>
                    </div>
                </div>

                <table class="table table-stripe table-bordered">
                    <thead>
                    <tr>
                        <th width="60"></th>
                        <th></th>
                        <th>Sản phẩm</th>
                        <th>Mã</th>
                        <th>Giá thường</th>
                        <th>Giá sale</th>
                    </tr>
                    </thead>
                    <tbody data-bind="foreach: related_products.result" class="text-center">
                    <tr>
                        <td>
                            <div class="checkbox c-checkbox w-100 mt-1 ml-2">
                                <label>
                                    <input class="form-check-input" type="checkbox"
                                           data-bind="checked: $root.related_products.selected, value: id"/>
                                    <span class="fa fa-check"></span>
                                </label>
                            </div>
                        </td>
                        <td>
                            <!--ko if: image() == null-->
                            <img src="/timthumb.php?src=/assets/img/no-image-available.jpg&w=50&h=50"/>
                            <!--/ko-->
                            <!--ko if: image() != null-->
                            <img data-bind="attr: {src: '/timthumb.php?src=' + image() + '&w=50&h=50'}"/>
                            <!--/ko-->
                        </td>
                        <td data-bind="text: name"></td>
                        <td data-bind="text: sku"></td>
                        <td>
                            <!--ko if: price() != null-->
                            <span data-bind="text: parseInt(price()).toMoney(0)"></span>
                            <!--/ko-->
                        </td>
                        <td>
                            <!--ko if: sale_price() != null-->
                            <span data-bind="text: parseInt(sale_price()).toMoney(0)"></span>
                            <!--ko if: sale_from() != null || sale_to() != null-->
                            <br/>
                            <i class="text-info">(
                                <span data-bind="text: sale_from() != null ? moment.unix(sale_from()).format('DD/MM/YY') : '-'"></span>
                                <span data-bind="text: sale_to() != null ? moment.unix(sale_to()).format('DD/MM/YY') : '-'"></span>
                                )
                            </i>
                            <!--/ko-->
                            <!--/ko-->
                        </td>
                    </tr>
                    </tbody>
                </table>

                @include('partial/editor/modal/related_products_pagination')
            </form>
            <div class="modal-footer">
                <button class="btn btn-success" data-bind="click: related_products.save">
                    <i class="fa fa-check"></i>
                    Thêm sản phẩm
                </button>
            </div>
        </div>
    </div>
</div>