<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<table class="table table-striped">
    <tbody data-bind="foreach: variants">
    <tr>
        <td>
            <div class="row">
                <div class="col-12 form-group">
                    <select class="form-control select2" multiple
                            data-bind="selectedOptions: value_id, event: {change: $root.change_variant_value}">
                        <!--ko foreach: $root.selected_attributes-->
                        <optgroup data-bind="attr: {label: attribute_name}"></optgroup>
                        <!--ko foreach: values-->
                        <!--ko if: $parent.value_id().includes(id())-->
                        <option data-bind="value: id, text: name"></option>
                        <!--/ko-->
                        <!--/ko-->
                        <!--/ko-->
                    </select>
                </div>

                <div class="col-sm-4 form-group" style="margin-bottom: 0">
                    <input type="text" class="form-control" placeholder="Mã sản phẩm"/>
                </div>

                <div class="col-sm-4 form-group" style="margin-bottom: 0">
                    <input type="number" class="form-control"
                           placeholder="Số lượng"/>
                </div>

                <div class="col-sm-4 form-group" style="margin-bottom: 0">
                    <div class="input-group" style="margin-bottom: 0">
                        <input type="text" class="form-control"
                               placeholder="Giá bán"/>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                đ
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </td>
        <td style="width: 60px">
            <a href="javsacript:" data-bind="click: $root.remove_variant">
                <i class="now-ui-icons ui-1_simple-remove"></i>
                Xóa
            </a>
        </td>
    </tr>
    </tbody>
</table>

<div class="text-center">
    <button class="btn btn-default btn-sm" data-bind="click: $root.add_variant">
        <i class="fa fa-plus"></i>
        Thêm mẫu mã
    </button>
</div>