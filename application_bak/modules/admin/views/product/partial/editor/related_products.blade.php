<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="card">
    <div class="card-header">
        <h6 class="card-title">
            Sản phẩm liên quan (Up Sell)
            <a href="#" class="float-right text-primary" data-bind="click: related_products.show">
                <i class="icon-plus"></i>
                Chọn
            </a>
        </h6>
    </div>
    <div class="card-body">
        <table class="table table-stripe table-bordered" data-bind="visible: related_products.list().length > 0">
            <thead>
            <tr>
                <th></th>
                <th>Sản phẩm</th>
                <th>Mã</th>
                <th>Giá thường</th>
                <th>Giá sale</th>
                <th></th>
            </tr>
            </thead>
            <tbody data-bind="foreach: related_products.list" class="text-center">
            <tr>
                <td>
                    <!--ko if: image() == null-->
                    <img src="/timthumb.php?src=/assets/img/no-image-available.jpg&w=50&h=50"/>
                    <!--/ko-->
                    <!--ko if: image() != null-->
                    <img data-bind="attr: {src: '/timthumb.php?src=' + image() + '&w=50&h=50'}"/>
                    <!--/ko-->
                </td>
                <td data-bind="text: name"></td>
                <td data-bind="text: sku"></td>
                <td>
                    <!--ko if: price() != null-->
                    <span data-bind="text: parseInt(price()).toMoney(0)"></span>
                    <!--/ko-->
                </td>
                <td>
                    <!--ko if: sale_price() != null-->
                    <span data-bind="text: parseInt(sale_price()).toMoney(0)"></span>
                    <!--ko if: sale_from() != null || sale_to() != null-->
                    <br/>
                    <i class="text-info">(
                        <span data-bind="text: sale_from() != null ? moment.unix(sale_from()).format('DD/MM/YY') : '-'"></span>
                        <span data-bind="text: sale_to() != null ? moment.unix(sale_to()).format('DD/MM/YY') : '-'"></span>
                        )
                    </i>
                    <!--/ko-->
                    <!--/ko-->
                </td>
                <td>
                    <a href="#" data-bind="click: $root.related_products.remove" class="text-danger text-bold">
                        <i class="icon-trash"></i>
                        Xóa
                    </a>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
