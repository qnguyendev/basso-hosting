<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="attribute_editor_modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-notice">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"
                    data-bind="text: attributes().id() == 0 ? 'Thêm thuộc tính' : 'Cập nhật thuộc tính'">
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>
                        Tên thuộc tính
                        <span class="text-danger">*</span>
                        <span class="validationMessage" data-bind="validationMessage: attributes().name"></span>
                    </label>
                    <input type="text" class="form-control" data-bind="value: attributes().name"/>
                </div>
                <div class="form-group">
                    <label>Mô tả/ghi chú</label>
                    <input type="text" class="form-control" data-bind="value: attributes().des"/>
                </div>
                <div class="form-group">
                    <label>
                        Kiểu giá trị
                        <span class="font-weight-normal font-italic">
                            (màu sắc, ảnh hay nhập chữ)
                        </span>
                    </label>
                    <select class="form-control" data-bind="value: attributes().type, enable: attributes().id() == 0">
                        <option value="text">Nhập giá trị</option>
                        <option value="color">Màu sắc</option>
                        <option value="image">Có ảnh</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Kích hoạt </label>
                    <select class="form-control" data-bind="value: attributes().active">
                        <option value="1">Có</option>
                        <option value="0">Không</option>
                    </select>
                </div>
                <div class="form-gorup text-center">
                    <button class="btn btn-success" data-bind="click: attributes().save">
                        <i class="fa fa-check"></i>
                        Lưu lại
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
