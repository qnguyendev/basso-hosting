ko.bindingHandlers.colorPicker = {
    init: function (element, valueAccessor) {
        let value = valueAccessor();
        $(element).val(ko.utils.unwrapObservable(value));
        $(element).ColorPicker({
            onChange: function (hsb, hex, rgb) {
                let observable = valueAccessor();
                observable('#' + hex);
            }
        })
    },
    update: function (element, valueAccessor) {
        $(element).val(ko.utils.unwrapObservable(valueAccessor()));
    }
};

function viewModel() {
    let self = this;

    self.attributes = ko.observable({
        id: ko.observable(0),
        name: ko.observable().extend({
            required: {message: LABEL.required}
        }),
        des: ko.observable(),
        type: ko.observable('text'),
        active: ko.observable(1),
        result: ko.mapping.fromJS([]),
        pagination: ko.observable({
            current_page: 1,
            total_page: 0,
            total_item: 0
        }),
        init: function (page) {
            AJAX.get(window.location, {id: 0, page: page}, true, (res) => {
                ko.mapping.fromJS(res.result, self.attributes().result);
                self.attributes().pagination(res.pagination);
            });
        },
        create: function () {
            self.attributes().id(0);
            self.attributes().name(undefined);
            self.attributes().des(undefined);
            self.attributes().type('text');
            self.attributes().active(1);

            self.attributes().errors.showAllMessages(false);
            MODAL.show('#attribute_editor_modal');
        },
        save: function () {
            if (!self.attributes().isValid())
                self.attributes().errors.showAllMessages();
            else {
                let data = {
                    id: self.attributes().id(),
                    name: self.attributes().name(),
                    des: self.attributes().des(),
                    type: self.attributes().type(),
                    active: self.attributes().active
                };

                AJAX.post('/admin/attribute/save', data, true, (res) => {
                    if (res.error)
                        NOTI.danger(res.message);
                    else {
                        self.attributes().init();
                        NOTI.success('Cập nhật thành công');
                        MODAL.hide('#attribute_editor_modal');
                    }
                });
            }
        },
        edit: function (item) {
            self.attributes().id(item.id());
            self.attributes().name(item.name());
            self.attributes().des(item.description());
            self.attributes().type(item.type());
            self.attributes().active(item.active());

            self.attributes().errors.showAllMessages(false);
            MODAL.show('#attribute_editor_modal');
        },
        delete: function (item) {
            ALERT.confirm('Xác nhận xóa thuộc tính', 'Các giá trị của thuộc tính cũng sẽ bị xóa theo', function () {
                AJAX.post('/admin/attribute/delete', {id: item.id()}, true, () => {
                    self.attributes().init(self.attributes().pagination().current_page);
                    NOTI.success('Xóa thuộc tính thành công');
                });
            });
        },
        view: function (item) {
            self.values().init(item);
        }
    });

    self.values = ko.observable({
        id: ko.observable(),
        attribute_id: ko.observable(),
        attribute_name: ko.observable(),
        name: ko.observable().extend({
            required: {message: LABEL.required}
        }),
        value: ko.observable(),
        type: ko.observable(),
        active: ko.observable(1),
        result: ko.mapping.fromJS([]),
        pagination: ko.observable({
            current_page: 1,
            total_page: 0,
            total_item: 0
        }),
        file: ko.observable(),
        init: function (attribute) {
            self.values().attribute_id(attribute.id());
            self.values().attribute_name(attribute.name());
            self.values().type(attribute.type());
            self.values().search(1);
        },
        search: function (page) {
            AJAX.get(window.location, {id: self.values().attribute_id(), page: page}, true, (res) => {
                ko.mapping.fromJS(res.result, self.values().result);
                self.values().pagination(res.pagination);
            });
        },
        create: function () {
            self.values().id(0);
            self.values().name(undefined);
            self.values().value(undefined);
            self.values().file(undefined);
            self.values().errors.showAllMessages(false);

            MODAL.show('#value_editor_modal');
        },
        //#region Xem trước ảnh upload
        preview_image: function (data, event) {
            if (event.target.files.length === 0)
                return;

            let file = event.target.files[0];
            if (file.size < 1024 * 1024) {
                if (file.type == "image/jpeg" || file.type == "image/jpg") {
                    let reader = new FileReader();
                    reader.onload = function (e) {
                        self.values().value(e.target.result);
                    };

                    reader.readAsDataURL(file);
                    self.values().file(file);
                } else {
                    NOTI.danger('Định dạng ảnh không hợp lệ.');
                }
            } else {
                NOTI.danger('Ảnh phải nhỏ hơn 1MB');
            }
        },
        clear_image: function () {
            self.values().file(undefined);
            self.values().value(undefined);
        },
        //#endregion
        save: function () {
            if (!self.values().isValid())
                self.values().errors.showAllMessages();
            else {
                let data = new FormData();
                data.append('name', self.values().name());
                data.append('attribute_id', self.values().attribute_id());
                data.append('type', self.values().type());
                data.append('value', self.values().value());
                data.append('active', self.values().active());
                data.append('id', self.values().id());

                if (self.values().file !== undefined && self.values().type() == 'image')
                    data.append('file', self.values().file());

                AJAX.upload('/admin/attribute/save_value', data, true, (res) => {
                    if (res.error)
                        NOTI.danger(res.message);
                    else {
                        self.values().search(self.values().pagination().current_page);
                        NOTI.success('Lưu giá trị thành công');
                        MODAL.hide('#value_editor_modal');
                    }
                });
            }
        },
        edit: function (item) {
            self.values().id(item.id());
            self.values().name(item.name());
            self.values().value(item.value());
            self.values().file(undefined);
            self.values().errors.showAllMessages(false);
            self.values().type(item.type());

            MODAL.show('#value_editor_modal');
        },
        delete: function (item) {
            ALERT.confirm('Xác nhận xóa giá trị', null, function () {
                AJAX.post('/admin/attribute/delete_value', {id: item.id()}, true, function () {
                    self.values().search(self.values().pagination().current_page);
                    NOTI.success('Xóa thành công');
                });
            });
        }
    });
}

let model = new viewModel();
model.attributes().init(1);
ko.validatedObservable(model.attributes());
ko.validatedObservable(model.values());
ko.applyBindings(model, document.getElementById('main-content'))