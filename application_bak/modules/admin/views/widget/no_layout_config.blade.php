<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="card">
        <div class="card-body">
            <p class="text-center text-danger">
                Không tìm thấy file cấu hình widget <b>layout.json</b>
                của theme <b>{{$this->config->item('theme')}}</b>
            </p>
        </div>
    </div>
@endsection
