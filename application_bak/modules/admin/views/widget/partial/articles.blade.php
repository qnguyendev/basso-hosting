<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="form-group">
    <label>Danh mục</label>
    <select class="form-control select2" multiple data-bind="selectedOptions: articles().terms">
        @foreach($terms as $term)
            <option value="{{$term->id}}">{{$term->name}}</option>
        @endforeach
    </select>
</div>
<div class="row">
    <div class="form-group col-12 col-sm-4">
        <label>Sắp xếp</label>
        <select class="form-control" data-bind="value: articles().order">
            <option value="newest">Bài mới nhất</option>
            <option value="views">Xem nhiều</option>
            <option value="featured">Bài nổi bật</option>
        </select>
    </div>
    <div class="form-group col-12 col-sm-4">
        <label>Số lượng bài lấy</label>
        <input type="number" min="1" class="form-control" value="1" data-bind="value: articles().limit"/>
    </div>
    <div class="form-group col-12 col-sm-4">
        <label>Hiển thị mô tả</label>
        <select class="form-control" data-bind="value: articles().show_des">
            <option value="0">Không</option>
            <option value="1">Có</option>
        </select>
    </div>
</div>

<div class="alert alert-info text-center">
    <b>Danh mục</b>: bỏ trống để lấy bài viết thuộc tất cả các danh mục
</div>