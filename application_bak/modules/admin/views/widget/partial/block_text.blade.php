<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<button class="btn btn-primary btn-block" data-bind="click: $root.block_text.add">
    <i class="icon-plus"></i>
    Thêm khối văn bản
</button>
<div class="mt-2" id="accordion" data-bind="foreach: block_text.list">
    <div class="card card-default mb-1">
        <div class="card-header" data-bind="attr: {id: 'heading-' + id()}">
            <h4 class="mb-0">
                <a href="javascript:" class="text-inherit collapsed" data-toggle="collapse"
                   data-bind="attr: {'data-target': '#c' + id()}"
                   aria-expanded="false">
                    <span data-bind="text: $index() + 1"></span>
                    -
                    <span data-bind="text: title"></span>
                    <a class="text-danger text-bold float-right" data-bind="click: $root.block_text.delete">
                        <i class="icon-trash"></i>
                        xóa
                    </a>
                </a>
            </h4>
        </div>

        <div data-bind="attr: {id: 'c' + id()}" class="collapse" data-parent="#accordion">
            <div class="card-body">
                <div class="form-group">
                    <label>
                        Tiêu đề
                        <span class="text-danger">*</span>
                        <span class="validationMessage" data-bind="validationMessage: title"></span>
                    </label>
                    <input type="text" class="form-control" data-bind="value: title"/>
                </div>
                <div class="form-group">
                    <label>Nội dung</label>
                    <textarea class="form-control" data-bind="ckeditor: content" rows="5" cols="10"></textarea>
                </div>
            </div>
        </div>
    </div>
</div>