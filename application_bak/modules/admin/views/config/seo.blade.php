<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
global $theme_config;
$active_post = false;
$active_product = false;

foreach ($theme_config['term'] as $term) {
    if ($term['id'] == 'post')
        $active_post = $term['active'];
    else if($term['id'] == 'product')
        $active_product = $term['active'];
}
?>
@layout('cpanel_layout')
@section('content')
    <form method="post">
        <div class="row">
            <div class="col-md-12 col-lg-6 col-xl-5 offset-xl-1">
                <div class="card card-default">
                    <div class="card-header">
                        <h6 class="card-title">Cấu hình SEO</h6>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Cho phép Google index</label>
                            <select class="form-control" name="robots_follow">
                                <option value="1" {{$data['robots_follow'] == 1 ? 'selected' : ''}}>Có</option>
                                <option value="0" {{$data['robots_follow'] == 0 ? 'selected' : ''}}>Không</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Nội dung robots.txt</label>
                            <textarea name="robots_content" class="form-control" rows="10"
                                      style="height: 200px !important;padding: 10px; line-height: 1.2; font-family: monospace;">{{join(null, $data['robots_content'])}}</textarea>
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        <button class="btn btn-success text-uppercase" type="submit">
                            <i class="fa fa-check"></i>
                            Cập nhật
                        </button>
                    </div>
                </div>
            </div>

            <div class="col-md-12 col-lg-6 col-xl-5">
                <div class="card card-default">
                    <div class="card-header">
                        <h6 class="card-title">Cấu hình sitemap</h6>
                    </div>
                    <div class="card-body">
                        @if($active_post)
                            <div class="form-group">
                                <label>Sitemap cho bài viết</label>
                                <select class="form-control" name="post_sitemap">
                                    <option value="1" {{$data['post_sitemap'] == 1 ? 'selected' : ''}}>Có</option>
                                    <option value="0" {{$data['post_sitemap'] == 0 ? 'selected' : ''}}>Không</option>
                                </select>
                            </div>
                        @endif
                        @if($active_product)
                            <div class="form-group">
                                <label>Sitemap cho sản phẩm</label>
                                <select class="form-control" name="product_sitemap">
                                    <option value="1" {{$data['product_sitemap'] == 1 ? 'selected' : ''}}>Có</option>
                                    <option value="0" {{$data['product_sitemap'] == 0 ? 'selected' : ''}}>Không</option>
                                </select>
                            </div>
                        @endif
                        <div class="form-group">
                            <label>Sitemap cho trang nội dung</label>
                            <select class="form-control" name="page_sitemap">
                                <option value="1" {{$data['page_sitemap'] == 1 ? 'selected' : ''}}>Có</option>
                                <option value="0" {{$data['page_sitemap'] == 0 ? 'selected' : ''}}>Không</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <a target="_blank" class="btn btn-info btn-sm" href="{{base_url('sitemap.xml')}}">
                                <i class="fa fa-external-link-alt"></i>
                                Xem sitemap
                            </a>
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        <button class="btn btn-success text-uppercase" type="submit">
                            <i class="fa fa-check"></i>
                            Cập nhật
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection