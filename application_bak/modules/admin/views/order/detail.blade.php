<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
$shipping = null;
if ($order->shipping_status == 'success') {
    if ($order->shipping_id != null) {
        $index = array_search($order->shipping_id, array_column($shipping_methods, 'id'));
        if ($index >= 0)
            $shipping = $shipping_methods[$index];
    }
}
?>
@layout('cpanel_layout')
@section('content')
    <form autocomplete="off" class="row">
        <div class="col-md-12 col-lg-7 col-xl-8">
            <!-- Thông tin khách hàng -->
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        Thông tin khách hàng
                    </div>
                </div>
                <div class="card-body">
                    <table class="table border-vertical" data-bind="with: order">
                        <tr>
                            <th class="text-left">Khách hàng</th>
                            <td class="text-left">
                                <input type="text" class="form-control"
                                       data-bind="value: name, visible: shipping_status == 'waiting'"/>
                                <span data-bind="text: name, visible: shipping_status != 'waiting'"></span>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-left">Điện thoại</th>
                            <td class="text-left">
                                <input type="text" class="form-control"
                                       data-bind="value: phone, visible: shipping_status == 'waiting'"/>
                                <span data-bind="text: phone, visible: shipping_status != 'waiting'"></span>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-left">Email</th>
                            <td class="text-left">
                                <input type="email" class="form-control"
                                       data-bind="value: email, visible: shipping_status == 'waiting'"/>
                                <span data-bind="text: email, visible: shipping_status != 'waiting'"></span>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-left">Địa chỉ</th>
                            <td class="text-left">
                                <input type="text" class="form-control"
                                       data-bind="value: address, visible: shipping_status == 'waiting'"/>
                                <span data-bind="text: address, visible: shipping_status != 'waiting'"></span>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-left">Tỉnh/thành</th>
                            <td class="text-left">
                                <!--ko if: shipping_status == 'waiting' -->
                                <select class="form-control select2" style="height: 32px" name="city"
                                        data-bind="value: city_id, event: {change: $root.change_city}">
                                    @foreach($cities as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                                <!--/ko-->
                                <span data-bind="text: city, visible: shipping_status != 'waiting'"></span>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-left">Quận/huyện</th>
                            <td class="text-left">
                                <!--ko if: shipping_status == 'waiting' -->
                                <select class="form-control select2" style="height: 32px;" name="district"
                                        data-bind="options: $root.districts, optionsText: 'name', optionsValue: 'id',
                                            value: district_id">
                                </select>
                                <!--/ko-->
                                <span data-bind="text: district, visible: shipping_status != 'waiting'"></span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <!-- Danh sách sản phẩm đặt mua -->
            @include('partial/detail/items')
        </div>

        <div class="col-md-12 col-lg-5 col-xl-4">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">Thông tin đơn hàng</div>
                </div>
                <div class="card-body">
                    <table class="table table-striped">

                        <tr>
                            <th class="text-left">Thời gian</th>
                            <td class="text-left">{{date('H:i d/m/Y', $order->created_time)}}</td>
                        </tr>
                        <tr>
                            <th class="text-left">Mã giảm giá</th>
                            <td class="text-left">{{$order->discount_code == null ? '-' : $order->discount_code}}</td>
                        </tr>
                        <tr>
                            <th class="text-left">Giảm trừ</th>
                            <td class="text-left" data-bind="text: parseInt(discount_total()).toMoney(0)"></td>
                        </tr>
                        <tr>
                            <th class="text-left">Ghi chú</th>
                            <td class="text-left">{{$order->note}}</td>
                        </tr>
                    </table>
                </div>
                <div class="card-footer text-center">
                    <button class="btn btn-success btn-sm" data-bind="click: $root.save">
                        <i class="fa fa-check"></i>
                        Cập nhật
                    </button>
                </div>
            </div>

            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">Thanh toán</div>
                </div>
                <div class="card-body">
                    <p>
                        <strong>Hình thức:</strong> {{$order->payment}}
                    </p>
                    <?php if(in_array($order->payment_type, array('banking', 'visa', 'bank-transfer'))) : ?>
                    <p>
                        <strong>Trạng thái: </strong>
                        <!--ko if: payment_status() == 'waiting'-->
                        <span class="badge badge-warning">Đợi thanh toán</span>
                        <!--/ko-->
                        <!--ko if: payment_status() == 'error'-->
                        <span class="badge badge-danger">Lỗi thanh toán</span>
                        <!--/ko-->
                        <!--ko if: payment_status() == 'success'-->
                        <span class="badge badge-success">Đã thanh toán</span>
                        <!--/ko-->
                    </p>

                    <!-- xác nhận thanh toán nếu chuyển khoản -->
                    <?php if ($order->payment_status == 'waiting' && $order->payment_type == 'bank-transfer' && $order->shipping_status == 'waiting') : ?>
                    <button class="btn btn-primary btn-block"
                            data-bind="click: $root.confirm_bank_transfer, visible: payment_status() == 'waiting'">
                        Xác nhận chuyển khoản
                    </button>
                    <?php elseif (in_array($order->payment_status, array('waiting', 'error')) && $order->shipping_status == 'waiting'
                    && in_array($order->payment_type, array('banking', 'visa')) && $order->transaction_id != null) : ?>
                    <button class="btn btn-primary btn-block"
                            data-bind="click: $root.check_payment, visible: payment_status() == 'waiting' || payment_status() == 'error'">
                        Kiểm tra giao dịch
                    </button>
                    <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">Quản lý đơn</div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label>Gán nhãn đơn hàng</label>
                        <select class="form-control" multiple
                                data-bind="selectedOptions: labels_id, options: labels, optionsText: 'name', optionsValue: 'id'">
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Trạng thái đơn hàng</label>
                        <?php if (in_array($order->shipping_status, array('waiting', 'processing'))) : ?>
                        <select class="form-control"
                                data-bind="value: order().shipping_status">
                            @foreach(OrderStatus::LIST as $key=>$value)
                                <option value="{{$key}}">{{$value['name']}}</option>
                            @endforeach
                        </select>
                        <?php elseif ($order->shipping_status == 'success') : ?>
                        <span class="badge-success badge">Đã hoàn thành</span>
                        <?php elseif ($order->shipping_status == 'cancelled') : ?>
                        <span class="badge-danger badge">Hủy đơn</span>
                        <?php endif; ?>
                    </div>
                    <?php if ($order->shipping_status != 'success') : ?>
                    <div class="form-group"
                         data-bind="visible: order().shipping_status == 'processing' || order().shipping_status == 'success'">
                        <label>Giao hàng</label>
                        <select class="form-control"
                                data-bind="value: order().shipping_id">
                            @foreach($shipping_methods as $item)
                                <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group"
                         data-bind="visible: order().shipping_status != 6 && (order().shipping_status == 'processing' || order().shipping_status == 'success')">
                        <label>Mã vận đơn</label>
                        <input type="text" class="form-control"
                               data-bind="value: order().tracking_code"/>
                    </div>
                    <?php else : ?>
                    <div class="form-group">
                        <label>Giao hàng</label>
                        <span class="text-indent-20">{{$shipping == null ? '-' : $shipping->name}}</span>
                    </div>
                    <div class="form-group">
                        <label>Mã vận đơn</label>
                        <span class="text-indent-20">
                            <?php if ($order->tracking_code == null) : echo '-'; else: ?>
                            <a href="{{$shipping->tracking_url == null ? $shipping->website : $shipping->tracking_url}}{{$order->tracking_code}}"
                               target="_blank">{{$order->tracking_code}}</a>
                            <?php endif; ?>
                        </span>
                    </div>
                    <?php endif; ?>
                    <div class="form-group">
                        <label>Ghi chú</label>
                        <textarea rows="5" cols="10" class="form-control" data-bind="value: order().cancel_note"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
@section('modal')
    @include('modal/add_product_modal')
@endsection
@section('script')
    <script>
        let order = {{json_encode($order)}};
        let labels = {{json_encode($labels)}};
        let products = {{json_encode($products)}};
        let labels_id = {{json_encode(array_column($labels_id, 'label_id'))}};
    </script>
    <script src="{{load_js('detail')}}"></script>
@endsection