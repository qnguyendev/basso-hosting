function viewModel() {
    let self = this;
    self.order = ko.observable(order);    //  Chi tiết đơn hàng
    self.id = ko.observable(order.id);
    self.districts = ko.mapping.fromJS([]);
    self.products = ko.mapping.fromJS(products);
    self.shipping_status = ko.observable(order.shipping_status);
    self.payment_status = ko.observable(order.payment_status);
    self.payment_type = ko.observable(order.payment_type);
    self.cancel_note = ko.observable(order.cancel_note);
    self.tracking_code = ko.observable(order.tracking_code);
    self.shipping_id = ko.observable(order.shipping_id);
    self.district_id = ko.observable(order.district_id);

    self.labels = ko.mapping.fromJS([]);
    self.labels_id = ko.observableArray([]);

    //  Tìm kiếm sản phẩm
    self.filter_product = ko.observable();
    self.filter_result = ko.mapping.fromJS([]);

    //
    self.discount_total = ko.observable();  //  Tổng giảm trừ đơn hàng
    self.sub_total = ko.observable();   //  Tổng đon hàng cần thanh toán

    self.init = function () {
        $('select[multiple], select.select2').select2();
        $('select[multiple]').val(self.labels_id()).trigger('change');
        $('select[name=city]').val(self.order().city_id).trigger('change');

        self.labels_id(labels_id);
        self.change_city();
        self.calc_total(undefined);
        ko.mapping.fromJS(labels, self.labels);

        $('#add_product_modal').on('hidden.bs.modal', function () {
            self.filter_product(undefined);
            ko.mapping.fromJS([], self.filter_result);
        });
    };

    //  Tìm sản phẩm thêm đơn hàng
    self.search_product = function () {
        if (self.filter_product() === undefined || self.filter_product() === null) {
            NOTI.danger('Vui lòng nhập mã hoặc tên sản phẩm');
            return;
        }

        AJAX.get(window.location, {filter_key: self.filter_product()}, true, (res) => {
            ko.mapping.fromJS(res.data, self.filter_result);
        });
    };

    //  Thêm sản phẩm vào giỏ hàng
    self.add_product = function (item) {
        let in_list = false;
        for (let i = 0; i < self.products().length; i++) {
            if (item.id() == self.products()[i].id()) {

                let prod_att = [];  //  List giá trị thuộc tính sản phẩm trong đơn hàng
                let new_att = [];   //  List giá trị thuộc tính sản phẩm được thêm vào
                let prod = self.products()[i];

                for (let j = 0; j < prod.attributes().length; j++)
                    prod_att.push(prod.attributes()[j].value_id());

                for (let j = 0; j < item.attributes().length; j++)
                    new_att.push(item.attributes()[j].value_id());

                let filter_result = prod_att.filter(function (n) {
                    return new_att.indexOf(n) >= 0;
                });

                in_list = filter_result.length == prod_att.length;
                if (in_list)
                    break;
            }
        }

        if (!in_list) {
            self.products.push({
                id: ko.observable(item.id()),
                name: ko.observable(item.name()),
                price: ko.observable(item.price()),
                quantity: ko.observable(1),
                image: ko.observable(item.image()),
                discount_total: ko.observable(0),
                total: ko.observable(0),
                sub_total: ko.observable(item.price()),
                attributes: ko.observable(item.attributes()),
                data: []
            });

            self.calc_total();
        } else
            NOTI.danger('Sản phẩm đã có trong đơn hàng');
    };

    //  Tính toán lại đơn hàng
    self.calc_total = function () {
        let sub_total = 0, discount_total = parseInt(order.discount_total);

        self.products.each((x) => {
            if (x.price() > 0) {
                x.sub_total(parseInt(x.quantity()) * parseInt(x.price()) - toNumber(x.discount_total()));
                sub_total += parseInt(x.sub_total());
                discount_total += parseInt(toNumber(x.discount_total()));
            }
        });

        sub_total = sub_total - discount_total;
        self.sub_total(sub_total < 0 ? 0 : sub_total);
        self.discount_total(discount_total);
    };

    //  Xóa sản phẩm khỏi đơn hàng
    self.delete_product = function (item) {
        self.products.remove(item);
        self.calc_total();
    };

    //  Thay đổi thành phố
    self.change_city = function () {
        AJAX.get(window.location, {city_id: self.order().city_id}, false, (res) => {
            ko.mapping.fromJS(res.data, self.districts);
            let in_list = false;
            for (let i = 0; i < res.data.length; i++) {
                if (res.data[i].id == order.district_id) {
                    in_list = true;
                    break;
                }
            }

            if (in_list)
                $('select[name=district]').val(self.district_id()).trigger('change');
            else {
                $('select[name=district]').val(res.data[0].id).trigger('change');
                self.order().district_id = res.data[0].id;
            }
        });
    };

    //  Xác nhận khách đã chuyển khoản
    self.confirm_bank_transfer = function () {
        ALERT.confirm('Xác nhận thanh toán', 'Bạn đã nhận được chuyển khoản của khách hàng?', () => {
            AJAX.post('/admin/order/payment', {id: self.id(), type: 'confirm_bank_transfer'}, true, (res) => {
                if (res.error)
                    NOTI.danger(res.message);
                else {
                    NOTI.success(res.message);
                    self.payment_status('success');
                }
            });
        });
    };

    //  Kiểm tra giao dịch online
    self.check_payment = function () {
        AJAX.post('/admin/order/payment', {id: self.id(), type: 'online', opt: self.payment_type()}, true, (res) => {
            if (res.error)
                NOTI.danger(res.message);
            else {
                NOTI.success(res.message);
                self.payment_status('success');
            }
        });
    };

    //  Cập nhật trạng thái đơn hàng
    self.save = function () {
        for (let i = 0; i < self.products().length; i++) {
            let attributes = [];
            for (let j = 0; j < self.products()[i].attributes().length; j++) {
                attributes.push({
                    attribute_id: self.products()[i].attributes()[j].attribute_id(),
                    attribute_value_id: self.products()[i].attributes()[j].value_id(),
                })
            }

            self.products()[i].discount_total(toNumber(self.products()[i].discount_total()));
            self.products()[i].data = attributes;
        }

        let data = {
            labels_id: self.labels_id(),
            order: self.order(),
            products: self.products()
        };

        AJAX.post(window.location, data, true, (res) => {
            if (res.error)
                NOTI.danger(res.message);
            else {
                NOTI.success('Cập nhật đơn hàng thành công', function () {
                    window.location.reload();
                });
            }
        });
    };
}

let model = new viewModel();
model.init();
ko.validatedObservable(model);
ko.applyBindings(model, document.getElementById('main-content'));