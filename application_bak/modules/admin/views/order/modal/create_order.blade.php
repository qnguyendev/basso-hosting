<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="create_order_modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Tạo đơn hàng
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form autocomplete="off" class="modal-body">
                <div class="row">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control" data-bind="value: modal().filter_product"
                                       placeholder="Nhập tên, mã sản phẩm"/>
                                <div class="input-group-append">
                                    <button class="btn btn-primary btn-sm" data-bind="click: modal().search">
                                        <i class="fa fa-plus"></i>
                                        Tìm
                                    </button>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered"
                               data-bind="visible: modal().filter_result().length > 0">
                            <thead>
                            <tr>
                                <th width="60"></th>
                                <th class="text-left">Sản phẩm</th>
                                <th class="text-right" width="90">Giá bán</th>
                                <th width="40"></th>
                            </tr>
                            </thead>
                            <tbody data-bind="foreach: modal().filter_result" class="text-center">
                            <tr>
                                <td>
                                    <img data-bind="attr: {src: '/timthumb.php?src=' + image() + '&w=50&h=50'}"/>
                                </td>
                                <td class="text-left">
                                    <span class="text-left" data-bind="text: name"></span>
                                    <!--ko if: attributes().length > 0-->
                                    <br/>
                                    <!--ko foreach: attributes-->
                                    <select class="form-control input-sm" style="display: inline-block; width: auto"
                                            data-bind="value: value_id, options: values, optionsText: 'name', optionsValue: 'id'">
                                    </select>
                                    <!--/ko-->
                                    <!--/ko-->
                                </td>
                                <td class="text-right" data-bind="text: parseInt(price()).toMoney(0)"></td>
                                <td>
                                    <button class="btn btn-success btn-xs" data-bind="click: $root.modal().add_product">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-7">
                        <div role="tabpanel" style="margin: 0 -1px -1px -1px">
                            <!-- Nav tabs-->
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link active" href="#tab_cart_info" aria-controls="ordertracking"
                                       role="tab" data-toggle="tab">
                                        Chi tiết đơn hàng
                                    </a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" href="#tab_customer_info" aria-controls="orderlabel"
                                       role="tab" data-toggle="tab">
                                        Thông tin khách hàng
                                    </a>
                                </li>
                            </ul>
                            <!-- Tab panes-->
                            <div class="tab-content">
                                @include('partial/cart_info')
                                @include('partial/customer_info')
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="modal-footer">
                <button class="btn btn-success" data-bind="click: modal().save">
                    <i class="fa fa-check"></i>
                    Tạo đơn
                </button>
            </div>
        </div>
    </div>
</div>