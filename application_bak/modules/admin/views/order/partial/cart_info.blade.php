<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="tab-pane active" id="tab_cart_info" role="tabpanel">
    <p class="text-center text-primary" data-bind="visible: modal().products().length == 0">
        Vui lòng tìm sản phẩm và thêm vào danh sách
    </p>
    <table class="table table-striped"
           data-bind="visible: modal().products().length > 0">
        <thead>
        <tr>
            <th width="80">Ảnh</th>
            <th class="text-left">Sản phẩm</th>
            <th width="80">SL</th>
            <th class="text-right" width="120">Thành tiền</th>
            <th width="70"></th>
        </tr>
        </thead>
        <tbody data-bind="foreach: modal().products">
        <tr>
            <td width="60">
                <img data-bind="attr: {src: '/timthumb.php?src=' + image() + '&w=50&h=50'}"/>
            </td>
            <td class="text-left">
                <span data-bind="text: name"></span>
                <!--ko if: attributes().length > 0-->
                <br/>
                <!--ko foreach: attributes -->
                <strong data-bind="text: name" style="font-size: .9em"></strong>:
                <span data-bind="text: value" style="font-size: .9em"></span><br/>
                <!--/ko-->
                <!--/ko-->
            </td>
            <td>
                <input type="number" class="form-control input-sm"
                       data-bind="value: quantity, event: {change: $root.modal().re_calc_detail}"/>
            </td>
            <td class="text-right"
                data-bind="text: parseInt(sub_total()).toMoney(0)"></td>
            <td>
                <a href="#" class="text-danger font-weight-bold" data-bind="click: $root.modal().delete_product">
                    <i class="icon-trash"></i>
                    Xóa
                </a>
            </td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <th colspan="3">Giảm giá</th>
            <th class="text-right" colspan="2">
                <input type="number" class="form-control text-right input-sm"
                       data-bind="value: modal().discount_total, event: {change: function(){$root.modal().re_calc_detail(undefined)}}"/>
            </th>
        </tr>
        <tr>
            <th colspan="3">Tổng tiền</th>
            <th class="text-right"
                data-bind="text: parseInt(modal().sub_total()).toMoney(0)"></th>
            <th></th>
        </tr>
        </tfoot>
    </table>
</div>