<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="card">
    <div class="card-header">
        <div class="card-title">
            Sản phẩm đặt mua
            <?php if ($order->shipping_status == 'waiting') : ?>
            <a href="javascript:" data-toggle="modal" data-target="#add_product_modal"
               class="float-right">
                <i class="fa fa-plus"></i>
                Thêm sản phẩm
            </a>
            <?php endif; ?>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th width="80">Ảnh</th>
                <th class="text-left">Sản phẩm</th>
                <th>Giá</th>
                <th width="70">SL</th>
                <th width="120">Giảm giá</th>
                <th>Thành tiền</th>
                <?php if ($order->shipping_status == 'waiting') : ?>
                <th width="70" data-bind="visible: order().shipping_status == 'waiting'"></th>
                <?php endif; ?>
            </tr>
            </thead>
            <tbody data-bind="foreach: products" class="text-center">
            <tr>
                <td width="60" style="padding: 3px">
                    <img data-bind="attr: {src: '/timthumb.php?src=' + image() + '&w=70&h=70'}"/>
                </td>
                <td class="text-left pl-3">
                    <span data-bind="text: name"></span>
                    <!--ko if: attributes().length > 0-->
                    <br/>
                    <!--ko if: $root.order().shipping_status == 'waiting'-->
                    <!--ko foreach: attributes-->
                    <select data-bind="value: value_id, options: values, optionsText: 'name', optionsValue: 'id'">
                    </select>
                    <!--/ko-->
                    <!--/ko-->
                    <!--ko if: $root.order().shipping_status != 'waiting'-->
                    <!--ko foreach: attributes -->
                    <strong data-bind="text: name" style="font-size: .9em"></strong>:
                    <span data-bind="text: value" style="font-size: .9em"></span><br/>
                    <!--/ko-->
                    <!--/ko-->
                    <!--/ko-->
                </td>
                <td data-bind="text: parseInt(price()).toMoney(0)"></td>
                <td>
                    <input type="number" class="form-control input-sm"
                           data-bind="value: quantity, enable: $root.order().shipping_status == 'waiting',
                                       visible: $root.order().shipping_status == 'waiting',
                                        event: {change: function(){$root.calc_total();}}"/>
                    <span data-bind="visible: $root.order().shipping_status != 'waiting', text: quantity"></span>
                </td>
                <td>
                    <div class="input-group"
                         data-bind="visible: $root.order().shipping_status == '{{OrderStatus::WAITING}}'">
                        <input type="text" class="form-control input-sm text-right"
                               data-bind="moneyMask: discount_total, event: {change: function(){$root.calc_total();}}">

                        <div class="input-group-append">
                            <span class="input-group-text text-sm">
                                đ
                            </span>
                        </div>
                    </div>
                    <span data-bind="visible: $root.order().shipping_status != 'waiting', text: parseInt(discount_total()).toMoney(0)"></span>
                </td>
                <td data-bind="text: parseInt(sub_total()).toMoney(0)"></td>
                <?php if ($order->shipping_status == 'waiting') : ?>
                <td data-bind="visible: order.shipping_status == 'waiting'">
                    <a href="#" class="text-danger font-weight-bold" data-bind="click: $root.delete_product">
                        <i class="icon-trash"></i>
                        Xóa
                    </a>
                </td>
                <?php endif; ?>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <th colspan="4">Tổng tiền</th>
                <th data-bind="text: parseInt(discount_total()).toMoney(0)"></th>
                <th data-bind="text: parseInt(sub_total()).toMoney(0)"></th>
                <?php if ($order->shipping_status == 'waiting') : ?>
                <th data-bind="visible: order().shipping_status == 'waiting'"></th>
                <?php endif; ?>
            </tr>
            </tfoot>
        </table>
    </div>
</div>
