<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

abstract class OrderStatus
{
    const LIST = [
        'waiting' => ['name' => 'Chưa xử lý', 'class' => 'warning'],
        'processing' => ['name' => 'Đang xử lý/giao hàng', 'class' => 'info'],
        'cancelled' => ['name' => 'Đã hủy', 'class' => 'danger'],
        'completed' => ['name' => 'Hoàn thành/đã giao hàng', 'class' => 'success']
    ];

    const WAITING = 'waiting';
    const PROCESSING = 'processing';
    const CANCELLED = 'cancelled';
    const COMPLETED = 'completed';
}