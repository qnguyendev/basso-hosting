<div style="max-width: 720px; margin: 0 auto">
    <?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
    <table style="width: 100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td align="left" style="background-color: #00354E; padding:20px">
                <img src="https://basso.vn/uploads/logo.png" style="max-height: 30px; width: auto"/>
            </td>
            <td align="right"
                style="color: #FFF; background-color: #00354E; padding:20px; font-family: Arial, Helvetica, sans-serif">
                0965687790 - <a href="mailto: cskh@basso.vn" style="color: #FFF">cskh@basso.vn</a>
            </td>
        </tr>
    </table>

    <table style="width: 100%; margin: 20px 0" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td align="center" style="color: #00354E; padding:20px; background: #EEE">
                <strong style="text-transform: uppercase; font-family: Arial, Helvetica, sans-serif; font-size: 20px; line-height: 1.7">
                    XÁC NHẬN THANH TOÁN THÀNH CÔNG<br/>
                    ĐƠN HÀNG #{{$order->order_code}}
                </strong>
            </td>
        </tr>
    </table>

    <p style="color: #00354E; padding:10px; font-family: Arial, Helvetica, sans-serif; line-height: 1.6; font-size: 13px">
        Xin chào Quý khách <strong style="color: #00354E;">{{$order->name}}</strong>,<br/>
        Basso rất vui được thông báo đơn hàng <strong>#{{$order->order_code}}</strong> của Quý khách đã được thanh toán
        thành
        công và đang tiến hành xử lý mua hàng.<br/>
        Cảm ơn Quý khách đã đặt hàng tại Basso.vn. Chúng tôi hy vọng Quý khách hài lòng với trải
        nghiệm mua sắm và các sản phẩm đã chọn.
    </p>

    <table style="width: 100%; margin: 10px 0 20px" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="4" style="padding: 10px 0">
                <strong style="text-transform: uppercase; font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #f3a23b">THÔNG
                    TIN ĐƠN HÀNG</strong>
            </td>
        </tr>
        <tr>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE">
                <strong>Mã đơn hàng</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none">
                #{{$order->order_code}}
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none">
                <strong>Hình thức thanh toán</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none">
                {{$order->payment_method}}
            </td>
        </tr>
        <tr>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-top: none">
                <strong>Ngày đặt hàng</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                {{date('d/m/Y', time())}}
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none; border-top: none">
                <strong>Trạng thái thanh toán</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                Chưa đặt cọc
            </td>
        </tr>
        <tr>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-top: none">
                <strong>Tổng giá trị đơn hàng</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                {{format_money($order->sub_total)}}
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none; border-top: none">
                <strong>Số tiền đã thanh toán</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                {{format_money($order->total_paid)}}
            </td>
        </tr>
        <tr>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-top: none">
                <strong>Trạng thái đơn hàng</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                Đã tiếp nhận
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none; border-top: none">
                <strong>Số tiền cần thanh toán</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                {{format_money($order->sub_total - $order->total_paid)}}
            </td>
        </tr>
    </table>

    <table style="width: 100%; margin: 20px 0" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="2" style="padding: 10px 0" width="50%">
                <strong style="text-transform: uppercase; font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #f3a23b">
                    THÔNG TIN ĐẶT HÀNG
                </strong>
            </td>
            <td colspan="2" style="padding: 10px 0" width="50%">
                <strong style="text-transform: uppercase; font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #f3a23b">
                    THÔNG TIN NHẬN HÀNG
                </strong>
            </td>
        </tr>
        <tr>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ">
                <strong>Họ tên</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ">
                {{$order->name}}
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ">
                <strong>Họ tên</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ">
                {{$order->name}}
            </td>
        </tr>
        <tr>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ; border-top: none">
                <strong>Email</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ; border-top: none">
                {{$order->email }}
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ; border-top: none">
                <strong>Email</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ; border-top: none">
                {{$order->email }}
            </td>
        </tr>
        <tr>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ; border-left: none">
                <strong>Điện thoại</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ">
                {{$order->phone}}
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ; border-left: none">
                <strong>Điện thoại</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ">
                {{$order->phone}}
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ; border-left: none; border-top: none">
                <strong>Địa chỉ</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ; border-top: none">
                {{$order->shipping_address}}, {{$order->district}}, {{$order->city}}
            </td>
        </tr>
    </table>

    <table style="width: 100%; margin: 20px 0" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="6" style="padding: 10px 0">
                <strong style="text-transform: uppercase; font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #f3a23b">
                    CHI TIẾT ĐƠN HÀNG
                </strong>
            </td>
        </tr>
        <tr>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE">
                <strong>STT</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none">
                <strong>Sản phẩm</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none" width="80">
                <strong>Giá ngoại tệ</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none">
                <strong>Giá sản phẩm</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none">
                <strong>Số lượng</strong>
            </td>
            <td style="width: 90px; font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none">
                <strong>Thành tiền</strong>
            </td>
        </tr>
        @for($i = 0; $i < count($items); $i++)
            <tr>
                <?php $item = $items[$i]; ?>
                <td  align="center" style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-top: none">
                    {{($i + 1)}}
                </td>
                <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                    {{$item->name}}
                </td>
                <td align="center" style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                    {{$order->currency_symbol}} {{$item->price}}
                </td>
                <td align="center" style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                    {{format_money($order->currency_rate * $item->price)}}
                </td>
                <td align="center" style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                    {{$item->quantity}}
                </td>
                <td align="center" style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                    {{format_money($order->currency_rate * $item->price * $item->quantity)}}
                </td>
            </tr>
        @endfor
        <tr>
            <td align="right" colspan="5"
                style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-top: none">
                <strong>Tổng tiền</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                <strong>{{format_money($order->sub_total)}}</strong>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="5"
                style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-top: none">
                <strong>Phí giao hàng trong nước</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                <strong>-</strong>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="5"
                style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-top: none">
                <strong>Tổng giá trị đơn hàng tạm tính</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                <strong>{{format_money($order->sub_total)}}</strong>
            </td>
        </tr>
    </table>

    <p style="color: #00354E; padding:10px; font-family: Arial, Helvetica, sans-serif; line-height: 1.6; font-size: 13px">
        <strong><i>(*) Phí vận chuyển quốc tế:</i></strong> Là phí vận chuyển từ Mỹ/Anh/Tây Ban Nha/… về Việt Nam. Phí
        ship
        được tính theo cân nặng
        thực tế khi hàng về Việt Nam, đơn giá <strong>260,000đ/kg</strong>.
        Để đảm bảo Quý khách mua được những sản phẩm ở đơn hàng này với giá như trên, quý khách <strong>vui lòng thanh
            toán trong vòng 24 giờ</strong> (Giá có thể thay đổi theo thời gian, đặc
        biệt
        trong chương trình khuyến mãi).<br/>
        (*) Nội dung chuyển khoản: thanh toan <strong>{{$order->order_code}}</strong>
    </p>

    <table style="width: 100%; margin: 10px 0 30px" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="4" style="padding: 10px 0">
                <strong style="text-transform: uppercase; font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #f3a23b">
                    THÔNG TIN CHUYỂN KHOẢN
                </strong>
            </td>
        </tr>
        <tr>
            <td align="center" width="50%"
                style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE">
                <strong>Ngân hàng Ngoại Thương Vietcombank</strong>
            </td>
            <td align="center" width="50%"
                style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none">
                <strong>Ngân hàng BIDV</strong>
            </td>
        </tr>
        <tr>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 15px; line-height: 1.6; color: #00354E; border: solid 1px #00354E; border-top: 0">
                <ul style="margin: 0; list-style: none; padding: 0">
                    <li>STK: 0011004213524</li>
                    <li>Nguyễn Thị Thùy Dương</li>
                    <li>Chi nhánh Sở Giao dịch, HN</li>
                </ul>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 15px; line-height: 1.6; color: #00354E; border: solid 1px #00354E; border-top: 0; border-left: 0">
                <ul style="margin: 0; list-style: none; padding: 0">
                    <li>STK: 15010000615850</li>
                    <li>Nguyễn Thị Thùy Dương</li>
                    <li>Chi nhánh Bắc Hà Nội, HN</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td align="center"
                style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-top: 0">
                <strong>Ngân hàng Techcombank</strong>
            </td>
            <td align="center"
                style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-top: 0; border-left: none">
                <strong>Ngân hàng Vietinbank</strong>
            </td>
        </tr>
        <tr>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 15px; line-height: 1.6; color: #00354E; border: solid 1px #00354E; border-top: 0">
                <ul style="margin: 0; list-style: none; padding: 0">
                    <li>STK: 19032994116991</li>
                    <li>Nguyễn Thị Thùy Dương</li>
                    <li>Chi nhánh Chương Dương, HN</li>
                </ul>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 15px; line-height: 1.6; color: #00354E; border: solid 1px #00354E; border-top: 0; border-left: 0">
                <ul style="margin: 0; list-style: none; padding: 0">
                    <li>STK: 103868789731</li>
                    <li>Nguyễn Thị Thùy Dương</li>
                    <li>Chi nhánh Bắc Hà Nội, HN</li>
                </ul>
            </td>
        </tr>
    </table>

    <strong style="text-transform: uppercase; font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #f3a23b">
        LƯU Ý:
    </strong>

    <div style="color: #00354E; padding:10px; font-family: Arial, Helvetica, sans-serif; line-height: 1.6; font-size: 13px">
        <ul style="margin: 5px; padding: 0">
            <li>Thời gian hàng về Việt Nam: Dự kiến 2 – 3 tuần. Để chủ động theo dõi hành trình đơn hàng, Quý khách có
                thể truy cập <a href="https://basso.vn/tai-khoan/don-hang?id={{$order->id}}">tại đây</a>.
            </li>
            <li>
                Trường hợp Quý khách có những băn khoăn về đơn hàng, có thể xem ở mục các <a
                        href="https://basso.vn/tro-giup?id=27">câu hỏi thường gặp</a>.
            </li>
            <li>
                Quý khách cần được hỗ trợ ngay? Chỉ cần email <strong><a style="color: #00354E;"
                                                                         href="mailto:cskh@basso.vn">cskh@basso.vn</a></strong>,
                hoặc gọi số điện thoại <strong><a style="color: #00354E;" href="tel:0965687790">0965687790</a></strong>
                (từ
                9h - 18h, Thứ 2 - Chủ
                Nhật). Đội ngũ Basso luôn sẵn sàng hỗ trợ Quý
                khách bất kì lúc nào.
            </li>
        </ul>
    </div>

    <strong style="color: #00354E; margin:10px 0; font-family: Arial, Helvetica, sans-serif; line-height: 1.6; font-size: 13px">
        Basso.vn xin cảm ơn quý khách đã tin tưởng và rất hân hạnh được phục vụ Quý khách.
    </strong>

    <table style="width: 100%; margin-top: 20px" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td align="center" colspan="2"
                style="background-color: #00354E; padding:20px; color: #FFF; line-height: 1.6; font-family: Arial, Helvetica, sans-serif;">
                CÔNG TY TNHH THƯƠNG MẠI HOÀI ĐỨC<br/>
                Giấy phép kinh doanh số: 0309913816<br/>
                <a href="https://BASSO.VN" target="_blank" style="color: #FFF">BASSO.VN</a> – dịch vụ mua hàng quốc tế hàng đầu Việt Nam.
            </td>
        </tr>
        <tr>
            <td align="center" width="50%"
                style="background-color: #00354E; padding:20px; color: #FFF; line-height: 1.6; font-family: Arial, Helvetica, sans-serif;">
                <strong>Chi nhánh Hà Nội</strong><br/>
                17 Tố Hữu, P. Trung Văn, Q. Nam Từ Liêm
            </td>
            <td align="center" width="50%"
                style="background-color: #00354E; padding:20px; color: #FFF; line-height: 1.6; font-family: Arial, Helvetica, sans-serif;">
                <strong>Chi nhánh Hồ Chí Minh</strong><br/>
                60 Lê Trung Nghĩa, P. 12, Q. Tân Bình
            </td>
        </tr>
    </table>
</div>