function viewModel() {
    let self = this;
    self.result = ko.mapping.fromJS([]);
    self.pagination = ko.observable();

    self.filter = {
        time: ko.observable('all'),
        start: ko.observable(moment(new Date()).format('DD-MM-YYYY')),
        end: ko.observable(moment(new Date()).format('DD-MM-YYYY'))
    };

    self.search = function (page) {
        let data = {
            time: self.filter.time(),
            start: self.filter.start(),
            end: self.filter.end(),
            page: page
        };

        AJAX.get(window.location, data, true, (res) => {
                if (!res.error) {
                    ko.mapping.fromJS(res.data, self.result);
                    self.pagination(res.pagination);
                } else
                    ALERT.error(res.message);
            }
        );
    };
}

let model = new viewModel();
model.search(1);
ko.applyBindings(model, document.getElementById('main-content'));