<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="col-sm-12 col-xl-8 offset-xl-2">
        <div class="card card-default">
            <div class="card-header">
                <div class="card-title">
                    <a href="javascript:" class="float-right" data-bind="click: modal.show">
                        <i class="fa fa-plus"></i>
                        Thêm danh mục
                    </a>
                    Danh mục phụ thu
                </div>
            </div>
            <div class="card-body">

                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th width="60">STT</th>
                            <th>Tên danh mục</th>
                            <th>Trạng thái</th>
                            <th width="180">Thao tác</th>
                        </tr>
                        </thead>
                        <tbody data-bind="foreach: result" class="text-center">
                        <tr>
                            <th data-bind="text: $index() + 1"></th>
                            <td data-bind="text: name"></td>
                            <td class="text-center">
                                <span class="badge badge-success" data-bind="visible: active() == 1">Kích hoạt</span>
                                <span class="badge badge-danger" data-bind="visible: active() == 0">Không kích hoạt</span>
                            </td>
                            <td class="text-center">
                                <button class="btn btn-xs btn-link font-weight-bold" data-bind="click: $root.modal.edit">
                                    <i class="icon-pencil"></i>
                                    Cập nhật
                                </button>
                                <a href="#" class="text-danger font-weight-bold" data-bind="click: $root.delete">
                                    <i class="icon-trash"></i>
                                    Xóa
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                @include('pagination')
            </div>
        </div>
    </div>
@endsection
@section('modal')
    @include('partial/editor_modal')
@endsection
@section('script')
    <script src="{{load_js('index')}}"></script>
@endsection