<div style="max-width: 720px; margin: 0 auto">
    <?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
    <table style="width: 100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td align="left">
                <img src="https://basso.vn/theme/basso/statics/images/basso_logo.jpg"/>
            </td>
            <td align="center"
                style="color: #00354E; padding:20px; font-family: Arial, Helvetica, sans-serif; line-height: 1.6">
                <strong>CÔNG TY TNHH THƯƠNG MẠI HOÀI ĐỨC</strong><br/>
                <small> MST: 0309913816<br/>
                    Chi nhánh HN: 17 Tố Hữu, P. Trung Văn, Q. Nam Từ Liêm, HN<br/>
                    Chi nhánh SG: 60 Lê Trung Nghĩa, P12, Q. Tân Bình, TP. HCM<br/>
                    Hotline: 0965687790 - Website: www.basso.vn
                </small>
            </td>
        </tr>
    </table>

    <table style="width: 100%; margin: 0" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td align="center" style="color: #00354E">
                <strong style="text-transform: uppercase; font-family: Arial, Helvetica, sans-serif; font-size: 20px; line-height: 1.7">
                    HÓA ĐƠN ĐẶT HÀNG
                </strong>
            </td>
        </tr>
    </table>

    <table style="width: 100%; margin: 10px 0 20px" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="4" style="padding: 10px 0">
                <strong style="text-transform: uppercase; font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #f3a23b">THÔNG
                    TIN ĐƠN HÀNG</strong>
            </td>
        </tr>
        <tr>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE">
                <strong>Mã đơn hàng</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none">
                #{{$order->order_code}}
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none">
                <strong>Hình thức thanh toán</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none">
                {{$order->payment_method}}
            </td>
        </tr>
        <tr>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-top: none">
                <strong>Ngày đặt hàng</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                {{date('d/m/Y', time())}}
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none; border-top: none">
                <strong>Trạng thái thanh toán</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                Chưa đặt cọc
            </td>
        </tr>
        <tr>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-top: none">
                <strong>Tổng giá trị đơn hàng</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                {{format_money($order->sub_total)}}
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none; border-top: none">
                <strong>Số tiền đã thanh toán</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                {{format_money($order->total_paid)}}
            </td>
        </tr>
        <tr>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-top: none">
                <strong>Trạng thái đơn hàng</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                Đã tiếp nhận
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none; border-top: none">
                <strong>Số tiền còn lại</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                {{format_money($order->sub_total - $order->total_paid)}}
            </td>
        </tr>
    </table>

    <table style="width: 100%; margin: 20px 0" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="2" style="padding: 10px 0" width="50%">
                <strong style="text-transform: uppercase; font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #f3a23b">
                    THÔNG TIN ĐẶT HÀNG
                </strong>
            </td>
            <td colspan="2" style="padding: 10px 0" width="50%">
                <strong style="text-transform: uppercase; font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #f3a23b">
                    THÔNG TIN NHẬN HÀNG
                </strong>
            </td>
        </tr>
        <tr>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ">
                <strong>Họ tên</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ">
                {{$order->name}}
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ">
                <strong>Họ tên</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ">
                {{$order->name}}
            </td>
        </tr>
        <tr>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ; border-top: none">
                <strong>Email</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ; border-top: none">
                {{$order->email }}
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ; border-top: none">
                <strong>Email</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ; border-top: none">
                {{$order->email }}
            </td>
        </tr>
        <tr>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ; border-left: none">
                <strong>Điện thoại</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ">
                {{$order->phone}}
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ; border-left: none">
                <strong>Điện thoại</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ">
                {{$order->phone}}
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ; border-left: none; border-top: none">
                <strong>Địa chỉ</strong>
            </td>
            <td style="font-family: Arial, Helvetica, sans-serif; padding: 5px; color: #00354E ; border-top: none">
                {{$order->shipping_address}}, {{$order->district}}, {{$order->city}}
            </td>
        </tr>
    </table>

        <table style="width: 100%; margin: 20px 0" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="6" style="padding: 10px 0">
                    <strong style="text-transform: uppercase; font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #f3a23b">
                        CHI TIẾT ĐƠN HÀNG
                    </strong>
                </td>
            </tr>
            <tr>
                <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE">
                    <strong>STT</strong>
                </td>
                <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none">
                    <strong>Sản phẩm</strong>
                </td>
                <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none"
                    width="80">
                    <strong>Giá ngoại tệ</strong>
                </td>
                <td style="width: 90px; font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none"
                    align="center">
                    <strong>Đơn giá</strong>
                </td>
                <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none">
                    <strong>Số lượng</strong>
                </td>
                <td style="width: 90px; font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none">
                    <strong>Thành tiền</strong>
                </td>
            </tr>
            @for($i = 0; $i < count($items); $i++)
                <tr>
                    <?php $item = $items[$i]; ?>
                    <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-top: none"
                        align="center">
                        {{($i + 1)}}
                    </td>
                    <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                        {{$item->name}}
                    </td>
                    <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none"
                        align="center">
                        {{$order->currency_symbol}} {{$item->price}}
                    </td>
                    <td style="font-family: Arial, Helvetica, sans-serif; text-align: center; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none"
                        align="center">
                        {{format_money($order->currency_rate * $item->price + $item->term_fee + $item->weight * 260000)}}
                    </td>
                    <td style="font-family: Arial, Helvetica, sans-serif; text-align: center; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none"
                        align="center">
                        {{$item->quantity}}
                    </td>
                    <td style="font-family: Arial, Helvetica, sans-serif; text-align: center; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none"
                        align="center">
                        {{format_money(($order->currency_rate * $item->price + $item->term_fee + $item->weight * 260000) * $item->quantity)}}
                    </td>
                </tr>
            @endfor
            <tr>
                <td align="right" colspan="5"
                    style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-top: none">
                    <strong>Tổng tiền</strong>
                </td>
                <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none"
                    align="center">
                    <strong>{{format_money($order->sub_total + $order->discount_amount)}}</strong>
                </td>
            </tr>
            <tr>
                <td align="right" colspan="5"
                    style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-top: none">
                    <strong>Giảm giá</strong>
                </td>
                <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none"
                    align="center">
                    <strong>{{format_money($order->discount_amount)}}</strong>
                </td>
            </tr>
            <tr>
                <td align="right" colspan="5"
                    style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-top: none">
                    <strong>Phí giao hàng trong nước</strong>
                </td>
                <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none">
                    <strong>-</strong>
                </td>
            </tr>
            <tr>
                <td align="right" colspan="5"
                    style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-top: none">
                    <strong>Tổng giá trị đơn hàng tạm tính</strong>
                </td>
                <td style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; border-left: none; border-top: none"
                    align="center">
                    <strong>{{format_money($order->sub_total)}}</strong>
                </td>
            </tr>
        </table>

    <p style="color: #00354E; padding:10px; font-family: Arial, Helvetica, sans-serif; line-height: 1.6; font-size: 13px">
        <strong><i>(*) Phí vận chuyển quốc tế:</i></strong> Là phí vận chuyển từ Mỹ/Anh/Tây Ban Nha/… về Việt Nam. Phí
        ship được tính theo cân nặng thực tế khi hàng về Việt Nam, đơn giá <strong>260,000đ/kg</strong>
    </p>

    <strong style="text-transform: uppercase; font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #f3a23b">
        LƯU Ý:
    </strong>

    <div style="color: #00354E; padding:10px; font-family: Arial, Helvetica, sans-serif; line-height: 1.6; font-size: 13px">
        <ul style="margin: 5px; padding: 0">
            <li>
                Thời gian hàng về Việt Nam: Dự kiến 2 – 3 tuần.
            </li>
            <li>
                Basso chỉ cam kết mua đúng sản phẩm, mẫu mã, số lượng từ người bán theo yêu cầu của khách hàng, không
                cam kết chất lượng sản phẩm, không đổi trả lại sản phẩm sau khi đã mua hàng.
            </li>
            <li>
                Basso cam kết hoàn lại tiền đặt cọc trong trường hợp không mua được hàng hóa.
            </li>
        </ul>
    </div>

    <strong style="color: #00354E; margin:10px 0; font-family: Arial, Helvetica, sans-serif; line-height: 1.6; font-size: 13px">
        Basso.vn xin cảm ơn quý khách đã tin tưởng và rất hân hạnh được phục vụ Quý khách.
    </strong>

    <table style="width: 100%; margin: 20px 0 0; height: 140px" cellpadding="0" cellspacing="0">
        <tr>
            <td width="50%" valign="top" style="color: #00354E; font-family: Arial, Helvetica, sans-serif"
                align="center">
                KHÁCH HÀNG
            </td>
            <td width="50%" valign="top" style="color: #00354E; font-family: Arial, Helvetica, sans-serif"
                align="center">
                BÊN BÁN
            </td>
        </tr>
    </table>
</div>

<script>
    window.onload = function () {
        window.print();
    }
</script>