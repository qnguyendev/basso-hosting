<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-sm-12 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">
                        Danh sách ngân hàng
                        <a href="#" data-bind="click: modal.show" class="float-right">
                            <i class="fa fa-plus"></i>
                            Thêm ngân hàng
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Tên</th>
                            <th>Ghi chú</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody data-bind="foreach: result" class="text-center">
                        <tr>
                            <th data-bind="text: $index() + 1"></th>
                            <td data-bind="text: name"></td>
                            <td data-bind="text: note"></td>
                            <td>
                                <a href="#" data-bind="click: $root.modal.edit">
                                    <i class="icon-pencil"></i>
                                    Cập nhật
                                </a>
                                <a href="#" data-bind="click: $root.delete" class="text-danger">
                                    <i class="icon-trash"></i>
                                    Xóa
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modal')
    @include('partial/editor_modal')
@endsection

@section('script')
    <script src="{{load_js('index')}}"></script>
@endsection