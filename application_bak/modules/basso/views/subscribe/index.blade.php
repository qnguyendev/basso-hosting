<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-12 col-lg-10 offset-lg-1 col-xl-8 offset-xl-2">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        Email đăng ký nhận tin

                        <a href="#" class="float-right" data-bind="click: export_list">
                            <i class="fa fa-file-export"></i>
                            Xuất excel
                        </a>
                    </div>
                </div>
                <form autocomplete="off" class="card-body">
                    <div class="row">
                        <div class="col-md-6 form-group col-lg-3">
                            <label>Thời gian</label>
                            <select class="form-control" data-bind="value: filter.time">
                                <option value="today">Hôm nay</option>
                                <option value="week">Trong tuần</option>
                                <option value="month">Trong tháng</option>
                                <option value="all">Tất cả</option>
                                <option value="custom">Tùy chọn</option>
                            </select>
                        </div>
                        <div class="col-md-6 form-group col-lg-3">
                            <label>Từ ngày</label>
                            <input type="text" class="form-control"
                                   data-bind="datePicker: filter.start, enable: filter.time() == 'custom'"/>
                        </div>
                        <div class="col-md-6 form-group col-lg-3">
                            <label>đến ngày</label>
                            <input type="text" class="form-control"
                                   data-bind="datePicker: filter.end, enable: filter.time() == 'custom'"/>
                        </div>
                        <div class="col-md-6 form-group col-lg-3">
                            <label>&nbsp;</label>
                            <button class="btn btn-dark btn-block" data-bind="click: function(){ $root.search(1); }">
                                <i class="fa fa-search"></i>
                                THÔNG KÊ
                            </button>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Email</th>
                                <th>Thời gian</th>
                            </tr>
                            </thead>
                            <tbody data-bind="foreach: result" class="text-center">
                            <tr>
                                <th data-bind="text: $index() + 1"></th>
                                <td data-bind="text: email"></td>
                                <td data-bind="text: moment.unix(created_time()).format('DD/MM/YYYY HH:mm')"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    @include('pagination')
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{load_js('index')}}"></script>
@endsection