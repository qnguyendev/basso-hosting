ko.bindingHandlers.popover = {
    init: function (element) {
        let $element = $(element);
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            if ($element.data('bs.popover')) {
                try {
                    $element.popover('destroy');
                } catch (err) {
                }
            }
        });
    },
    update: function (element, valueAccessor) {
        let $element = $(element), value = ko.unwrap(valueAccessor());
        $element.popover({
            html: true,
            animation: true,
            placement: 'left',
            trigger: 'hover',
            content: function () {
                return $(value).find(".popover-body").html();
            },
            title: function () {
                return $(value).find(".popover-heading").html();
            }
        });
    }
};

function viewModel() {
    let self = this;
    self.result = ko.mapping.fromJS([]);
    self.pagination = ko.observable();
    self.current_tab = ko.observable($('.nav-tabs a:first').attr('rel'));

    self.filter = {
        warehouse_id: ko.observable(0),
        key: ko.observable()
    };

    self.set_current = function (t) {
        self.current_tab(t);
        self.search(1);
    };

    self.search = function (page) {
        let data = {
            page: page,
            action: self.current_tab(),
            warehouse_id: self.filter.warehouse_id(),
            key: self.filter.key()
        };

        AJAX.get(window.location, data, true, (res) => {
            if (res.error)
                NOTI.danger(res.message);
            else {
                ko.mapping.fromJS(res.data, self.result);
                /*self.result.each((x) => {
                    if (x.delivered_date() != null) {
                        if (x.delivered_date() != '') {
                            x.delivered_date(moment.unix(x.delivered_date()).format('DD-MM-YYYY'));
                        }
                    }
                });*/

                self.pagination(res.pagination);
            }
        });
    };

    self.update = function (item) {
        let data = {
            order_id: item.id(),
            note: item.note(),
            item_id: item.item_id(),
            delivered_date: item.delivered_date()
        };

        AJAX.post(window.location, data, true, (res) => {
            if (!res.error)
                NOTI.success(res.message);
            else
                NOTI.danger(res.message);
        });
    };
}

let model = new viewModel();
model.search(1);
ko.applyBindings(model, document.getElementById('main-content'));