<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">
                        Quản lý ngoại tệ
                        <a href="#" data-bind="click: modal.show" class="float-right">
                            <i class="fa fa-plus"></i>
                            Thêm ngoại tệ
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Tên</th>
                            <th>Biểu tượng/ký hiệu</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody data-bind="foreach: result" class="text-center">
                            <tr>
                                <th data-bind="text: $index() + 1"></th>
                                <td data-bind="text: name"></td>
                                <td data-bind="text: symbol"></td>
                                <td>
                                    <a href="#" class="text-primary text-bold" data-bind="click: $root.modal.edit">
                                        <i class="fa fa-edit"></i>
                                        Cập nhật
                                    </a>
                                    <a href="#" class="text-danger text-bold ml-2" data-bind="click: $root.delete">
                                        <i class="fa fa-trash"></i>
                                        Xóa
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{load_js('index')}}"></script>
@endsection

@section('modal')
    @include('partial/editor_modal')
@endsection