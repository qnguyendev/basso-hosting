<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<form class="form-group" autocomplete="off">
    <div class="input-group">
        <input type="text" class="form-control" data-bind="value: filter.key"
               placeholder="Tìm theo mã đơn lẻ, sđt, link web...">
        <div class="input-group-append" style="width: 200px">
            <select class="form-control no-radius select2" data-bind="value: filter.site"
                    style="border-radius: 0 !important">
                <option value="all">Tất cả site</option>
                <!--ko foreach: data.websites-->
                <option data-bind="value: name, text: name"></option>
                <!--/ko-->
            </select>
        </div>
        <div class="input-group-append">
            <button class="btn btn-primary btn-sm"
                    data-bind="click: filter.search">
                <i class="fa fa-search"></i>
                Tìm đơn
            </button>
        </div>
    </div>
</form>

<table class="table table-hover table-bordered">
    <thead>
    <tr>
        <th>Mã ĐH</th>
        <th>Chi nhánh</th>
        <th>Khách hàng</th>
        <th>Tổng tiền</th>
        <th>Ship web</th>
        <th>Website</th>
        <th>Nhân viên</th>
        <th width="180">Thao tác</th>
        <th></th>
    </tr>
    </thead>
    <tbody data-bind="foreach: search_result.orders" class="text-center">
    <tr>
        <th>
            <a href="javascript:" class="order-expand"
               data-bind="click: $root.search_result.toggle_row, text: order_code() == null ? id() : order_code()"></a>
        </th>
        <td>
        @foreach(Branch::LIST as $key=>$value)
            <!--ko if: branch() == '{{$key}}'-->
            {{$value}}
            <!--/ko-->
            @endforeach
        </td>
        <td data-bind="text: customer_id() == null ? name() : customer_name()"></td>
        <td data-bind="text: `${currency_symbol()} ${parseFloat(total()).toFixed(2)}`"></td>
        <td data-bind="text: `${currency_symbol()} ${parseFloat(web_shipping_fee()).toFixed(2)}`"></td>
        <td data-bind="text: website"></td>
        <td data-bind="text: user() == null ? 'Website' : user()"></td>
        <td class="text-center">
            <a href="javascript:" class="text-success font-weight-bold" data-bind="click: $root.search_result.add_all">
                <i class="fa fa-plus"></i>
                Thêm tất cả
            </a>
        </td>
        <td>
            <a href="#" target="_blank" data-bind="attr: {href: `/basso/customer_order/detail/${id()}/`}">
                Chi tiết
            </a>
        </td>
    </tr>
    <tr class="tr-collapse">
        <td colspan="9" class="pl-0 pr-0">
            <div class="item-detail">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th colspan="2">Sản phẩm</th>
                        <th>Variant</th>
                        <th>SL</th>
                        <th>Giá mua</th>
                        <th>Tổng giá mua</th>
                        <th>Ghi chú</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody class="text-center" data-bind="foreach: items">
                    <tr>
                        <td width="80">
                            <img data-bind="attr: {src: `/timthumb.php?src=${image_path()}&w=200&h=200`}"
                                 class="img-fluid ie-fix-flex"/>
                        </td>
                        <td>
                            <a href="#" data-bind="attr: {href: link}, text: name" target="_blank"> </a>
                        </td>
                        <td class="text-left">
                            <!--ko foreach: variations-->
                            <b data-bind="text: name"></b>: <span data-bind="text: value"></span><br/>
                            <!--/ko-->
                        </td>
                        <td data-bind="text: quantity"></td>
                        <td data-bind="text: `${$parent.currency_symbol()} ${price()}`"></td>
                        <td data-bind="text: `${$parent.currency_symbol()} ${(price() * quantity())}`"></td>
                        <td data-bind="text: note"></td>
                        <td>
                            <a href="javascript:" class="text-primary font-weight-bold"
                               data-bind="click: $root.search_result.add_item">
                                <i class="fa fa-plus"></i>
                                Thêm
                            </a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </td>
    </tr>
    </tbody>
</table>
