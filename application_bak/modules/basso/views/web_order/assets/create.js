function viewModel() {
    let self = this;
    self.filter = {
        key: ko.observable(),
        status: ko.observable('all'),
        site: ko.observable('all'),
        search: function (page) {
            let data = {
                key: self.filter.key(),
                status: self.filter.status(),
                site: self.filter.site(),
                action: 'search',
                page: page
            };
            ko.mapping.fromJS([], self.search_result.orders);

            AJAX.get(window.location, data, true, (res) => {
                if (res.error) NOTI.danger(res.message);
                else {
                    ko.mapping.fromJS(res.orders, self.search_result.orders);
                    ko.mapping.fromJS(res.items, self.search_result.items);

                    self.search_result.pagination(res.pagination);
                    self.search_result.orders.each((x) => {
                        let items = self.search_result.items.filter((t) => {
                            return t.order_id() == x.id();
                        });

                        x.items(items);
                    });
                }
            });
        }
    };

    self.search_result = {
        orders: ko.mapping.fromJS([]),
        pagination: ko.observable(),
        items: ko.mapping.fromJS([]),
        add_item: function (item) {
            self.search_result.orders.each((order) => {
                if (order.id() == item.order_id()) {
                    if (self.buy_list.is_valid(order.currency_symbol())) {
                        let exist_items = self.buy_list.items.filter((x) => {
                            return x.id() == item.id();
                        });

                        if (exist_items.length === 0) {
                            self.buy_list.items.push(item);
                            self.order_info.website(item.website());
                            self.order_info.calc_total();
                            NOTI.success('Đã thêm sản phẩm');
                        } else
                            NOTI.danger('Sản phẩm đã có trong danh sách');
                    } else {
                        NOTI.danger('Bạn chỉ được thêm sản phẩm cùng ngoại tệ');
                    }
                }
            });
        },
        add_all: function (order) {
            if (self.buy_list.is_valid(order.currency_symbol())) {
                let exist_items = 0;
                order.items.each((t) => {
                    exist_items = self.buy_list.items.filter((x) => {
                        return x.id() == t.id();
                    });

                    if (exist_items.length === 0) {
                        self.buy_list.items.push(t);
                        self.order_info.website(t.website());
                        self.order_info.calc_total();
                    }
                });

                NOTI.success(`Đã thêm ${order.items().length - exist_items} sản phẩm`);
            } else
                NOTI.danger('Bạn chỉ được thêm sản phẩm cùng ngoại tệ');
        },
        toggle_row: function (data, event) {
            toggle_row(event.currentTarget);
        }
    };

    self.buy_list = {
        items: ko.mapping.fromJS([]),
        delete: function (item) {
            self.buy_list.items.remove(item);
            self.order_info.calc_total();
        },
        is_valid: function (currency_symbol) {
            if (self.buy_list.items().length === 0) {
                self.order_info.currency_symbol(currency_symbol);
                return true;
            }

            return currency_symbol === self.order_info.currency_symbol();
        }
    };

    self.data = {
        countries: ko.mapping.fromJS([]),
        websites: ko.mapping.fromJS([]),
        payments: ko.mapping.fromJS([]),
        warehouses: ko.mapping.fromJS([]),
        init: function () {
            AJAX.get(window.location, {action: 'init'}, true, (res) => {
                ko.mapping.fromJS(res.websites, self.data.websites);
                ko.mapping.fromJS(res.countries, self.data.countries);
                ko.mapping.fromJS(res.payments, self.data.payments);
                ko.mapping.fromJS(res.warehouses, self.data.warehouses);

                if (res.countries.length > 0)
                    self.order_info.country_id(res.countries[0].id);


                self.order_info.errors.showAllMessages(false);
            });
        },
        change_country: function () {
            self.data.countries.each((x) => {
                if (x.id() == self.order_info.country_id()) {
                    self.order_info.currency_symbol(x.currency_symbol());
                }
            });
        },
        change_payment: function () {
            self.data.payments.each((x) => {
                if (x.id() == self.order_info.payment_id()) {
                    self.order_info.buy_rate(moneyFormat(x.currency_rate()));
                }
            });

            self.order_info.calc_total();
        }
    };

    self.order_info = {
        created_time: ko.observable(moment(new Date()).format('DD-MM-YYYY')).extend({required: {message: LABEL.required}}),
        branch: ko.observable().extend({required: {message: LABEL.required}}),
        country_id: ko.observable().extend({required: {message: LABEL.required}}),
        order_number: ko.observable().extend({required: {message: LABEL.required}}),
        total: ko.observable(0).extend({required: {message: LABEL.required}}),
        ship_fee: ko.observable(0).extend({required: {message: LABEL.required}}),
        sub_total: ko.observable(0).extend({required: {message: LABEL.required}}),
        buy_rate: ko.observable(0).extend({required: {message: LABEL.required}}),
        total_paid: ko.observable(0).extend({required: {message: LABEL.required}}),
        warehouse_id: ko.observable().extend({required: {message: LABEL.required}}),
        create_billing: ko.observable(true),
        note: ko.observable(),
        website: ko.observable(),
        currency_symbol: ko.observable(),
        buyer_id: ko.observable().extend({required: {message: LABEL.required}}),
        payment_id: ko.observable().extend({required: {message: LABEL.required}}),
        calc_total: function () {
            let total = 0, sub_total, total_paid;
            self.buy_list.items.each((x) => {
                total += parseFloat(x.price()) * parseInt(x.quantity());
            });

            total = parseFloat(total.toFixed(2));
            sub_total = total + parseFloat(self.order_info.ship_fee());
            total_paid = parseFloat(sub_total) * toNumber(self.order_info.buy_rate()).toFixed(2);
            self.order_info.total(total);
            self.order_info.sub_total(sub_total);
            self.order_info.total_paid(moneyFormat(total_paid));
        },
        save: function () {
            if (!self.order_info.isValid())
                self.order_info.errors.showAllMessages();
            else if (self.buy_list.items().length === 0)
                NOTI.danger('Bạn chưa chọn sản phẩm');
            else {
                ALERT.confirm('Xác nhận tạo đơn hàng', 'Các sản phẩm sẽ được đánh dấu là đã mua', () => {
                    let items = [];
                    self.buy_list.items.each((x) => {
                        items.push({
                            order_id: x.order_id(),
                            id: x.id(),
                            name: x.name(),
                            note: x.note(),
                            link: x.link(),
                            quantity: x.quantity(),
                            price: x.price(),
                            variations: ko.toJSON(x.variations())
                        });
                    });

                    let data = {
                        items: items,
                        created_time: self.order_info.created_time(),
                        create_billing: self.order_info.create_billing(),
                        order_number: self.order_info.order_number(),
                        note: self.order_info.note(),
                        branch: self.order_info.branch(),
                        country_id: self.order_info.country_id(),
                        buyer_id: self.order_info.buyer_id(),
                        ship_fee: self.order_info.ship_fee(),
                        buy_rate: toNumber(self.order_info.buy_rate()),
                        website: self.order_info.website(),
                        warehouse_id: self.order_info.warehouse_id(),
                        payment_id: self.order_info.payment_id()
                    };

                    AJAX.post(window.location, data, true, (res) => {
                        if (res.error) {
                            ALERT.error(res.message);
                        } else {
                            if (res.redirect_url) {
                                window.location = res.redirect_url;
                            }
                        }
                    });
                });
            }
        }
    };

    self.export = function () {

        window.location = `/basso/web_order/export/?key=${self.filter.key()}&site=${self.filter.site()}&type=create`;
    };
}

let model = new viewModel();
model.data.init();
model.filter.search(1);
ko.validatedObservable(model.order_info);
ko.applyBindings(model, document.getElementById('main-content'));