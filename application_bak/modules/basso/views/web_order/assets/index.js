function viewModel() {
    let self = this;
    self.filter = {
        status: ko.observable('all'),
        branch: ko.observable('all'),
        key: ko.observable(),
        date: ko.observable(),
        payment_id: ko.observable(0),
        buyer_id: ko.observable(0),
        warehouse_id: ko.observable(0)
    };

    self.data = {
        buyers: ko.mapping.fromJS([]),
        warehouses: ko.mapping.fromJS([]),
        payments: ko.mapping.fromJS([]),
        init: function () {
            AJAX.get(window.location, {action: 'init'}, true, (res) => {
                ko.mapping.fromJS(res.buyers, self.data.buyers);
                ko.mapping.fromJS(res.warehouses, self.data.warehouses);
                ko.mapping.fromJS(res.payments, self.data.payments);

                if (localStorage.getItem('prev_url') != null) {
                    if (localStorage.getItem('prev_url') == 'web_order_detail') {
                        if (localStorage.getItem('search_data') != null) {
                            var search_data = JSON.parse(localStorage.getItem('search_data'));
                            if (search_data.branch != null)
                                self.filter.branch(search_data.branch);
                            if (search_data.status != null)
                                self.filter.status(search_data.status);
                            if (search_data.buyer_id != null)
                                self.filter.buyer_id(search_data.buyer_id);
                            if (search_data.buyer_id != null)
                                self.filter.buyer_id(parseInt(search_data.buyer_id));
                            if (search_data.key != null)
                                self.filter.key(search_data.key);
                            if (search_data.payment_id != null)
                                self.filter.payment_id(search_data.payment_id);
                            if (search_data.date != null)
                                self.filter.date(search_data.date);

                            localStorage.removeItem('search_data');
                        }

                        localStorage.removeItem('prev_url');
                    }
                }

                self.search(1);
            });
        }
    };

    self.result = ko.mapping.fromJS([]);
    self.pagination = ko.observable();
    self.items = ko.mapping.fromJS([]);
    self.tracking_code = ko.observable();

    self.search = function (page) {
        let data = {
            action: 'search',
            page: page,
            branch: self.filter.branch(),
            status: self.filter.status(),
            key: self.filter.key(),
            buyer_id: self.filter.buyer_id(),
            warehouse_id: self.filter.warehouse_id(),
            payment_id: self.filter.payment_id(),
            date: self.filter.date()
        };

        localStorage.setItem('search_data', ko.toJSON(data));
        AJAX.get(window.location, data, true, (res) => {
            if (res.error) {
                NOTI.danger(res.message);
                ko.mapping.fromJS([], self.result);
                ko.mapping.fromJS([], self.items);
            } else {
                ko.mapping.fromJS(res.orders, self.result);
                ko.mapping.fromJS(res.items, self.items);
                self.pagination(res.pagination);

                self.result.each((x) => {
                    let items = self.items.filter((t) => {
                        return t.web_order_id() == x.id();
                    });
                    x.items(items);
                });
            }
        });
    };

    self.update_item = function (item) {
        let data = {
            action: 'update-item',
            id: item.id(),
            order_id: item.web_order_id(),
            tracking_code: item.tracking_code(),
            delivered_date: item.delivered_date()
        };
        AJAX.post(window.location, data, true, (res) => {
            if (res.error)
                ALERT.error(res.message);
            else {
                NOTI.success(res.message);
            }
        });
    };

    //  Xuất mã tracking của các đơn hàng đã mua chưa hoàn thành
    self.export_tracking = function () {
        AJAX.get(window.location, {action: 'tracking'}, true, (res) => {
            self.tracking_code(res.data.join('\n'));
            $('#tracking-code-modal textarea').linenumbers({
                col_width: '25px',
                start: 1,
                digits: 4

            });

            MODAL.show('#tracking-code-modal');
            $('#tracking-code-modal textarea').focus();
        });
    };

    self.update_tracking = {
        data: ko.observable(),
        show: function () {
            MODAL.show('#update-tracking-modal');
        },
        post: function () {
            if (self.update_tracking.data() == undefined) {
                NOTI.danger('Vui lòng nhập thông tin tracking');
                return;
            }

            let data = self.update_tracking.data().split('\n');
            if (data.length === 0)
                NOTI.danger('Vui lòng nhập thông tin tracking');
            else {
                let valid_data = [];
                for (let i = 0; i < data.length; i++) {
                    let temp = data[i].split('\t');
                    if (temp.length >= 5) {
                        if (temp[4].toLocaleLowerCase() == 'delivered') {
                            let tracking = temp[0];
                            let date = temp[3].split(',')[0];
                            valid_data.push({tracking: tracking, date: date});
                        }
                    }
                }

                if (valid_data.length > 0) {
                    AJAX.post(window.location, {action: 'update-tracking', data: valid_data}, true, (res) => {
                        if (res.error)
                            ALERT.error(res.message);
                        else {
                            self.result.each(function (t) {
                                t.items.each(function (x) {
                                    for (let i = 0; i < valid_data.length; i++) {
                                        if (valid_data[i].tracking == x.tracking_code()) {
                                            x.delivered_date(moment(valid_data[i].date).format('X'));
                                        }
                                    }
                                });
                            });
                            MODAL.hide('#update-tracking-modal');
                            ALERT.success(res.message);
                        }
                    });
                } else {
                    ALERT.error('Không có tracking nào đã được giao');
                }
            }
        }
    };

    self.toggle_row = function (data, event) {
        toggle_row(event.currentTarget);
    };

    self.export = function () {
        let data = {
            branch: self.filter.branch(),
            status: self.filter.status(),
            key: self.filter.key(),
            buyer_id: self.filter.buyer_id(),
            warehouse_id: self.filter.warehouse_id(),
            payment_id: self.filter.payment_id(),
            date: self.filter.date(),
            type: 'index'
        };

        window.location = '/basso/web_order/export?' + $.param(data);
    };
}

let model = new viewModel();
model.data.init();
ko.applyBindings(model, document.getElementById('main-content'));