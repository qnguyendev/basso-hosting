<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="card card-default">
        <div class="card-header">
            <div class="card-title">
                Tạo đơn mua hàng website
                <button class="float-right btn btn-success btn-xs" data-bind="click: $root.export">
                    <i class="fa fa-file-export"></i>
                    Xuất Excel
                </button>
            </div>
        </div>
        <div class="card-body">
            <div role="tabpanel">
                <!-- Nav tabs-->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" href="#paymenthistory" aria-controls="orderlabel" role="tab"
                           data-toggle="tab">
                            Cần mua
                        </a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" href="#paymentadd" aria-controls="paymentadd" role="tab" data-toggle="tab">
                            Đang mua
                        </a>
                    </li>
                </ul>
                <!-- Tab panes-->
                <div class="tab-content">
                    <div class="tab-pane active" id="paymenthistory" role="tabpanel">
                        @include('partial/create/filter_tab')
                        @include('partial/create/pagination')
                    </div>
                    <div class="tab-pane" id="paymentadd" role="tabpanel">
                        @include('partial/create/result_tab')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{load_js('create')}}"></script>
@endsection