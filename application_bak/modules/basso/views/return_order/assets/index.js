function viewModel() {
    let self = this;
    let date = new Date();
    date.setDate(date.getDate() - 14);

    self.result = ko.mapping.fromJS([]);
    self.pagination = ko.observable();

    self.filter = {
        time: ko.observable('all'),
        status: ko.observable('all'),
        start: ko.observable(moment(date).format('DD-MM-YYYY')),
        end: ko.observable(moment(new Date()).format('DD-MM-YYYY')),
        key: ko.observable()
    };

    self.modal = {
        filter: {
            start: ko.observable(moment(date).format('DD-MM-YYYY')),
            end: ko.observable(moment(new Date()).format('DD-MM-YYYY')),
            key: ko.observable()
        },
        result: ko.mapping.fromJS([]),
        pagination: ko.observable(),
        show: function () {
            self.modal.filter.key(undefined);
            self.modal.search(1);
            MODAL.show('#orders-modal');
        },
        search: function (page) {
            let data = {
                start: self.modal.filter.start(),
                end: self.modal.filter.end(),
                key: self.modal.filter.key(),
                page: page,
                action: 'modal-search'
            };

            AJAX.get(window.location, data, true, (res) => {
                if (!res.error) {
                    ko.mapping.fromJS(res.data, self.modal.result);
                    self.modal.pagination(res.pagination);
                } else
                    NOTI.danger(res.message);
            });
        },
        select: function (item) {
            window.location = `/basso/return_order/create/${item.id()}`;
        }
    };

    self.search = function (page) {
        let data = {
            page: page,
            action: 'search',
            time: self.filter.time(),
            start: self.filter.start(),
            end: self.filter.end(),
            key: self.filter.key(),
            status: self.filter.status()
        };

        AJAX.get(window.location, data, true, (res) => {
            ko.mapping.fromJS(res.data, self.result);
            self.pagination(res.pagination);
        });
    };

    self.toggle_row = function (data, event) {
        toggle_row(event.currentTarget);
    };
}

let model = new viewModel();
model.search(1);
ko.applyBindings(model, document.getElementById('main-content'));