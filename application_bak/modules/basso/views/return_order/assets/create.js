function viewModel() {
    let self = this;
    self.items = ko.mapping.fromJS(items);

    self.selected_items = ko.observableArray([]);
    self.note = ko.observable();
    self.summary = {
        total: ko.observable(),
        quantity: ko.observable(0)
    };

    //  Thanh toán
    self.payment = {
        created_time: ko.observable(moment(new Date()).format('DD-MM-YYYY HH:mm')),
        amount: ko.observable().extend({required: {message: LABEL.required}}),
        note: ko.observable().extend({required: {message: LABEL.required}}),
        type: ko.observable($('select[name=payment_type] option:first').val()),
        bank_id: ko.observable(0)
    };

    self.check_all = function (data, event) {
        if ($(event.currentTarget).is(':checked')) {
            self.items.each((x) => {
                if (!self.selected_items().includes(x.id()))
                    self.selected_items.push(x.id());
            });
        } else {
            self.selected_items([]);
        }

        self.calc();
    };

    self.calc = function () {
        let total = 0, total_qty = 0;
        self.items.each(function (x) {
            if (self.selected_items().includes(x.id())) {
                x.price(toNumber(x.formatted_price()));
                total_qty += parseInt(x.quantity());
                total += parseInt(x.quantity()) * parseInt(x.price());
            }
        });

        self.summary.total(total);
        self.summary.quantity(total_qty);
        self.payment.amount(moneyFormat(total));
    };

    self.save = function () {
        if (self.selected_items().length == 0 || self.summary.quantity() <= 0)
            NOTI.danger('Bạn chưa chọn sản phẩm');
        else {
            ALERT.confirm('Xác nhận tạo đơn hoàn trả', null, () => {
                let data = {
                    items: [],
                    note: self.note(),
                    payment: {
                        time: self.payment.created_time(),
                        amount: self.payment.amount(),
                        note: self.payment.note(),
                        type: self.payment.type(),
                        bank_id: self.payment.bank_id()
                    }
                };

                if (self.payment.amount() != undefined) {
                    if (self.payment.amount() != null) {
                        data.payment.amount = toNumber(self.payment.amount());
                    }
                }

                self.items.each((x) => {
                    if (self.selected_items().includes(x.id())) {
                        data.items.push({
                            item_id: x.id(),
                            quantity: x.quantity(),
                            price: x.price()
                        });
                    }
                });

                AJAX.post(window.location, data, true, (res) => {
                    if (res.error)
                        ALERT.error(res.message);
                    else {
                        window.location = res.redirect_url;
                    }
                });
            });
        }
    };

    self.init = function () {
        self.items.each((x) => {
            x.formatted_price(moneyFormat(x.price()));
        });
    };
}

let model = new viewModel();
model.init();
ko.applyBindings(model, document.getElementById('main-content'));