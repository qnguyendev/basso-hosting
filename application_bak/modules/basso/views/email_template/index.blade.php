<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-12 col-lg-10 offset-lg-1 col-xl-8 offset-xl-2">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">
                        Email template

                        <a href="javascript:" class="text-primary float-right" data-bind="click: save">
                            <i class="fa fa-check"></i>
                            Cập nhật
                        </a>
                    </div>
                </div>

                <div class="card-body">
                    <div class="form-group">
                        <label>Chọn template email</label>
                        <select class="form-control" data-bind="value: prefix, event: {change: change_prefix}">
                            <option value="email_confirm">Xác nhận đơn hàng</option>
                            <option value="email_received">Báo hàng về</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Trước thông tin đơn hàng</label>
                        <textarea data-bind="ckeditor: before_order" id="before_order"></textarea>
                    </div>

                    <div class="form-group">
                        <label>Sau thông tin đơn hàng</label>
                        <textarea data-bind="ckeditor: after_order" id="after_order"></textarea>
                    </div>

                    <div class="form-group">
                        <label>Trước footer</label>
                        <textarea data-bind="ckeditor: before_footer" id="before_footer"></textarea>
                    </div>

                    <div class="form-group">
                        <label>Footer</label>
                        <textarea data-bind="ckeditor: footer" id="footer"></textarea>
                    </div>

                    <div class="form-group text-center">
                        <button class="btn btn-primary" data-bind="click: save">
                            <i class="fa fa-check"></i>
                            Cập nhật
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    {{ckeditor()}}
    <script src="{{load_js('index')}}"></script>
@endsection