function viewModel() {
    let self = this;
    self.prefix = ko.observable('email_confirm');
    self.before_order = ko.observable();
    self.after_order = ko.observable();
    self.before_footer = ko.observable();
    self.footer = ko.observable();

    self.change_prefix = function () {
        AJAX.get(window.location, {prefix: self.prefix()}, true, function (res) {
            if (!res.error) {
                self.before_order(res.before_order == null ? '' : res.before_order);
                self.after_order(res.after_order);
                self.before_footer(res.before_footer);
                self.footer(res.footer);
            }
        });
    };

    self.save = function () {
        let data = {
            prefix: self.prefix(),
            before_order: self.before_order(),
            after_order: self.after_order(),
            before_footer: self.before_footer(),
            footer: self.footer()
        };

        AJAX.post(window.local, data, true, function (res) {
            if (!res.error)
                NOTI.success('Cập nhật thành công');
        });
    };
}

let model = new viewModel();
model.change_prefix();
ko.applyBindings(model, document.getElementById('main-content'));