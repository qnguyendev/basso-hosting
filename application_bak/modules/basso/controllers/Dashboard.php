<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Home
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Dashboard_model $dashboard_model
 * @property CI_Form_validation $form_validation
 * @property Order_term_model $order_term_model
 * @property Customer_order_model $customer_order_model
 * @property Web_order_model $web_order_model
 * @property Common_model $common_model
 */
class Dashboard extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('dashboard_model');
        $this->load->model('customer_order_model');
    }

    public function index()
    {
        $this->blade->set('breadcrumbs', [['text' => 'Tổng quan']]);
        global $user_roles;
        if (in_array('editor', $user_roles) && count($user_roles) == 1)
            return $this->blade->render('editor');

        return $this->blade->render();
    }

    public function GET_index()
    {
        $this->form_validation->set_data($this->input->get());
        $this->form_validation->set_rules('time', null, 'required');
        $this->form_validation->set_rules('start', null, 'required');
        $this->form_validation->set_rules('end', null, 'required');

        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $time = $this->input->get('time');
        $from = $this->input->get('start');
        $to = $this->input->get('end');

        switch ($time) {
            case 'week':
                $day = strtotime('this week', time());
                $start = strtotime(date('Y-m-d 00:00:00', $day));
                $end = time();
                break;
            case 'month':
                $start = strtotime(date('Y-m-1 00:00:00'));
                $end = time();
                break;
            case 'custom':
                $start = strtotime("$from 00:00:00");
                $end = strtotime("$to 23:59:59");
                break;
            case 'all':
                $start = strtotime('2018-10-10');
                $end = time();
                break;
            default:
                $start = strtotime(date('Y-m-d 00:00:00'));
                $end = time();
                break;
        }

        $payments = $this->dashboard_model->revenue($start, $end);
        $charts = [];
        for ($i = $start; $i <= $end; $i += 24 * 3600) {
            $_payments = array_filter($payments, function ($item) use ($i) {
                return $item->created_time >= $i && $item->created_time <= $i + 24 * 3600 - 1;
            });

            if (array_sum(array_column($_payments, 'amount')) == 0)
                continue;

            array_push($charts, [
                'date' => date('d/m/y', $i),
                'total' => array_sum(array_column($_payments, 'amount'))
            ]);
        }

        json_success(null, [
            'payments' => array_column($charts, 'total'),
            'dates' => array_column($charts, 'date'),
            'total_orders' => format_number($this->dashboard_model->total_orders($start, $end), 0),
            'revenue' => format_money(array_sum(array_column($payments, 'amount'))),
            'total_customers' => format_number($this->dashboard_model->total_customers($start, $end)),
            'new_customers' => format_number($this->dashboard_model->new_customers($start, $end))
        ]);
    }

    public function test()
    {
        $shipped_order = $this->customer_order_model->get_shipped_order(597);
        var_dump($shipped_order);
    }
}