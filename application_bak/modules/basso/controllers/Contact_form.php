<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Contact_form
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Option_model $option_model
 * @property Image_model $image_model
 * @property Contact_form_model $contact_form_model
 */
class Contact_form extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('contact_form_model');
    }

    public function index()
    {
        $this->blade->set('breadcrumbs', [['text' => 'Danh sách liên hệ']]);
        return $this->blade->render();
    }

    public function GET_index()
    {
        $this->form_validation->set_data($this->input->get());
        $this->form_validation->set_rules('action', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $action = $this->input->get('action');
        switch ($action) {
            case 'search':
                $this->form_validation->set_rules('page', null, 'required|numeric');
                if (!$this->form_validation->run())
                    json_error('Yêu cầu không hợp lệ');

                $page = $this->input->get('page');
                $key = $this->input->get('key');

                $result = $this->contact_form_model->get($page, $key);
                $total_item = $this->contact_form_model->count($key);

                json_success(null, ['data' => $result, 'pagination' => Pagination::calc($total_item, $page)]);
                break;

            case 'contact_info':
                $contact_info = null;
                $config = $this->option_model->get('contact_info');
                if ($config != null)
                    $contact_info = $config->value;

                json_success(null, ['data' => $contact_info]);
                break;
        }
    }

    public function POST_index()
    {
        $this->form_validation->set_rules('action', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $action = $this->input->post('action');
        switch ($action) {
            case 'update':
                $this->form_validation->set_rules('id', null, 'required|numeric');
                if (!$this->form_validation->run())
                    json_error('Yêu cầu không hợp lệ');

                $id = $this->input->post('id');
                $note = $this->input->post('note');
                $this->contact_form_model->update_note($id, $note);

                json_success(null);
                break;

            case 'contact_info':
                $data = $this->input->post('data');
                if (is_enull($data, null) == null)
                    json_error('Vui lòng nhập thông tin liên hệ');

                $this->option_model->save('contact_info', $data);
                json_success(null);
                break;
        }
    }
}