<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Inventory
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Option_model $option_model
 * @property Image_model $image_model
 * @property Country_model $country_model
 * @property Warehouse_model $warehouse_model
 * @property Inventory_model $inventory_model
 * @property Customer_order_model $customer_order_model
 */
class Inventory extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('country_model');
        $this->load->model('warehouse_model');
        $this->load->model('customer_order_model');
        $this->load->model('inventory_model');
    }

    public function index()
    {
        $this->blade->set('breadcrumbs', [['text' => 'Quản lý kho']]);
        $this->blade->set('warehouses', $this->warehouse_model->get());
        $this->blade->set('image_uploader', true);

        return $this->blade->render();
    }

    public function GET_index()
    {
        $this->form_validation->set_data($this->input->get());
        $this->form_validation->set_rules('page', null, 'required|numeric');
        $this->form_validation->set_rules('warehouse_id', null, 'required|numeric');
        $this->form_validation->set_rules('time', null, 'required');
        $this->form_validation->set_rules('date', null, 'required');
        $this->form_validation->set_rules('status', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $page = $this->input->get('page');
        $time = $this->input->get('time');
        $date = $this->input->get('date');
        $warehouse_id = $this->input->get('warehouse_id');
        $status = $this->input->get('status');
        $key = $this->input->get('key');
        $start = strtotime("$date 00:00:00");
        $end = strtotime("$date 23:59:59");

        switch ($time) {
            case 'all':
                $end = 0;
                $start = 0;
                break;
            case 'week':
                $day = strtotime('this week', time());
                $start = strtotime(date('Y-m-d 00:00:00', $day));
                $end = time();
                break;
            case 'month':
                $start = date('Y-m-1 00:00:00');
                $end = time();
                break;
        }

        $result = $this->inventory_model->get($page, $status, $warehouse_id, $start, $end, $key);
        if (count($result) == 0) {
            json_error(null, [
                'pagination' => Pagination::calc(0, 1),
                'data' => [],
                'items' => []
            ]);
        }

        for ($i = 0; $i < count($result); $i++) {
            $customer_order_id = array_unique(explode(',', $result[$i]->customer_order_id));
            $order_code = array_unique(explode(',', $result[$i]->order_code));

            unset($result[$i]->order_code);
            unset($result[$i]->customer_order_id);

            $result[$i]->customer_orders = [];
            for ($j = 0; $j < count($customer_order_id); $j++) {
                $_item = [
                    'order_id' => null,
                    'order_code' => null
                ];

                if (!empty($customer_order_id[$j])) {
                    $_item['order_id'] = $customer_order_id[$j];
                    if (isset($order_code[$j])) {
                        if (!empty($order_code[$j])) {
                            $_item['order_code'] = $order_code[$j];
                        }
                    }

                    array_push($result[$i]->customer_orders, $_item);
                }
            }

            $result[$i]->items = [];
        }

        $total_item = $this->inventory_model->count($status, $warehouse_id, $start, $end, $key);
        $pagination = Pagination::calc($total_item, $page);
        $cus_order_items = $this->inventory_model->get_customer_order_items(array_column($result, 'id'));
        for ($i = 0; $i < count($cus_order_items); $i++) {
            $cus_order_items[$i]->variations = json_decode($cus_order_items[$i]->variations);
        }

        json_success(null,
            [
                'data' => $result,
                'pagination' => $pagination,
                'items' => $cus_order_items
            ]);
    }

    public function POST_index()
    {
        $this->form_validation->set_rules('action', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        switch ($this->input->post('action')) {
            case 'search-tracking':
                $this->_search_tracking();
                break;

            case 'save-package':
                $this->_save_package();
                break;

            case 'update-package':
                $this->_update_package();
                break;

            case 'update-item-weight':
                $this->_update_item_weight();
                break;

            case 'update-lost-status':
                $id = $this->input->post('id');
                $package = $this->inventory_model->get_by_id($id);
                $data = [
                    'lost_pending' => $package->lost_pending == 1 ? 0 : 1
                ];

                $this->inventory_model->update_package($id, $data);
                json_success(null);
                break;

            default:
                json_error('Yêu cầu không hợp lệ');
                break;
        }
    }

    /**
     * Cập nhật cân nặng của sản phẩm
     */
    private function _update_item_weight()
    {
        $this->form_validation->set_rules('item_id', null, 'required|numeric');
        $this->form_validation->set_rules('order_id', null, 'required|numeric');
        $this->form_validation->set_rules('package_id', null, 'required|numeric');
        $this->form_validation->set_rules('weight', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $order_id = $this->input->post('order_id');
        $package_id = $this->input->post('package_id');
        $item_id = $this->input->post('item_id');
        $weight = $this->input->post('weight');

        $order = $this->customer_order_model->get_by_id($order_id);
        if ($order == null)
            json_error('Đơn hàng không tồn tại');

        //  Cập nhật cân nặng của sp
        if ($this->customer_order_model->update_item($order_id, $item_id, ['weight' => floatval($weight)])) {
            $this->customer_order_model->update_order_total($order_id);
            $this->inventory_model->update_package_total_weight($package_id);

            if (floatval($weight) > 0) {
                $this->inventory_model->update_package($package_id, ['status' => InventoryItemStatus::FOUND]);
            } else
                $this->inventory_model->update_package($package_id, ['status' => InventoryItemStatus::LOST]);

            json_success('Cập nhật thành công');
        }

        json_error('Có lỗi, vui lòng F5 thử lại');
    }

    private function _update_package()
    {
        $this->form_validation->set_rules('id', null, 'required|numeric');
        $this->form_validation->set_rules('status', null, 'required');
        $this->form_validation->set_rules('total_weight', null, 'required');
        $this->form_validation->set_rules('name', null, 'required');
        $this->form_validation->set_rules('real_tracking', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $id = $this->input->post('id');
        $status = $this->input->post('status');
        $total_weight = is_enull($this->input->post('total_weight'), 0);
        $real_tracking = is_enull($this->input->post('real_tracking'), null);
        $web_tracking = is_enull($this->input->post('web_tracking'), null);
        $lost_pending = is_enull($this->input->post('lost_pending'), 0, 'numeric');
        $name = is_enull($this->input->post('name'), null);
        $branch = $this->input->post('branch');
        $branch = in_array($branch, array_keys(Branch::LIST)) ? $branch : null;

        if (!in_array($status, array_keys(InventoryItemStatus::LIST)) || !in_array($lost_pending, [0, 1]))
            json_error('Yêu cầu không hợp lệ');

        $package = $this->inventory_model->get_by_real_tracking($real_tracking);
        if ($package != null) {
            if ($package->id != $id)
                json_error('Tracking đã được nhập kho');
        }

        $update_data = [
            'web_tracking' => $web_tracking,
            'real_tracking' => $real_tracking,
            'total_weight' => $total_weight,
            'lost_pending' => $lost_pending,
            'name' => $name,
            'branch' => $branch
        ];

        $customer_items = $this->inventory_model->search_by_tracking($real_tracking);
        if (count($customer_items) > 0) {
            $package = $this->inventory_model->get_by_id($id);
            if ($package != null) {
                $this->inventory_model->delete_package($package->id);
            }
        }
        $_customer_items = [];
        foreach ($customer_items as $item) {
            array_push($_customer_items, [
                'order_id' => intval($item->order_id),
                'item_id' => intval($item->id)
            ]);
        }

        if ($package->status == InventoryItemStatus::NOT_FOUND) {
            if (is_enull($web_tracking, null) != null)
                $update_data['status'] = InventoryItemStatus::FOUND;
        } else if ($package->status == InventoryItemStatus::LOST) {
            if (count($_customer_items) > 0) {
                if ($update_data['total_weight'] == 0)
                    $update_data['status'] = InventoryItemStatus::LOST;
                elseif ($update_data['total_weight'] >= 0)
                    $update_data['status'] = InventoryItemStatus::FOUND;
            }
        }

        if ($this->inventory_model->update_package($id, $update_data, $_customer_items)) {
            foreach ($_customer_items as $t)
                $this->customer_order_model->update_order_total($t['order_id']);

            json_success('Cập nhật thành công');
        }

        json_error('Có lỗi, vui lòng F5 thử lại');
    }

    /**
     * Tìm sản phẩm theo mã tracking
     */
    private function _search_tracking()
    {
        $data = $this->input->post('data');
        if ($data == null)
            json_error('Yêu cầu không hợp lệ');

        $items = [];
        $result = $this->inventory_model->search_by_tracking(array_unique(array_column($data, 'tracking')));
        $exist_tracking = $this->inventory_model->get_by_real_tracking(array_unique(array_column($data, 'tracking')));

        foreach (array_unique(array_column($data, 'tracking')) as $t) {

            //  Nếu tracking đã nhập kho -> bỏ qua
            if (in_array($t, array_column($exist_tracking, 'real_tracking'))) {
                continue;
            }

            $item = [
                'web_tracking' => null,
                'real_tracking' => $t,
                'name' => null,
                'total_weight' => 0,
                'delivered_date' => date('d-m-Y'),
                'status' => InventoryItemStatus::NOT_FOUND,
                'quantity' => 0,
                'lost_pending' => 1,
                'customer_items' => [],
                'customer_order_id' => null,
                'branch' => null,
                'order_code' => null
            ];

            //  Cân nặng
            $index = array_search($t, array_column($data, 'tracking'));
            if ($index >= 0) {
                $item['weight'] = floatval($data[$index]['weight']);
                if (floatval($item['weight']) == 0)
                    $item['status'] = InventoryItemStatus::NOT_FOUND;
            }

            if (count($result) > 0) {
                $found_items = [];
                foreach ($result as $_item) {
                    if ($_item->tracking_code != null) {
                        if (!empty($_item->tracking_code))
                            if (strpos($_item->tracking_code, $t) !== false)
                                array_push($found_items, $_item);
                    }
                }

                //  Tìm thấy sp trong đơn của khách hàng
                if (count($found_items) > 0) {
                    foreach (array_unique(array_column($found_items, 'order_id')) as $order_id) {
                        $_found_items = array_filter($found_items, function ($t) use ($order_id) {
                            return $t->order_id == $order_id;
                        });

                        foreach ($_found_items as $fi) {
                            $item['customer_items'] = [];
                            if (!in_array($fi->id, array_column($items, 'item_id'))) {
                                $item['quantity'] = $fi->quantity;
                                $item['name'] = $fi->name;
                                $item['branch'] = $fi->branch;
                                $item['order_code'] = $fi->order_code;

                                array_push($item['customer_items'], [
                                    'order_id' => $order_id,
                                    'item_id' => intval($fi->id)
                                ]);
                            }

                            $item['status'] = InventoryItemStatus::FOUND;
                            $item['customer_order_id'] = $order_id;
                            $item['web_tracking'] = $fi->tracking_code;
                            array_push($items, $item);
                        }
                    }
                } else {
                    array_push($items, $item);
                }
            } else {
                array_push($items, $item);
            }
        }

        json_success(null, ['items' => $items]);
    }

    private function _save_package()
    {
        $this->form_validation->set_rules('warehouse_id', null, 'required|numeric');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $items = $this->input->post('items');
        if ($items == null)
            json_error('Không có sản phẩm');

        if (!is_array($items))
            json_error('Không có sản phẩm');

        $warehouse_id = $this->input->post('warehouse_id');
        $warehouse = $this->warehouse_model->get($warehouse_id);
        if ($warehouse == null)
            json_error('Không tìm thấy warehouse');

        if ($this->inventory_model->insert($warehouse_id, $items)) {
            foreach ($items as $item) {
                if (is_enull($item['customer_order_id'], null, 'numeric') != null)
                    $this->customer_order_model->update_order_total($item['customer_order_id']);
            }

            json_success('Nhập kho thành công');
        }

        json_error('Có lỗi, vui lòng F5 thử lại');
    }

    public function DELETE_index()
    {
        $id = $this->input->input_stream('id');
        if ($this->inventory_model->update_item($id, ['lost_pending' => 0]))
            json_success('Cập nhật thành công');
        json_error('Có lỗi, vui lòng F5 thử lại');
    }
}