<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Coupon
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Country_model $country_model
 */
class Coupon extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('country_model');
    }

    public function index()
    {
        $this->blade->set('breadcrumbs', [['text' => 'Mã giảm giá']]);
        return $this->blade->render();
    }

    public function GET_index()
    {

    }

    public function POST_index()
    {

    }

    public function DELETE_index()
    {

    }
}