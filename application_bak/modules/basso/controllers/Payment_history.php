<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Payment_history
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Option_model $option_model
 * @property Image_model $image_model
 * @property Payment_history_model $payment_history_model
 * @property User_model $user_model
 * @property Customer_order_model $customer_order_model
 * @property Billing_model $billing_model
 * @property Payment_model $payment_model
 * @property Email_model $email_model
 */
class Payment_history extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/user_model');
        $this->load->model('payment_history_model');
        $this->load->model('customer_order_model');
        $this->load->model('management/billing_model');
        $this->load->model('payment_model');
        $this->load->model('email_model');
    }

    public function index()
    {
        $this->blade->set('breadcrumbs', [['text' => 'Lịch sử thanh toán']]);
        return $this->blade->render();
    }

    public function GET_index()
    {
        $page = $this->input->get('page');
        $init = $this->input->get('init');
        if ($init != null) {
            $users = $this->user_model->get();
            json_success(null, ['data' => $users]);
        }

        if ($page != null) {
            $page = !is_numeric($page) ? 1 : ($page < 1 ? 1 : $page);
            $time = $this->input->get('time');
            $date = $this->input->get('date');
            $status = $this->input->get('status');
            $user_id = $this->input->get('user_id');
            $start = strtotime($date . ' 00:00:00');
            $end = strtotime($date . ' 23:59:59');

            switch ($time) {
                case 'all':
                    $start = 0;
                    $end = time();
                    break;
                case 'today':
                    $end = time();
                    $start = strtotime(date('Y-m-d 00:00:00'));
                    break;
                case 'yesterday':
                    $end = strtotime(date('Y-m-d 00:00:00')) - 1;
                    $start = $end + 1 - 24 * 3600;
                    break;
                case 'month':
                    $end = time();
                    $start = strtotime(date('Y-m-1 00:00:00'));
                    break;
            }

            $data = $this->payment_history_model->get($page, $start, $end, $status, $user_id);
            $total_item = $this->payment_history_model->count($start, $end, $status, $user_id);
            $pagination = Pagination::calc($total_item, $page, PAGING_SIZE);

            json_success(null, ['data' => $data, 'pagination' => $pagination]);
        }
    }

    public function POST_index()
    {
        $this->form_validation->set_rules('id', null, 'required|numeric');
        $this->form_validation->set_rules('status', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $id = $this->input->post('id');
        $status = $this->input->post('status');
        if (!in_array($status, array_keys(PaymentHistoryStatus::LIST)))
            json_error('Yêu cầu không hợp lệ');

        $payment = $this->payment_history_model->get_by_id($id);
        if ($payment == null)
            json_error('Không tìm thấy thanh toán');

        if ($payment->status != PaymentHistoryStatus::PENDING)
            json_error('Yêu cầu không hợp lệ');

        $order = null;
        if ($payment->order_id != null) {
            $order = $this->customer_order_model->get_by_id($payment->order_id);
            if ($order == null)
                json_error('Không tìm thấy đơn hàng');
        }

        if ($this->payment_history_model->update_status($id, $status, $order)) {
            //  Tạo phiếu thu tự động nếu chấp nhận thanh toán
            if ($status == PaymentHistoryStatus::COMPLETED) {
                $content = 'Thanh toán đơn hàng #' . $order->id;
                $this->billing_model->insert(time(), BillingType::IN, AUTO_BILLING_TERM, $order->name,
                    $payment->amount, null, $content, null, null, null,
                    $payment->order_id, null);

                $items = $this->customer_order_model->get_items($order->id);
                $order = $this->customer_order_model->get_by_id($payment->order_id);
                $order->payment_method = 'Chuyển khoản';

                if (isset($order->payment_method)) {
                    $payment = $this->payment_model->get_by_type($order->payment_method);
                    if ($payment != null)
                        $order->payment_method = $payment->name;
                }

                if (strpos($order->email, 'no_email.com') == false) {
                    $content = $this->blade->render('confirm_payment', ['order' => $order, 'items' => $items], true);
                    $subject = "[Basso] - Xác nhận thanh toán - #$order->order_code";
                    $this->email_model->insert($subject, $content, $order->email);
                }
            }

            if ($order != null)
                json_success($status == PaymentHistoryStatus::COMPLETED ? 'Đã xác nhận thanh toán đơn hàng #' . $order->order_code :
                    'Đã hủy thanh toán đơn hàng #' . $order->order_code);
            json_success($status == PaymentHistoryStatus::COMPLETED ? 'Đã xác nhận thanh toán' : 'Đã hủy thanh toán');
        }

        json_error('Có lỗi, vui lòng F5 thử lại');
    }
}