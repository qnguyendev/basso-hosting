<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Test
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Option_model $option_model
 * @property Image_model $image_model
 */
class Test extends Site_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        return $this->blade->render();
    }
}