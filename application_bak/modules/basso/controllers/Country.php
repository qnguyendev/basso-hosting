<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Warehouse
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Country_model $country_model
 * @property Currency_model $currency_model
 * @property Customer_group_model $customer_group_model
 */
class Country extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('country_model');
        $this->load->model('management/customer_group_model');
    }

    public function index()
    {
        $this->blade->set('currencies', $this->currency_model->get());
        $this->blade->set('breadcrumbs', [['text' => 'Hệ thống'], ['text' => 'Quản lý quốc gia']]);
        return $this->blade->render();
    }

    public function GET_index()
    {
        $this->form_validation->set_data($this->input->get());
        $this->form_validation->set_rules('action', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        switch ($this->input->get('action')) {
            case 'init':
                json_success(null, [
                    'data' => $this->country_model->get(),
                    'customer_groups' => $this->customer_group_model->get()
                ]);
                break;

            case 'detail':
                $this->form_validation->set_rules('id', null, 'required|numeric');
                $this->form_validation->set_rules('action', null, 'required');
                if (!$this->form_validation->run())
                    json_error('Yêu cầu không hợp lệ');

                $id = $this->input->get('id');
                json_success(null, ['data' => $this->country_model->get_customer_rates($id)]);
                break;

            default:
                json_success('Yêu cầu không hợp lệ');
                break;
        }
    }

    public function DELETE_index()
    {
        $this->form_validation->set_data($this->input->input_stream());
        $this->form_validation->set_rules('id', null, 'required|numeric');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $id = $this->input->input_stream('id');
        $warehouse = $this->country_model->get($id);
        if ($warehouse == null)
            json_error('Không tìm thấy quốc gia');

        if ($this->country_model->delete($id))
            json_success('Xóa quốc gia thành công');
        json_error('Có lỗi, vui lòng F5 thử lại');
    }

    public function POST_index()
    {
        $this->form_validation->set_rules('action', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        switch ($this->input->post('action')) {
            case 'save-warehouse':
                $this->_save_warehouse();
                break;

            case 'save-customer-rates':
                $this->_save_customer_rates();
                break;

            default:
                json_error('Yêu cầu không hợp lệ');
                break;
        }
    }

    private function _save_customer_rates()
    {
        $warehouse_id = $this->input->post('warehouse_id');
        $rates = $this->input->post('rates');

        if ($warehouse_id == null || $rates == null)
            json_error('Yêu cầu không hợp lệ');

        if ($this->country_model->save_customer_rates($warehouse_id, $rates))
            json_success('Cập nhật thành công');

        json_error('Có lỗi, vui lòng F5 thử lại');
    }

    private function _save_warehouse()
    {
        $this->form_validation->set_rules('id', null, 'required|numeric');
        $this->form_validation->set_rules('name', null, 'required');
        $this->form_validation->set_rules('currency_id', null, 'required|numeric');
        $this->form_validation->set_rules('customer_rate', null, 'required|numeric');
        $this->form_validation->set_rules('buy_rate', null, 'required|numeric');
        $this->form_validation->set_rules('shipping_fee', null, 'required|numeric');

        if (!$this->form_validation->run())
            json_error('Vui lòng nhập đủ thông tin yêu cầu');

        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $currency_id = $this->input->post('currency_id');
        $customer_rate = $this->input->post('customer_rate');
        $buy_rate = $this->input->post('buy_rate');
        $shipping_fee = $this->input->post('shipping_fee');

        $data = [
            'name' => $name,
            'currency_id' => $currency_id,
            'customer_rate' => $customer_rate,
            'buy_rate' => $customer_rate,
            'buy_rate' => $buy_rate,
            'shipping_fee' => $shipping_fee
        ];

        if ($this->country_model->insert($id, $data))
            json_success($id == 0 ? 'Thêm quốc gia thành công' : 'Cập nhật quốc gia thành công');
        json_error('Có lỗi, vui lòng F5 thử lại');
    }
}