<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Basso_CustomerOrderStatus
 * Trạng thái đơn hàng của khách
 */
abstract class CustomerOrderStatus
{
    const LIST = [
        'pending' => ['name' => 'Chờ duyệt', 'class' => 'warning'],
        'processing' => ['name' => 'Đang GD', 'class' => 'info'],
        'completed' => ['name' => 'Hoàn thành', 'class' => 'success'],
        'cancelled' => ['name' => 'Đã hủy', 'class' => 'danger']
    ];

    const PENDING = 'pending';
    const PROCESSING = 'processing';
    const COMPLETED = 'completed';
    const CANCELLED = 'cancelled';
}

/**
 * Class Basso_PaymentHistoryStatus
 * Trạng thái các thanh toán, giao dịch
 */
abstract class PaymentHistoryStatus
{
    const LIST = [
        'pending' => ['name' => 'Chờ duyệt', 'class' => 'info'],
        'completed' => ['name' => 'Đã xác nhận', 'class' => 'success'],
        'cancelled' => ['name' => 'Đã hủy', 'class' => 'danger']
    ];

    const PENDING = 'pending';
    const COMPLETED = 'completed';
    const CANCELLED = 'cancelled';
}

/**
 * Class Basso_PaymentType
 * Hình thức thanh toán
 */
abstract class PaymentType
{
    const LIST = [
        'transfer' => 'Chuyển khoản',
        'cod' => 'COD',
        'online' => 'Thanh toán online',
        'cash' => 'Tiền mặt'
    ];

    const TRANSFER = 'transfer';
    const COD = 'cod';
    const ONLINE = 'online';
    const CASH = 'cash';
}

/**
 * Trạng thái đơn giao hàng
 * Class Basso_ShippingOrderStatus
 */
abstract class ShippingOrderStatus
{
    const LIST = [
        'waiting' => ['name' => 'Chờ giao hàng', 'class' => 'warning'],
        'completed' => ['name' => 'Đã giao hàng', 'class' => 'success']
    ];

    const WAITING = 'waiting';
    const COMPLETED = 'completed';
}

/**
 * Trạng thái đơn hàng website
 * Class Basso_WebOrderStatus
 */
abstract class WebOrderStatus
{
    const LIST = [
        'bought' => 'Đã mua',
        'in_warehouse' => 'Đã về kho VC',
        'in_inventory' => 'Đã về VN',
        'cancelled' => 'Đã hủy'
    ];

    const BOUGHT = 'bought';
    const IN_WAREHOUSE = 'in_warehouse';
    const IN_INVENTORY = 'in_inventory';
    const CANCELLED = 'cancelled';
}

/**
 * Yêu cầu sản phẩm đơn khách hàng
 * Class CustomerOrderItem
 */
abstract class CustomerOrderItemRequirement
{
    const LIST = [
        'pending' => 'Chờ duyệt',
        'buy_now' => 'Mua ngay',
        'wait_more' => 'Chờ gom',
        'biding' => 'Đấu giá'
    ];

    const PENDING = 'pending';
    const BUY_NOW = 'buy_now';
    const WAIT_MORE = 'wait_more';
    const BIDING = 'biding';
}

/**
 * Trạng thái sản phẩm đơn khách hàng
 * Class Basso_CustomerOrderItemStatus
 */
abstract class CustomerOrderItemStatus
{
    const LIST = [
        'pending' => ['name' => '-', 'index' => 1],
        'bought' => ['name' => 'Đã mua hàng', 'index' => 2],
        'not_available' => ['name' => 'Không mua được', 'index' => 2],
        'in_warehouse' => ['name' => 'Đã về kho VC', 'index' => 3],
        'in_inventory' => ['name' => 'Đã về Việt Nam', 'index' => 4],
        'shipped' => ['name' => 'Đã giao hàng', 'index' => 5],
        'returned' => ['name' => 'Hoàn trả', 'index' => 6]
    ];

    const NOT_AVAILABLE = 'not_available';
    const PENDING = 'pending';
    const BOUGHT = 'bought';
    const IN_WAREHOUSE = 'in_warehouse';
    const IN_INVENTORY = 'in_inventory';
    const RETURNED = 'returned';
    const SHIPPED = 'shipped';
}

/**
 * Trạng thái sản phẩm trong quản lý kho
 * Class Basso_InventoryItemStatus
 */
abstract class InventoryItemStatus
{
    const LIST = [
        'found' => ['name' => 'Found', 'class' => 'success'],
        'not_found' => ['name' => 'Not Found', 'class' => 'warning'],
        'lost' => ['name' => 'Hàng thiếu', 'class' => 'secondary']
    ];

    const FOUND = 'found';
    const NOT_FOUND = 'not_found';
    const LOST = 'lost';
}

/**
 * Tình trạng thanh toán đơn hàng của khách
 * Class Basso_OrderPaymentStatus
 */
abstract class OrderPaymentStatus
{
    const LIST = [
        'no_payment' => 'Chưa đặt cọc',
        'first_payment' => 'Thanh toán lần 1',
        'second_payment' => 'Thanh toán lần 2',
        'uncompleted' => 'Còn thiếu',
        'completed' => 'Đã thanh toán đủ'
    ];

    const NO_PAYMENT = 'no_payment';
    const FIRST_PAYMENT = 'first_payment';
    const SECOND_PAYMENT = 'second_payment';
    const COMPLETED = 'completed';
    const UNCOMPLETED = 'uncompleted';
}

/**
 * Class CustomerOrderShipStatus
 * Tình trạng giao hàng (các sp trong đơn của khách)
 */
abstract class CustomerOrderShipStatus
{
    const LIST = [
        'part' => 'Giao một phần',
        'all' => 'Đã giao hàng',
        'none' => 'Chưa giao hàng'
    ];

    const PART = 'part';
    const ALL = 'all';
    const NONE = 'none';
}

/**
 * Trạng thái tracking của sản phẩm đơn khách hàng
 * Class CustomerOrderItemTracking
 */
abstract class CustomerOrderItemTracking
{
    const LIST = [
        'all' => 'Tất cả',
        'none' => 'Chưa có',
        'has' => 'Đã có'
    ];

    const ALL = 'all';
    const NONE = 'none';
    const HAS = 'has';
}

/**
 * Kiểu danh mục phụ thu
 * Class OrderTermType
 */
abstract class OrderTermType
{
    const LIST = [
        'percent' => 'Phần trăm',
        'amount' => 'Số tiền'
    ];

    const PERCENT = 'percent';
    const AMOUNT = 'amount';
}

abstract class DeliveryManager
{
    const LIST = [
        'all' => 'Tất cả',
        'no_tracking' => 'Chưa có tracking',
        'no_delivered' => 'Chưa về kho VC',
        'no_inventory' => 'Chưa về VN'
    ];

    const ALL = 'all';
    const NO_TRACKING = 'no_tracking';
    const NO_DELIVERED = 'no_delivered';
    const NO_INVENTORY = 'no_inventory';
}

abstract class CustomerAccountOrderTab
{
    const LIST = [
        'processing' => 'Đang giao dịch',
        'unpaid' => 'Chưa thanh toán',
        'completed' => 'Hoàn thành'
    ];

    const PROCESSING = 'processing';
    const UNPAID = 'unpaid';
    const COMPLETED = 'completed';
}

abstract class CustomerOrdersReturnedStatus
{
    const LIST = [
        'processing' => 'Đang nhận',
        'completed' => 'Đã nhận'
    ];

    const PROCESSING = 'processing';
    const COMPLETED = 'completed';
}

abstract class OrderTermCategoryType
{
    const LIST = [
        'include' => 'Bao gồm',
        'exclude' => 'Loại trừ'
    ];

    const INCLUDE = 'include';
    const EXCLUDE = 'exclude';
}