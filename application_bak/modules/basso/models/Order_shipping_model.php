<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Order_shipping_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Order_shipping_model extends CI_Model
{
    private $_table = 'order_shippings';
    private $_item_table = 'order_shipping_items';
    private $_cus_order_table = 'customer_orders';
    private $_cus_order_item_table = 'customer_order_items';

    public function insert(int $order_id, array $customer, array $shipping, array $items)
    {
        $this->db->trans_begin();
        $data = [
            'order_id' => $order_id,
            'created_time' => time(),
            'status' => ShippingOrderStatus::WAITING,
            'name' => $customer['name'],
            'phone' => $customer['phone'],
            'address' => $customer['address'],
            'note' => is_enull($customer['note'], null),
            'city_id' => $customer['city_id'],
            'district_id' => $customer['district_id'],
            'shipping_id' => $shipping['shipping_id'],
            'code' => strtoupper(is_enull($shipping['code'], null)),
            'fee' => is_enull($shipping['fee'], 0),
            'cod_amount' => is_enull($shipping['cod_amount'], 0),
            'weight' => is_enull($shipping['weight'], null),
            'size' => is_enull($shipping['size'], null),
            'total' => $shipping['total'],
            'real_shipping_fee' => is_enull($shipping['real_shipping_fee'], 0, 'numeric')
        ];

        $this->db->insert($this->_table, $data);
        $id = $this->db->insert_id();
        if ($this->_insert_items($order_id, $id, $items)) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    private function _insert_items(int $order_id, int $shipping_id, array $items)
    {
        $this->db->trans_begin();
        $data = [];
        foreach ($items as $item) {
            array_push($data, [
                'created_time' => time(),
                'order_shipping_id' => $shipping_id,
                'order_item_id' => $item['id'],
                'name' => $item['name'],
                'quantity' => $item['quantity'],
                'total' => $item['total']
            ]);
        }
        if ($this->_mark_item_shipped($items)) {
            $this->db->insert_batch($this->_item_table, $data);
            if ($this->db->trans_status()) {
                if ($this->_check_customer_order_shipping_status($order_id)) {
                    $this->db->trans_commit();
                    return true;
                }
            }
        }

        $this->db->trans_rollback();
        return false;
    }

    private function _mark_item_shipped(array $items)
    {
        $this->db->trans_begin();
        $this->db->where_in('id', array_column($items, 'id'));
        $this->db->update('customer_order_items', ['shipped' => 1]);
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    /**
     * Kiểm tra/đánh dấu trạng thái giao hàng của đơn khách hàng
     * @param int $order_id
     * @return bool
     */
    private function _check_customer_order_shipping_status(int $order_id)
    {
        $this->db->select('shipped');
        $this->db->where(' order_id', $order_id);
        $result = $this->db->get($this->_cus_order_item_table)->result();

        $this->db->trans_begin();
        $shipped_count = count(array_filter($result, function ($item) {
            return $item->shipped == 1;
        }));

        $status = CustomerOrderShipStatus::NONE;
        if ($shipped_count == count($result))
            $status = CustomerOrderShipStatus::ALL;
        else if ($shipped_count > 0)
            $status = CustomerOrderShipStatus::PART;

        $this->db->where('id', $order_id);
        $this->db->update($this->_cus_order_table, ['shipping_status' => $status]);
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function get(int $page = 0, $branch = null, $shipping_id = null, $status = null, $search = null)
    {
        $this->db->select('t.*, o.name as city, d.name as district, k.name as shipping');
        $this->db->from("{$this->_table} t");
        $this->db->join('cities o', 't.city_id = o.id', 'left');
        $this->db->join('districts d', 'd.id = t.district_id', 'left');
        $this->db->join('shippings k', 'k.id = t.shipping_id', 'left');

        if ($branch != null) {
            if (!empty($branch)) {
                $this->db->join("$this->_cus_order_table l", 'l.id = t.order_id', 'left');
                $this->db->where('l.branch', $branch);
            }
        }

        if ($shipping_id != null)
            $this->db->where('t.shipping_id', $shipping_id);

        if ($status != null)
            $this->db->where('t.status', $status);

        if ($search != null) {
            if (!empty($search)) {
                $this->db->like('t.name', $search, 'both');
                $this->db->or_like('t.phone', $search, 'both');
            }
        }

        if ($page > 0) {
            $this->db->limit(PAGING_SIZE);
            $this->db->offset(($page - 1) * PAGING_SIZE);
        }

        $this->db->order_by('t.created_time', 'desc');
        $this->db->group_by('t.id');
        return $this->db->get()->result();
    }

    public function count($branch = null, $shipping_id = null, $status = null, $search = null)
    {
        $this->db->select('t.id');
        $this->db->from("$this->_table t");

        if ($branch != null) {
            if (!empty($branch)) {
                $this->db->join("$this->_cus_order_table l", 'l.id = t.order_id', 'left');
                $this->db->where('l.branch', $branch);
            }
        }

        if ($shipping_id != null)
            $this->db->where('t.shipping_id', $shipping_id);
        if ($status != null)
            $this->db->where('t.status', $status);
        if ($search != null) {
            if (!empty($search)) {
                $this->db->like('t.name', $search, 'both');
                $this->db->or_like('t.phone', $search, 'both');
            }
        }

        return $this->db->count_all_results();
    }

    public function get_by_id($id)
    {
        $this->db->select('t.*, o.name as city, d.name as district, k.name as shipping');
        $this->db->from("{$this->_table} t");
        $this->db->join('cities o', 't.city_id = o.id', 'left');
        $this->db->join('districts d', 'd.id = t.district_id', 'left');
        $this->db->join('shippings k', 'k.id = t.shipping_id', 'left');

        if (!is_array($id)) {
            $this->db->where('t.id', $id);
            return $this->db->get($this->_table)->row();
        }

        $this->db->where_in('t.id', $id);
        $this->db->group_by('t.id');
        return $this->db->get($this->_table)->result();
    }

    public function get_by_order(int $order_id)
    {
        $this->db->select('t.*, o.name as city, d.name as district, k.name as shipping');
        $this->db->from("{$this->_table} t");
        $this->db->join('cities o', 't.city_id = o.id', 'left');
        $this->db->join('districts d', 'd.id = t.district_id', 'left');
        $this->db->join('shippings k', 'k.id = t.shipping_id', 'left');
        $this->db->where_in('t.order_id', $order_id);
        $this->db->order_by('t.created_time', 'desc');
        $this->db->group_by('t.id');
        return $this->db->get()->result();
    }

    public function get_items($shipping_id)
    {
        if (!is_array($shipping_id))
            $this->db->where('order_shipping_id', $shipping_id);
        else
            $this->db->where_in('order_shipping_id', $shipping_id);
        return $this->db->get($this->_item_table)->result();
    }

    /**
     * Đánh dấu đơn hàng đã được giao
     * @param $id
     * @param $time
     * @return bool
     */
    public function update_shipped_time($id, $time)
    {
        $this->db->trans_begin();
        if (!is_array($id))
            $this->db->where('id', $id);
        else
            $this->db->where_in('id', $id);

        $this->db->update($this->_table, ['shipped_time' => $time, 'status' => ShippingOrderStatus::COMPLETED]);
        if ($this->db->trans_status()) {

            if ($this->_change_items_status($id)) {
                $this->db->trans_commit();
                return true;
            }
        }

        $this->db->trans_rollback();
        return false;
    }

    /**
     * Cập nhật trạng thái sản phẩm đã được giao
     * @param $shipping_id
     * @return bool
     */
    private function _change_items_status($shipping_id)
    {
        if (!is_array($shipping_id))
            $this->db->where('order_shipping_id', $shipping_id);
        else
            $this->db->where_in('order_shipping_id', $shipping_id);

        $result = $this->db->get($this->_item_table)->result();
        if (count($result) == 0)
            return true;


        $items_id = array_column($result, 'order_item_id');
        $this->db->trans_begin();
        $this->db->where_in('id', $items_id);
        $this->db->update($this->_cus_order_item_table, ['status' => CustomerOrderItemStatus::SHIPPED]);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }
}