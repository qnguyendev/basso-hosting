<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Amazon_crawler_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 * @property Crawl_history_model $crawl_history_model
 * @property CI_Loader $load
 */
class Amazon_crawler_model extends CI_Model
{
    private $_api_key = 'key-aa1cc26a1a4349d8858ab688';
    private $_api_url = 'https://amzproductapi.com/';
    private $_category = null;
    private $_first_json = null;
    private $_json = null;

    public function get($asin)
    {
        return $this->_crawl($asin);
    }

    private function _crawl($asin)
    {
        $client = new GuzzleHttp\Client(['base_uri' => $this->_api_url]);
        $url = "api/xml?Operation=ItemLookup&ResponseGroup=Large,Variations&IdType=ASIN&ItemId=$asin";

        $res = $client->post($url, [
            'headers' => ['Authorization' => "Bearer $this->_api_key"]
        ]);

        if ($res->getStatusCode() == 200) {
            $body = $res->getBody();
            $xml = simplexml_load_string($body);
            $json = json_decode(json_encode($xml));
            $this->_json = $json;

            if ($json->Items != null) {
                if (isset($json->Items->Request)) {
                    if (isset($json->Items->Request->Errors)) {
                        if ($this->_first_json != null) {
                            $result = $this->_parse_data($this->_first_json);
                            return $result;
                        }

                        return null;
                    }
                }

                if (isset($json->Items->Item)) {
                    $asin = $json->Items->Item->ASIN;
                    $parent_asin = null;

                    if (isset($json->Items->Item->ParentASIN)) {
                        $parent_asin = $json->Items->Item->ParentASIN;
                    }

                    if ($asin == $parent_asin || $parent_asin == null) {
                        $result = $this->_parse_data($json);

                        return $result;
                    }

                    #region Lấy danh mục
                    $product['categories'] = [];
                    if (isset($json->Items->Item->BrowseNodes)) {
                        if (isset($json->Items->Item->BrowseNodes->BrowseNode)) {
                            if (is_array($json->Items->Item->BrowseNodes->BrowseNode)) {
                                foreach ($json->Items->Item->BrowseNodes->BrowseNode as $t)
                                    $this->_get_categories($product, $t);
                            } else
                                $this->_get_categories($product, $json->Items->Item->BrowseNodes->BrowseNode);

                            $this->_category = json_encode($product['categories']);
                        }
                    }
                    #endregion

                    sleep(5);
                    $this->_first_json = $json;
                    return $this->_crawl($parent_asin);
                }
            }
        }

        return null;
    }

    private function _parse_data($json)
    {
        $product = [
            'color_images' => [],
            'variations' => [
                'list' => [],
                'detail' => []
            ],
            'url' => $json->Items->Item->DetailPageURL,
            'description' => [],
            'title' => $json->Items->Item->ItemAttributes->Title,
            'brand' => null,
            'id' => $json->Items->Item->ASIN,
            'image' => null,
            'highest_price' => 0.0,
            'lowest_price' => 0.0,
            'categories' => [],
            'term_id' => null
        ];

        if (isset($json->Items->Item->ItemAttributes->Brand)) {
            $product['brand'] = $json->Items->Item->ItemAttributes->Brand;
        }

        if (isset($json->Items->Item->ItemAttributes->Feature)) {
            if (is_array($json->Items->Item->ItemAttributes->Feature)) {
                foreach ($json->Items->Item->ItemAttributes->Feature as $key => $value)
                    array_push($product['description'], $value);
            } else {
                array_push($product['description'], $json->Items->Item->ItemAttributes->Feature);
            }
        }

        $variations = [];
        if (isset($json->Items->Item->Variations)) {
            $product['highest_price'] = floatval($json->Items->Item->VariationSummary->HighestPrice->Amount) / 100;
            $product['lowest_price'] = floatval($json->Items->Item->VariationSummary->LowestPrice->Amount) / 100;
            $index = 0;
            if (is_array($json->Items->Item->Variations->Item)) {
                foreach ($json->Items->Item->Variations->Item as $variation) {
                    if ($product['brand'] == null)
                        $product['brand'] = $variation->ItemAttributes->Brand;

                    $this->_parse_child($product, $variations, $variation, $index);
                    $index++;
                }
            } else {
                $this->_parse_child($product, $variations, $json->Items->Item->Variations->Item, 0);

                if ($product['brand'] == null)
                    $product['brand'] = $json->Items->Item->Variations->Item->ItemAttributes->Brand;
            }
        } else {
            $this->_parse_child($product, $variations, $json->Items->Item, 0);
        }

        #region Lấy danh mục
        if (isset($json->Items->Item->BrowseNodes)) {
            if (isset($json->Items->Item->BrowseNodes->BrowseNode)) {
                if (is_array($json->Items->Item->BrowseNodes->BrowseNode)) {
                    foreach ($json->Items->Item->BrowseNodes->BrowseNode as $t)
                        $this->_get_categories($product, $t);
                } else
                    $this->_get_categories($product, $json->Items->Item->BrowseNodes->BrowseNode);
            }
        }
        #endregion

        $product['variations'] = json_encode($product['variations']);
        $product['description'] = json_encode($product['description']);
        $product['color_images'] = json_encode($product['color_images']);

        if (count($product['categories']) > 0)
            $product['categories'] = json_encode($product['categories']);
        else
            $product['categories'] = $this->_category;

        return ['product' => $product, 'variations' => $variations];
    }

    private function _parse_child(&$product, &$variations, $variation, $index)
    {
        $item = [
            'id' => $variation->ASIN,
            'created_time' => time(),
            'parent_id' => $product['id'],
            'images' => [],
            'price' => 0.0,
            'sale_price' => 0.0,
            'weight' => 0.0,
            'attributes' => [],
            'condition' => null
        ];

        if (in_array($item['id'], array_column($variations, 'id')))
            return;

        $product['variations']['detail'][$index] = [
            'asin' => $item['id'],
            'value' => []
        ];

        if (isset($variation->Offers)) {
            if (isset($variation->Offers->Offer)) {

                if (isset($variation->Offers->Offer->OfferAttributes)) {
                    if (isset($variation->Offers->Offer->OfferAttributes->Condition)) {
                        $item['condition'] = $variation->Offers->Offer->OfferAttributes->Condition;
                    }
                }

                $offer_listing = $variation->Offers->Offer->OfferListing;
                if (isset($offer_listing->Price))
                    if (isset($offer_listing->Price->Amount))
                        $item['price'] = floatval($offer_listing->Price->Amount) / 100;
                if (isset($offer_listing->SalePrice))
                    if (isset($offer_listing->SalePrice->Amount))
                        $item['sale_price'] = floatval($offer_listing->SalePrice->Amount) / 100;
            }
        }

        if ($item['price'] == 0) {
            if (isset($variation->ItemAttributes)) {
                if (isset($variation->ItemAttributes->ListPrice)) {
                    if (isset($variation->ItemAttributes->ListPrice)) {
                        if (isset($variation->ItemAttributes->ListPrice->Amount)) {
                            $item['price'] = floatval($variation->ItemAttributes->ListPrice->Amount) / 100;
                        }
                    }
                }
            }
        }

        if (isset($variation->ItemAttributes->PackageDimensions)) {
            if (isset($variation->ItemAttributes->PackageDimensions->Weight)) {
                $weight = $variation->ItemAttributes->PackageDimensions->Weight;
                $item['weight'] = floatval($weight) / 100 * 0.453592;
            }
        }

        if ($item['weight'] <= 0) {
            if (isset($variation->ItemAttributes->ItemDimensions)) {
                if (isset($variation->ItemAttributes->ItemDimensions->Weight)) {
                    $weight = $variation->ItemAttributes->ItemDimensions->Weight;
                    $item['weight'] = floatval($weight) / 100 * 0.453592;
                }
            }
        }

        $item['weight'] = round($item['weight'], 2);

        #region Ảnh variation
        if (isset($variation->ImageSets)) {
            if (is_array($variation->ImageSets->ImageSet)) {
                foreach ($variation->ImageSets->ImageSet as $image_set) {
                    $image = [
                        'thumbnail' => null,
                        'large' => null
                    ];

                    if (isset($image_set->HiResImage))
                        $image['large'] = $image_set->HiResImage->URL;
                    elseif (isset($image_set->LargeImage))
                        $image['large'] = $image_set->LargeImage->URL;
                    elseif (isset($image_set->MediumImage))
                        $image['large'] = $image_set->MediumImage->URL;
                    elseif (isset($image_set->TinyImage))
                        $image['large'] = $image_set->TinyImage->URL;
                    elseif (isset($image_set->ThumbnailImage))
                        $image['large'] = $image_set->ThumbnailImage->URL;

                    if (isset($image_set->MediumImage))
                        $image['thumbnail'] = $image_set->MediumImage->URL;
                    elseif (isset($image_set->TinyImage))
                        $image['thumbnail'] = $image_set->TinyImage->URL;
                    elseif (isset($image_set->ThumbnailImage))
                        $image['thumbnail'] = $image_set->ThumbnailImage->URL;
                    elseif (isset($image_set->SmallImage))
                        $image['thumbnail'] = $image_set->SmallImage->URL;
                    elseif (isset($image_set->HiResImage))
                        $image['thumbnail'] = $image_set->HiResImage->URL;
                    elseif (isset($image_set->LargeImage))
                        $image['thumbnail'] = $image_set->LargeImage->URL;

                    array_push($item['images'], $image);
                }
            } else {
                $image = [
                    'thumbnail' => null,
                    'large' => null
                ];

                $image_set = $variation->ImageSets->ImageSet;
                if (isset($image_set->HiResImage))
                    $image['large'] = $image_set->HiResImage->URL;
                elseif (isset($image_set->LargeImage))
                    $image['large'] = $image_set->LargeImage->URL;
                elseif (isset($image_set->MediumImage))
                    $image['large'] = $image_set->MediumImage->URL;
                elseif (isset($image_set->TinyImage))
                    $image['large'] = $image_set->TinyImage->URL;
                elseif (isset($image_set->ThumbnailImage))
                    $image['large'] = $image_set->ThumbnailImage->URL;

                if (isset($image_set->MediumImage))
                    $image['thumbnail'] = $image_set->MediumImage->URL;
                elseif (isset($image_set->TinyImage))
                    $image['thumbnail'] = $image_set->TinyImage->URL;
                elseif (isset($image_set->ThumbnailImage))
                    $image['thumbnail'] = $image_set->ThumbnailImage->URL;
                elseif (isset($image_set->SmallImage))
                    $image['thumbnail'] = $image_set->SmallImage->URL;
                elseif (isset($image_set->HiResImage))
                    $image['thumbnail'] = $image_set->HiResImage->URL;
                elseif (isset($image_set->LargeImage))
                    $image['thumbnail'] = $image_set->LargeImage->URL;

                array_push($item['images'], $image);
            }
        }

        if (count($item['images']) == 0)
            $this->_get_images_from_parent($item);
        #endregion

        if (isset($variation->VariationAttributes)) {
            if (is_array($variation->VariationAttributes->VariationAttribute)) {
                foreach ($variation->VariationAttributes->VariationAttribute as $key => $val) {
                    if (isset($val->Name) && isset($val->Value)) {
                        if (!empty($val->Name) && !empty($val->Value)) {

                            if (!isset($product['variations']['list'][$val->Name]))
                                $product['variations']['list'][$val->Name] = [];

                            array_push($product['variations']['list'][$val->Name], $val->Value);
                            $product['variations']['list'][$val->Name] = array_unique($product['variations']['list'][$val->Name]);

                            array_push($product['variations']['detail'][$index]['value'], $val->Value);

                            if (strtolower($val->Name) == 'color') {
                                if (!isset($product['color_images'][$val->Value])) {
                                    $product['color_images'][$val->Value] = end($item['images'])['thumbnail'];
                                }
                            }
                        }
                    }
                }
            } else {
                $val = $variation->VariationAttributes->VariationAttribute;
                if (!isset($product['variations']['list'][$val->Name]))
                    $product['variations']['list'][$val->Name] = [];

                array_push($product['variations']['list'][$val->Name], $val->Value);
                array_push($product['variations']['detail'][$index]['value'], $val->Value);
            }
        }

        $att = [];
        if (isset($variation->ItemAttributes->PackageQuantity))
            $att['Package Quantity'] = $variation->ItemAttributes->PackageQuantity;
        if (isset($variation->ItemAttributes->Brand))
            $att['Brand'] = $variation->ItemAttributes->Brand;
        if (isset($variation->ItemAttributes->PackageQuantity))
            $att['Package Quantity'] = $variation->ItemAttributes->PackageQuantity;
        if (isset($variation->ItemAttributes->Manufacturer))
            $att['Manufacturer'] = $variation->ItemAttributes->Manufacturer;
        if (isset($variation->ItemAttributes->PartNumber))
            $att['Part Number'] = $variation->ItemAttributes->PartNumber;

        if (isset($variation->ItemAttributes->PackageDimensions)) {
            if (isset($variation->ItemAttributes->PackageDimensions->Weight)) {
                $att['Weight'] = $variation->ItemAttributes->PackageDimensions->Weight;
            }
        }

        if (isset($variation->ItemAttributes->PackageDimensions)) {
            if (isset($variation->ItemAttributes->PackageDimensions->Height))
                $att['Height'] = floatval($variation->ItemAttributes->PackageDimensions->Height) / 100 . ' inches';
            if (isset($variation->ItemAttributes->PackageDimensions->Length))
                $att['Length'] = floatval($variation->ItemAttributes->PackageDimensions->Length) / 100 . ' inches';
            if (isset($variation->ItemAttributes->PackageDimensions->Width))
                $att['Width'] = floatval($variation->ItemAttributes->PackageDimensions->Width) / 100 . ' inches';
        }

        if ($product['image'] == null) {
            if (count($item['images']) > 0)
                $product['image'] = end($item['images'])['thumbnail'];
        }

        $item['attributes'] = json_encode($att);
        $item['images'] = json_encode($item['images']);

        array_push($variations, $item);
    }

    private function _get_categories(&$product, $json)
    {
        if (isset($json->Name)) {
            if (!isset($json->IsCategoryRoot))
                array_push($product['categories'], $json->Name);

            if (isset($json->Ancestors))
                if (isset($json->Ancestors->BrowseNode))
                    $this->_get_categories($product, $json->Ancestors->BrowseNode);
        }

        if (count($product['categories']) <= 1) {
            if (isset($this->_first_json->Items->Item->BrowseNodes)) {
                if (isset($this->_first_json->Items->Item->BrowseNodes->BrowseNode)) {
                    if (is_array($this->_first_json->Items->Item->BrowseNodes->BrowseNode)) {
                        $product['categories'] = [];
                        foreach ($this->_first_json->Items->Item->BrowseNodes->BrowseNode as $t)
                            $this->_get_categories($product, $t);
                    } else {
                        $product['categories'] = [];
                        $this->_get_categories($product, $this->_first_json->Items->Item->BrowseNodes->BrowseNode);
                    }
                }
            }
        }
    }

    private function _get_images_from_parent(&$item)
    {
        if (isset($this->_json->Items->Item->ImageSets)) {
            if (is_array($this->_json->Items->Item->ImageSets->ImageSet)) {
                foreach ($this->_json->Items->Item->ImageSets->ImageSet as $image_set) {
                    $image = [
                        'thumbnail' => null,
                        'large' => null
                    ];

                    if (isset($image_set->HiResImage))
                        $image['large'] = $image_set->HiResImage->URL;
                    elseif (isset($image_set->LargeImage))
                        $image['large'] = $image_set->LargeImage->URL;
                    elseif (isset($image_set->MediumImage))
                        $image['large'] = $image_set->MediumImage->URL;
                    elseif (isset($image_set->TinyImage))
                        $image['large'] = $image_set->TinyImage->URL;
                    elseif (isset($image_set->ThumbnailImage))
                        $image['large'] = $image_set->ThumbnailImage->URL;

                    if (isset($image_set->MediumImage))
                        $image['thumbnail'] = $image_set->MediumImage->URL;
                    elseif (isset($image_set->TinyImage))
                        $image['thumbnail'] = $image_set->TinyImage->URL;
                    elseif (isset($image_set->ThumbnailImage))
                        $image['thumbnail'] = $image_set->ThumbnailImage->URL;
                    elseif (isset($image_set->SmallImage))
                        $image['thumbnail'] = $image_set->SmallImage->URL;

                    array_push($item['images'], $image);
                }
            } else {
                $image = [
                    'thumbnail' => null,
                    'large' => null
                ];

                $image_set = $this->_json->Items->Item->ImageSets->ImageSet;
                if (isset($image_set->HiResImage))
                    $image['large'] = $image_set->HiResImage->URL;
                elseif (isset($image_set->LargeImage))
                    $image['large'] = $image_set->LargeImage->URL;
                elseif (isset($image_set->MediumImage))
                    $image['large'] = $image_set->MediumImage->URL;
                elseif (isset($image_set->TinyImage))
                    $image['large'] = $image_set->TinyImage->URL;
                elseif (isset($image_set->ThumbnailImage))
                    $image['large'] = $image_set->ThumbnailImage->URL;

                if (isset($image_set->MediumImage))
                    $image['thumbnail'] = $image_set->MediumImage->URL;
                elseif (isset($image_set->TinyImage))
                    $image['thumbnail'] = $image_set->TinyImage->URL;
                elseif (isset($image_set->ThumbnailImage))
                    $image['thumbnail'] = $image_set->ThumbnailImage->URL;
                elseif (isset($image_set->SmallImage))
                    $image['thumbnail'] = $image_set->SmallImage->URL;

                array_push($item['images'], $image);
            }
        }
    }
}