<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Web_order_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Web_order_model extends CI_Model
{
    private $_table = 'web_orders';
    private $_item_table = 'web_order_items';
    private $_cus_item_table = 'customer_order_items';
    private $_cus_order_table = 'customer_orders';
    private $_warehouse_table = 'warehouses';
    private $_buyer_table = 'buyers';
    private $_web_payment_table = 'web_order_payments';

    public function insert(array $data, array $items)
    {
        $data = [
            'created_time' => $data['created_time'],
            'user_id' => $data['user_id'],
            'order_number' => $data['order_number'],
            'branch' => $data['branch'],
            'buyer_id' => $data['buyer_id'],
            'warehouse_id' => $data['warehouse_id'],
            'payment_id' => $data['payment_id'],
            'country_id' => $data['country_id'],
            'note' => is_enull($data['note'], null),
            'total' => $data['total'],
            'ship_fee' => $data['ship_fee'],
            'buy_rate' => $data['buy_rate'],
            'currency_symbol' => $data['currency_symbol'],
            'sub_total' => $data['sub_total'],
            'status' => WebOrderStatus::BOUGHT,
            'website' => $data['website']
        ];

        $this->db->trans_begin();
        $this->db->insert($this->_table, $data);
        $id = $this->db->insert_id();

        if ($this->_insert_items($id, $items)) {

            if ($this->_set_bought($items)) {
                $this->db->trans_commit();
                return $id;
            }
        }

        $this->db->trans_rollback();
        return false;
    }

    private function _insert_items(int $order_id, array $items)
    {
        $this->db->trans_begin();
        for ($i = 0; $i < count($items); $i++)
            $items[$i]['web_order_id'] = $order_id;

        $this->db->insert_batch($this->_item_table, $items);
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    /**
     * Cập nhật trạng thái đơn hàng
     * @param int $order_id
     * @param string $status
     * @param array|null $data
     * @return bool
     */
    public function update_order(int $order_id, string $status, array $data = null)
    {
        $this->db->trans_begin();
        $this->db->where('id', $order_id);

        if ($data != null) {
            if (is_array($data)) {
                $allow_keys = ['country_id', 'buyer_id', 'warehouse_id', 'buy_rate', 'currency_symbol', 'total', 'sub_total', 'ship_fee', 'order_number'];
                foreach ($data as $key => $value) {
                    if (!in_array($key, $allow_keys))
                        unset($data[$key]);
                }
            }
        }

        $data['status'] = $status;
        $this->db->update($this->_table, $data);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function update(int $order_id, $data)
    {
        $this->db->trans_begin();
        $allow_keys = ['total', 'sub_total', 'note'];

        foreach ($data as $key => $value) {
            if (!in_array($key, $allow_keys))
                unset($data[$key]);
        }

        $this->db->where('id', $order_id);
        $this->db->update($this->_table, $data);
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    /**
     * Đánh dấu các sản phẩm trong đơn của khách đã được mua
     * @param array $items
     * @return bool
     */
    private function _set_bought(array $items)
    {
        $this->db->trans_begin();
        $id = array_column($items, 'item_id');
        $this->db->where_in('id', $id);
        $this->db->update($this->_cus_item_table, ['status' => CustomerOrderItemStatus::BOUGHT]);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function get_by_id(int $order_id)
    {
        $this->db->select('t.*, d.first_name as user, k.name as country');
        $this->db->select('o.name as buyer, l.name as warehouse, m.name as payment');
        $this->db->from("{$this->_table} t");
        $this->db->join('countries k', 't.country_id = k.id', 'left');
        $this->db->join('users d', 't.user_id = d.id', 'left');
        $this->db->join("$this->_buyer_table o", 'o.id = t.buyer_id', 'left');
        $this->db->join("$this->_warehouse_table l", 'l.id = t.warehouse_id', 'left');
        $this->db->join("$this->_web_payment_table m", 'm.id = t.payment_id', 'left');
        $this->db->where('t.id', $order_id);
        return $this->db->get()->row();
    }

    /**
     * Lấy sản phẩm theo mã tracking
     * @param $tracking
     * @return array
     */
    public function get_by_tracking($tracking)
    {
        if (is_array($tracking))
            $this->db->where_in('tracking_code', $tracking);
        else
            $this->db->where('tracking_code', $tracking);

        return $this->db->get($this->_item_table)->result();
    }

    public function get(int $page = 1, $date = null, $warehouse_id = 0, $payment_id = 0, $buyer_id = 0, $branch = null, $status = null, $key = null)
    {
        $this->db->select('t.*');
        $this->db->select('o.name as buyer, l.name as warehouse, m.name as payment');
        $this->db->from("{$this->_table} t");
        $this->db->join("{$this->_item_table} d", 't.id = d.web_order_id', 'left');
        $this->db->join("$this->_buyer_table o", 'o.id = t.buyer_id', 'left');
        $this->db->join("$this->_warehouse_table l", 'l.id = t.warehouse_id', 'left');
        $this->db->join("$this->_web_payment_table m", 'm.id = t.payment_id', 'left');

        if ($date != null) {
            if (!empty($date)) {
                try {
                    $start = strtotime("$date 00:00:00");
                    $end = strtotime("$date 23:59:59");
                    $this->db->where('t.created_time >=', $start);
                    $this->db->where('t.created_time <=', $end);
                } catch (Exception $ex) {

                }
            }
        }

        if ($branch != null) {
            if ($branch != 'all')
                $this->db->where('t.branch', $branch);
        }

        if ($status != null)
            $this->db->where('t.status', $status);

        if ($key != null) {
            $this->db->where("(t.order_number like '%$key%' or d.tracking_code like '%$key%')");
        }

        if ($warehouse_id != 0)
            $this->db->where('t.warehouse_id', $warehouse_id);
        if ($payment_id != 0)
            $this->db->where('t.payment_id', $payment_id);
        if ($buyer_id != 0)
            $this->db->where('t.buyer_id', $buyer_id);

        $this->db->order_by('t.id', 'desc');

        if ($page > 0) {
            $this->db->limit(PAGING_SIZE);
            $this->db->offset(($page - 1) * PAGING_SIZE);
        }

        $this->db->group_by('t.id');
        return $this->db->get()->result();
    }

    public function count($date = null, $warehouse_id = 0, $payment_id = 0, $buyer_id = 0, $branch = null, $status = null, $key = null)
    {
        $this->db->select('t.id');
        $this->db->from("{$this->_table} t");
        $this->db->join("{$this->_item_table} d", 't.id = d.web_order_id', 'left');

        if ($branch != null) {
            if ($branch != 'all')
                $this->db->where('t.branch', $branch);
        }

        if ($date != null) {
            if (!empty($date)) {
                try {
                    $start = strtotime("$date 00:00:00");
                    $end = strtotime("$date 23:59:59");
                    $this->db->where('t.created_time >=', $start);
                    $this->db->where('t.created_time <=', $end);
                } catch (Exception $ex) {

                }
            }
        }

        if ($status != null)
            $this->db->where('t.status', $status);
        if ($warehouse_id != 0)
            $this->db->where('t.warehouse_id', $warehouse_id);
        if ($payment_id != 0)
            $this->db->where('t.payment_id', $payment_id);
        if ($buyer_id != 0)
            $this->db->where('t.buyer_id', $buyer_id);

        if ($key != null) {
            $this->db->where("(t.order_number like '%$key%' or d.tracking_code like '%$key%')");
        }

        $this->db->group_by('t.id');
        return $this->db->count_all_results();
    }

    public function get_items($order_id, $key = null)
    {
        $this->db->select('t.*');
        $this->db->select('o.order_code');
        $this->db->from("$this->_item_table t");
        $this->db->join("$this->_cus_order_table o", 'o.id = t.customer_order_id', 'left');

        if (is_array($order_id))
            $this->db->where_in('t.web_order_id', $order_id);
        else
            $this->db->where('t.web_order_id', $order_id);

        /*if ($key != null)
            $this->db->like('t.tracking_code', $key, 'both');*/

        return $this->db->get()->result();
    }

    public function get_tracking()
    {
        $this->db->distinct();
        $this->db->select('t.tracking_code');
        $this->db->from("$this->_item_table t");
        $this->db->join("$this->_table o", 't.web_order_id = o.id', 'left');
        $this->db->where('t.tracking_code !=', null);
        $this->db->where('o.status', WebOrderStatus::BOUGHT);
        return $this->db->get()->result();
    }

    /**
     * Cập nhật mã tracking, ngày về kho vận chuyển
     * @param int $order_id
     * @param null $item_id
     * @param array $data
     * @param null|int $imported_time
     * @return bool
     */
    public function update_item(int $order_id = null, $item_id = null, array $data, $imported_time = null)
    {
        $allow_keys = ['tracking_code', 'delivered_date', 'note', 'link'];
        foreach ($data as $key => $value) {
            if (!in_array($key, $allow_keys))
                unset($data[$key]);
        }

        $this->db->trans_begin();

        if ($order_id == null) {
            $this->db->where('item_id', $item_id);
            $this->db->update($this->_item_table, $data);

            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            }

            $this->db->trans_rollback();
            return false;
        }

        if (array_key_exists('tracking_code', $data)) {
            if (is_enull($data['tracking_code'], null) != null) {
                $data['tracking_date'] = time();
            }
        }

        if (count($data) == 0)
            return true;

        if (isset($data['delivered_date']))
            $data['delivered_date'] = is_enull($data['delivered_date'], 0);

        $this->db->where('web_order_id', $order_id);
        if ($item_id != null)
            $this->db->where('id', $item_id);
        $this->db->update($this->_item_table, $data);

        if ($this->db->trans_status()) {

            //  Cập nhật sản phẩm đơn khách hàng -> đã về kho VC
            $this->db->where('web_order_id', $order_id);
            if ($item_id != null)
                $this->db->where('id', $item_id);

            $items = $this->db->get($this->_item_table)->result();
            if (count($items) > 0) {
                $id = array_column($items, 'item_id');
                $data['tracking_code'] = isset($data['tracking_code']) ? $data['tracking_code'] : null;
                $data['delivered_date'] = isset($data['delivered_date']) ? $data['delivered_date'] : null;

                if ($this->_update_customer_item_status($id, $data['tracking_code'], $data['delivered_date'], $imported_time)) {
                    $this->check_order_items_delivered($order_id);
                    $this->db->trans_commit();
                    return true;
                }
            }

            $this->check_order_items_delivered($order_id);
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    /**
     * Cập nhật ngày về kho vận chuyển theo mã tracking
     * @param $tracking
     * @param $delivered_date
     * @return bool
     */
    public function update_delivered_date($tracking, $delivered_date)
    {
        $this->db->trans_begin();
        $this->db->where('tracking_code', $tracking);
        $this->db->update($this->_item_table, ['delivered_date' => $delivered_date]);
        if ($this->db->trans_status()) {

            //  Cập nhật sản phẩm đơn khách hàng -> đã về kho VC
            $this->db->select('item_id');
            $this->db->where('tracking_code', $tracking);
            $items = $this->db->get($this->_item_table)->result();

            if (count($items) > 0) {
                $id = array_column($items, 'item_id');
                if ($this->_update_customer_item_status($id, $tracking, $delivered_date)) {
                    $this->db->trans_commit();
                    return true;
                }
            }

            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    /**
     * Cập nhật ngày về kho theo ID sản phẩm
     * @param $item_id
     * @param $delivered_date
     * @return bool
     */
    public function update_delivered_date_by_item($item_id, $delivered_date)
    {
        $this->db->trans_begin();
        $this->db->where('item_id', $item_id);
        $this->db->update($this->_item_table, ['delivered_date' => $delivered_date]);
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    /**
     * Kiểm tra các sp đã về warehouse
     * @param int $order_id
     */
    public function check_order_items_delivered(int $order_id)
    {
        $this->db->select('id');
        $this->db->where('web_order_id', $order_id);
        $this->db->where('delivered_date !=', null);

        $total = $this->db->count_all_results($this->_item_table);
        if ($total == 0) {
            $this->db->where('id', $order_id);
            $this->db->update($this->_table, ['status' => WebOrderStatus::IN_WAREHOUSE]);
        }
    }

    /**
     * Cập nhật các sp đơn hàng của khách đã về kho VC
     * @param $item_id
     * @param $tracking_code
     * @param $delivered_date
     * @return bool
     */
    private function _update_customer_item_status($item_id, $tracking_code = null, $delivered_date = null)
    {
        $this->db->trans_begin();
        if (is_array($item_id))
            $this->db->where_in('id', $item_id);
        else
            $this->db->where('id', $item_id);

        $data = [];
        if ($tracking_code != null)
            $data['tracking_code'] = $tracking_code;
        if ($delivered_date != null)
            $data['status'] = CustomerOrderItemStatus::IN_WAREHOUSE;

        if (count($data) > 0) {
            $this->db->update($this->_cus_item_table, $data);
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            }

            $this->db->trans_rollback();
            return false;
        }

        return true;
    }

    /**
     * Cập nhật warehouse cho đơn khách hàng chưa set
     * @param int $cus_order_id
     * @param int $warehouse_id
     */
    public function set_cus_order_warehouse(int $cus_order_id, int $warehouse_id)
    {
        $this->db->where('id', $cus_order_id);
        $this->db->where('warehouse_id', null);
        $this->db->update($this->_cus_order_table, ['warehouse_id' => $warehouse_id]);
    }
}