<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Backup
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Dropbox $dropbox
 * @property Option_model $option_model
 * @property Backup_model $backup_model
 * @property CI_DB_utility $dbutil
 */
class Backup extends Cpanel_Controller
{
    private $dropbox_config;

    public function __construct()
    {
        parent::__construct();

        global $theme_config;
        $this->dropbox_config = $theme_config['dropbox'];

        $app = [
            'app_key' => $this->dropbox_config['app_key'],
            'app_secret' => $this->dropbox_config['app_secret'],
            'app_full_access' => true
        ];

        $this->load->library('dropbox', $app);
        $this->load->helper('file');
        $this->load->model('backup_model');
    }

    public function index()
    {
        $breadcrumb = [['text' => 'Backup Dropbox']];
        return $this->blade
            ->set('breadcrumbs', $breadcrumb)
            ->render();
    }

    public function GET_index()
    {
        $type = $this->input->get('type');
        if ($type != null) {
            if ($type == 'init') {
                $dropbox_access_token = null;
                $dropbox_account_id = null;

                #region lấy cấu hình dropbox
                $config = $this->option_model->get('dropbox_access_token');
                if ($config != null) {
                    if ($config->value != null) {
                        if (!empty($config->value)) {
                            $dropbox_access_token = $config->value;
                        }
                    }
                }

                $config = $this->option_model->get('dropbox_account_id');
                if ($config != null) {
                    if ($config->value != null) {
                        if (!empty($config->value)) {
                            $dropbox_account_id = $config->value;
                        }
                    }
                }
                #endregion

                $this->dropbox->SetBearerToken([
                    't' => $dropbox_access_token,
                    'account_id' => $dropbox_account_id
                ]);

                $result['is_authorized'] = $this->dropbox->IsAuthorized();
                $result['result'] = [];

                $backups = $this->backup_model->get();
                foreach ($backups as $item) {
                    array_push($result['result'], [
                        'created_time' => date('H:i d/m/Y', $item->created_time),
                        'file_name' => basename($item->file_name),
                        'size' => format_number(intval($item->file_size) / 1024, 1) . ' KB',
                        'id' => $item->id
                    ]);
                }

                json_success(null, $result);
            }

            if ($type == 'get_token') {
                $redirect_url = $this->dropbox->BuildAuthorizeUrl();
                json_success(null, ['redirect_url' => $redirect_url]);
            }

            if ($type == 'download') {
                $id = $this->input->get('id');
                if ($id != null) {
                    $backup = $this->backup_model->get($id);
                    if ($backup != null) {
                        $dropbox_access_token = null;
                        $dropbox_account_id = null;

                        #region lấy cấu hình dropbox
                        $config = $this->option_model->get('dropbox_access_token');
                        if ($config != null) {
                            if ($config->value != null) {
                                if (!empty($config->value)) {
                                    $dropbox_access_token = $config->value;
                                }
                            }
                        }

                        $config = $this->option_model->get('dropbox_account_id');
                        if ($config != null) {
                            if ($config->value != null) {
                                if (!empty($config->value)) {
                                    $dropbox_account_id = $config->value;
                                }
                            }
                        }
                        #endregion

                        $this->dropbox->SetBearerToken(array(
                            't' => $dropbox_access_token,
                            'account_id' => $dropbox_account_id
                        ));

                        if ($this->dropbox->IsAuthorized()) {
                            $files = $this->dropbox->Search("/Backup/{$_SERVER['HTTP_HOST']}/", $backup->file_name);
                            if (count($files) == 0)
                                json_error('Không tìm thấy file trên Dropbox');

                            $file = $this->dropbox->GetLink($files[0]->path);
                            json_success(null, ['file' => $file]);
                        }

                        json_error('Vui lòng xác thực lại Dropbox');
                    }

                    json_error('Không tìm thấy file backup');
                }

                json_error('Không tìm thấy ID');
            }
        }

        json_error();
    }

    public function POST_index()
    {
        $type = $this->input->post('type');
        if ($type != null) {
            if ($type == 'save_token') {
                $code = $this->input->post('token_code');
                if ($code != null) {
                    $error = false;
                    try {
                        $token = $this->dropbox->GetBearerToken($code);
                        $this->option_model->save('dropbox_access_token', $token['t']);
                        $this->option_model->save('dropbox_account_id', $token['account_id']);
                        $this->option_model->save('dropbox_code', $code);
                        $this->option_model->save('dropbox_path', "/Backup/{$_SERVER['HTTP_HOST']}/");
                    } catch (DropboxException $exception) {
                        $error = true;
                    }

                    if ($error)
                        json_error('Token đã hết hạn. Vui lòng lấy token mới');
                    json_success('Kết nối Dropbox thành công');
                }

                json_error('Vui lòng nhập mã code');
            }

            if ($type == 'backup_now') {
                $app = [
                    'app_key' => $this->dropbox_config['app_key'],
                    'app_secret' => $this->dropbox_config['app_secret'],
                    'app_full_access' => true
                ];

                $dropbox_access_token = null;
                $dropbox_account_id = null;

                #region lấy cấu hình dropbox
                $config = $this->option_model->get('dropbox_access_token');
                if ($config != null) {
                    if ($config->value != null) {
                        if (!empty($config->value)) {
                            $dropbox_access_token = $config->value;
                        }
                    }
                }

                $config = $this->option_model->get('dropbox_account_id');
                if ($config != null) {
                    if ($config->value != null) {
                        if (!empty($config->value)) {
                            $dropbox_account_id = $config->value;
                        }
                    }
                }
                #endregion

                $this->load->library('dropbox', $app);
                $this->dropbox->SetBearerToken([
                    't' => $dropbox_access_token,
                    'account_id' => $dropbox_account_id
                ]);

                //  if authorized dropbox
                if ($this->dropbox->IsAuthorized()) {
                    $this->load->dbutil();
                    $prefs = [
                        'format' => 'zip',                       // gzip, zip, txt
                        'filename' => 'backup_db.sql'              // File name - NEEDED ONLY WITH ZIP FILES
                    ];
                    $backup = $this->dbutil->backup($prefs);
                    $file_name = 'backup-' . date('H-i-s-d-m-Y') . '.zip';
                    $file_path = "./uploads/$file_name";
                    write_file($file_path, $backup);

                    $upload = $this->dropbox->UploadFile($file_path, "/Backup/{$_SERVER['HTTP_HOST']}/$file_name");
                    $id = $this->backup_model->save_history($file_name, $upload->size);
                    unlink($file_path);

                    json_success(null, ['data' => [
                        'id' => $id,
                        'file_name' => $file_name,
                        'size' => format_number(intval($upload->size) / 1024, 1) . ' KB',
                        'time' => time()
                    ]]);
                }

                json_error('Vui lòng xác thực lại Dropbox');
            }
        }

        json_error();
    }

    public function DELETE_index()
    {
        $id = $this->input->input_stream('id');
        $backup = $this->backup_model->get($id);

        if ($backup != null) {

            $dropbox_access_token = null;
            $dropbox_account_id = null;

            #region lấy cấu hình dropbox
            $config = $this->option_model->get('dropbox_access_token');
            if ($config != null) {
                if ($config->value != null) {
                    if (!empty($config->value)) {
                        $dropbox_access_token = $config->value;
                    }
                }
            }

            $config = $this->option_model->get('dropbox_account_id');
            if ($config != null) {
                if ($config->value != null) {
                    if (!empty($config->value)) {
                        $dropbox_account_id = $config->value;
                    }
                }
            }
            #endregion

            $this->dropbox->SetBearerToken(array(
                't' => $dropbox_access_token,
                'account_id' => $dropbox_account_id
            ));

            if ($this->dropbox->IsAuthorized()) {
                $files = $this->dropbox->Search("/Backup/{$_SERVER['HTTP_HOST']}/", $backup->file_name);
                $this->backup_model->delete($id);

                if (count($files) == 0)
                    json_error('Không tìm thấy file trên Dropbox');

                $this->dropbox->Delete($files[0]->path);
                json_success('Xóa file backup thành công');
            }
            json_error('Vui lòng xác thực lại Dropbox');
        }
        json_error('Không tìm thấy file backup');
    }
}