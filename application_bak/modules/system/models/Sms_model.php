<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Sms_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Sms_model extends \CI_Model
{
    public function insert(string $send_to, string $content)
    {
        $data = [
            'content' => $content,
            'created_time' => time(),
            'send_to' => $send_to
        ];
        $this->db->insert('sms_jobs', $data);
    }

    public function get($id = null)
    {
        if ($id == null) {
            $this->db->where('sent', 0);
            $this->db->limit(1);
        } else
            $this->db->where('id', $id);

        return $this->db->get('sms_jobs')->row();
    }

    public function update_sent(int $id, string $error_log = null)
    {
        $data = [
            'sent' => 1,
            'sent_time' => time()
        ];

        if ($error_log != null) {
            if (!empty($error_log))
                $data['error_log'] = $error_log;
        }

        $this->db->where('id', $id);
        $this->db->update('sms_jobs', $data);
    }

    public function delete(int $day = 30)
    {
        $this->db->where('sent_time <=', $day * 24 * 3600);
        $this->db->delete('sms_jobs');
    }
}