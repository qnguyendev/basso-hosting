<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Log_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Log_model extends CI_Model
{
    private $_table = 'system_logs';

    public function insert($object_id, $controller, $action, $content)
    {
        $this->db->insert($this->_table, [
            'created_time' => time(),
            'controller' => $controller,
            'action' => $action,
            'content' => $content,
            'object_id' => $object_id
        ]);
    }

    public function get_by_object($object_id)
    {
        $this->db->where('object_id', $object_id);
        $this->db->order_by('created_time', 'desc');
        return $this->db->get($this->_table)->result();
    }
}