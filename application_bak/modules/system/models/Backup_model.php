<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Backup_model
 * @property CI_Driver|CI_DB_mysql_driver|CI_DB_driver $db
 */
class Backup_model extends CI_Model
{
    /**
     * save dropbox backup file history
     * @param $file_name
     * @param $file_size
     * @return int
     */
    public function save_history(string $file_name, int $file_size)
    {
        $data = [
            'created_time' => time(),
            'file_name' => $file_name,
            'file_size' => $file_size
        ];

        $this->db->insert('backup_histories', $data);
        return $this->db->insert_id();
    }

    /**
     * get all backup which older than max day
     * @param int $max_day the number of day's older
     * @return array
     */
    public function get_older_backups(int $max_day)
    {
        $this->db->where('created_time <=', time() - intval($max_day) * 24 * 3600);
        return $this->db->get('backup_histories')->result();
    }

    /**
     * delete backup file
     * @param $id
     */
    public function delete(int $id)
    {
        $this->db->where('id', $id);
        $this->db->delete('backup_histories');
    }

    public function get(int $id = null)
    {
        if ($id != null) {
            $this->db->where('id', $id);
            return $this->db->get('backup_histories')->row();
        }

        $this->db->order_by('created_time', 'desc');
        return $this->db->get('backup_histories')->result();
    }
}