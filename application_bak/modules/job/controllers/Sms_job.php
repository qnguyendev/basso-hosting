<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Sms
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Sms_model $sms_model
 */
class Sms_job extends Worker_Controller
{
    private $sms_config;

    public function __construct()
    {
        parent::__construct();
        $this->input->is_cli_request() or exit("Execute via command line: php index.php migrate");

        $config_file = "./theme/config.php";
        if (!is_file($config_file))
            return;

        require "./theme/config.php";
        if (!isset($theme_config))
            $theme_config = [];

        $this->sms_config = $theme_config['sms'];
        $this->load->model('system/sms_model');
    }

    public function run()
    {
        if ($this->worker_enable) {
            while ($this->worker_status) {
                $data = $this->reverse_job('sms', false);
                if ($data != null) {
                    if (is_numeric($data))
                        $this->sms_model->get($data);

                    $this->send($data);
                }
            }
        } else {
            $sms_job = $this->sms_model->get();
            $this->send($sms_job);
        }
    }

    private function send($job)
    {
        if ($job == null)
            return;

        try {
            $guzzleHttp = new GuzzleHttp\Client([
                'verify' => false
            ]);

            $url = 'http://rest.esms.vn/MainService.svc/json/SendMultipleMessage_V4_get';
            $params = [
                'Phone' => $job->send_to,
                'Content' => urlencode($job->content),
                'SmsType' => $this->sms_config['type'],
                'ApiKey' => $this->sms_config['api_key'],
                'SecretKey' => $this->sms_config['secret_key']
            ];

            if (isset($this->sms_config['brandname'])) {
                if ($this->sms_config['brandname'] != null) {
                    $params['Brandname'] = $this->sms_config['brandname'];
                }
            }

            $data = [];
            foreach ($params as $key => $value)
                array_push($data, "$key=$value");

            $url .= '?' . join('&', $data);
            $http_response = $guzzleHttp->request('GET', $url);
            $response = json_decode($http_response->getBody()->getContents());

            $error_log = null;
            if (intval($response->CodeResult) != 100) {
                $error_log = $response->CodeResult;
                if (isset($response->ErrorMessage))
                    $error_log .= '|' . $response->ErrorMessage;
            }

            if (isset($job->id)) {
                if ($job->id != null)
                    $this->sms_model->update_sent($job->id, $error_log);
            }
        } catch (Exception $e) {
            log_message('error', $e->getMessage());
        }
    }

    public function clean()
    {
        $this->sms_model->delete(30);
    }
}