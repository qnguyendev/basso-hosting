<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Customer_group_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Customer_group_model extends CI_Model
{
    /**
     * Thêm nhóm khách hàng
     * @param int $id
     * @param string $name
     * @param string $des
     * @return bool
     */
    public function insert(int $id = 0, string $name, string $des)
    {
        $this->db->trans_begin();
        $data = ['name' => $name, 'description' => $des];
        if ($id == 0)
            $this->db->insert('customer_groups', $data);
        else {
            $this->db->where('id', $id);
            $this->db->update('customer_groups', $data);
        }
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function get(int $id = null)
    {
        if ($id == null) {
            $this->db->order_by('name', 'asc');
            return $this->db->get('customer_groups')->result();
        }

        $this->db->where('id', $id);
        return $this->db->get('customer_groups')->row();
    }

    /**
     * xóa nhóm khách hàng
     * @param int $id
     * @return bool|mixed
     */
    public function delete(int $id)
    {
        $this->db->trans_begin();
        $this->db->where('id', $id);
        return $this->db->delete('customer_groups');
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }
}