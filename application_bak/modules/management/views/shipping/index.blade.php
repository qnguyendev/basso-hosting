<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-sm-12 col-md-8 offset-md-2">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">
                        <a href="javascript:" class="float-right" data-bind="click: add_shipping">
                            <i class="fa fa-plus"></i>
                            Thêm đơn vị
                        </a>
                        Đơn vị vận chuyển
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Đơn vị</th>
                            <th>Hotline</th>
                            <th>Website</th>
                            <th>Thao tác</th>
                        </tr>
                        </thead>
                        <tbody class="text-center" data-bind="foreach: result">
                        <tr>
                            <td data-bind="text: name"></td>
                            <td data-bind="text: phone"></td>
                            <td data-bind="text: website"></td>
                            <td>
                                <button class="btn btn-xs btn-info" data-bind="click: $root.edit">
                                    <i class="fa fa-edit"></i>
                                    Cập nhật
                                </button>
                                <button class="btn btn-xs btn-danger" data-bind="click: $root.delete">
                                    <i class="icon-trash"></i>
                                    Xóa
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    @include('partial/editor_modal')
@endsection
@section('script')
    <script src="{{load_js('index')}}"></script>
@endsection