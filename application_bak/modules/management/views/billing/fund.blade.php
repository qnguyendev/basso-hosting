<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="card card-default">
        <div class="card-header">
            <div class="card-title">
                Sổ quỹ
                <button class="float-right btn btn-xs btn-success" data-bind="click: $root.export">
                    <i class="icon-share-alt"></i>
                    Xuất Excel
                </button>
            </div>
        </div>
        <div class="card-body">

            <div class="row">
                <div class="col-12 col-md-6 col-lg-2">
                    <div class="form-group">
                        <label>Thời gian</label>
                        <select class="form-control" data-bind="value: filter.time">
                            <option value="all">Tất cả</option>
                            <option value="today">Hôm nay</option>
                            <option value="week">Trong tuần</option>
                            <option value="month">Trong tháng</option>
                            <option value="custom">Tùy chọn</option>
                        </select>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-2">
                    <div class="form-group">
                        <label>Từ ngày</label>
                        <input type="text" class="form-control"
                               data-bind="datePicker: filter.start, enable: filter.time() == 'custom'"/>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-2">
                    <div class="form-group">
                        <label>đến ngày</label>
                        <input type="text" class="form-control"
                               data-bind="datePicker: filter.end, enable: filter.time() == 'custom'"/>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-2">
                    <div class="form-group">
                        <label>Đối tượng</label>
                        <select class="form-control" data-bind="value: filter.group">
                            <option value="all">Tất cả</option>
                            @foreach(BillingGroup::LIST as $key=>$value)
                                <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-2">
                    <label>&nbsp;</label>
                    <button class="btn btn-block btn-primary" data-bind="click: function(){$root.search(1)}">
                        TÌM
                    </button>
                </div>
            </div>

            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>STT</th>
                    <th>Loại phiếu</th>
                    <th>Ngày ghi nhận</th>
                    <th>Mã phiếu</th>
                    <th>Người nhận/nộp</th>
                    <th>PTTT</th>
                    <th class="text-right">Tiền thu</th>
                    <th class="text-right">Tiền chi</th>
                    <th>Mô tả</th>
                </tr>
                </thead>
                <tbody data-bind="foreach: result" class="text-center">
                <tr>
                    <th data-bind="text: $index() + 1"></th>
                    <td data-bind="text: term"></td>
                    <td data-bind="text: moment.unix(created_time()).format('DD/MM/YYYY')"></td>
                    <td data-bind="text: id"></td>
                    <td data-bind="text: name"></td>
                    <td>
                        @foreach(BillingPaymentMethod::LIST as $key=>$value)
                            <span data-bind="visible: payment_method() == '{{$key}}'">{{$value}}</span>
                        @endforeach
                    </td>
                    <td class="text-right">
                        <span data-bind="text: parseInt(amount()).toMoney(0), visible: type() == '{{BillingType::IN}}'"></span>
                    </td>
                    <td class="text-right">
                        <span data-bind="text: parseInt(amount()).toMoney(0), visible: type() == '{{BillingType::OUT}}'"></span>
                    </td>
                    <td data-bind="text: description"></td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <th colspan="6">TỔNG</th>
                    <th class="text-right" data-bind="text: parseInt(total_in()).toMoney(0)"></th>
                    <th class="text-right" data-bind="text: parseInt(total_out()).toMoney(0)"></th>
                    <th></th>
                </tr>
                </tfoot>
            </table>

            @include('pagination')
        </div>
    </div>
@endsection

@section('script')
    <script src="{{load_js('fund')}}"></script>
@endsection