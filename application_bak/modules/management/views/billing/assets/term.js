function viewModel() {
    let self = this;
    self.result = ko.mapping.fromJS([]);
    self.id = ko.observable(0);
    self.name = ko.observable().extend({required: {message: LABEL.required}});
    self.description = ko.observable();

    self.init = function () {
        AJAX.get(window.location, null, true, (res) => {
            ko.mapping.fromJS(res.data, self.result);
        });
    };

    self.add_modal = function () {
        self.id(0);
        self.name(undefined);
        self.description(undefined);
        self.errors.showAllMessages(false);
        MODAL.show('#term-editor-modal');
    };

    self.delete = function (item) {
        ALERT.confirm('Xác nhận xóa danh mục thu chi?', null, function () {
            AJAX.delete(window.location, {id: item.id()}, true, (res) => {
                if (res.error)
                    ALERT.error(res.message);
                else {
                    ALERT.success(res.message);
                    self.result.remove(item);
                }
            });
        });
    };

    self.edit = function (item) {
        self.id(item.id());
        self.name(item.name());
        self.description(item.description());

        MODAL.show('#term-editor-modal');
    };

    self.save = function () {
        if (!self.isValid())
            self.errors.showAllMessages();
        else {
            let data = {id: self.id(), name: self.name(), des: self.description()};
            AJAX.post(window.location, data, true, (res) => {
                if (res.error)
                    ALERT.error(res.message);
                else {
                    MODAL.hide('#term-editor-modal');
                    ALERT.success(res.message);
                    self.init();
                }
            });
        }
    };
}

let model = new viewModel();
model.init();
ko.validatedObservable(model);
ko.applyBindings(model, document.getElementById('main-content'));