<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="card card-default">
        <div class="card-header">
            <div class="card-title">
                Phiếu chi
                <a href="#" class="float-right" data-bind="click: modal.show">
                    <i class="fa fa-plus"></i>
                    Tạo phiếu chi
                </a>
                <a href="#" class="float-right mr-2 text-success" data-bind="click: export_excel">
                    <i class="fa fa-file-export"></i>
                    Xuất Excel
                </a>
            </div>
        </div>
        <form autocomplete="off" class="card-body">

            <div class="row">
                <div class="col-12 col-md-6 col-lg-2">
                    <div class="form-group">
                        <label>Thời gian</label>
                        <select class="form-control" data-bind="value: filter.time">
                            <option value="all">Tất cả</option>
                            <option value="today">Hôm nay</option>
                            <option value="week">Trong tuần</option>
                            <option value="month">Trong tháng</option>
                            <option value="custom">Tùy chọn</option>
                        </select>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-2">
                    <div class="form-group">
                        <label>Từ ngày</label>
                        <input type="text" class="form-control"
                               data-bind="datePicker: filter.start, enable: filter.time() == 'custom'"/>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-2">
                    <div class="form-group">
                        <label>đến ngày</label>
                        <input type="text" class="form-control"
                               data-bind="datePicker: filter.end, enable: filter.time() == 'custom'"/>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-2">
                    <div class="form-group">
                        <label>PTTT</label>
                        <select class="form-control select2" data-bind="value: filter.payment_method">
                            <option value="all">Tất cả</option>
                            @foreach($payment_method as $item)
                                <option value="{{$item->name}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="color-12 col-md-6 col-lg-2">
                    <label>Lọc</label>
                    <input type="text" class="form-control" data-bind="value: filter.key"
                           placeholder="Mã phiếu, mã đơn hàng"/>
                </div>
                <div class="col-12 col-md-6 col-lg-2">
                    <label>&nbsp;</label>
                    <button class="btn btn-block btn-primary" data-bind="click: function(){$root.search(1)}">
                        TÌM
                    </button>
                </div>
            </div>

            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>Mã phiếu</th>
                    <th>Đối tượng</th>
                    <th>Loại phiếu</th>
                    <th>PTTT</th>
                    <th>Người tạo</th>
                    <th>Số tiền chi</th>
                    <th>Ngày ghi nhận</th>
                    <th>Nội dung</th>
                </tr>
                </thead>
                <tbody data-bind="foreach: result" class="text-center">
                <tr>
                    <td>
                        <a data-bind="visible: web_order_id() != null, text: id,
                        attr: {href: `/basso/web_order/detail/${web_order_id()}`}"></a>
                        <a data-bind="visible: customer_order_id() != null, text: id,
                        attr: {href: `/basso/web_order/detail/${customer_order_id()}`}"></a>
                        <span data-bind="text: id, visible: web_order_id() == null && customer_order_id() == null"></span>
                    </td>
                    <td>
                        @foreach(BillingGroup::LIST as $key=>$value)
                            <span data-bind="visible: group == '{{$key}}'">{{$value}}</span>
                        @endforeach
                    </td>
                    <td data-bind="text: term"></td>
                    <td>
                        @foreach(BillingPaymentMethod::LIST as $key=>$value)
                            <span data-bind="visible: payment_method() == '{{$key}}'">{{$value}}</span>
                        @endforeach
                        <span data-bind="text: payment_method(), visible: term_id() == 1"></span>
                    </td>
                    <td data-bind="text: term_id() == 1 ? name() : user()"></td>
                    <td data-bind="text: parseInt(amount()).toMoney(0)"></td>
                    <td data-bind="text: moment.unix(time()).format('DD/MM/YYYY')"></td>
                    <td data-bind="text: description"></td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <th colspan="5">TỔNG</th>
                    <th data-bind="text: parseInt(total()).toMoney(0)"></th>
                    <th></th>
                    <th></th>
                </tr>
                </tfoot>
            </table>

            @include('pagination')
        </form>
    </div>
@endsection
@section('modal')
    @include('partial/billing_editor_modal')
@endsection
@section('script')
    <script src="{{load_js('out')}}"></script>
@endsection