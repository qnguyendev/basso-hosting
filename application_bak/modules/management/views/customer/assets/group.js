function viewModel() {
    let self = this;
    self.id = ko.observable(0).extend({requried: {message: LABEL.required}});
    self.name = ko.observable().extend({requried: {message: LABEL.required}});
    self.des = ko.observable();
    self.result = ko.mapping.fromJS([]);

    self.init = function () {
        AJAX.get(window.location, null, true, (res) => {
            ko.mapping.fromJS(res.data, self.result);
        });
    };

    self.add = function () {
        self.id(0);
        self.name(undefined);
        self.des(undefined);
        self.errors.showAllMessages(false);
        MODAL.show('#group-editor-modal');
    };

    self.edit = function (item) {
        self.id(item.id());
        self.name(item.name());
        self.des(item.description());
        MODAL.show('#group-editor-modal');
    };

    self.delete = function (item) {
        ALERT.confirm('Xác nhận xóa nhóm', null, () => {
            AJAX.delete(window.location, {id: item.id()}, true, (res) => {
                if (res.error)
                    ALERT.error(res.message);
                else {
                    ALERT.success(res.message);
                    self.result.remove(item);
                }
            });
        });
    };

    self.save = function () {
        if (!self.isValid())
            self.errors.showAllMessages(false);
        else {
            let data = {id: self.id(), name: self.name(), des: self.des()};
            AJAX.post(window.location, data, true, (res) => {
                if (res.error)
                    ALERT.error(res.message);
                else {
                    self.init();
                    MODAL.hide('#group-editor-modal');
                    ALERT.success(res.message);
                }
            });
        }
    };
}

let model = new viewModel();
model.init();
ko.validatedObservable(model);
ko.applyBindings(model, document.getElementById('main-content'));