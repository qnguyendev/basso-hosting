<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="group-editor-modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-notice">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"
                    data-bind="text: id() == 0 ? 'Thêm nhóm khách hàng' : 'Cập nhật nhóm khách hàng'">
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>
                        Tên nhóm khách hàng
                        <span class="text-danger">*</span>
                        <span class="validationMessage" data-bind="validationMessage: name"></span>
                    </label>
                    <input type="text" class="form-control" data-bind="value: name"/>
                </div>
                <div class="form-group">
                    <label>Mô tả nhóm</label>
                    <input type="text" class="form-control" data-bind="value: des"/>
                </div>
            </div>
            <div class="modal-footer text-center">
                <button class="btn btn-success" data-bind="click: $root.save">
                    <i class="fa fa-check"></i>
                    Lưu lại
                </button>
            </div>
        </div>
    </div>
</div>