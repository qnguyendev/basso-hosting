//Knockout Binding
(function (ko) {
    ko.bindingHandlers.datePicker = {
        init: function (element, valueAccessor, allBindingsAccessor) {
            jQuery.datetimepicker.setLocale('vi');
            var $element = $(element);
            var myFormatter = {
                parseDate: function (vDate, vFormat) {
                    return moment(vDate, vFormat).toDate();
                },
                guessDate: function (vDateStr, vFormat) {
                    return moment(vDateStr, vFormat).toDate();
                },
                parseFormat: function (vChar, vDate) {
                    return vDate;
                },
                formatDate: function (vChar, vDate) {
                    var res = moment(vChar).format(vDate);
                    return res;
                }
            };

            $.datetimepicker.setDateFormatter(myFormatter);
            $element.datetimepicker({
                step: 15,
                format: "DD-MM-YYYY",
                formatTime: "H:mm",
                formatDate: "DD-MM-YYYY",
                yearStart: 2000,
                timepicker: false,
                yearEnd: (new Date().getFullYear()) + 1,
                scrollMonth: false,
                scrollInput: false,
                onChangeDateTime: function (dp, $input) {
                    ko.unwrap(valueAccessor());
                    var date = moment($input.val(), "DD-MM-YYYY H:mm:ss");
                    date.set('second', 0);
                    var observable = valueAccessor();
                    if (date.isValid())
                        observable(date.format('DD-MM-YYYY'));
                    else
                        observable(null);

                }
            });

        },
        update: function (element, valueAccessor) {
            ko.unwrap(valueAccessor());
            var valueUnwrapped = ko.utils.unwrapObservable(valueAccessor());
            var date = moment(valueUnwrapped, "DD-MM-YYYY H:mm:ss");
            if (date.isValid())
                $(element).val(date.format("DD-MM-YYYY"));
            else
                $(element).val(null);
        }
    };
})(ko);

(function (ko) {
    ko.bindingHandlers.formatDateTime = {
        init: function (element, value, allBindingsAccessor) {
            observable(moment(value).format('HH:mm DD/MM/YYYY'));
        }
    }
})(ko);

(function (ko) {
    ko.bindingHandlers.dateTimePicker = {
        init: function (element, valueAccessor, allBindingsAccessor) {
            jQuery.datetimepicker.setLocale('vi');
            var $element = $(element);
            var myFormatter = {
                parseDate: function (vDate, vFormat) {
                    return moment(vDate, vFormat).toDate();
                },
                guessDate: function (vDateStr, vFormat) {
                    return moment(vDateStr, vFormat).toDate();
                },
                parseFormat: function (vChar, vDate) {
                    return vDate;
                },
                formatDate: function (vChar, vDate) {
                    var res = moment(vChar).format(vDate);
                    return res;
                }
            };

            $.datetimepicker.setDateFormatter(myFormatter);
            $element.datetimepicker({
                step: 15,
                format: "DD-MM-YYYY HH:mm",
                formatTime: "H:mm",
                formatDate: "DD-MM-YYYY",
                yearStart: 2000,
                timepicker: true,
                yearEnd: (new Date().getFullYear()) + 1,
                onChangeDateTime: function (dp, $input) {
                    var date = moment($input.val(), "DD-MM-YYYY H:mm:ss");
                    date.set('second', 0);
                    var observable = valueAccessor();
                    observable(date.format("DD-MM-YYYY HH:mm"));
                },
                scrollMonth: false,
                scrollInput: false
            });

        },
        update: function (element, valueAccessor) {
            var valueUnwrapped = ko.utils.unwrapObservable(valueAccessor());
            $(element).val(moment(valueUnwrapped, "DD-MM-YYYY H:mm:ss").format("DD-MM-YYYY HH:mm"));
        }
    };
})(ko);

ko.validation.rules.comparePass = {
    validator: function (val, params) {
        var otherValue = params;
        return val === ko.validation.utils.getValue(otherValue);
    },
    message: 'Xác nhận mật khẩu không khớp',
};

ko.validation.rules.minVal = {
    validator: function (val, min) {
        val = parseInt(val, 10);
        if (!isNaN(val))
            return val >= min;
        return false;
    },
    message: 'Please enter a value greater than or equal to {0}.'
};

ko.validation.rules.maxVal = {
    validator: function (val, max) {
        val = parseInt(val, 10);
        if (!isNaN(val))
            return val <= max;
        return false;
    },
    message: 'Please enter a value less than or equal to {0}.'
};

ko.validation.rules.between = {
    validator: function (value, params) {
        var min = params[0];
        var max = params[1];

        value = parseInt(value, 10);

        if (!isNaN(value)) {
            return value >= min && value <= max;
        }
        return false;
    },
    message: 'Value must be between {0} and {1}'
};

ko.validation.rules.minDateTime = {
    validator: function (value, params) {
        return moment(value) > moment(params);
    },
    message: 'Time input must be greater than {0}'
};

ko.validation.rules.url = {
    validator: function (val, required) {
        if (!val) {
            return !required
        }
        val = val.replace(/^\s+|\s+$/, ''); //Strip whitespace
        //Regex by Diego Perini from: http://mathiasbynens.be/demo/url-regex
        return val.match(/^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.‌​\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[‌​6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1‌​,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00‌​a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u‌​00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i);
    },
    message: 'Không đúng định dạng link'
};
ko.validation.registerExtenders();

ko.validation.registerExtenders();
ko.validation.configure({
    insertMessages: false,
    decorateElement: true,
    decorateInputElement: true
});

ko.bindingHandlers.moneyMask = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        ko.utils.registerEventHandler(element, 'keyup', function (event) {
            var val = $(element).val().replace(/[^0-9]/g, '');
            var observable = valueAccessor();
            observable(moneyFormat(val));
            observable.notifySubscribers(5);
        });

    },
    update: function (element, valueAccessor, allBindingsAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).val(value);
    }
};

function moneyFormat(value) {

    value += '';

    value = value.replace(/,/g, '');
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(value)) {
        value = value.replace(rgx, '$1' + ',' + '$2');
    }

    return value;
}

ko.bindingHandlers.tagsinput = {
    init: function (element, valueAccessor, allBindings) {
        var options = allBindings().tagsinputOptions || {};
        var value = valueAccessor();
        var valueUnwrapped = ko.unwrap(value);

        var el = $(element);

        el.tagsinput(options);

        for (var i = 0; i < valueUnwrapped.length; i++) {
            el.tagsinput('add', valueUnwrapped[i]);
        }

        el.on('itemAdded', function (event) {
            if (valueUnwrapped.indexOf(event.item) === -1) {
                value.push(event.item);
            }
        })

        el.on('itemRemoved', function (event) {
            value.remove(event.item);
        });
    },
    update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
        var value = valueAccessor();
        var valueUnwrapped = ko.unwrap(value);

        var el = $(element);
        var prev = el.tagsinput('items');

        var added = valueUnwrapped.filter(function (i) {
            return prev.indexOf(i) === -1;
        });
        var removed = prev.filter(function (i) {
            return valueUnwrapped.indexOf(i) === -1;
        });

        // Remove tags no longer in bound model
        for (var i = 0; i < removed.length; i++) {
            el.tagsinput('remove', removed[i]);
        }

        // Refresh remaining tags
        el.tagsinput('refresh');

        // Add new items in model as tags
        for (i = 0; i < added.length; i++) {
            el.tagsinput('add', added[i]);
        }
    }
};

ko.bindingHandlers.singleClick = {
    init: function (element, valueAccessor) {
        var handler = valueAccessor(),
            delay = 200,
            clickTimeout = false;

        $(element).click(function () {
            if (clickTimeout !== false) {
                clearTimeout(clickTimeout);
                clickTimeout = false;
            } else {
                clickTimeout = setTimeout(function () {
                    clickTimeout = false;
                    handler();
                }, delay);
            }
        });
    }
};
