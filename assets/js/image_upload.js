function imageUploadModel() {
    let self = this;
    self.image_uploader = {
        is_single: ko.observable(true),
        result: ko.observableArray([]),
        selected_id: ko.observableArray([]),
        selected_images: [],
        show: function (is_single) {
            self.image_uploader.result([]);
            self.image_uploader.selected_id([]);
            self.image_uploader.is_single(is_single);

            MODAL.show('#image-upload-modal');
        },
        select: function (item) {
            if (self.image_uploader.selected_id.indexOf(item.id) > -1)
                self.image_uploader.selected_id.remove(item.id);
            else {
                if (self.image_uploader.is_single() == false) {
                    self.image_uploader.selected_id.push(item.id);
                } else {
                    self.image_uploader.selected_id([]);
                    self.image_uploader.selected_id.push(item.id);
                }
            }

            self.image_uploader.reload();
        },
        reload: function () {
            self.image_uploader.selected_images = self.image_uploader.result.filter((x) => {
                return self.image_uploader.selected_id.indexOf(x.id) >= 0;
            });
        },
        close: function () {
            MODAL.hide('#image-upload-modal');
        },
        upload: {
            do: function (item, event) {
                if (event.target.files.length === 0) {
                    NOTI.danger('Vui lòng chọn file');
                    return;
                }

                let formData = new FormData();
                for (let i = 0; i < event.target.files.length; i++) {
                    let file = event.target.files[i];
                    if (file.size < 5 * 1024 * 1024) {
                        if (file.type == "image/jpeg" || file.type == "image/jpg" || file.type == "image/png") {
                            formData.append('files[]', file);
                        }
                    }
                }

                $('#image-upload-modal input[type=file]').val(null);
                AJAX.upload(URL.upload_image, formData, true, (res) => {
                    for (let i = 0; i < res.data.length; i++) {
                        self.image_uploader.result.push(res.data[i]);
                        if (!self.image_uploader.is_single())
                            self.image_uploader.selected_id.unshift(res.data[i].id);
                        else {
                            if (self.image_uploader.selected_id().length == 0)
                                self.image_uploader.selected_id.push(res.data[0].id);
                        }
                    }
                    self.image_uploader.reload();
                });
            }
        },
        external: {
            url: ko.observable(),
            add: function () {
                if (self.image_uploader.external.url() == undefined) {
                    NOTI.danger('Vui lòng nhập đường dẫnh ảnh');
                    return;
                }

                if (self.image_uploader.external.url() == null) {
                    NOTI.danger('Vui lòng nhập đường dẫnh ảnh');
                    return;
                }

                if (self.image_uploader.external.url().length == 0) {
                    NOTI.danger('Vui lòng nhập đường dẫnh ảnh');
                    return;
                }

                AJAX.post(URL.upload_image, {url: self.image_uploader.external.url()}, true, (res) => {
                    for (let i = 0; i < res.data.length; i++) {
                        self.image_uploader.result.push(res.data[i]);
                        if (!self.image_uploader.is_single())
                            self.image_uploader.selected_id.unshift(res.data[i].id);
                        else {
                            if (self.image_uploader.selected_id().length == 0)
                                self.image_uploader.selected_id.push(res.data[0].id);
                        }
                    }

                    self.image_uploader.reload();
                    self.image_uploader.external.url(undefined);
                });
            }
        },
        clipboard: {
            do: function (image) {
                let formData = new FormData();
                formData.append('files[]', image);
                AJAX.upload(URL.upload_image, formData, true, (res) => {
                    for (let i = 0; i < res.data.length; i++) {
                        self.image_uploader.result.push(res.data[i]);
                        if (!self.image_uploader.is_single())
                            self.image_uploader.selected_id.unshift(res.data[i].id);
                        else {
                            if (self.image_uploader.selected_id().length == 0)
                                self.image_uploader.selected_id.push(res.data[0].id);
                        }
                    }
                    self.image_uploader.reload();
                });
            }
        }
    };

    document.onpaste = function (event) {
        if ($('#paste-image-area').hasClass('active') && $('#image-upload-modal').hasClass('show')) {
            var items = (event.clipboardData || event.originalEvent.clipboardData).items;
            for (var i = 0; i < items.length; i++) {
                var item = items[i];
                if (item.type.indexOf("image") != -1) {
                    var file = item.getAsFile();
                    self.image_uploader.clipboard.do(file);
                }
            }
        }
    }
}
