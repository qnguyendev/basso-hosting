function seoViewModel(title, des) {
    let self = this;
    self.seo_editor = {
        title: ko.observable(),
        description: ko.observable(),
        index: ko.observable(1),
        preview: {
            title: ko.observable(LABEL.default_seo_title),
            description: ko.observable(LABEL.default_seo_des)
        },
        change: function () {
            if (self.seo_editor.title() !== undefined) {
                if (self.seo_editor.title() !== null)
                    if (self.seo_editor.title().length > 0)
                        title = self.seo_editor.title();
            }

            if (self.seo_editor.description() !== undefined) {
                if (self.seo_editor.description() !== null)
                    if (self.seo_editor.description().length > 0)
                        des = self.seo_editor.description();
            }

            if (title == null)
                title = LABEL.default_seo_title;
            if (des == null)
                des = LABEL.default_seo_des;

            self.seo_editor.preview.title(title);
            self.seo_editor.preview.description(des);
        }
    };

    self.seo_editor.title.subscribe(self.seo_editor.change);
    self.seo_editor.description.subscribe(self.seo_editor.change);
}