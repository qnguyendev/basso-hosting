function resetViewModel() {
    let self = this;
    self.email = ko.observable().extend({
        required: {message: 'Vui lòng nhập email'},
        email: true
    });

    self.reset = function () {
        if (!self.isValid())
            NOTI.danger('Vui lòng nhập email');
        else {
            AJAX.post(window.location, {email: self.email()}, true, (res) => {
                if (res.error)
                    ALERT.error(res.message);
                else {
                    self.email(undefined);
                    self.errors.showAllMessages(false);
                    ALERT.success(res.message);
                }
            });
        }
    };
}

let resetModel = new resetViewModel();
ko.validatedObservable(resetModel);
ko.applyBindings(resetModel);