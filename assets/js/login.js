function loginViewModel() {
    let self = this;
    self.email = ko.observable().extend({
        required: {message: 'Vui lòng nhập email'},
        email: true
    });

    self.pass = ko.observable().extend({
        required: {message: 'Vui lòng nhập mật khẩu'}
    });

    self.login = function () {
        if (!self.isValid()) {
            self.errors.showAllMessages();
        } else {
            AJAX.post(window.location, {email: self.email(), pass: self.pass()}, true, (res) => {
                if (res.error)
                    ALERT.error(res.message);
                else {
                    window.location = res.url;
                }
            });
        }
    };

    self.init = function () {
        $page = $('.full-page');
        let image_src = $page.data('image');

        if (image_src !== undefined) {
            let image_container = '<div class="full-page-background" style="background-image: url(' + image_src + ') "/>';
            $page.append(image_container);
        }
    }
}

let loginModel = new loginViewModel();
loginModel.init();
ko.validatedObservable(loginModel);
ko.applyBindings(loginModel);