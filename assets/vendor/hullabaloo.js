/**
 * hullabaloo v 0.2
 *
 */
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(['buoy'], factory(root));
    } else if (typeof exports === 'object') {
        module.exports = factory(require('buoy'));
    } else {
        root.hullabaloo = factory(root, root.buoy);
    }
})(typeof global !== 'undefined' ? global : this.window || this.global, function (root) {
    var init = function (root) {

        var hullabaloo = function () {
            // ĐĐ±ÑĐµĐºÑ‚ ÑĐ¾Đ·Đ´Đ°Đ²Đ°ĐµĐ¼Ñ‹Đ¹ ÑĐµĐ¹Ñ‡Đ°Ñ.
            // Đ³ĐµĐ½ĐµÑ€Đ¸Ñ€ÑƒĐµÑ‚ÑÑ Đ² this.generate()
            this.hullabaloo = {};

            // ĐœĐ°ÑÑĐ¸Đ² Ñ Đ¾Đ±ÑĐµĐºÑ‚Đ°Đ¼Đ¸ Đ°ĐºÑ‚Đ¸Đ²Đ½Ñ‹Ñ… Đ°Đ»ĐµÑ€Ñ‚Đ¾Đ²
            this.hullabaloos = [];

            this.success = false;

            // Đ”Đ¾Đ¿Đ¾Đ»Đ½Đ¸Ñ‚ĐµĐ»ÑŒĐ½Ñ‹Đµ Đ½Đ°ÑÑ‚Ñ€Đ¾Đ¹ĐºĐ¸ Đ´ÑĐ» Đ°Đ»ĐµÑ€Ñ‚Đ°
            this.options = {
                ele: "body",
                offset: {
                    from: "top",
                    amount: 20
                },
                align: "right",
                width: 250,
                delay: 5000,
                allow_dismiss: true,
                stackup_spacing: 10,
                text: "Notification Message Here",
                icon: "times-circle",
                status: "danger",
                alertClass: "", // Đ”Đ¾Đ¿Đ¾Đ»Đ½Đ¸Ñ‚ĐµĐ»ÑŒĐ½Ñ‹Đµ ĐºĐ»Đ°ÑÑ Đ´Đ»Ñ Đ±Đ»Đ¾ĐºĐ° Đ°Đ»ĐµÑ€Ñ‚Đ°
                fnStart: false, // Đ¤-Đ¸Ñ Đ±ÑƒĐ´ĐµÑ‚ Đ²Ñ‹Đ¿Đ¾Đ»Đ½ÑÑ‚ÑŒÑÑ Đ¿Ñ€Đ¸ ÑÑ‚Đ°Ñ€Ñ‚Đµ
                fnEnd: false, // Đ¤-Đ¸Ñ Đ±ÑƒĐ´ĐµÑ‚ Đ²Ñ‹Đ¿Đ¾Đ»Đ½ÑÑ‚ÑŒÑÑ Đ¿Đ¾ Đ·Đ°Đ²ĐµÑ€ÑˆĐ¸Đ½Đ¸Ñ
                fnEndHide: false, // Đ¤-Đ¸Ñ Đ±ÑƒĐ´ĐµÑ‚ Đ²Ñ‹Đ¿Đ¾Đ»Đ½ÑÑ‚ÑŒÑÑ Đ¿Đ¾ÑĐ»Đµ Đ·Đ°ĐºÑ€Ñ‹Ñ‚Đ¸Ñ ÑĐ¾Đ¾Đ±Ñ‰ĐµĐ½Đ¸Ñ

            };
        };

        // Đ’Ñ‹Đ²Đ¾Đ´Đ¸Đ¼ ÑĐ¾Đ¾Đ±Ñ‰ĐµĐ½Đ¸Đµ
        hullabaloo.prototype.send = function (text, status) {
            // Đ—Đ°Đ¿ÑƒÑÑ‚Đ¸Đ¼ Ñ„ÑƒĐ½ĐºÑ†Đ¸Ñ Đ¿Ñ€Đ¸ ÑÑ‚Đ°Ñ€Ñ‚Đµ
            if (typeof this.options.fnStart == "function")
                this.options.fnStart();

            // Đ¡ÑÑ‹Đ»ĐºĐ° Đ½Đ° Đ¾Đ±ÑĐµĐºÑ‚
            var self = this;
            // Đ¤Đ»Đ°Đ³ Đ´Đ»Ñ Đ¾Đ±Đ¾Đ·Đ½Đ°Ñ‡ĐµĐ½Đ¸Đµ Ñ‡Ñ‚Đ¾ Đ½Đ°Đ¹Đ´ĐµĐ½Đ½Đ° Đ³Ñ€ÑƒĐ¿Đ¿Đ° Đ¾Đ´Đ¸Đ½Đ°ĐºĐ¾Đ²Ñ‹Ñ… Đ°Đ»ĐµÑ€Ñ‚Đ¾Đ²
            var flag = 1;
            // Đ¡Ñ‡ĐµÑ‚Ñ‡Đ¸Đº Đ´Đ»Ñ Đ¿ĐµÑ€ĐµĐ±Đ¾Ñ€ĐºĐ¸ Đ²ÑĐµÑ… Đ°Đ»ĐµÑ€Ñ‚Đ¾Đ². ĐŸĐ¾Đ¸ÑĐº Đ¾Đ´Đ¸Đ½Đ°ĐºĐ¾Đ²Ñ‹Ñ…
            var i = +this.hullabaloos.length - 1;
            // Đ“Đ»Đ°Đ²Đ½Ñ‹Đ¹ Đ°Đ»ĐµÑ€Ñ‚Đ° ĐµÑĐ»Đ¸ ÑƒĐ¶Đµ ĐµÑÑ‚ÑŒ Ñ‚Đ°ĐºĐ¸Đµ Đ¶Đµ Đ°Đ»ĐµÑ€Ñ‚Ñ‹
            var parent;

            // Đ¡Đ³ĐµĐ½ĐµÑ€Đ¸Ñ€ÑƒĐµĐ¼ ÑĐ¾Đ¾Đ±Ñ‰ĐµĐ½Đ¸Đµ
            var hullabaloo = this.generate(text, status);

            // ĐŸÑ€Đ¾Đ²ĐµÑ€Đ¸Đ¼ Đ½ĐµÑ‚ Đ»Đ¸ ÑƒĐ¶Đµ Ñ‚Đ°ĐºĐ¸Ñ… Đ¶Đµ ÑĐ¾Đ¾Đ±Ñ‰ĐµĐ½Đ¸Đ¹
            if (this.hullabaloos.length) {
                // ĐŸĐµÑ€ĐµĐ±ĐµÑ€ĐµĐ¼ Đ²ĐµÑŒ Đ¼Đ°ÑÑĐ¸Đ²
                while (i >= 0 && flag) {
                    // Đ•ÑĐ»Đ¸ Ñƒ Đ½Đ°Ñ Đ¿Ñ€Đ¸ÑÑƒÑ‚ÑÑ‚Đ²ÑƒÑÑ‚ Đ¾Đ´Đ¸Đ½Đ°ĐºĐ¾Đ²Ñ‹Đµ ÑĐ¾Đ¾Đ±Ñ‰ĐµĐ½Đ¸Ñ (ÑĐ³Ñ€ÑƒĐ¿Đ¿Đ¸Ñ€ÑƒĐµĐ¼ Đ¸Ñ…)
                    if (this.hullabaloos[i].text == hullabaloo.text && this.hullabaloos[i].status == hullabaloo.status) {
                        // Đ—Đ°Đ¿Đ¾Đ¼Đ½Đ¸Đ¼ Đ³Đ»Đ°Đ²Đ½Ñ‹Đ¹ Đ°Đ»ĐµÑ€Ñ‚
                        parent = this.hullabaloos[i];
                        // Đ¤Đ»Đ°Đ³ Đ²Ñ‹Ñ…Đ¾Đ´Đ° Đ¸Đ· Ñ†Đ¸ĐºĐ»Đ°
                        flag = 0;
                        // ĐŸĐµÑ€ĐµĐ¼ĐµÑÑ‚Đ¸Đ¼ Đ½Đ°Ñˆ Đ°Đ»ĐµÑ€Ñ‚ Đ½Đ° Đ¼ĐµÑÑ‚Đ¾ Đ³Đ»Đ°Đ½Đ¾Đ³Đ¾ ÑĐ¾ ÑĐ¼ĐµÑ‰ĐµĐ½Đ¸ĐµĐ¼
                        hullabaloo.elem.css("top", parseInt(parent.elem.css("top")) + 4);
                        hullabaloo.elem.css("right", parseInt(parent.elem.css("right")) + 4);
                    }
                    i--;
                }
            }

            // Đ—Đ°Đ¿Đ¾Đ¼Đ½Đ¸Đ¼ Đ¿Đ¾Đ·Đ¸Ñ†Đ¸Ñ Đ°Đ»ĐµÑ€Ñ‚Đ°, Đ¿Đ¾Đ½Đ°Đ´Đ¾Đ±Đ¸Ñ‚ÑŒÑÑ Đ´Đ»Ñ Đ¿ĐµÑ€ĐµĐ¼ĐµÑ‰ĐµĐ½Đ¸Ñ Đ°Đ»ĐµÑ€Ñ‚Đ¾Đ² Đ²Đ²ĐµÑ€Ñ…
            hullabaloo.posTop = parseInt(hullabaloo.elem.css("top"));

            // ĐŸÑ€Đ¾Đ²ĐµÑ€ÑĐµĐ¼, Đ³Ñ€ÑƒĐ¿Đ¿Đ° Đ°Đ»ĐµÑ€Ñ‚Đ¾Đ² Ñƒ Đ½Đ°Ñ Đ¸Đ»Đ¸ Ñ‚Đ¾Đ»ÑŒĐºĐ¾ Đ¾Đ´Đ¸Đ½
            if (typeof parent == 'object') {
                // Đ•ÑĐ»Đ¸ Đ°Đ»ĐµÑ€Ñ‚ Đ² Đ³Ñ€ÑƒĐ¿Đ¿Đµ Ñ‚Đ¾ Đ´Đ¾Đ±Đ°Đ²Đ¸Đ¼ ĐµĐ³Đ¾ Đ² Đ³Ñ€ÑƒĐ¿Đ¿Ñƒ Đ¸ Đ¾Đ±Đ½ÑƒĐ»Đ¸Đ¼ ÑÑ‡ĐµÑ‚Ñ‡Đ¸Đº Đ³Ñ€ÑƒĐ¿Đ¿Ñ‹
                clearTimeout(parent.timer);
                // Đ—Đ°Đ´Đ°Đ´Đ¸Đ¼ Đ½Đ¾Đ²Ñ‹Đ¹ ÑÑ‡ĐµÑ‚Ñ‡Đ¸Đº Đ´Đ»Ñ Đ³Ñ€ÑƒĐ¿Đ¿Ñ‹
                parent.timer = setTimeout(function () {
                    self.closed(parent);
                }, this.options.delay);
                // Đ¿Ñ€Đ¸ÑĐ²Đ¾Đ¸Đ¼ Đ½Đ°Ñˆ Đ°Đ»ĐµÑ€Ñ‚ Đ² Đ³Ñ€ÑƒĐ¿Đ¿Ñƒ Đº Ñ€Đ¾Đ´Đ¸Ñ‚ĐµĐ»Ñ
                parent.hullabalooGroup.push(hullabaloo);
                // Đ•ÑĐ»Đ¸ Đ°Đ»ĐµÑ€ Đ¾Đ´Đ¸Đ½
            } else {
                // ĐĐºÑ‚Đ¸Đ²Đ¸Ñ€ÑƒĐµĐ¼ Ñ‚Đ°Đ¹Đ¼ĐµÑ€
                hullabaloo.timer = setTimeout(function () {
                    self.closed(hullabaloo);
                }, this.options.delay);
                // Đ”Đ¾Đ±Đ°Đ²Đ¸Đ¼ Đ°Đ»ĐµÑ€Ñ‚ Đ² Đ¾Đ±Ñ‰Đ¸Đ¹ Đ¼Đ°ÑÑĐ¸Đ² Đ°Đ»ĐµÑ€Ñ‚Đ¾Đ²
                this.hullabaloos.push(hullabaloo);
            }

            // ĐŸĐ¾ĐºĐ°Đ¶ĐµĐ¼ Đ°Đ»ĐµÑ€Ñ‚ Đ¿Đ¾Đ»ÑŒĐ·Đ¾Đ²Đ°Ñ‚ĐµĐ»Ñ
            hullabaloo.elem.fadeIn();

            // Đ—Đ°Đ¿ÑƒÑÑ‚Đ¸Đ¼ Ñ„ÑƒĐ½ĐºÑ†Đ¸Ñ Đ¿Đ¾ Đ·Đ°Đ²ĐµÑ€ÑˆĐµĐ½Đ¸Ñ
            if (typeof this.options.fnEnd == "function")
                this.options.fnEnd();
        }

        // Đ—Đ°ĐºÑ€Ñ‹Đ²Đ°ĐµÑ‚ Đ°Đ»ĐµÑ€Ñ‚
        hullabaloo.prototype.closed = function (hullabaloo) {
            var self = this;
            var idx, i, move, next;

            // Đ¿Ñ€Đ¾Đ²ĐµÑ€ÑĐµĐ¼ ĐµÑÑ‚ÑŒ Đ»Đ¸ Đ¼Đ°ÑÑĐ¸Đ² Ñ Đ°Đ»ĐµÑ€Ñ‚Đ°Đ¼Đ¸
            if (this.hullabaloos !== null) {
                // ĐĐ°Đ¹Đ´ĐµĐ¼ Đ² Đ¼Đ°ÑÑĐ¸Đ²Đµ Đ·Đ°ĐºÑ€Ñ‹Đ²Đ°ĐµĐ¼Ñ‹Đ¹ Đ°Đ»ĐµÑ€Ñ‚
                idx = $.inArray(hullabaloo, this.hullabaloos);

                // Đ•ÑĐ»Đ¸ ÑÑ‚Đ¾ Đ³Ñ€ÑƒĐ¿Đ° Đ°Đ»ĐµÑ€Ñ‚Đ¾Đ², Ñ‚Đ¾ Đ·Đ°ĐºÑ€Đ¾Đ¸Đ¼ Đ²ÑĐµ
                if (hullabaloo.hullabalooGroup !== "undefined" && hullabaloo.hullabalooGroup.length) {
                    for (i = 0; i < hullabaloo.hullabalooGroup.length; i++) {
                        // Đ·Đ°ĐºÑ€Ñ‹Ñ‚ÑŒ Đ°Đ»ĐµÑ€Ñ‚
                        $(hullabaloo.hullabalooGroup[i].elem).alert("close");
                    }
                }

                // Đ—Đ°ĐºÑ€Ñ‹Đ²Đ°ĐµĐ¼ Đ½Đ°Ñˆ Đ°Đ»ĐµÑ€Ñ‚
                $(this.hullabaloos[idx].elem).alert("close");

                if (idx !== -1) {
                    // Đ•ÑĐ»Đ¸ Đ² Đ¼Đ°ÑÑĐ¸Đ²Đµ ĐµÑÑ‚ÑŒ Đ´Ñ€ÑƒĐ³Đ¸Đµ Đ°Đ»ĐµÑ€Ñ‚Ñ‹, Đ¿Đ¾Đ´Đ½Đ¸Đ¼ĐµĐ¼ Đ¸Ñ… Đ½Đ° Đ¼ĐµÑÑ‚Đ¾ Đ·Đ°ĐºÑ€Ñ‹Ñ‚Đ¾Đ³Đ¾
                    if (this.hullabaloos.length > 1) {
                        next = idx + 1;
                        // ĐÑ‚Đ½Đ¸Đ¼Đ°ĐµĐ¼ Đ²ĐµÑ€Ñ…Đ½ÑÑ Đ³Ñ€Đ°Đ½Đ¸Đ·Ñƒ Đ·Đ°ĐºÑ€Ñ‹Ñ‚Đ¾Đ³Đ¾ Đ°Đ»ĐµÑ€Ñ‚Đ° Đ¾Ñ‚ Đ²ĐµÑ€Ñ…Đ½ĐµĐ¹ Đ³Ñ€Đ°Đ½Đ¸Ñ†Ñ‹ ÑĐ»ĐµĐ´ÑƒÑÑ‰ĐµĐ³Đ¾ Đ°Đ»ĐµÑ€Ñ‚Đ°
                        // Đ¸ Ñ€Đ°ÑÑ‡Đ¸Ñ‚Ñ‹Đ²Đ°ĐµĐ¼ Đ½Đ° ÑĐºĐ¾Đ»ÑŒĐºĐ¾ Đ´Đ²Đ¸Đ³Đ°Ñ‚ÑŒ Đ²ÑĐµ Đ°Đ»ĐµÑ€Ñ‚Ñ‹
                        move = this.hullabaloos[next].posTop - this.hullabaloos[idx].posTop;

                        // Đ´Đ²Đ¸Đ³Đ°ĐµĐ¼ Đ²ÑĐµ Đ°Đ»ĐµÑ€Ñ‚Ñ‹, ĐºĐ¾Ñ‚Đ¾Ñ€Ñ‹Đµ Đ¸Đ´ÑƒÑ‚ Đ·Đ° Đ·Đ°ĐºÑ€Ñ‹Ñ‚Ñ‹Đ¼
                        for (i = idx; i < this.hullabaloos.length; i++) {
                            this.animate(self.hullabaloos[i], parseInt(self.hullabaloos[i].posTop) - move);
                            self.hullabaloos[i].posTop = parseInt(self.hullabaloos[i].posTop) - move
                        }
                    }

                    // Đ£Đ´Đ°Đ»Đ¸Đ¼ Đ·Đ°ĐºÑ€Ñ‹Ñ‚Ñ‹Đ¹ Đ°Đ»ĐµÑ€Ñ‚ Đ¸Đ· Đ¼Đ°ÑÑĐ¸Đ²Đ° Ñ Đ°Đ»ĐµÑ€Ñ‚Đ°Đ¼Đ¸
                    this.hullabaloos.splice(idx, 1);

                    // Đ—Đ°Đ¿ÑƒÑÑ‚Đ¸Đ¼ Ñ„ÑƒĐ½ĐºÑ†Đ¸Ñ Đ¿Đ¾ÑĐ»Đµ Đ·Đ°ĐºÑ€Ñ‹Ñ‚Đ¸Ñ ÑĐ¾Đ¾Đ±Ñ‰ĐµĐ½Đ¸Ñ
                    if (typeof this.options.fnEndHide == "function")
                        this.options.fnEndHide();
                }
            }
        }


        // ĐĐ½Đ¸Đ¼Đ°Ñ†Đ¸Ñ Đ´Đ»Ñ Đ¿Đ¾Đ´ÑĐµĐ¼Đ° Đ°Đ»ĐµÑ€Ñ‚Đ¾Đ² Đ²Đ²ĐµÑ€Ñ…
        hullabaloo.prototype.animate = function (hullabaloo, move) {
            var timer,
                top, // Đ’ĐµÑ€Ñ… Đ°Đ»ĐµÑ€Ñ‚Đ°, ĐºĐ¾Ñ‚Đ¾Ñ€Ñ‹Đ¹ Ñ‚Đ°Ñ‰Đ¸Đ¼
                i, // Đ¡Ñ‡ĐµÑ‚Ñ‡Đ¸Đº Đ´Đ»Ñ Đ¿ĐµÑ€ĐµĐ±Đ¾Ñ€Đ° Đ³Ñ€ÑƒĐ¿Đ¿Ñ‹ Đ°Đ»ĐµÑ€Ñ‚Đ¾Đ²
                group = 0; // ĐĐ±Đ¾Đ·Đ½Đ°Ñ‡ĐµĐ½Đ¸Đµ, Đ³Ñ€ÑƒĐ¿Đ¿Đ° Đ°Đ»ĐµÑ€Ñ‚Đ¾Đ² Đ¸Đ»Đ¸ Đ¾Đ´Đ¸Đ½Đ¾Ñ‡Đ½Ñ‹Đ¹

            // Đ’ĐµÑ€Ñ… Đ°Đ»ĐµÑ€Ñ‚Đ°, ĐºĐ¾Ñ‚Đ¾Ñ€Ñ‹Đ¹ Ñ‚Đ°Ñ‰Đ¸Đ¼
            top = parseInt(hullabaloo.elem.css("top"));
            // Đ•ÑĐ»Đ¸ ÑÑ‚Đ¾ Đ³Ñ€ÑƒĐ¿Đ¿Đ° Đ°Đ»ĐµÑ€Ñ‚Đ¾Đ²
            group = hullabaloo.hullabalooGroup.length;

            // Đ—Đ°Đ¿ÑƒÑÑ‚Đ¸Đ¼ Ñ‚Đ°Đ¹Đ¼ĐµÑ€
            timer = setInterval(frame, 2);

            // Đ¤-Đ¸Ñ Đ´Đ»Ñ Ñ‚Đ°Đ¹Đ¼ĐµÑ€Đ°
            function frame() {
                if (top == move) {
                    clearInterval(timer);
                } else {
                    top--;
                    hullabaloo.elem.css("top", top);

                    // Đ•ÑĐ»Đ¸ ÑÑ‚Đ¾ Đ³Ñ€ÑƒĐ¿Đ¿Đ° Đ°Đ»ĐµÑ€Ñ‚Đ¾Đ²
                    if (group) {
                        for (i = 0; i < group; i++) {
                            hullabaloo.hullabalooGroup[i].elem.css("top", top + 5);
                        }
                    }
                }
            }
        }

        // Đ“ĐµĐ½ĐµÑ€Đ°Ñ†Đ¸Ñ Đ°Đ»ĐµÑ€Ñ‚Đ° Đ½Đ° ÑÑ‚Ñ€Đ°Đ½Đ¸Ñ†Đµ
        hullabaloo.prototype.generate = function (text, status) {
            var alertsObj = {
                icon: "", // Đ˜ĐºĐ¾Đ½ĐºĐ°
                status: status || this.options.status, // Đ¡Ñ‚Đ°Ñ‚ÑƒÑ
                text: text || this.options.text, // Đ¢ĐµĐºÑ‚
                elem: $("<div>"), // HTML ĐºĐ¾Đ´ ÑĐ°Đ¼Đ¾Đ³Đ¾ Đ°Đ»ĐµÑ€Ñ‚Đ°

                // Đ“Ñ€ÑƒĐ¿Đ¿Đ¸Ñ€Đ¾Đ²ĐºĐ° Đ¾Đ´Đ¸Đ½Đ°ĐºĐ¾Đ²Ñ‹Ñ… Đ°Đ»ĐµÑ€Ñ‚Đ¾Đ²
                hullabalooGroup: []
            };
            var option, // ĐĐ°ÑÑ‚Ñ€Đ¾Đ¹ĐºĐ¸ Đ°Đ»ĐµÑ€Ñ‚Đ°
                offsetAmount, // ĐÑ‚ÑÑ‚ÑƒĐ¿Ñ‹ Đ°Đ»ĐµÑ€Ñ‚Đ°
                css; // CSS ÑĐ²Đ¾Đ¹ÑÑ‚Đ²Đ° Đ°Đ»ĐµÑ€Ñ‚Đ°

            option = this.options;

            // Đ”Đ¾Đ±Đ°Đ²Đ¸Đ¼ Đ´Đ¾Đ¿Đ¾Đ»Đ½Đ¸Ñ‚ĐµĐ»ÑŒĐ½Ñ‹Đ¹ ĐºĐ»Đ°ÑÑ
            alertsObj.elem.attr("class", "hullabaloo alert " + option.alertClass);

            // Đ¡Ñ‚Đ°Ñ‚ÑƒÑ
            alertsObj.elem.addClass("alert-" + alertsObj.status);

            // ĐĐ½Đ¾Đ¿ĐºĐ° Đ·Đ°ĐºÑ€Ñ‹Ñ‚Đ¸Ñ ÑĐ¾Đ¾Đ±Ñ‰ĐµĐ½Đ¸Ñ
            if (option.allow_dismiss) {
                alertsObj.elem.addClass("alert-dismissible");
                alertsObj.elem.append("<button  class=\"close\" data-dismiss=\"alert\" type=\"button\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>");
            }

            // Icon
            if (alertsObj.status == "success")
                alertsObj.icon = "check";
            else if (alertsObj.status == "info-circle")
                alertsObj.icon = "info";
            else if (this.hullabaloo.status == "danger")
                alertsObj.icon = "times-circle";
            else if (this.hullabaloo.status == "warning")
                alertsObj.icon = "exclamation-triangle";
            else
                alertsObj.icon = option.icon;

            // Đ”Đ¾Đ±Đ°Đ²Đ¸Đ¼ Ñ‚ĐµĐºÑÑ‚ Đ² ÑĐ¾Đ¾Đ±Ñ‰ĐµĐ½Đ¸Đµ
            alertsObj.elem.append('<i class="fa fa-' + alertsObj.icon + '"></i> ' + alertsObj.text);

            // ĐŸÑ€Đ¸ÑĐ²Đ¾Đ¸Đ¼ Đ¾Ñ‚ÑÑ‚ÑƒĐ¿ Đ¾Ñ‚ Đ²ĐµÑ€Ñ…Đ°
            offsetAmount = option.offset.amount;
            // Đ•ÑĐ»Đ¸ ĐµÑÑ‚ÑŒ Đ´Ñ€ÑƒĐ³Đ¸Đµ Đ°Đ»ĐµÑ€Ñ‚Ñ‹ Ñ‚Đ¾ Đ¿Ñ€Đ¸Đ±Đ°Đ²Đ¸Đ¼ Đº Đ¾Ñ‚ÑÑ‚ÑƒĐ¿Ñƒ Đ¸Ñ… Đ²Ñ‹ÑĐ¾Ñ‚Ñƒ
            $(".hullabaloo").each(function () {
                return offsetAmount = Math.max(offsetAmount, parseInt($(this).css(option.offset.from)) + $(this).outerHeight() + option.stackup_spacing);
            });

            // Đ”Đ¾Đ±Đ°Đ²Đ¸Đ¼ CSS ÑÑ‚Đ¸Đ»Đ¸
            css = {
                "position": (option.ele === "body" ? "fixed" : "absolute"),
                "margin": 0,
                "z-index": "9999",
                "display": "none"
            };
            css[option.offset.from] = offsetAmount + "px";
            alertsObj.elem.css(css);

            if (option.width !== "auto") {
                alertsObj.elem.css("width", option.width + "px");
            }
            $(option.ele).append(alertsObj.elem);
            switch (option.align) {
                case "center":
                    alertsObj.elem.css({
                        "left": "50%",
                        "margin-left": "-" + (alertsObj.elem.outerWidth() / 2) + "px"
                    });
                    break;
                case "left":
                    alertsObj.elem.css("left", "20px");
                    break;
                default:
                    alertsObj.elem.css("right", "20px");
            }

            return alertsObj;
        };


        return hullabaloo;
    };
    return init(root);
});