<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');

if (!defined('PAGING_SIZE'))
    define('PAGING_SIZE', 20);

$theme_config = [
    'theme' => 'basso',
    'exclude_user_role' => [6],
    'dropbox' => [
        'app_key' => '50672v39ieg2n0t',
        'app_secret' => 'b545rlmoanj0j09'
    ],
    'google' => [
        'captcha' => [
            'site_key' => '6LfcFoUUAAAAADpkMHcy1W4W8ZLBHTa9uS9RUAwa',
            'secret_key' => '6LfcFoUUAAAAAIGOL6ICeQRrBjmdvvNnrHtHVYgt'
        ]
    ],
    /*'vn_pay' => [
        'vnp_TmnCode' => 'BASSO001',
        'vnp_HashSecret' => 'ZREPJUSQIVGZRNVOKTLKZSVMMBAVITRJ',
        'vnp_Url' => 'https://sandbox.vnpayment.vn/paymentv2/vpcpay.html'
    ],*/

	/* 'vn_pay' => [
        'vnp_TmnCode' => '8X1GYPWV',
        'vnp_HashSecret' => 'EWLSOLZMHQZAIDPRBCAHCVAEJNRSDFTX',
        'vnp_Url' => 'https://sandbox.vnpayment.vn/paymentv2/vpcpay.html'
    ], */

	/*
	|
	VNPay
	|
	 */

	'vn_pay' => [
        'vnp_TmnCode' => 'THLONG01',
        'vnp_HashSecret' => 'FCOXOIPRDJSIFCXREGQWUYJGDDTXECFI',
        'vnp_Url' => 'https://pay.vnpay.vn/vpcpay.html'
    ],


    'ebay' => [
        'credentials' => [
            'devId' => 'a77c1131-a6d7-48ab-81c0-6209ff869003',
            'appId' => 'HuyQuang-LinhYeuD-PRD-9bff0c33e-df0d28ec',
            'certId' => 'PRD-16e557a4a66d-d9de-4bc5-aaef-6422'
        ]
    ],
    'amazon' => [
        'url' => 'https://amzproductapi.com/api/xml',
        'key' => 'key-aa1cc26a1a4349d8858ab688'
    ],
    'languages' => [
        'vi' => 'Tiếng Việt'
    ],
    'ckfinder_key' => ['3ce.com' => 'CNJNW6B6HDPQVJ8T8BNQHN1YHHFNB'],
    'sms' => [
        'api_key' => null,
        'secret_key' => null,
        'brandname' => null,
        'type' => 2
    ],
    'pushover' => [
        'enable' => false,
        'api_token' => 'as8xtqvda4cno5x4rx2fh9kosigs7j',
        'user_key' => 'uksvxdrsnp8qnfahbi2itxkaxvbbw4',
        'callback_url' => null
    ],
    'pheanstalk' => [
        'enable' => true,
        'server' => '123.31.43.155',    //  bce server
        'port' => 11300,
        'tube_prefix' => 'basso_'
    ],
    'redis' => [
        'host' => 'localhost',
        'port' => 6379,
        'password' => null
    ],
    'route' => [
        'cpanel' => 'basso/dashboard/index',
        'admin' => 'basso/dashboard/index',
        'default_controller' => 'basso_front/front/index',
        'subscribe' => 'basso_front/front/subscribe',
        'send_contact' => 'basso_front/front/contact',
        'test' => 'basso_front/front/test',
        'dang-ky' => 'basso_front/register/index',
        'quen-mat-khau' => 'basso_front/register/forgot_pass',
        'reset' => 'basso_front/register/reset_pass',
        'reset/success' => 'basso_front/register/reset_pass_success',
        'tai-khoan' => 'basso_front/account/index',
        'tai-khoan/don-hang' => 'basso_front/account/orders',
        'tim-kiem/don-hang' => 'basso_front/search/searchs',
        'tim-kiem/amazon' => 'basso_front/search/amzsearch',
        'tai-khoan/don-hang/([0-9]+)' => 'basso_front/search/searchs/$1',
        'tai-khoan/don-hang/([0-9]+)' => 'basso_front/account/orders/$1',
        'tai-khoan/thanh-toan' => 'basso_front/account/payments',
        'confirm_payment' => 'basso_front/payment/confirm',
        'ipn' => 'basso_front/payment/ipn',
        'tai-khoan/thanh-toan/([0-9]+)' => 'basso_front/account/payments/$1',
        'item/([0-9a-zA-Z-_]+).html' => 'basso_front/crawled_product/index/$1',
        'item/description/([0-9a-zA-Z]+).html' => 'basso_front/crawled_product/description/$1',
        'gio-hang' => 'basso_front/cart/index',
        'tro-giup/([0-9]+)' => 'basso_front/front/support/$1',
//        'sitemap.xml' => 'basso_front/site_map/index',
        'tro-giup' => 'basso_front/front/support',
        'don-hang' => 'basso_front/cart/tracking_order',
        'thanh-toan/xac-nhan' => 'basso_front/cart/confirm_trans',
        '([a-z0-9-]+)' => 'term/index/$1',
        '([a-z0-9-]+)/([0-9]+)' => 'term/index/$1/$2',
        '([a-zA-Z0-9-]+).html' => 'article/detail/$1'
    ],
    'autoload' => [
        'helper' => ['basso/status', 'basso/config', 'management/billing'],
        'model' => ['basso/currency_model', 'basso/subscribe_model'],
        'library' => []
    ],
    'term' => [
        [
            'name' => 'Trang chủ',
            'id' => 'home',
            'active' => true
        ],
        [
            'name' => 'Bài viết',
            'id' => 'post',
            'active' => true
        ],
        [
            'name' => 'Danh mục',
            'id' => 'post_page',
            'active' => true
        ],
        [
            'name' => 'Trang nội dung',
            'id' => 'page',
            'active' => true
        ],
        [
            'name' => 'Trang sản phẩm',
            'id' => 'product_page',
            'active' => true
        ],
        [
            'name' => 'Trang liên hệ',
            'id' => 'contact',
            'active' => true
        ],
        [
            'name' => 'Liên kết tùy chỉnh',
            'id' => 'external_link',
            'active' => true
        ]
    ]
];
