<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

$this->load->model('contact_form_model');
$count_contact_forms = $this->contact_form_model->count_unread();

$sidebars = [
    [
        'name' => 'Tổng quan',
        'icon' => 'icon-speedometer',
        'module' => 'basso',
        'controller' => 'dashboard',
        'action' => 'index'
    ],
	[
        'name' => 'Kế toán',
        'icon' => 'icon-refresh',
        'items' => [
			[
				'name' => 'Duyệt thanh toán',
				'icon' => 'icon-refresh',
				'module' => 'basso',
				'controller' => 'payment_history',
				'action' => ['index']
			],
			[
				'name' => 'Phiếu thu',
				'module' => 'management',
				'controller' => 'billing',
				'action' => 'in'
			],
			[
				'name' => 'Phiếu chi',
				'module' => 'management',
				'controller' => 'billing',
				'action' => 'out'
			],
			[
				'name' => 'Sổ quỹ',
				'module' => 'management',
				'controller' => 'billing',
				'action' => 'fund'
			],
			[
				'name' => 'Phân loại',
				'module' => 'management',
				'controller' => 'billing',
				'action' => 'term'
			]
		]
	],
    [
        'name' => 'Đơn Đặt Hàng',
        'icon' => 'icon-chemistry',
        'module' => 'basso',
        'controller' => 'customer_order',
        'items' => [
            [
                'name' => 'Tất cả',
                'module' => 'basso',
                'controller' => 'customer_order',
                'action' => ['index', 'detail']

            ], [
                'name' => 'Tạo đơn',
                'module' => 'basso',
                'controller' => 'customer_order',
                'action' => ['create']
            ], 
			/* [
                'name' => 'Giao hàng',
                'module' => 'basso',
                'controller' => 'shipping_order',
                'action' => ['index']
            ], */
			[
                'name' => 'Đơn hoàn trả',
                'module' => 'basso',
                'controller' => 'return_order',
                'action' => ['index', 'create']
            ]
        ]
    ],
    [
        'name' => 'Đơn Admin',
        'icon' => 'icon-vector',
        'module' => 'basso',
        'controller' => 'web_order',
        'items' => [
            [
                'name' => 'Tất cả',
                'module' => 'basso',
                'controller' => 'web_order',
                'action' => ['index', 'detail']
            ],
            [
                'name' => 'Mua Hàng',
                'module' => 'basso',
                'controller' => 'web_order',
                'action' => ['create']
            ],
            [
                'name' => 'Người mua hàng',
                'module' => 'basso',
                'controller' => 'web_order',
                'action' => ['buyer']
            ],
            [
                'name' => 'Phương thức TT',
                'module' => 'basso',
                'controller' => 'web_order',
                'action' => 'payment'
            ]
        ]
    ],
    [
        'name' => 'Quản lý kho',
        'icon' => 'icon-home',
        'module' => 'basso',
        'controller' => 'inventory',
        'action' => ['index'],
		'items' => [
			[
                'name' => 'Tất cả',
                'module' => 'basso',
                'controller' => 'inventory',
                'action' => ['index', 'detail']
            ],
			[
                'name' => 'Giao hàng',
                'module' => 'basso',
                'controller' => 'shipping_order',
                'action' => ['index']
            ]
		]
    ],
    [
        'name' => 'Quản lý vận chuyển',
        'icon' => 'icon-anchor',
        'module' => 'basso',
        'controller' => 'delivery_manager',
        'action' => ['index']
    ],
    [
        'name' => 'Khách hàng',
        'icon' => 'icon-bag',
        'module' => 'management',
        'controller' => 'customer',
        'items' => [
            [
                'name' => 'Tất cả',
                'module' => 'management',
                'controller' => 'customer',
                'action' => ['index', 'detail']
            ], [
                'name' => 'Phân nhóm',
                'module' => 'management',
                'controller' => 'customer',
                'action' => ['group']
            ]
        ]
    ],
    [
        'name' => 'Danh mục',
        'icon' => 'icon-grid',
        'module' => 'admin',
        'controller' => 'term',
        'action' => ['index', 'create', 'edit']
    ],
    [
        'name' => 'Bài viết',
        'icon' => 'icon-docs',
        'module' => 'admin',
        'controller' => 'article',
        'action' => ['index', 'create', 'edit']
    ],
    [
        'name' => 'Cài đặt',
        'icon' => 'icon-key',
        'items' => [
            [
                'name' => 'Quốc gia',
                'module' => 'basso',
                'controller' => 'country',
                'action' => ['index']
            ],
            [
                'name' => 'Ngoại tệ',
                'module' => 'basso',
                'controller' => 'currency',
                'action' => ['index']
            ],
            [
                'name' => 'Ngân hàng',
                'module' => 'basso',
                'controller' => 'bank',
                'action' => ['index']
            ],
            [
                'name' => 'Warehouse',
                'module' => 'basso',
                'controller' => 'warehouse',
                'action' => ['index']
            ],
            [
                'name' => 'Nơi chốt đơn',
                'module' => 'basso',
                'controller' => 'order_place',
                'action' => ['index']
            ],
            [
                'name' => 'Nhãn đơn hàng',
                'module' => 'basso',
                'controller' => 'order_label',
                'action' => ['index']
            ],
            [
                'name' => 'Cấp độ',
                'module' => 'basso',
                'controller' => 'level',
                'action' => ['index']
            ],
            [
                'name' => 'Đơn vị vận chuyển',
                'module' => 'management',
                'controller' => 'shipping',
                'action' => ['index']
            ]
        ]
    ],
    [
        'name' => 'Hệ thống',
        'icon' => 'icon-settings',
        'items' => [
            [
                'name' => 'Phương thức',
                'module' => 'admin',
                'controller' => 'order_setting',
                'action' => ['payment']
            ],
            [
                'name' => 'Hạn mức',
                'module' => 'basso',
                'controller' => 'payment_limit',
                'action' => ['index']
            ],
            [
                'name' => 'Mã giảm giá',
                'module' => 'admin',
                'controller' => 'voucher',
                'action' => ['index']
            ],
            [
                'name' => 'Danh mục phụ thu',
                'module' => 'basso',
                'controller' => 'order_term',
                'action' => ['index']
            ],
            /*[
                'name' => 'Cấu hình đặt hàng',
                'module' => 'admin',
                'controller' => 'order_setting',
                'action' => 'index'
            ],*/
            [
                'name' => 'Cài đặt Email',
                'module' => 'system',
                'controller' => 'email',
                'action' => ['index']
            ],
            [
                'name' => 'Email template',
                'module' => 'basso',
                'controller' => 'email_template',
                'action' => ['index']
            ],
            [
                'name' => 'Backup Dropbox',
                'module' => 'system',
                'controller' => 'backup',
                'action' => ['index']
            ],
            [
                'name' => 'Lịch sử báo giá',
                'module' => 'basso',
                'controller' => 'crawl_history',
                'action' => ['index']
            ]
        ]
    ],
    [
        'name' => 'Quản trị website'.'<span style="margin-left: 7px; color: red;" class="contact_count">('.$count_contact_forms.')</spam>',
        'icon' => 'icon-screen-desktop',
        'items' => [
            [
                'name' => 'Slide trang chủ',
                'module' => 'admin',
                'controller' => 'slider',
                'action' => 'index'
            ],
            [
                'name' => 'Email nhận tin',
                'module' => 'basso',
                'controller' => 'subscribe',
                'action' => 'index'
            ],
            [
                'name' => 'Form liên hệ',
                'module' => 'basso',
                'controller' => 'contact_form',
                'action' => 'index'
            ],
            [
                'name' => 'Cấu hình website',
                'url' => 'config',
                'module' => 'admin',
                'controller' => 'config',
                'action' => 'index'
            ],
            [
                'name' => 'Cấu hình SEO',
                'module' => 'admin',
                'controller' => 'config',
                'action' => 'seo'
            ],
            [
                'name' => 'Widgets',
                'module' => 'admin',
                'controller' => 'widget',
                'action' => ['index', 'edit', 'create']
            ],
            [
                'name' => 'Mã javascript',
                'module' => 'admin',
                'controller' => 'tracking_code',
                'action' => ['index', 'edit', 'save', 'create']
            ]
        ]
    ],
    [
        'name' => 'Tài khoản',
        'icon' => 'icon-people',
        'module' => 'admin',
        'controller' => 'user',
        'action' => 'index'
    ]
];

$widget = [
    'type' => [
        [
            'name' => 'Danh mục bài viết',
            'view' => 'article_terms',
            'id' => 'article_terms',
            'active' => true
        ],
        [
            'name' => 'Bài viết',
            'view' => 'articles',
            'id' => 'articles',
            'active' => true
        ],
        [
            'name' => 'Liên kết',
            'view' => 'links',
            'id' => 'links',
            'active' => true
        ],
        [
            'name' => 'Ảnh (banner/slide)',
            'view' => 'photos',
            'id' => 'photos',
            'active' => true
        ],
        [
            'name' => 'Mã code (javascript/html)',
            'view' => 'script',
            'id' => 'script',
            'active' => true
        ],
        [
            'name' => 'Văn bản',
            'view' => 'text',
            'id' => 'text',
            'active' => true
        ],
        [
            'name' => 'Phản hồi khách hàng',
            'id' => 'testimonial',
            'view' => 'testimonial',
            'active' => true
        ],
        [
            'name' => 'Khối văn bản',
            'id' => 'block_text',
            'view' => 'block_text',
            'active' => true
        ]
    ],
    'position' => [
        [
            "id" => "footer-1",
            "name" => "Footer cột 1",
            "description" => "Chân website, cột số 1"
        ],
        [
            "id" => "footer-2",
            "name" => "Footer cột 2",
            "description" => "Chân website, cột số 2"
        ],
        [
            "id" => "footer-3",
            "name" => "Footer cột 3",
            "description" => "Chân website, cột số 3"
        ],
        [
            "id" => "bottom-footer",
            "name" => "Footer copyright - cuối trang web"
        ],
        [
            'id' => 'home-brand',
            'name' => 'Slider logo thương hiệu'
        ],
        [
            'id' => 'home-testinomial',
            'name' => 'Phản hồi khách hàng'
        ],
        [
            'id' => 'home-featured',
            'name' => 'Đặc điểm nổi bật'
        ],
        [
            'id' => 'home-featured-img',
            'name' => 'Ảnh đặc điểm nổi bật'
        ],
        [
            'id' => 'help-link',
            'name' => 'Link trợ giúp'
        ],
        [
            'id' => 'flow-order',
            'name' => 'Quy trình đặt hàng'
        ],
        [
            'id' => 'flow-ship',
            'name' => 'Quy trình vận chuyển'
        ],
        [
            'id' => 'flow-receive',
            'name' => 'Quy trình nhận hàng'
        ],
        [
            'id' => 'front-youtube',
            'name' => 'Youtube trang chủ'
        ],
        [
            'id' => 'support-news',
            'name' => 'Hỗ trợ - Tin tức'
        ],
        [
            'id' => 'support-main',
            'name' => 'Hỗ trợ - Trợ giúp'
        ],
        [
            'id' => 'support-faq',
            'name' => 'Hỗ trợ - Câu hỏi thường gặp'
        ]
    ]
];

$document_url = 'https://1drv.ms/w/s!Anpzz2Sy-EXWkzU9uKHrNH9o5kax';