<div style="max-width: 720px; margin: 0 auto">
    <?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
    <table style="width: 100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td align="left" style="background-color: #00354E; padding:20px">
                <img src="https://basso.eprtech.com/uploads/logo.png" style="max-height: 30px; width: auto"/>
            </td>
            <td align="left" width="110"
                style="color: #FFF; background-color: #00354E; padding:20px; font-family: Arial, Helvetica, sans-serif">
                <img src="https://basso.eprtech.com/theme/basso/statics/images/icon-hotline.png"
                     style="margin-right: 5px; height: 20px; float: left"/>
                0965687790
            </td>
            <td align="left" width="120"
                style="color: #FFF; background-color: #00354E; padding:20px; font-family: Arial, Helvetica, sans-serif">
                <img src="https://basso.eprtech.com/theme/basso/statics/images/icon-email.png"
                     style="margin-right: 5px; height: 20px; float: left"/>
                <a href="mailto: cskh@basso.eprtech.com" style="color: #FFF">cskh@basso.eprtech.com</a>
            </td>
        </tr>
    </table>

    <table style="width: 100%; margin: 20px 0" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td align="center" style="color: #00354E; padding:20px; background: #EEE">
                <strong style="text-transform: uppercase; font-family: Arial, Helvetica, sans-serif; font-size: 20px; line-height: 1.7">
                    KHÔI PHỤC MẬT KHẨU
                </strong>
            </td>
        </tr>
    </table>

    <p style="color: #00354E; padding:10px; font-family: Arial, Helvetica, sans-serif; line-height: 1.6; font-size: 1em; font-size: 13px">
        Xin chào Quý khách
        @if(isset($name))
            <strong style="color: #00354E;">{{$name}}</strong>,<br/>
        @else
            ,<br/>
        @endif
        Chúng tôi đã nhận được yêu cầu khôi phục mật khẩu của quý khách. Vui lòng <a href="{{$reset_url}}"><strong>click
                vào đây</strong></a> để tiến
        hành thay đổi mật khẩu.<br/>
    <p>Hoặc copy đường dẫn sau vào trình duyệt của bạn: <a href="{{$reset_url}}">{{$reset_url}}</a></p><br/>
    Quý khách vui lòng không chia sẻ đường dẫn này với ai.
    </p>

    <strong style="color: #00354E; margin:10px 0; font-family: Arial, Helvetica, sans-serif; line-height: 1.6; font-size: 13px">
        Basso.vn xin cảm ơn quý khách đã tin tưởng và rất hân hạnh được phục vụ Quý khách.
    </strong>

    <table style="width: 100%; margin-top: 20px; font-size: 13px" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td align="center" colspan="2"
                style="background-color: #00354E; padding:20px; color: #FFF; line-height: 1.6; font-family: Arial, Helvetica, sans-serif;">
                CÔNG TY TNHH THƯƠNG MẠI HOÀI ĐỨC<br/>
                Giấy phép kinh doanh số: 0309913816<br/>
                <a href="https://BASSO.VN" target="_blank" style="color: #FFF">BASSO.VN</a> – dịch vụ mua hàng quốc tế
                hàng đầu Việt Nam.
            </td>
        </tr>
        <tr>
            <td align="center" width="50%"
                style="background-color: #00354E; padding:20px; color: #FFF; line-height: 1.6; font-family: Arial, Helvetica, sans-serif;">
                <strong>Chi nhánh Hà Nội</strong><br/>
                17 Tố Hữu, P. Trung Văn, Q. Nam Từ Liêm
            </td>
            <td align="center" width="50%"
                style="background-color: #00354E; padding:20px; color: #FFF; line-height: 1.6; font-family: Arial, Helvetica, sans-serif;">
                <strong>Chi nhánh Hồ Chí Minh</strong><br/>
                60 Lê Trung Nghĩa, P. 12, Q. Tân Bình
            </td>
        </tr>
    </table>

    <p style="color: #00354E; margin:10px 0; font-family: Arial, Helvetica, sans-serif; line-height: 1.6; font-size: 13px">
        ** Đây là email tự động. Xin quý khách vui lòng không trả lời lại email này!
    </p>
</div>