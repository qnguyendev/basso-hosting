<div style="max-width: 720px; margin: 0 auto">
    <?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
    <table style="width: 100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td align="left" style="background-color: #00354E; padding:20px">
                <img src="https://basso.eprtech.com/uploads/logo.png" style="max-height: 30px; width: auto"/>
            </td>
            <td align="left" width="110"
                style="color: #FFF; background-color: #00354E; padding:20px; font-family: Arial, Helvetica, sans-serif">
                <img src="https://basso.eprtech.com/theme/basso/statics/images/icon-hotline.png"
                     style="margin-right: 5px; height: 20px; float: left"/>
                0965687790
            </td>
            <td align="left" width="120"
                style="color: #FFF; background-color: #00354E; padding:20px; font-family: Arial, Helvetica, sans-serif">
                <img src="https://basso.eprtech.com/theme/basso/statics/images/icon-email.png"
                     style="margin-right: 5px; height: 20px; float: left"/>
                <a href="mailto: cskh@basso.eprtech.com" style="color: #FFF">cskh@basso.eprtech.com</a>
            </td>
        </tr>
    </table>

    <div style="color: #00354E; padding:60px 0; font-family: Arial, Helvetica, sans-serif; line-height: 1.6; font-size: 13px">
        <p>
            Xin chào Quý khách <strong style="color: #00354E;">{{$name}}</strong>,
        </p>
        <p>
            Basso xin thông báo Quý khách đã tạo tài khoản thành công trên website của Basso. Quý khách vui lòng đăng
            nhập
            tài khoản và tiếp tục trải nghiệm dịch vụ mua hàng quốc tế của Basso.
        </p>
        <p>
            Thông tin đăng nhập như sau:<br/>
            Tên đăng nhập: <strong>{{$email}}</strong><br/>
            Mật khẩu: <strong>{{$pass}}</strong>
        </p>
        Nếu Quý khách không thực hiện được đăng nhập vui lòng liên hệ với Basso qua số hotline
        096 568 7790 hoặc email đến địa chỉ <strong>cskh@basso.eprtech.com</strong> để nhận được hỗ trợ
    </div>

    <strong style="color: #00354E; margin:10px 0; font-family: Arial, Helvetica, sans-serif; line-height: 1.6; font-size: 13px">
        Basso.vn xin cảm ơn quý khách đã tin tưởng và rất hân hạnh được phục vụ Quý khách.
    </strong>

    <table style="width: 100%; margin-top: 20px; font-size: 13px" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td align="center" colspan="2"
                style="background-color: #00354E; padding:20px; color: #FFF; line-height: 1.6; font-family: Arial, Helvetica, sans-serif;">
                CÔNG TY TNHH THƯƠNG MẠI HOÀI ĐỨC<br/>
                Giấy phép kinh doanh số: 0309913816<br/>
                <a href="https://BASSO.VN" target="_blank" style="color: #FFF">BASSO.VN</a> – dịch vụ mua hàng quốc tế
                hàng đầu Việt Nam.
            </td>
        </tr>
    </table>
</div>