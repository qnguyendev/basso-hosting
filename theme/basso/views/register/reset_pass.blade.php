<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
if ($this->session->flashdata('error_msg') != null)
    $error_msg = $this->session->flashdata('error_msg');

if ($this->session->flashdata('success_msg') != null)
    $success_msg = $this->session->flashdata('success_msg');
?>
@layout('default_layout')
@section('content')
    <main class="order-page">
        <div class="container">
            <h2 class="h2-title mb-5">ĐẶT LẠI MẬT KHẨU</h2>
            <div class="row">
                <div class="col-md-12 col-lg-6 offset-lg-3">
                    @if(isset($error_msg))
                        <div class="alert alert-danger">{{$error_msg}}</div>
                    @endif

                    @if(!isset($success_msg))
                        <form method="post" autocomplete="off">
                            <div class="form-group row">
                                <label class="col-md-12 col-lg-4">
                                    Địa chỉ email
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-md-12 col-lg-8">
                                    <input type="email" required class="form-control" name="email"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-12 col-lg-4">
                                    Mật khẩu mới
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-md-12 col-lg-8">
                                    <input type="password" required class="form-control" name="pass"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-12 col-lg-4">
                                    Xác nhận mật khẩu
                                    <span class="text-danger">*</span>
                                </label>
                                <div class="col-md-12 col-lg-8">
                                    <input type="password" required class="form-control" name="re_pass"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 col-lg-8 offset-lg-4">
                                    <button class="btn button" type="submit">
                                        CẬP NHẬT
                                    </button>
                                </div>
                            </div>
                        </form>
                    @else
                        <div class="bg-light border border-white p-4">
                            <div class="text-center text-success" style="font-size: 6em">
                                <i class="fa fa-check-circle"></i>
                            </div>
                            <p>
                                Basso đã cập nhật mật khẩu mới cho quý khách. Quý khách có thể bắt đầu
                                <a href="javascript:" data-toggle="modal" data-target="#modal-login">
                                    đăng nhập tại đây
                                </a>.
                            </p>
                            Basso trân trọng!
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </main>
@endsection