<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
$user = $this->ion_auth->user()->row();
$action = $this->router->fetch_method();
?>
@layout('default_layout')
@section('content')
    <main class="order-page">
        <div class="container">
            <h2 class="h2-title mb-5">TÀI KHOẢN</h2>
            <div class="order-wrapper">
                <div class="order-menu">
                    <div class="li parent">
                        <i class="fas fa-user"></i>
                        {{$user->first_name}}
                    </div>
                    <div class="li {{$action == 'orders' ? 'active' : ''}}">
                        <a href="{{base_url('tai-khoan/don-hang')}}"><i class="far fa-circle"></i> Quản lý đơn hàng</a>
                    </div>
                    <div class="li {{$action == 'payments' ? 'active' : ''}}">
                        <a href="{{base_url('tai-khoan/thanh-toan')}}">
                            <i class="far fa-circle"></i>
                            Lịch sử thanh toán
                        </a>
                    </div>
                    <div class="li {{$action == 'index' ? 'active' : ''}}">
                        <a href="{{base_url('tai-khoan')}}"><i class="far fa-circle"></i> Quản lý tài khoản</a>
                    </div>
                </div>
                <div class="order-content">
                    @yield('right-content')
                </div>
            </div>
        </div>
    </main>
@endsection