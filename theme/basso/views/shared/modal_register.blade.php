<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal" role="dialog" id="modal-register">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">ĐĂNG KÝ</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="p-3">
                    <div class="form-group">
                        <label class="d-block">
                            Họ tên
                            <span class="text-danger">*</span>
                            <span class="validationMessage float-right text-danger font-italic"
                                  data-bind="validationMessage: name"/>
                        </label>
                        <input type="text" required class="form-control" data-bind="value: name"/>
                    </div>
                    <div class="form-group">
                        <label class="d-block">
                            Địa chỉ email
                            <span class="text-danger">*</span>
                            <span class="validationMessage float-right text-danger font-italic"
                                  data-bind="validationMessage: email"/>
                        </label>
                        <input type="email" required class="form-control" autocomplete="off"
                               data-bind="value: email"/>
                    </div>
                    <div class="form-group">
                        <label class="d-block">
                            Điện thoại
                            <span class="text-danger">*</span>
                            <span class="validationMessage float-right text-danger font-italic"
                                  data-bind="validationMessage: phone"/>
                        </label>
                        <input type="text" required class="form-control" data-bind="value: phone"
                               autocomplete="off"/>
                    </div>
                    <div class="form-group">
                        <label class="d-block">
                            Mật khẩu
                            <span class="text-danger">*</span>
                            <span class="validationMessage float-right text-danger font-italic"
                                  data-bind="validationMessage: pass"
                                  autocomplete="off"/>
                        </label>
                        <input type="password" class="form-control" autocomplete="off" required
                               data-bind="value: pass"/>
                    </div>
                    <div class="form-group">
                        <label class="d-block">
                            Xác nhận mật khẩu
                            <span class="text-danger">*</span>
                            <span class="validationMessage float-right text-danger font-italic"
                                  data-bind="validationMessage: re_pass"/>
                        </label>
                        <input type="password" class="form-control" autocomplete="off" required
                               data-bind="value: re_pass"/>
                    </div>
                    <div class="form-group">
                        <button class="btn button btn-block" data-bind="click: $root.register">
                            ĐĂNG KÝ
                        </button>
                    </div>
                    <div class="input-group">
                        Bạn đã có tài khoản? &nbsp;
                        <a href="javascript:" onclick="javascript: show_login_modal()">Đăng nhập</a>&nbsp; hoặc
                    </div>
					<div class="social-login-google" style="padding-top:5px;"><a href="{{$GLOBALS['google_login_url']}}" class="aforget">Đăng nhập với Google</a></div>
			    </div>
            </div>
        </div>
    </div>
</div>