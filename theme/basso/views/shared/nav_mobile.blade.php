<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
global $terms;
?>
<div class="head"><i class="fas fa-angle-double-left"></i> BASSO MENU</div>
{{display_categories(0, $terms)}}
<ul>
    <li><a href="{{base_url('auth/logout')}}">THOÁT</a></li>
</ul>