<?php if (!defined('BASEPATH')) exit('No direct access script allowed');
global $terms;
$data = [
    'active_class' => 'active',
    'nav_attributes' => ['class' => 'navbar-nav'],
    'top_child_attributes' => ['class' => 'nav-item'],
    'nav_item_attributes' => ['class' => 'nav-item'],
    'anchor_attributes' => ['class' => 'nav-link'],
    'dropdown_attributes' => ['class' => 'dropdown-menu']
]
?>
{{display_categories(0, $terms, $data)}}
<div class="search-order nav-item">
	<a href="javascript:void(0);" class="nav-link" rel="modal-quick-search">Xem đơn hàng</a>
</div>
<!-- fixcode 
-->
<!--
<form class="" action="/tim-kiem/amazon" style="float: right">
	<div class="row">
		<div class="col-md-12">
			<div class="input-group" style="margin-left: 25px; width: 195px;">
				<?php
				if(isset($_GET['field-keywords'])) {
					$text_search = $_GET['field-keywords'];
				} else {
					$text_search = '';
				}
				?>
				<input type="text" id="twotabsearchtextbox" value="<?php echo $text_search; ?>" name="field-keywords" autocomplete="off" placeholder="Gõ tên sản phẩm ..." class="form-control" dir="auto">
				<div class="input-group-prepend">
					<button id="btn_amz_seach" type="submit" class="input-group-text" style="background-color: #fff; border-left: 0;"><i class="fas fa-search"></i></button>
				</div>
			</div>
		</div>
	</div>
</form>
<!-- end fixcode -->