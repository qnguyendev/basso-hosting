<?php if (!defined('BASEPATH')) exit('No direct access script allowed');
$_product_url = null;
if (isset($product_url))
    $_product_url = $product_url;
?>
<div id="modal-quick-crawl">
    <section class="auto-quotes" id="crawl-form">
        <button class="close">
            x
        </button>
        <div class="container">
            <h4 class="h2-title">BÁO GIÁ TỰ ĐỘNG</h4>
            <p class="p-intro">
                Nhập đường link các sản phẩm từ <b>Ebay</b> hoặc <b>Amazon</b> để được báo giá nhanh nhất từ Basso
            </p>

            <form method="post" onsubmit="return submit_crawl()">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="www." name="url" value="{{$_product_url}}">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fas fa-dollar-sign"></i></div>
                    </div>
                </div>
                <div class="action">
                    <button type="submit" class="btn button">XEM GIÁ</button>
                </div>
            </form>
        </div>
    </section>
</div>