<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('default_layout')
@section('content')
    <div class='d-none'>
        <h1>Basso - Dịch vụ mua hộ hàng từ nước ngoài ship về Việt Nam</h1>
    </div>
    <section class="banner">
        @include('partial/slide')
    </section>
    <section class="utility">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-6">
                    <div class="img"><img data-src="{{theme_dir('images/utility-1.png')}}" alt=""
                                          class="img-fluid mx-auto d-block lazyload"></div>
                    <h3>CHI PHÍ THẤP</h3>
                </div>
                <div class="col-md-3 col-6">
                    <div class="img"><img data-src="{{theme_dir('images/utility-2.png')}}" alt=""
                                          class="img-fluid mx-auto d-block lazyload"></div>
                    <h3>VẬN CHUYỂN NHANH</h3>
                </div>
                <div class="col-md-3 col-6 mt30">
                    <div class="img"><img data-src="{{theme_dir('images/utility-3.png')}}" alt=""
                                          class="img-fluid mx-auto d-block lazyload"></div>
                    <h3>UY TÍN ĐI ĐẦU</h3>
                </div>
                <div class="col-md-3 col-6 mt30">
                    <div class="img"><img data-src="{{theme_dir('images/utility-4.png')}}" alt=""
                                          class="img-fluid mx-auto d-block lazyload"></div>
                    <h3>MIỄN THUẾ MỸ</h3>
                </div>
            </div>
        </div>
    </section>
    @include('partial/crawl_form')
    @include('partial/workflow')
    <section class="pricing">
        <div class="container">
            <h4 class="h2-title">Ở ĐÂU RẺ, CHÚNG TÔI RẺ HƠN !</h4>
            <!--p class="p-intro">Consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore
                magna
                aliquam erat volutpat. Ut wisi enim ad minim Ut wisi enim ad minim
            </p-->
            <div class="pricing-ul">
                <div class="li c1">
                    <h3>CHI PHÍ</h3>
                    <ul>
                        <li><b>THUẾ MỸ</b></li>
                        <li><b>SHIP WEB</b></li>
                        <li><b>SHIP MỸ VIỆT</b></li>
                        <li><b>THỜI GIAN CHỜ<br>BÁO GIÁ</b></li>
                        <li><b>GIÁ VỀ VN</b>
                            <p>(chưa bao gồm phí ship)</p></li>
                    </ul>
                </div>
                <div class="li c2">
                    <h3>BASSO</h3>
                    <ul>
                        <li><b>0%</b></li>
                        <li><b>FREE</b>
                            <p>(gom đơn cho khách)</p></li>
                        <li><b>KHÔNG MIN</b>
                            <p>(theo cân nặng thực tế)</p></li>
                        <li><b>3S</b>
                            <p>(tự tính giá đơn giản)</p></li>
                        <li><b>1,455,000</b>
                            <p class="icon"><i class="fas fa-check-circle"></i></p></li>
                    </ul>
                </div>
                <div class="li c3">
                    <h3>Giaonhan247</h3>
                    <ul>
                        <li>8.75%</li>
                        <li>$4 - $6</li>
                        <li>Min 1 kg</li>
                        <li>Chờ 60p</li>
                        <li>1,742,800 <p class="icon"><i class="fas fa-times-circle"></i></p></li>
                    </ul>
                </div>
                <div class="li c4">
                    <h3>Phongduy</h3>
                    <ul>
                        <li>8.75%</li>
                        <li>$4 - $6</li>
                        <li>Min 1 kg</li>
                        <li>Chờ 60p</li>
                        <li>1,742,800 <p class="icon"><i class="fas fa-times-circle"></i></p></li>
                    </ul>
                </div>
                <div class="li c5">
                    <h3>Weshop</h3>
                    <ul>
                        <li>8.75%</li>
                        <li>$4 - $6</li>
                        <li>Min 1 kg</li>
                        <li>Chờ 60p</li>
                        <li>1,742,800 <p class="icon"><i class="fas fa-times-circle"></i></p></li>
                    </ul>
                </div>
                <div class="li c6">
                    <h3>FADO</h3>
                    <ul>
                        <li>8.75%</li>
                        <li>$4 - $6</li>
                        <li>Min 1 kg</li>
                        <li>Chờ 60p</li>
                        <li>1,742,800 <p class="icon"><i class="fas fa-times-circle"></i></p></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <?php
    $widgets = get_widgets('home-featured', ['block_text']);
    if (count($widgets) > 0) {
        foreach ($widgets as $widget) {
            show_widget($widget, 'featured');
        }
    }
    ?>
    <section class="comunity">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <h4 class="title">THAM GIA CỘNG ĐỒNG<br>SĂN HÀNG HIỆU GIÁ SALE</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <h3 class="h3-title">HÀNG SALE NỮ</h3>
                            <a href="https://www.facebook.com/groups/asale.vn" rel="nofollow" target="_blank">
                                <img data-src="{{theme_dir('images/img5.jpg')}}" alt="" class="img-fluid mx-auto d-block lazyload">
                            </a>
                        </div>
                        <div class="col-md-6 mt30">
                            <h3 class="h3-title">HÀNG SALE NAM</h3>
                            <a href="https://www.facebook.com/groups/115480639076182" rel="nofollow" target="_blank">
                                <img data-src="{{theme_dir('images/img6.jpg')}}" alt="" class="img-fluid mx-auto d-block lazyload">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 video">
                    <?php
                    $widgets = get_widgets('front-youtube', ['photos']);
                    if (count($widgets) > 0) {
                        show_widget($widgets[0], 'home_youtube');
                    }
                    ?>
                </div>
            </div>
        </div>
    </section>
    <?php

    $widgets = get_widgets('home-testinomial', ['testimonial']);
    if (count($widgets) > 0) {
        foreach ($widgets as $widget) {
            show_widget($widget, 'testimonial');
        }
    }

    $widgets = get_widgets('home-brand', ['photos']);
    if (count($widgets) > 0) {
        foreach ($widgets as $widget) {
            show_widget($widget, 'brand_slider');
        }
    }
    ?>
@endsection

@section('modal')
    <?php
    $widgets = get_widgets('front-youtube', ['photos']);
    if (count($widgets) > 0) {
        show_widget($widgets[0], 'home_youtube_modal');
    }
    ?>
@endsection
