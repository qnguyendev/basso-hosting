<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<!-- SLIDER -->
<div class="wpb_revslider_element wpb_content_element">
    <div id="start_wrapper" class="rev_slider_wrapper fullwidthbanner-container">
        <div id="banner1" class="rev_slider fullwidthabanner"
             data-version="5.2.5.4">
            <ul>
                @foreach($sliders as $item)
                    <li data-index="rs-{{$item->id}}" data-transition="boxslide,curtain-1"
                        data-slotamount="0,0"
                        data-hideafterloop="0" data-hideslideonmobile="off"
                        data-easeout="0,0"
                        data-masterspeed="0,0" data-thumb="{{$item->image_path}}"
                        data-rotate="0,0" data-fstransition="notransition"
                        data-fsmasterspeed="0" data-fsslotamount="7"
                        data-saveperformance="on" data-title="Home01"
                        data-param1="" data-param2="" data-param3=""
                        data-param4="" data-param5="" data-param6=""
                        data-param7="" data-param8="" data-param9=""
                        data-param10="" data-description="">
                        <img src="{{$item->image_path}}"
                             alt="" title="Home 01"
                             data-bgposition="75% top" data-bgfit="cover"
                             data-bgrepeat="no-repeat" data-bgparallax="0"
                             class="rev-slidebg" data-no-retina>
                        <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme overlay-shape d-lg-block d-none"
                             id="slide-4-layer-{{time()}}" data-x="['center','center','center','center']"
                             data-hoffset="['0','0','0','0']" data-y="['top','top','top','top']"
                             data-voffset="['0','0','0','0']" data-width="full"
                             data-height="full" data-whitespace="normal"
                             data-transform_idle="o:1;"
                             data-transform_in="x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                             data-transform_out="x:[-100%];s:1500;e:Power3.easeInOut;"
                             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="200" data-basealign="slide"
                             data-responsive_offset="on"
                             style="z-index: 5;background-color: #00354E;border-color:rgba(0, 0, 0, 0.35); padding-right: 100px;
                             right:50%;background-clip:content-box;"></div>

                        <div class="tp-caption tp-resizeme d-lg-block d-none" id="slide-4-layer-{{time()}}"
                             data-x="['left','left','left','left']"
                             data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']"
                             data-voffset="['-130','-130','-157','-157']"
                             data-fontsize="['80','60','50','40']"
                             data-lineheight="['80','60','60','60']"
                             data-width="none" data-height="none"
                             data-whitespace="nowrap" data-transform_idle="o:1;"
                             data-transform_in="x:50px;opacity:0;s:1000;e:Power3.easeOut;"
                             data-transform_out="x:[-100%];s:1500;e:Power3.easeInOut;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="500" data-splitin="chars"
                             data-splitout="chars" data-responsive_offset="on"
                             data-elementdelay="0.1" data-endelementdelay="0.1"
                             style="z-index: 6; white-space: nowrap; font-size: 70px; line-height: 80px; font-weight: 700;
                             color: rgba(255, 255, 255, 1.00); font-family:UTMAvoBold;text-transform:uppercase; max-width: 100%">
                            {{$item->title}}
                        </div>

                        <div class="tp-caption tp-resizeme d-lg-block d-none" id="slide-4-layer-{{time()}}"
                             data-x="['left','left','left','left']"
                             data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']"
                             data-voffset="['-9','-9','-9','-15']"
                             data-fontsize="['26','24','24','18']"
                             data-width="['646','588','588','445']"
                             data-height="['115','116','116','116']"
                             data-whitespace="normal" data-transform_idle="o:1;"
                             data-transform_in="x:[-175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:1;s:1500;e:Power3.easeOut;"
                             data-transform_out="x:[-100%];s:1000;e:Power3.easeInOut;"
                             data-mask_in="x:[100%];y:0;s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="800" data-splitin="none"
                             data-splitout="none" data-responsive_offset="on"
                             style="z-index: 7; min-width: 646px; max-width: 646px; max-width: 115px; max-width: 115px; white-space: normal; font-size: 26px; line-height: 38px; font-weight: 400; color: rgba(255, 255, 255, 1.00);">
                            {{$item->content}}
                        </div>

                        <div class="tp-caption tp-resizeme d-lg-none" id="slide-4-layer-{{time()}}"
                             data-x="['center','center','center','center']"
                             data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']"
                             data-voffset="['-157','-157','-200','-200']"
                             data-fontsize="['80','60','40','35']"
                             data-lineheight="['80','60','60','60']"
                             data-width="none" data-height="none"
                             data-whitespace="nowrap" data-transform_idle="o:1;"
                             data-transform_in="x:50px;opacity:0;s:1000;e:Power3.easeOut;"
                             data-transform_out="x:[-100%];s:1500;e:Power3.easeInOut;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="500" data-splitin="chars"
                             data-splitout="chars" data-responsive_offset="on"
                             data-elementdelay="0.1" data-endelementdelay="0.1"
                             style="z-index: 6; white-space: nowrap; font-size: 70px; line-height: 80px; font-weight: 700;
                             color: rgba(255, 255, 255, 1.00); font-family:UTMAvoBold;text-transform:uppercase; max-width: 100%">
                            {{$item->title}}
                        </div>
                        <div class="tp-caption tp-resizeme" id="slide-4-layer-{{time()}}"
                             data-x="['center','center','center','center']"
                             data-hoffset="['-210','-210','-210','-210']" data-y="['top','top','top','top']"
                             data-voffset="['144','147','83','83']"
                             data-width="none" data-height="none"
                             data-whitespace="nowrap" data-visibility="['on','on','off','off']"
                             data-transform_idle="o:1;"
                             data-transform_in="x:[175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:1;s:1500;e:Power3.easeOut;"
                             data-transform_out="x:[100%];s:1000;e:Power3.easeInOut;"
                             data-mask_in="x:[-100%];y:0;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                             data-start="1000" data-responsive_offset="on"
                             style="z-index: 8;">
                            <img src="{{theme_dir('images/slide/dummy.png')}}"
                                                      alt="" width="221" height="300"
                                                      data-ww="['221px','221px','221px','221px']"
                                                      data-hh="['300px','300px','300px','300px']"
                                                      data-lazyload="{{theme_dir('images/slide/boder.png')}}"
                                                      data-no-retina>
                        </div>
                        @if($item->button_text != null)
                            <div class="tp-caption rev-btn tp-resizeme d-lg-none" id="slide-4-layer-{{time()}}"
                                 data-x="['center','center','center','center']"
                                 data-hoffset="['15','15','15','16']" data-y="['middle','middle','middle','middle']"
                                 data-voffset="['137','137','137','120']"
                                 data-width="none" data-height="none"
                                 data-whitespace="nowrap" data-transform_idle="o:1;"
                                 data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Linear.easeNone;"
                                 data-style_hover="c:rgba(255, 255, 255, 1.00);bg:rgba(29, 48, 82, 1.00);"
                                 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power2.easeInOut;"
                                 data-transform_out="y:[-100%];s:1500;"
                                 data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                                 data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                 data-start="1100" data-splitin="none"
                                 data-splitout="none" data-responsive_offset="on"
                                 style="z-index: 9; white-space: nowrap; font-size: 14px; line-height: 20px; font-weight: 800; color: #fff;background-color:#F3A23B;padding:15px 30px 15px 30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
                                @if($item->redirect_url != null)
                                    <a href="{{$item->redirect_url}}">
                                        {{$item->button_text}}
                                    </a>
                                @else
                                    {{$item->button_text}}
                                @endif
                            </div>

                            <div class="tp-caption rev-btn tp-resizeme d-lg-block d-none" id="slide-4-layer-{{time()}}"
                                 data-x="['left','left','left','left']"
                                 data-hoffset="['15','15','15','16']" data-y="['middle','middle','middle','middle']"
                                 data-voffset="['137','137','137','120']"
                                 data-width="none" data-height="none"
                                 data-whitespace="nowrap" data-transform_idle="o:1;"
                                 data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Linear.easeNone;"
                                 data-style_hover="c:rgba(255, 255, 255, 1.00);bg:rgba(29, 48, 82, 1.00);"
                                 data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power2.easeInOut;"
                                 data-transform_out="y:[-100%];s:1500;"
                                 data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                                 data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                                 data-start="1100" data-splitin="none"
                                 data-splitout="none" data-responsive_offset="on"
                                 style="z-index: 9; white-space: nowrap; font-size: 14px; line-height: 20px; font-weight: 800; color: #fff;background-color:#F3A23B;padding:15px 30px 15px 30px;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
                                @if($item->redirect_url != null)
                                    <a href="{{$item->redirect_url}}">
                                        {{$item->button_text}}
                                    </a>
                                @else
                                    {{$item->button_text}}
                                @endif
                            </div>
                        @endif
                    </li>
                @endforeach
            </ul>
            <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
        </div>
    </div>
</div>
<!-- END -->
