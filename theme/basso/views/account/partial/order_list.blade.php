<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
$status = null;
if ($this->input->get('s') != null) {
    $_s = $this->input->get('s');
    if (in_array($_s, array_keys(CustomerAccountOrderTab::LIST_STATE)))
        $status = $_s;
}
?>

<div class="order-tabs text-uppercase">
    <a href="{{base_url('tai-khoan/don-hang')}}" class="{{$status == null ?'active' : ''}}">TẤT CẢ ĐƠN HÀNG</a>
    @foreach(CustomerAccountOrderTab::LIST_STATE as $key=>$value)
        <a href="{{base_url('tai-khoan/don-hang?s='.$key)}}" class="{{$status == $key ? 'active' : ''}}">
            {{$value}}
        </a>
    @endforeach
</div>

<div class="order-tabcontent">
    @if(count($orders) == 0)
        <div class="alert alert-info text-center mt-2">Không tìm thấy đơn hàng</div>
    @else
        <form class="search-form" action="{{current_url()}}">
            <div class="row">
                <div class="col-md-7">
                    <span class="label">Tổng:</span>
                    <span class="label price">{{format_money($total_revenue->sub_total)}}</span>
                    <span class="label count">{{format_number($pagination->total_item, 0)}} đơn hàng</span>
                </div>
                <div class="col-md-5">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Tìm đơn hàng ..." name="q"
                               value="{{$this->input->get('q')}}"/>
                        <div class="input-group-prepend">
                            <button type="submit" class="input-group-text"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </form>

        <div class="table-responsive">
            <table class="table table-bordered account-order-table">
                <thead>
                <tr>
                    <th width="15%" class="text-center">Ngày đặt hàng</th>
                    <th width="15%" class="text-center">Mã ĐH</th>
                    <th width="15%" class="text-center">Website</th>
                    <th width="10%" class="text-center">Số lượng</th>
                    <th width="15%" class="text-center">Tổng tiền<br>ngoại tệ</th>
                    <th width="15%" class="text-center">Tổng tiền VNĐ</th>
                    <th width="15%" class="text-center">Trạng thái</th>
                </tr>
                </thead>
                <tbody>
                @foreach($orders as $item)
                    <tr>
                        <td class="text-center">
                            {{date('d/m/Y', $item->created_time)}}
                        </td>
                        <td class="text-center">
                            {{$item->order_code}}
                        </td>
                        <td class="text-center">
                            {{str_replace('www.', '', $item->website)}}
                        </td>
                        <td class="text-center">
                            {{$item->quantity}}
                        </td>
                        <td class="text-center">
                            {{$item->currency_symbol}} {{$item->total + $item->web_shipping_fee}}
                        </td>
                        <td class="text-center">
                            {{format_money($item->sub_total)}}
                        </td>
                        <td class="text-center">
                            @foreach(CustomerOrderStatus::LIST_STATE as $key=>$value)
                                @if($key == $item->status)
                                    <span class="text-{{$value['class']}}">{{$value['name']}}</span>
                                @endif
                            @endforeach
                            <a href="?id={{$item->order_code}}" class="detail mt-1">Chi tiết</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="row">
            <div class="col-sm-12 col-md-6">
                <?php
                $attributes = [
                    'ul_attr' => ['class' => 'pagination'],
                    'li_attr' => ['class' => 'page-item'],
                    'anchor_attr' => ['class' => 'page-link']
                ];
                Pagination::display(base_url('tai-khoan/don-hang'), $pagination->current_page, $pagination->total_page, $attributes);
                ?>
            </div>
            <div class="col-sm-12 col-md-6 text-right">
                Trang {{$pagination->current_page}}/{{$pagination->total_page}},
                có {{format_number($pagination->total_item, 0)}} kết quả
            </div>
        </div>
    @endif
</div>