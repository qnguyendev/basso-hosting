<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal" tabindex="-1" role="dialog" id="item-detail-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ghi chú sản phẩm</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <textarea data-bind="value: items.modal.note" cols="10" rows="5" class="form-control"></textarea>
            </div>
            <div class="modal-footer text-center">
                <button class="btn btn-primary m-auto" data-bind="click: items.modal.update">
                    <i class="fa fa-check"></i>
                    CẬP NHẬT
                </button>
            </div>
        </div>
    </div>
</div>