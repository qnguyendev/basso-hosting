<?php if (!defined('BASEPATH')) exit('No direct access script allowed');
?>
<div class="table-responsive">
    <table class="table table-bordered order-table">
        <thead>
        <tr>
            <th colspan="2">Tên/Link sản phẩm</th>
            <th width="200">Mô tả</th>
            <th style="min-width: 100px">Giá sp</th>
            <th class="text-center" width="150">Số lượng</th>
            <th style="min-width: 120px">Giá phụ thu</th>
            <th style="min-width: 130px">Phí vận chuyển</th>
            <th width="140" class="text-right">Thành tiền</th>
            <th class="text-center" width="80"></th>
        </tr>
        </thead>
        <tbody data-bind="foreach: items.list">
        <tr>
            <td>
                <img data-bind="attr: {src: image()}" width="80"/>
            </td>
            <td>
                <a data-bind="text: name, attr: {href: `/item/${product_id()}.html`}"></a><br/>
                Website: <span data-bind="text: website"></span>
            </td>
            <td>
                <!--ko foreach: variations-->
                <b data-bind="text: name"></b>:
                <span data-bind="text: value"></span><br/>
                <!--/ko-->
            </td>
            <td data-bind="text: `$ ${parseFloat(origin_price()).toFixed(2)}`"></td>
            <td>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <button class="btn btn-sm" data-bind="click: $root.items.minus" style="background: #DDD">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                    <input type="number" style="width: 30px" min="1" class="text-center form-control"
                           data-bind="value: qty, event: {change: $root.items.change_qty}"/>
                    <div class="input-group-append">
                        <button class="btn btn-sm" data-bind="click: $root.items.plus" style="background: #DDD">
                            <i class="fa fa-plus"></i>
                        </button>
                    </div>
                </div>
            </td>
            <td class="text-right" data-bind="text: `${parseFloat(term_fee()).toMoney(0)}`"></td>
            <td class="text-right" data-bind="text: `${parseFloat(shipping_fee()).toMoney(0)}`"></td>
            <td class="text-right">
                <b data-bind="text: parseFloat(sub_total()).toMoney(0)"></b>
            </td>
            <td class="text-center">
                <a href="#" data-bind="click: $root.items.delete" class="text-danger">
                    <i class="far fa-trash-alt"></i>
                </a>
            </td>
        </tr>
        </tbody>
    </table>
</div>
