<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
$banks = $this->payment_model->get_banks();
?>
<div class="order-payment">
    <div class="order-wrapper">
        <div class="order-menu">
            <div class="li sub">
                Phương thức thanh toán
            </div>
            @for($i = 0; $i < count($payments); $i++)
                <div class="li {{$i == 0 ? 'active' : null}}">
                    <a href="javascript:" rel="{{$payments[$i]->type}}">
                        <i class="far fa-circle"></i> {{$payments[$i]->name}}
                    </a>
                </div>
            @endfor
        </div>
        <div class="order-content">
            <div class="accordion" id="accordionPayment">
                @for($i = 0; $i < count($payment_limits); $i++)
                    <div class="card">
                        <div class="card-header p-0" id="heading-{{$i}}">
                            <div class="custom-control custom-radio custom-control-inline d-block p-0"
                                 data-toggle="collapse" data-target="#collapse-{{$i}}" aria-expanded="true"
                                 aria-controls="collapse-{{$i}}">

                                <input type="radio" id="headship{{$i}}" name="headship"
                                       value="{{$payment_limits[$i]->id}}"
                                       class="custom-control-input" {{$i == 0 ? 'checked' : null}}/>

                                <label class="custom-control-label d-block m-0 pt-3 pb-3 pl-4 pr-4"
                                       for="headship{{$i}}"/>
                                Thanh toán
                                {{$payment_limits[$i]->limit == 100 ? 'toàn bộ' : "{$payment_limits[$i]->limit}%"}}
                                chi phí đơn hàng
                                @if($payment_limits[$i]->fee > 0)
                                    <i>
                                        (+{{$payment_limits[$i]->fee}}% phí dịch vụ)
                                    </i>
                                    @endif
                                    </label>
                            </div>
                        </div>

                        <div id="collapse-{{$i}}" class="collapse {{$i == 0 ? 'show': null}}"
                             aria-labelledby="heading-{{$i}}"
                             data-parent="#accordionPayment">
                            <div class="card-body">
                                @for($j = 0; $j < count($payments); $j++)
                                    <div class="payment-method {{$j > 0 ? 'd-none' : null}}"
                                         rel="{{$payments[$j]->type}}">
                                        <p><b>{{$payments[$j]->name}}</b></p>
                                        <p class="text-blue">
                                            {{$payments[$j]->description}}
                                        </p>
                                    </div>
                                @endfor
                                <div class="order-discount">
                                    <label class="order-label">Mã giảm giá:</label>
                                    <div class="fg">
                                        <div class="input-group">
                                            <input type="twext" class="form-control" data-bind="value: voucher.code"/>
                                            <div class="input-group-prepend">
                                                <button type="submit" class="input-group-text"
                                                        data-bind="click: voucher.apply">
                                                    ÁP DỤNG
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <p class="order-total pb-1 mb-0">
                                    <label class="order-label text-dark">Tổng giá sản phẩm (<span
                                                data-bind="text: cart.total_qty"></span>):</label>
                                    <b class="text-dark"
                                       data-bind="text: parseInt(cart.total() * {{($payment_limits[$i]->fee + 100)/100}}).toMoney(0)"></b>
                                    <!--ko if: voucher.detail.value() > 0-->
                                    <br/>
                                <!--ko if: voucher.detail.type() == '{{DiscountType::AMOUNT}}'-->
                                    <label class="order-label text-dark">Giảm giá:</label>
                                    <b class="text-dark"
                                       data-bind="text: parseInt(voucher.detail.value()).toMoney(0)"></b>
                                    <!--/ko-->
                                <!--ko if: voucher.detail.type() == '{{DiscountType::PERCENT}}'-->
                                    <label class="order-label text-dark">Giảm giá:</label>
                                    <b class="text-dark"
                                       data-bind="text: parseInt(calc_discount_value(cart.total() * {{$payment_limits[$i]->limit/100}} * {{($payment_limits[$i]->fee + 100)/100}}, voucher.detail.value())).toMoney(0)"></b>
                                    <!--/ko-->
                                    <!--/ko-->
                                </p>
                                <p class="order-total mt-0">
                                    <!--ko if: voucher.detail.value() == 0-->
                                    <label class="order-label text-dark font-weight-bold">
                                        Thanh toán {{$payment_limits[$i]->limit}}%:
                                    </label>
                                    <b class="text-dark" style="font-size: 1.2em"
                                       data-bind="text: parseInt(cart.total() * {{$payment_limits[$i]->limit/100}} * {{($payment_limits[$i]->fee + 100)/100}}).toMoney(0)"></b>
                                    <!--/ko-->
                                    <!--ko if: voucher.detail.value() > 0-->
                                <!--ko if: voucher.detail.type() == '{{DiscountType::AMOUNT}}'-->
                                    <label class="order-label text-dark font-weight-bold">
                                        Thanh toán {{$payment_limits[$i]->limit}}%:
                                    </label>
                                    <b class="text-dark" style="font-size: 1.2em"
                                       data-bind="text: parseInt(cart.total() * {{$payment_limits[$i]->limit/100}} * {{($payment_limits[$i]->fee + 100)/100}} - voucher.detail.value()).toMoney(0)"></b>
                                    <!--/ko-->
                                <!--ko if: voucher.detail.type() == '{{DiscountType::PERCENT}}'-->
                                    <label class="order-label text-dark font-weight-bold">
                                        Thanh toán {{$payment_limits[$i]->limit}}%:
                                    </label>
                                    <b class="text-dark" style="font-size: 1.2em"
                                       data-bind="text: parseInt(calc_sub_total(cart.total() * {{$payment_limits[$i]->limit/100}} * {{($payment_limits[$i]->fee + 100)/100}}, voucher.detail.value())).toMoney(0)"></b>
                                    <!--/ko-->
                                    <!--/ko-->
                                </p>
                                <p>
                                    <b>Ghi chú đơn hàng:</b>
                                </p>
                                <textarea class="form-control" data-bind="value: shipping_info.note"></textarea>
                            </div>
                        </div>
                    </div>
                @endfor
            </div>

            @if(in_array('bank-transfer', array_column($payments, 'type')))
                <div rel="bank-transfer" class="payment-method d-none row">
                    @foreach($banks as $bank)
                        <div class="col-12 col-lg-6">
                            <strong>{{$bank->bank_name}}</strong>
                            <ul>
                                <li>STK: {{$bank->bank_number}}</li>
                                <li>{{$bank->bank_account}}</li>
                                <li>{{$bank->bank_branch}}</li>
                            </ul>
                        </div>
                    @endforeach
                </div>
            @endif

            <div class="order-footer">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" id="agree" name="agree" class="custom-control-input">
                    <label class="custom-control-label" for="agree">Tôi đồng ý với tất cả các
                        <a target="_blank" href="#term-modal" data-toggle="modal" class="mfp-ajax">
                            điều khoản và điều kiện</a>
                        trên
                    </label>
                </div>
                <button class="btn button btnpayment" data-bind="click: function(){ $root.cart.create(); }">THANH TOÁN</button>
            </div>
        </div>
    </div>
</div>
