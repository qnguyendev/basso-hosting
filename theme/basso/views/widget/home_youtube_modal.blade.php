<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal" tabindex="-1" role="dialog" id="modal-home-youtube">
    <div class="modal-dialog modal-lg modal-xl" role="document">
        <div class="modal-content">
            <?php
            $url = '';
            if ($widget->data[0]->link != null) {
                if (!empty($widget->data[0]->link))
                    $url = str_replace('watch?v=', 'embed/', $widget->data[0]->link);
            }
            ?>
            <div class="videoWrapper">
                <iframe src="about:blank" width="1903" height="768" data-src="{{$url}}" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
