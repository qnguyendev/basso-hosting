<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
$index = 1;
?>
<section class="featured">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-12 img">
                <?php
                $widgets = get_widgets('home-featured-img', ['photos']);
                ?>
                @if(count($widgets) > 0)
                    <?php show_widget($widgets[0], 'home_featured_img'); ?>
                @else
                    <img data-src="{{theme_dir('images/img4.jpg')}}" alt="" class="img-fluid mx-auto d-block lazyload"/>
                @endif
            </div>
            <div class="col-lg-8 col-md-12">
                <div class="row">
                    @foreach($widget->data as $item)
                        <div class="col-lg-6 col-md-12">
                            <div class="li">
                                @if($index == 1)
                                    <i class="fa fa-plane"></i>
                                @elseif($index == 2)
                                    <i class="fa fa-money"></i>
                                @elseif($index == 3)
                                    <i class="fa fa-gift"></i>
                                @elseif($index == 4)
                                    <i class="fa fa-life-ring"></i>
                                @endif
                                <h3 class="h3-title">{{$item->title}}</h3>
                                <div class="content">{{$item->content}}</div>
                            </div>
                        </div>
                        <?php $index++; ?>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
