<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@if($widget->show_title)
    <h3 class="title">{{$widget->title}}</h3>
@endif
{{$widget->data}}
