<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@if(count($widget->data) > 0)
    <?php $item = $widget->data[0]; ?>
    <li class="help">
        <a href="{{$item->link}}" {{$item->follow == 'nofollow' ? 'rel="nofollow"' : ''}}
                {{$item->new_tab == 1 ? 'target="_blank"' : ''}}>
            {{$item->text}}
        </a>
    </li>
@endif
