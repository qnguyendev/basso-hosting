<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('default_layout')
@section('content')
    <div class="w" id="product-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    <img src="https://cdn.samsung.com/etc/designs/smg/global/imgs/support/cont/NO_IMG_600x600.png"
                         class="img-fluid"/>
                </div>
                <div class="col-lg-7 mt-5">
                    <div class="row">
                        <div class="col-lg-7">
                            <div class="alert alert-warning">
                                Giá sản phẩm này chưa có, quý khách nhấn phím F5 để được cập nhập giá. Quý khách muốn
                                mua sản phẩm chưa có giá thì nhấn "Yêu cầu báo giá" nhập thông tin để thông báo giá cho
                                quý khách qua email.
                            </div>

                            <a class="btn btn-buy" href="javascript:void(Tawk_API.toggle())"
                               target="_blank">
                                <i></i>
                                CHAT VỚI CSKH
                            </a>
                        </div>
                        <div class="col-lg-5">
                            @include('partial/add_info')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection