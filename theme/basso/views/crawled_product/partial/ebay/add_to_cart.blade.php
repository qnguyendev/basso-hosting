<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<form method="post" class="product-cart" id="cart-form" onsubmit="return submit_cart()">
    <table class="table table-borderless mb-0">
        @foreach($product->variations as $key=>$value)
            <tr>
                <td>{{$key}}</td>
                <td>
                    <select name="{{$key}}" class="form-control" rel="variant"
                            data-bind="event: {change: $root.change_variant}">
                        @foreach($value as $k=>$v)
                            <option value="{{$v}}">{{$v}}</option>
                        @endforeach
                    </select>
                </td>
            </tr>
        @endforeach
        <td>Số lượng</td>
        <td>
            <select name="quantity" class="form-control">
                @for($i = 1; $i <= 10; $i++)
                    <option value="{{$i}}">{{$i}}</option>
                @endfor
            </select>
        </td>
    </table>
    <button class="btn btn-buy" type="submit">
        <i></i>
        Mua ngay
    </button>
    Hiện còn <b class="text-success">{{$product->avaiable_quantity}}</b>
    sản phẩm,
    <b class="text-success">{{$product->sold_quantity}}</b> đã bán
    <input type="hidden" name="id" value="{{$product->id}}"/>
    <input type="hidden" name="image" value="{{$product->images[0]}}"/>
</form>

@include('partial/social_share')
