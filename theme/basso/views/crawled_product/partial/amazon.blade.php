<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="w" id="product-page">
    <div class="container">
        <div class="row">
            <div class="col-lg-5">
                @include('partial/amazon/images')
                <p class="pt-2">
                    <a href="{{$product->url}}" target="_blank">
                        Xem link gốc <i class="fa fa-arrow-right"></i>
                    </a>
                </p>
            </div>
            <div class="col-lg-7">
                <h1 class="product-head">
                    {{$product->title}}
                </h1>

                <div class="row">
                    <div class="col-lg-7">
                        <span class="product-price" data-bind="text: price"></span>
						<?php
                        /* <a href="javascript:" class="view-price-detail">
                            (
                            <!--ko if: condition() != null-->
                            <span data-bind="text: `Buy ${condition()}`"></span> -
                            <!--/ko-->
                            Xem chi tiết)
                        </a> */
						?>
                        <ul class="view-price-detail">
                            <li>
                                Giá sản phẩm trên Amazon: <b data-bind="text: origin_price"></b>
                            </li>
                            <li data-bind="visible: term_fee() > 0">
                                Giá phụ thu (nếu có): <b data-bind="text: parseInt(term_fee()).toMoney(0)"></b>
                            </li>
                            <li data-bind="visible: shipping_fee() > 0">
                                Phí vận chuyển: <b data-bind="text: parseInt(shipping_fee()).toMoney(0)"></b>
                            </li>
                        </ul>
                        <div class="product-meta highlight" data-bind="visible: weight() > 0">
                            (Giá trọn gói về Việt Nam với trọng lượng ước tính
                            <b><span data-bind="text: parseFloat(weight()).toFixed(2)"></span> kg</b>)
                        </div>
                        <div class="product-meta highlight p-3" data-bind="visible: weight() <= 0">
                            Giá trên chưa bao gồm phí vận chuyển quốc tế. Basso sẽ thu thêm
                            <strong>260k/kg</strong>
                            theo cân
                            nặng thực tế khi hàng về
                        </div>
                        <div class="product-meta">
                            <p>
                                <strong>Bán bởi:</strong> {{$product->brand}}
                            </p>
                        </div>

                        <form method="post" class="product-cart" id="cart-form" onsubmit="return submit_cart()">

                            <!--ko foreach: variations-->
                            <div class="mb-2">
                                <div class="mb-1">
                                    <strong data-bind="text: `${name()}`"></strong>: <span
                                            data-bind="text: selected"></span>
                                </div>
                                <!--ko foreach: values-->
                                <div class="d-inline" data-bind="click: $root.change_variant_label">
                                    <label data-bind="attr: {name: $parent.name}, click: $root.change_variant_label,
                                    css: 'variant-label ' + (enable() ? 'enable' : '') + (value() == $parent.selected() ? ' active' : '')
                                    + (image() != null ? ' image' : '')">
                                        <!--ko if: image() == null-->
                                        <span data-bind="text: value, click: $root.change_variant_label"></span>
                                        <!--/ko-->
                                        <!--ko if: image() != null-->
                                        <img data-bind="attr: {src: image}, click: $root.change_variant_label"/>
                                        <!--/ko-->
                                    </label>
                                </div>
                                <!--/ko-->
                                <input type="hidden" data-bind="attr: {name: name}, value: selected" rel="variant"/>
                            </div>
                            <!--/ko-->

                            <table class="table table-borderless mb-0">
                                <!--tbody data-bind="foreach: variations">
                                <tr>
                                    <td data-bind="text: name"></td>
                                    <td>
                                        <select data-bind="attr: {name: name}, value: selected, foreach: values, event: {change: $root.change_variant}"
                                                class="form-control" rel="variant">
                                            <option data-bind="value: value, text: value, enable: enable"></option>
                                        </select>
                                    </td>
                                </tr>
                                </tbody-->
                                <td width="70">Số lượng</td>
                                <td>
                                    <select name="quantity" class="form-control">
                                        @for($i = 1; $i <= 10; $i++) 
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </td>
                            </table>
							@if($this->ion_auth->logged_in())
                            <button class="btn btn-buy" data-login="1"  type="submit">
                                <i></i>
                                Mua ngay
                            </button>
							@else
                            <button class="btn btn-buy" data-login="0" onclick="return check_login();"  type="button">
                                <i></i>
                                Mua ngay
                            </button>
							@endif
                            <input type="hidden" name="id" data-bind="value: product_id"/>
                            <input type="hidden" name="image" data-bind="value: thumbnail"/>
                        </form>
                        @include('partial/social_share')
                        <strong>Danh mục:</strong> {{join(', ', json_decode($product->categories))}}
                    </div>
                    <div class="col-lg-5">
                        @include('partial/add_info')
                    </div>
                </div>
            </div>
        </div>

        <div class="product-content pt-3 clearfix">
            <div class="product-content-title">
                THÔNG SỐ SẢN PHẨM (MÃ SP: {{$child->id}})
            </div>
            <div class="product-content-data">
                <table class="table table-striped table-bordered">
                    <tbody data-bind="foreach: attributes">
                    <tr>
                        <th data-bind="text: name"></th>
                        <td data-bind="text: value"></td>
                    </tr>
                    </tbody>
                </table>

            </div>
            <div class="product-content-title">THÔNG TIN SẢN PHẨM</div>
            <div class="product-content-data p-0">
                <ul>
                    @foreach($product->description as $key=>$value)
                        <li>{{$value}}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>