<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
$rating_avg = 0;
if (isset($rating)) {
    $rating_avg = floatval($rating);
}
?>
@layout('default_layout')
@section('content')
    <main class="product-page">
        @if(isset($website))
            @if($website == 'ebay')
                @include('partial/ebay')
            @elseif($website == 'amazon')
                @include('partial/amazon')
            @endif
        @else
            @include('partial/no_product')
        @endif
    </main>
@endsection

@section('footer')
    @if(!isset($product_id))
        @if(isset($website))
            @if($website == 'amazon')
                <script>
                    var product = {
                        id: '{{$child->id}}',
                        images: {{json_encode($child->images)}},
                        price: '{{format_money($child->price)}}',
                        origin_price: '$ {{$child->origin_price}}',
                        shipping_fee: '{{$child->shipping_fee}}',
                        term_fee: '{{$child->term_fee}}',
                        weight: '{{$child->weight}}',
                        variations: {{json_encode($product->variations->detail)}},
                        variation_list: {{json_encode($variations)}},
                        attributes: {{json_encode($child->attributes)}},
                        condition: '{{$child->condition}}'
                    };
                </script>
                <script src="/assets/js/core/underscore.min.js"></script>
                <script src="/assets/js/core/underscore.ko.min.js"></script>
                <script src="{{theme_dir('js/product.js?v='.time())}}"></script>
            @elseif($website == 'ebay')
                <script>
                    var product = {
                        id: '{{$product->id}}',
                        price: '{{format_money($product->price)}}',
                        origin_price: '$ {{$product->origin_price}}',
                        web_shipping_fee: '{{$product->web_shipping_fee}}',
                        term_fee: '{{$product->term_fee}}'
                    };
                </script>
                <script src="{{theme_dir('js/product_ebay.js?v='.time())}}"></script>
            @endif
        @endif
    @else
        <script>
            window.onload = function () {
                if ($('#crawl-form-head').is(':hidden')) {
                    $('#modal-quick-crawl').fadeIn(0, function () {
                        $('#modal-quick-crawl .auto-quotes').slideDown(0, function(){
                            submit_crawl();
                        });
                    });
                } else {
                    submit_crawl_head();
                }
            };
        </script>
    @endif
@endsection

@section('head')
    <style>
        .swal-footer {
            text-align: center !important;
        }
    </style>
@endsection