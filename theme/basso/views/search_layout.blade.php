<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
$user = $this->ion_auth->user()->row();
$action = $this->router->fetch_method();
?>
@layout('default_layout')
@section('content')
    <main class="order-page">
        <div class="container">
            <h2 class="h2-title mb-5">THÔNG TIN ĐƠN HÀNG <?php echo isset($_GET["q"])?$_GET["q"]:""; ?></h2>
			@if($order != null)
            <div class="order-wrapper">
                <div class="">
                    @yield('right-content')
                </div>
            </div>
			@else
				<p>Không tìm thấy đơn hàng</p>
			@endif
        </div>
    </main>
@endsection