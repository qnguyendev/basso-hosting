<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

require('./envConfig.php');

if (!isset($theme_config)) {
    $theme_config = [];
}

$theme_config['license'] = [
    'domain' => $envConfig['DOMAIN_LICENSE'] ?? [],
    'expire' => strtotime('3019-04-30 00:00:00')
];