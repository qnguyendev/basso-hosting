function productViewModel() {
    var self = this;
    self.id = ko.observable(product.id);
    self.price = ko.observable(product.price);
    self.origin_price = ko.observable(product.origin_price);
    self.term_fee = ko.observable(product.term_fee);
    self.web_shipping_fee = ko.observable(product.web_shipping_fee);

    self.init = function () {
        window.onload = function () {
            $('.gallery').slick({
                autoplay: true,
                vertical: false,
                verticalSwiping: false,
                prevArrow: '<a class="slick-prev"><i class="fas fa-chevron-left"></i></button>',
                nextArrow: '<a class="slick-next"><i class="fas fa-chevron-right"></i></button>',
                //mobileFirst: true,
                slidesToShow: 5,
                responsive: [
                    {
                        breakpoint: 1025,
                        settings: {
                            slidesToShow: 4
                        }
                    },
                    {
                        breakpoint: 769,
                        settings: {
                            slidesToShow: 4
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 3
                        }
                    }
                ]
            });
        };
    };

    self.change_variant = function () {
        var selected_variant = [];
        $('#product-page select[rel=variant]').each(function () {
            selected_variant.push($(this).val());
        });

        AJAX.get(window.location, {item_id: self.id(), variant: selected_variant}, true, function (res) {
            self.price(res.price);
            self.origin_price(res.origin_price);
        });
    };
}

var productModel = new productViewModel();
productModel.init();
ko.applyBindings(productModel, document.getElementById('product-page'));