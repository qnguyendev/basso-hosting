/******************************************
 -    PREPARE PLACEHOLDER FOR SLIDER    -
 ******************************************/
(function ($) {
    try {
        var e = new Object,
            i = jQuery(window).width(),
            t = 9999,
            r = 0,
            n = 0,
            l = 0,
            f = 0,
            s = 0,
            h = 0;
        e.c = jQuery('#banner1');
        e.responsiveLevels = [1240, 1024, 778, 480];
        e.gridwidth = [1200, 1024, 778, 480];
        e.gridheight = [550, 550, 550, 550];

        e.sliderLayout = "fullwidth";
        if (e.responsiveLevels && (jQuery.each(e.responsiveLevels,
            function (e, f) {
                f > i && (t = r = f, l = e), i >
                f && f > r && (r = f, n = e)
            }), t > r && (l = n)), f = e.gridheight[
            l] || e.gridheight[0] || e.gridheight,
            s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth,
            h = i / s, h = h > 1 ? 1 : h, f = Math.round(
            h * f), "fullscreen" == e.sliderLayout) {
            var u = (e.c.width(), jQuery(window).height());
            if (void 0 != e.fullScreenOffsetContainer) {
                var c = e.fullScreenOffsetContainer.split(",");
                if (c) jQuery.each(c, function (e, i) {
                    u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                }), e.fullScreenOffset.split("%").length >
                1 && void 0 != e.fullScreenOffset &&
                e.fullScreenOffset.length > 0 ? u -=
                    jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 :
                    void 0 != e.fullScreenOffset && e.fullScreenOffset
                        .length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
            }
            f = u
        } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
        e.c.closest(".rev_slider_wrapper").css({
            height: f
        })
    } catch (d) {
        console.log("Failure at Presize of Slider:" + d)
    }

    jQuery("#banner1").show().revolution({
        sliderType: "standard",
        jsFileLocation: "theme/basso/statics/libraries/g5plus/js/",
        sliderLayout: "responsive",
        dottedOverlay: "true",
        delay: 5000,
        onHoverStop: 'off',
        navigation: {
            keyboardNavigation: "off",
            keyboard_direction: "horizontal",
            mouseScrollNavigation: "off",
            mouseScrollReverse: "default",
            onHoverStop: "off",
            arrows: {
                style: "my-nav",
                enable: true,
                rtl: true,
                hide_onmobile: true,
                hide_under: 600,
                hide_onleave: true,
                hide_delay: 200,
                hide_delay_mobile: 1200,
                tmp: '',
                left: {
                    h_align: "left",
                    v_align: "center",
                    h_offset: 30,
                    v_offset: 0
                },
                right: {
                    h_align: "right",
                    v_align: "center",
                    h_offset: 30,
                    v_offset: 0
                }
            }
        },
        responsiveLevels: [1240, 1024, 778, 480],
        visibilityLevels: [1240, 1024, 778, 480],
        gridwidth: [1200, 1024, 778, 480],
        gridheight: [550, 550, 550, 550],
        lazyType: "none",
        parallax: {
            type: "mouse",
            origo: "slidercenter",
            speed: 2000,
            levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50, -50, -60, 15, 10, 100, 55],
            type: "mouse",
        },
        shadow: 0,
        spinner: "off",
        stopLoop: "off",
        stopAfterLoops: -1,
        stopAtSlide: -1,
        shuffle: "off",
        autoHeight: "off",
        disableProgressBar: "on",
        hideThumbsOnMobile: "off",
        hideSliderAtLimit: 0,
        hideCaptionAtLimit: 0,
        hideAllCaptionAtLilmit: 0,
        debugMode: false,
        fallbacks: {
            simplifyAll: "off",
            nextSlideOnWindowFocus: "off",
            disableFocusListener: false,
        }
    });

    // carousel
    $('.partner').slick({
        autoplaySpeed: 3000,
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        dots: false,
        arrows: true,
        prevArrow: '<a class="slick-prev" href="javascript:;"><i class="fas fa-angle-left"></i></a>',
        nextArrow: '<a class="slick-next" href="javascript:;"><i class="fas fa-angle-right"></i></a>',
        responsive: [
            {
                breakpoint: 1025,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 4
                }
            }
        ]
    });

    $('.reviews').slick({
        autoplaySpeed: 3000,
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: true,
        dots: false,
        arrows: true,
        prevArrow: '<a class="slick-prev" href="javascript:;"><i class="fas fa-angle-left"></i></a>',
        nextArrow: '<a class="slick-next" href="javascript:;"><i class="fas fa-angle-right"></i></a>',
        responsive: [
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1
                }
            }
        ]
    });

    $(document).on('click', '.mfp-dismiss', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });

    // Automation gif
    if ($(".order-workflow").length > 0) {
        var winW = $(window).width();
        var winH = $(window).height();

        var play1 = 1;
        var play2 = 1;
        var play3 = 1;

        $(window).scroll(function () {
            var text1H = $('.text1').offset().top + $('.text1').outerHeight() - window.innerHeight;
            var text2H = $('.text2').offset().top + $('.text2').outerHeight() - window.innerHeight;
            var text3H = $('.text3').offset().top + $('.text3').outerHeight() - window.innerHeight;

            if (winW < 481) {
                text1H = $('.img1').offset().top + $('.img1').outerHeight() - window.innerHeight;
                text2H = $('.img3').offset().top + $('.img3').outerHeight() - window.innerHeight;
                text3H = $('.img4').offset().top + $('.img4').outerHeight() - window.innerHeight;
            }

            if ($(window).scrollTop() >= text1H) {
                if (play1) {
                    var img1 = $('.order-workflow .img1 img');
                    img1.attr('src', img1.data('play'));
                    play1 = 0;
                }
            }

            if ($(window).scrollTop() >= text2H) {
                if (play2) {
                    if (winW < 481) {
                        var img3 = $('.order-workflow .img3 img');
                        img3.attr('src', img3.data('play'));
                    } else {
                        var img2 = $('.order-workflow .img2 img');
                        img2.attr('src', img2.data('play'));
                    }

                    play2 = 0;
                }
            }

            if ($(window).scrollTop() >= text3H) {
                if (play3) {
                    var img4 = $('.order-workflow .img4 img');
                    img4.attr('src', img4.data('play'));
                    play3 = 0;
                }
            }
        });
    }

    // menu
    var sticky = $(".search-menu").offset().top;
    $(window).scroll(function () {
        if ($(document).scrollTop() > sticky) {
            $(".search-menu").addClass("sticky");
        } else {
            $(".search-menu").removeClass("sticky");
        }
    });

    // run to top
    $(window).scroll(function () {
        if ($(this).scrollTop() > 500) {
            $('#backtop').fadeIn();
        } else {
            $('#backtop').fadeOut();
        }
    });

    $('#backtop').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
    });

    // device menu
    $('.navdevice .head').click(function () {
        $('#navdevice').removeClass('active');
        $(".navdevice").animate({"left": "-100%"});
    });

    $('#navdevice').click(function () {
        var left = "-100%";
        if ($(this).is('.active')) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
            left = "0";
        }

        $(".navdevice").animate({"left": left}).niceScroll();
    });

    $(window).bind('load resize', function () {
        var w = $(this).width();
        if (w > 768) {
            $('#navdevice').removeClass('active');
            //$(".navdevice").animate({"left": "-100%"}).niceScroll();
        }
    });

    $('.readmore').click(function (e) {
        e.preventDefault();
        $('.product-intro').toggleClass('active');

        if ($('.product-intro').is('.active')) {
            $(this).html('rút gọn <i class="fas fa-caret-up"></i>');
        } else {
            $(this).html('xem thêm <i class="fas fa-caret-down"></i>');
        }
    });

    $('a[rel=modal-quick-crawl]').click(function () {
        $('#modal-quick-crawl').fadeIn(200, function () {
            $('#modal-quick-crawl .auto-quotes').slideDown(200);
        });
    });

    $('#modal-quick-crawl button.close').click(function () {
        $('#modal-quick-crawl .auto-quotes').slideUp(200, function () {
            $('#modal-quick-crawl input').attr('value', null);
            $('#modal-quick-crawl').fadeOut(200);
        });
    });

    $('a[rel=modal-quick-search]').click(function () {
        $('#modal-quick-search').fadeIn(200, function () {
            $('#modal-quick-search .auto-quotes').slideDown(200);
        });
    });

    $('#modal-quick-search button.close').click(function () {
        $('#modal-quick-search .auto-quotes').slideUp(200, function () {
            $('#modal-quick-search input').attr('value', null);
            $('#modal-quick-search').fadeOut(200);
        });
    });

    $('a.view-price-detail').click(function () {
        $('ul.view-price-detail').slideToggle();
    });

    $('#modal-quick-crawl').on('click', function (e) {
        if ($(e.target).closest("#modal-quick-crawl .auto-quotes").length === 0) {
            $('#modal-quick-crawl button.close').click();
        }
    });
}(jQuery));

var crawl_error = false;
var crawl_source = 'front';
var interval = null;

function submit_cart() {
    var url = $('form#cart-form').attr('action');
    var data = $('form#cart-form').serialize();
    AJAX.post(url, data, true, function (res) {
        if (res.error) {
            NOTI.danger(res.message);
        } else {
            AJAX.get('/', {action: 'cart'}, false, function (res) {
                    $('span#cart-total').html(res.data);
                    Swal.fire({
                        icon: 'success',
                        title: 'Đã thêm sản phẩm vào giỏ hàng',
                        /* buttons: {
                            cancel: "Ở lại trang",
                            catch: {
                                text: "Tới giỏ hàng",
                                value: "go_cart",
                            },
                            defeat: false,
                        }, */
						showCancelButton: true,      
						/* confirmButtonColor: "#DD6B55", */   
						cancelButtonColor: "#F3A23B",   
						confirmButtonText: "Ở lại trang",   
						cancelButtonText: "Đến giỏ hàng",   
						closeOnConfirm: false,   
						closeOnCancel: true,
						customClass: "Custom_Cancel"
                    }).then(function (value) {
                        /* switch (value) {
                            case "go_cart":
                                window.location = '/gio-hang';
                                break;
                                break;
                            default:
                                break;
                        } */
						
						if (value.dismiss == "cancel") {     
							window.location = '/gio-hang';   
						}
					});

                }
            );
        }
    });

    return false;
}

function submit_crawl() {
    $('#countdown-bg').show();
    crawl_source = 'front';

    var countdownNumberEl = document.getElementById('countdown-number');
    var countdown = 10;
    countdownNumberEl.textContent = countdown;

    interval = setInterval(function () {
        if (countdown <= 1) {
            $('#modal-crawl-error').modal();
            $('#countdown-bg').hide();
            crawl_error = true;
            window.clearInterval(interval);
        } else {
            countdown = --countdown <= 0 ? 10 : countdown;
            countdownNumberEl.textContent = countdown;
        }
    }, 1000);

    var data = {url: $('#crawl-form input').val()};
    AJAX.post('/', data, false, function (res) {
        if (res.error) {
            if (res.error_sites == null)
                ALERT.error(res.message);
            else {
				/* fixcode nhap chung search amazon url va ten sp thanh 1 hop search */
				$("#home_search_form").attr("onsubmit", "");
				$("#home_search_form").attr("method", "get");
				$("#home_search_form").attr("action", "/tim-kiem/amazon");
				$("#home_search_form").submit();
				
                /* MODAL.show('#modal-no-support_sites');
                $('#modal-quick-crawl button.close').click(); */
            }

            $('#countdown-bg').hide();
            window.clearInterval(interval);
        } else {
            if (res.redirect_url != null) {
                window.location = res.redirect_url;
            }
        }
    });

    return false;
}


function submit_crawl_search(url_input) {
    $('#countdown-bg').show();
    crawl_source = 'front';

    var countdownNumberEl = document.getElementById('countdown-number');
    var countdown = 10;
    countdownNumberEl.textContent = countdown;

    interval = setInterval(function () {
        if (countdown <= 1) {
            $('#modal-crawl-error').modal();
            $('#countdown-bg').hide();
            crawl_error = true;
            window.clearInterval(interval);
        } else {
            countdown = --countdown <= 0 ? 10 : countdown;
            countdownNumberEl.textContent = countdown;
        }
    }, 1000);

    var data = {url: url_input};
    AJAX.post('/', data, false, function (res) {
        if (res.error) {
            if (res.error_sites == null)
                ALERT.error(res.message);
            else {
                MODAL.show('#modal-no-support_sites');
                $('#modal-quick-crawl button.close').click();
            }

            $('#countdown-bg').hide();
            window.clearInterval(interval);
        } else {
            if (res.redirect_url != null) {
                window.location = res.redirect_url;
            }
        }
    });

    return false;
}

function submit_crawl_head() {
    $('#countdown-bg').show();
    crawl_source = 'header';

    var countdownNumberEl = document.getElementById('countdown-number');
    var countdown = 10;
    countdownNumberEl.textContent = countdown;
    interval = setInterval(function () {
        if (countdown <= 1) {
            $('#modal-crawl-error').modal();
            $('#countdown-bg').hide();
            crawl_error = true;
            window.clearInterval(interval);
        } else {
            countdown = --countdown <= 0 ? 10 : countdown;
            countdownNumberEl.textContent = countdown;
        }
    }, 1000);

    var data = {url: $('#crawl-form-head input').val()};
    AJAX.post('/', data, false, function (res) {
        if (res.error) {
            if (res.error_sites == null)
                ALERT.error(res.message);
            else {
				
				/* fixcode nhap chung search amazon url va ten sp thanh 1 hop search */
				$("#crawl-form-head > form").attr("onsubmit", "");
				$("#crawl-form-head > form").attr("method", "get");
				$("#crawl-form-head > form").attr("action", "/tim-kiem/amazon");
				$("#crawl-form-head > form").submit();
				
                /* MODAL.show('#modal-no-support_sites');
                $('#modal-quick-crawl button.close').click(); */
            }

            window.clearInterval(interval);
            $('#countdown-bg').hide();
        } else {
            if (res.redirect_url != null) {
                window.location = res.redirect_url;
            }
        }
    });

    return false;
}


function retry_crawl() {
    crawl_error = false;
    window.clearInterval(interval);

    $('#modal-crawl-error').modal('hide');

    if (crawl_source == 'front')
        submit_crawl();
    else
        submit_crawl_head();
}

function subscribe() {
    var email = $('#subscribe-form input[type=text]').val();
    if (email == '')
        alert('Vui lòng nhập email của bạn');

    AJAX.post('/subscribe', {email: email}, true, function (res) {

        $('#subscribe-form input[type=text]').attr('value', '');
        $('#subscribe-form input[type=text]').val('');

        if (res.error == false) {
            ALERT.success('Cảm ơn bạn đã đăng ký. Basso sẽ gửi những tin sale hot nhất tới email của bạn');
        } else {
            NOTI.danger(res.message);
        }
    });

    return false;
}

function show_login_modal() {
    Swal.close();
    window.setTimeout(function () {
        $('#modal-login').modal('show');
    }, 10);

    $('.modal-backdrop').hide();
    $("#modal-register").modal('hide');
}

function show_register_modal() {
    Swal.close();
    window.setTimeout(function () {
        $('#modal-register').modal('show');
    }, 10);

    $('.modal-backdrop').hide();
    $("#modal-login").modal('hide');
}

function check_login(){
	const swalWithBootstrapButtons = Swal.mixin({
	  customClass: {
		confirmButton: 'btn btn-warning btn-primary',
		cancelButton: 'btn btn-info btn-second'
	  },
	  buttonsStyling: false
	});
	swalWithBootstrapButtons.fire({
	  title: 'Bạn mua hàng mà chưa Đăng nhập !',
	  text: "Bạn muốn Đăng nhập tài khoản để mua hàng và thanh toán dễ dàng hơn.",
	  footer: '<a href="javascript:show_register_modal()">Nếu chưa có tài khoản, Đăng ký !</a>',
	  icon: 'warning',
	  showCloseButton: true,
	  showCancelButton: true,
	  confirmButtonColor: '#F8AF3F',
	  cancelButtonColor: '#00354E',
	  confirmButtonText: 'Đăng nhập',
	  cancelButtonText: 'Mua hàng không cần tài khoản'
	}).then((result) => {
	  if (result.value) {
		show_login_modal();
	  }else if(result.dismiss === Swal.DismissReason.cancel){
		submit_cart();
	  }
	});
      return false;
}