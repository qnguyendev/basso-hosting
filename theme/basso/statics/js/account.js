function accountViewModel() {
    var self = this;
    self.first_init = false;

    self.data = {
        cities: ko.mapping.fromJS([]),
        districts: ko.mapping.fromJS([]),
        init: function () {
            AJAX.get(window.location, {action: 'init'}, false, function(res) {
                ko.mapping.fromJS(res.cities, self.data.cities);
                ko.mapping.fromJS(res.districts, self.data.districts);

                self.user.name(res.user.name);
                self.user.gender(res.user.gender);
                self.user.phone(res.user.phone);
                self.user.email(res.user.email);
                self.user.address(res.user.address);
                self.user.city_id(res.user.city_id);
                self.user.district_id(res.user.district_id);
				self.user.debit(moneyFormat(res.debit));
                $('select[name=city]').attr('value', res.user.city_id);
                self.user.new_pass(undefined);
                self.first_init = true;
            });
        },
        change_city: function () {
            if (self.first_init) {
                AJAX.get(window.location, {action: 'change-city', id: self.user.city_id()}, false, function(res) {
                    ko.mapping.fromJS(res.data, self.data.districts);
                });
            }
        }
    };

    self.user = {
        name: ko.observable().extend({required: {message: 'Bắt buộc'}}),
        gender: ko.observable(),
        phone: ko.observable().extend({required: {message: 'Bắt buộc'}}),
        email: ko.observable(),
        address: ko.observable().extend({required: {message: 'Bắt buộc'}}),
        city_id: ko.observable(),
        district_id: ko.observable(),
        new_pass: ko.observable(),
        re_pass: ko.observable(),
		debit:ko.observable(0),
    };

    self.update = function () {
        if (!self.user.isValid())
            self.user.errors.showAllMessages();
        else {
            var valid = true;
            if (self.user.new_pass() !== undefined) {
                if (self.user.new_pass().length > 0) {
                    if (self.user.new_pass() !== self.user.re_pass()) {
                        valid = false;
                        NOTI.danger('Xác nhận mật khẩu không khớp');
                    }
                }
            }

            if (valid) {
                var json = {
                    name: self.user.name(),
                    phone: self.user.phone(),
                    gender: self.user.gender(),
                    address: self.user.address(),
                    city_id: self.user.city_id(),
                    district_id: self.user.district_id(),
                    new_pass: self.user.new_pass(),
                    re_pass: self.user.re_pass()
                };

                AJAX.post(window.location, json, true, function(res) {
                    if (res.error)
                        ALERT.error(res.message);
                    else {
                        ALERT.success(res.message);
                        self.user.re_pass(undefined);
                        self.user.new_pass(undefined);
                    }
                });
            }
        }
    };
}

var accountModel = new accountViewModel();
accountModel.data.init();
ko.validatedObservable(accountModel.user);
ko.applyBindings(accountModel, document.getElementById('form-account'));