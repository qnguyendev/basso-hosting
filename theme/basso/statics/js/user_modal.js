function is_valid_email(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function loginViewModel() {
    var self = this;
    self.email = ko.observable();
    self.pass = ko.observable();
    self.error_msg = ko.observable();

    self.login = function () {
        var valid_data = true;
        if (self.email() == undefined || self.pass() == undefined)
            valid_data = false;
        else {
            if (self.email() == null || self.pass() == null)
                valid_data = false;
            else if (self.email().length == 0 || self.pass().length == 0)
                valid_data = false;
        }

        if (!valid_data)
            self.error_msg('Vui lòng điền thông tin đăng nhập');
        else {
            self.error_msg(undefined);
            if (!is_valid_email(self.email()))
                self.error_msg('Emai không hợp lệ');
            else {
                self.error_msg(undefined);
                $.ajax({
                    type: 'post',
                    url: '/auth/login',
                    data: {email: self.email(), pass: self.pass()},
                    error: function (jqXHR) {
                        var res = JSON.parse(jqXHR.responseText);
                        self.error_msg(res.message);
                    },
                    success: function () {
                        window.location.reload();
                    }
                });
            }
        }
    }
}

var loginModel = new loginViewModel();
ko.applyBindings(loginModel, document.getElementById('modal-login'));