<?php if (!defined('BASEPATH')) exit('No direct access script allowed');
$role_config = [
    'basso' => [
        'dashboard' => ['admin', 'editor', 'order_manager', 'shop_manamger', 'web_order'],
        //'coupon' => ['admin'],
        'order_label' => ['admin'],
        'email_template' => ['admin'],
        'order_term' => ['admin'],
        'order_place' => ['admin'],
        'payment_limit' => ['admin'],
        'country' => ['admin'],
        'customer_group' => ['admin'],
        'inventory' => ['admin', 'shop_manamger', 'inventory_manager', 'accounting_manager'],
        'web_order' => [
            'index' => ['admin', 'web_order', 'accounting_manager'],
            'create' => ['admin', 'web_order', 'accounting_manager'],
            'export' => ['admin', 'web_order', 'accounting_manager'],
            'edit' => ['admin', 'web_order', 'accounting_manager'],
            'detail' => ['admin', 'web_order', 'accounting_manager'],
            'save' => ['admin', 'web_order', 'accounting_manager'],
            'buyer' => ['admin', 'accounting_manager'],
            'payment' => ['admin', 'accounting_manager']
        ],
        'customer_order' => ['admin', 'order_manager', 'shop_manamger', 'accounting_manager'],
        'payment_history' => ['admin', 'accounting_manager'],
        'shipping_order' => ['admin', 'shop_manamger', 'inventory_manager'],
        'invoice' => ['admin', 'shop_manamger'],
        'warehouse' => ['admin'],
        'delivery_manager' => ['admin', 'shop_manamger', 'inventory_manager'],
        'bank' => ['admin'],
        'currency' => ['admin'],
        'level' => ['admin'],
        'return_order' => ['admin', 'order_manager', 'shop_manamger'],
        'crawl_history' => ['admin'],
        'subscribe' => ['admin', 'shop_manamger'],
        'contact_form' => ['admin', 'shop_manamger'],
        'test' => ['admin']
    ],
    'admin' => [
        'home' => [
            'index' => ['admin', 'editor', 'shop_manamger'],
            'clear_cache' => ['admin'],
            'lang' => ['admin', 'editor']
        ],
        'term' => ['admin', 'editor', 'shop_manamger'],
        'article' => ['admin', 'editor', 'shop_manamger'],
        'order_setting' => ['admin'],
        'user' => ['admin'],
        'slider' => ['admin'],
        'widget' => ['admin'],
        'tracking_code' => ['admin'],
        'config' => ['admin'],
        'image_upload' => ['admin', 'editor', 'shop_manamger'],
        'voucher' => ['admin']
    ],
    'management' => [
        'shipping' => ['admin'],
        'billing' => ['admin', 'accounting_manager'],
        'customer' => [
            'index' => ['admin', 'order_manager', 'shop_manamger'],
            'detail' => ['admin', 'order_manager', 'shop_manamger'],
            'export' => ['admin', 'shop_manamger'],
            'export_list' => ['admin'],
            'group' => ['admin']
        ]
    ]
];