<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
$data = array(
    'active_class' => 'active'
);
?>
<div class="widget-{{$widget->area}} widget-{{$widget->type}}">
    <?php if ($widget->show_title) : ?>
    <div class="widget-{{$widget->id}}">
        <h4 class="widget-title">{{$widget->title}}</h4>
    </div>
    <?php endif; ?>
    <div class="widget-content">
        {{display_categories(0, $terms, $data)}}
    </div>
</div>