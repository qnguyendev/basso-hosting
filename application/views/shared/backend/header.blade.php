<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
global $theme_config;
require './theme/cpanel.php';
$lang_cookie = get_cookie('admin_lang');
$current_lang = null;

if (isset($lang_cookie)) {
    if ($lang_cookie != null) {
        $current_lang = $lang_cookie;
    }
}

if ($current_lang == null) {
    if (isset($theme_config['languages'])) {
        if (is_array($theme_config['languages'])) {
            $current_lang = array_keys($theme_config['languages'])[0];
        }
    }
}
?>
<!-- top navbar-->
<header class="topnavbar-wrapper">
    <!-- START Top Navbar-->
    <nav class="navbar topnavbar">
        <!-- START navbar header-->
        <div class="navbar-header">
            <a class="navbar-brand" href="#/">
                <div class="brand-logo">
                    <img class="img-fluid" src="/assets/img/logo.png" alt="App Logo">
                </div>
                <div class="brand-logo-collapsed">
                    <img class="img-fluid" src="/assets/img/logo.png" alt="App Logo">
                </div>
            </a>
        </div>
        <!-- END navbar header-->
        <!-- START Left navbar-->
        <ul class="navbar-nav mr-auto flex-row">
            <li class="nav-item">
                <!-- Button used to collapse the left sidebar. Only visible on tablet and desktops-->
                <a class="nav-link d-none d-md-block d-lg-block d-xl-block" href="#" data-trigger-resize=""
                   data-toggle-state="aside-collapsed">
                    <em class="fas fa-bars"></em>
                </a>
                <!-- Button to show/hide the sidebar on mobile. Visible on mobile only.-->
                <a class="nav-link sidebar-toggle d-md-none" href="#" data-toggle-state="aside-toggled"
                   data-no-persist="true">
                    <em class="fas fa-bars"></em>
                </a>
            </li>
        </ul>
        <!-- END Left navbar-->
        <!-- START Right Navbar-->
        <ul class="navbar-nav flex-row">
            <li class="nav-item dropdown dropdown-list">
                @if(isset($theme_config['languages']))
                    @if(is_array($theme_config['languages']))
                        @if(count($theme_config['languages']) > 1)
                            <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" href="#" data-toggle="dropdown"
                               aria-expanded="false">
                                @foreach($theme_config['languages'] as $key=>$value)
                                    @if($key == $current_lang)
                                        <img src="/assets/flags/{{$key}}.png" class="mr-1"/>
                                        {{$value}}
                                    @endif
                                @endforeach
                            </a>
                            <!-- START Dropdown menu-->
                            <div class="dropdown-menu dropdown-menu-right animated flipInX">
                                <div class="dropdown-item">
                                    <!-- START list group-->
                                    <div class="list-group">
                                        <!-- list item-->
                                        @foreach($theme_config['languages'] as $key=>$value)
                                            @if($key != $current_lang)
                                                <div class="list-group-item list-group-item-action">
                                                    <a class="media" href="{{base_url('admin/home/lang/'.$key)}}">
                                                        <div class="align-self-start mr-2">
                                                            <img src="/assets/flags/{{$key}}.png"/>
                                                        </div>
                                                        <div class="media-body">
                                                            {{$value}}
                                                        </div>
                                                    </a>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                    <!-- END list group-->
                                </div>
                            </div>
                            <!-- END Dropdown menu-->
                        @endif
                    @endif
                @endif
            </li>
            @if(isset($document_url))
                @if(!empty($document_url))
                    <li class="nav-item">
                        <a class="nav-link" href="{{$document_url}}" target="_blank">
                            <i class="icon-docs mr-1"></i>
                            Hướng dẫn
                        </a>
                    </li>
                @endif
            @endif
            <li class="nav-item">
                <a class="nav-link" href="{{base_url()}}" target="_blank">
                    <em class="icon-action-redo ml-1"></em>
                    Vào trang web
                </a>
            </li>
            <!-- START Alert menu-->
            <li class="nav-item dropdown dropdown-list">
                <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" href="#" data-toggle="dropdown">
                    <em class="icon-user"></em>
                </a>
                <!-- START Dropdown menu-->
                <div class="dropdown-menu dropdown-menu-right animated flipInX">
                    <div class="dropdown-item">
                        <!-- START list group-->
                        <ul class="list-group">
                            <!-- list item-->
                            <li class="list-group-item list-group-item-action">
                                <div class="media">
                                    <div class="align-self-start mr-2">
                                        <em class="fa fa-passport"></em>
                                    </div>
                                    <div class="media-body">
                                        <a data-toggle="modal" data-target="#change_pass_modal" href="javascript:">
                                            <p class="m-0">Đổi mật khẩu</p>
                                        </a>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item list-group-item-action">
                                <div class="media">
                                    <div class="align-self-start mr-2">
                                        <em class="fa fa-lock"></em>
                                    </div>
                                    <div class="media-body">
                                        <a href="{{LOGOUT_URL}}">
                                            <p class="m-0">Thoát</p>
                                        </a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <!-- END list group-->
                    </div>
                </div>
                <!-- END Dropdown menu-->
            </li>
            <!-- END Alert menu-->
        </ul>
        <!-- END Right Navbar-->
    </nav>
    <!-- END Top Navbar-->
</header>
