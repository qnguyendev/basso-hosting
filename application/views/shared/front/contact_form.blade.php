<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
$error_msg = $this->session->flashdata('error');
$success_msg = $this->session->flashdata('success');
?>

<?php if (!empty($data['form_title']) || !empty($data['form_description'])) : ?>
<div class="small-title mb-30">
    <?php if (!empty($data['form_title'])) : ?>
    <h2>{{$data['form_title']}}</h2>
    <?php endif; ?>
    <?php if (!empty($data['form_description'])) : ?>
    <p>{{$data['form_description']}}</p>
    <?php endif; ?>
</div>
<?php endif; ?>
<form id="contact-form" method="post">

    <?php if (isset($error_msg)) : ?>
    <div class="alert alert-warning">{{$error_msg}}</div>
    <?php endif; ?>

    <?php if (isset($success_msg)) : ?>
    <div class="alert alert-success">{{$success_msg}}</div>
    <?php endif; ?>

    <div class="row">
        <div class="col-lg-6">
            <div class="contact-form-style form-group">
                <?php if (!empty($data['name_label'])) : ?>
                <label>{{$data['name_label']}}</label>
                <?php endif; ?>
                <input name="name" type="text" class="form-control" required placeholder="{{$data['name_holder']}}"/>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="contact-form-style form-group">
                <?php if (!empty($data['email_label'])) : ?>
                <label>{{$data['email_label']}}</label>
                <?php endif; ?>
                <input name="email" type="email" required class="form-control" placeholder="{{$data['email_holder']}}"/>
            </div>
        </div>
    </div>

    <div class="contact-form-style form-group">
        <?php if (!empty($data['phone_label'])) : ?>
        <label>{{$data['phone_label']}}</label>
        <?php endif; ?>
        <input name="phone" type="text" required class="form-control" placeholder="{{$data['phone_holder']}}"/>
    </div>

    <div class="contact-form-style form-group">
        <?php if (!empty($data['message_label'])) : ?>
        <label>{{$data['message_label']}}</label>
        <?php endif; ?>
        <textarea name="message" required class="form-control" placeholder="{{$data['message_holder']}}"/></textarea>
    </div>
    <div class="form-group contact-form-style">
        <button type="submit" class="btn btn-primary {{$data['button_class']}}">{{$data['button_label']}}</button>
    </div>
</form>