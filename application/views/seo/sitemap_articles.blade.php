<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach($data as $item)
        <url>
            <loc>{{article_url($item->slug)}}</loc>

            <lastmod>{{$item->updated_time == null ?  date('c',$item->created_time) :  date('c',$item->updated_time)}}</lastmod>
            <priority>0.8</priority>
        </url>
    @endforeach
</urlset>
