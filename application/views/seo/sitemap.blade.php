<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <?php if ($post_sitemap) : ?>
    <sitemap>
        <loc>{{base_url('post-sitemap.xml')}}</loc>
    </sitemap>
    <?php endif; ?>
    <?php if ($product_sitemap) : ?>
    <sitemap>
        <loc>{{base_url('products.xml')}}</loc>
    </sitemap>
    <?php endif; ?>
    <?php if ($page_sitemap) : ?>
    <sitemap>
        <loc>{{base_url('page-sitemap.xml')}}</loc>
    </sitemap>
    <?php endif; ?>
        <sitemap>
            <loc>{{base_url('category-sitemap.xml')}}</loc>
        </sitemap>
</sitemapindex>