<?php if (!defined('BASEPATH')) exit('No direct access script allowed');
if (!function_exists('load_site_config')) {
    /**
     * Nạp cấu hình website: sidebar, widgets, terms, models...
     */
    function load_site_config()
    {
        global $ci;
        $config_file = './theme/config.php';
        if (!is_file($config_file)) {
            echo $ci->blade->render('errors/theme_error', ['msg' => 'Can not found config.php!'], true);
            die;
        }

        require './theme/config.php';
        require './env.php';

        if (!isset($theme_config)) {
            echo $ci->blade->render('errors/theme_error', ['msg' => 'Invalid config data structure'], true);
            die;
        }

        if (!isset($theme_config['theme'])) {
            echo $ci->blade->render('errors/theme_error', ['msg' => "Can not found \$theme_config['theme']"], true);
            die;
        }

        if (!is_dir('./theme/' . $theme_config['theme'])) {
            echo $ci->blade->render('errors/theme_error', ['msg' => "Can not found theme {$theme_config['theme']}"], true);
            die;
        }

        if (!is_file('./theme/' . $theme_config['theme'] . '/key.php')) {
            echo $ci->blade->render('errors/theme_error', ['msg' => "Can not found key file"], true);
            die;
        }

        #region Kiểm tra key
        require './theme/' . $theme_config['theme'] . '/key.php';
        if (!isset($theme_config['license'])) {
            echo $ci->blade->render('errors/theme_error', ['msg' => "Invalid domain domain license"], true);
            die;
        }

        $license = $theme_config['license'];
        if (!isset($license['domain'])) {
            echo $ci->blade->render('errors/theme_error', ['msg' => "Invalid domain domain license"], true);
            die;
        }

        if (isset($_SERVER['HTTP_HOST'])) {
            if (!in_array($_SERVER['HTTP_HOST'], $license['domain'])) {
                echo $ci->blade->render('errors/theme_error', ['msg' => "Invalid domain domain license"], true);
                die;
            }
        }

        if (!isset($license['expire'])) {
            echo $ci->blade->render('errors/theme_error',
                ['msg' => 'Domain license was expired in ' . date('d-m-Y', time())], true);
            die;
        }

        if ($license['expire'] <= time()) {
            echo $ci->blade->render('errors/theme_error',
                ['msg' => 'Domain license was expired in ' . date('d-m-Y', $license['expire'])], true);
            die;
        }
        #endregion


        if (isset($theme_config['autoload'])) {
            $autoload = $theme_config['autoload'];
            if (isset($autoload['helper'])) {
                if (is_array($autoload['helper'])) {
                    if (count($autoload['helper']) > 0)
                        foreach ($autoload['helper'] as $model)
                            $ci->load->helper($model);
                }
            }

            if (isset($autoload['library'])) {
                if (is_array($autoload['library'])) {
                    if (count($autoload['library']) > 0) {
                        foreach ($autoload['library'] as $model)
                            $ci->load->library($model);
                    }
                }
            }

            if (isset($autoload['model'])) {
                if (is_array($autoload['model'])) {
                    if (count($autoload['model']) > 0)
                        foreach ($autoload['model'] as $model)
                            $ci->load->model($model);
                }
            }
        }

        $GLOBALS['theme_config'] = $theme_config;
    }
}

if (!function_exists('check_null_data')) {

    /**
     * Kiểm tra biết hoặc mảng có phần tử null hay không
     * @param $data
     * @return array|null
     */
    function check_null_data($data)
    {
        if ($data != null) {
            if (!is_array($data)) {
                $data = strlen($data) > 0 ? $data : null;
            } else {
                foreach ($data as $key => $value) {
                    $data[$key] = strlen($data[$key]) > 0 ? $data[$key] : null;
                }
            }
        }

        return $data;
    }
}

if (!function_exists('json_error')) {
    function json_error($msg, $data = null)
    {
        global $ci;
        if ($ci == null)
            $ci =& get_instance();
        $ci->output->set_status_header(500);

        $result = array('error' => true);
        if ($msg != null) {
            if (!empty($msg))
                $result['message'] = $msg;
        }

        if ($data != null) {
            if (is_array($data)) {
                foreach ($data as $key => $value) {
                    $result[$key] = $value;
                }
            }
        }
        echo json_encode($result);
        die;
    }
}

if (!function_exists('json_success')) {
    function json_success($msg, $data = null)
    {
        $result = array('error' => false);
        if ($msg != null) {
            if (!empty($msg))
                $result['message'] = $msg;
        }

        if ($data != null) {
            if (is_array($data)) {
                foreach ($data as $key => $value) {
                    $result[$key] = $value;
                }
            } else
                $result['data'] = $data;
        }
        echo json_encode($result);
        die;
    }
}

if (!function_exists('success_msg')) {
    function success_msg($msg, $name = 'success')
    {
        global $ci;
        $ci->session->set_flashdata($name, $msg);
    }
}

if (!function_exists('error_msg')) {
    function error_msg($msg, $name = 'error ')
    {
        global $ci;
        $ci->session->set_flashdata($name, $msg);
    }
}

if (!function_exists('is_enull')) {
    function is_enull($param, $default_value = null, string $validate_type = null)
    {
        if (isset($param)) {
            if ($param != null) {
                if (!empty($param)) {
                    if ($validate_type != null) {
                        if ($validate_type == 'numeric')
                            return is_numeric($param) ? $param : $default_value;

                        if ($validate_type == 'int')
                            return is_int($param) ? $param : $default_value;

                        if ($validate_type == 'float')
                            return is_float($param) ? $param : $default_value;

                        if ($validate_type == 'url')
                            return filter_has_var($param, FILTER_VALIDATE_URL) ? $param : $default_value;
                    }

                    return $param;
                }
            }
        }

        return $default_value;
    }
}