<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

define('CART_URL', base_url('gio-hang'));
define('CHECK_OUT_URL', base_url('thanh-toan'));
define('THANK_YOU_URL', base_url('hoan-thanh-dat-hang'));
define('TRACKING_URL', base_url('don-hang'));
define('CONFIRM_TRANSACTION_URL', base_url('thanh-toan/xac-nhan'));
define('SEARCH_URL', base_url('tim-kiem'));

if (!function_exists('set_head_title')) {
    function set_head_title($title)
    {
        $GLOBALS['head_title'] = $title;
    }
}

if (!function_exists('set_head_des')) {
    function set_head_des($title)
    {
        $GLOBALS['head_des'] = $title;
    }
}

if (!function_exists('set_head_image')) {
    function set_head_image($title)
    {
        $GLOBALS['head_image'] = $title;
    }
}

if (!function_exists('set_head_index')) {
    function set_head_index($index)
    {
        global $ci;
        $allow_index = is_bool($index) ? $index : ($index == 1);
        $option = $ci->option_model->get('seo_index');
        if ($option != null) {
            $allow_index = $option->value == 1;
        }

        $GLOBALS['head_index'] = $allow_index;
    }
}

if (!function_exists('set_head_custom')) {
    function set_head_custom($type, $type_value, $content)
    {
        global $head_custom;
        if (!isset($head_custom))
            $head_custom = array();

        array_push($head_custom, array(
            'type' => $type,
            'type_content' => $type_value,
            'content' => $content
        ));

        $GLOBALS['head_custom'] = $head_custom;
    }
}

if (!function_exists('logo_url')) {
    /**
     * Đường dẫn logo
     * @return null
     */
    function logo_url()
    {
        global $ci;
        $item = $ci->option_model->get('logo');
        if ($item != null)
            return $item->value;
        return null;
    }
}

if (!function_exists('fav_icon_url')) {
    /**
     * Đường dẫn favicon
     * @return null
     */
    function fav_icon_url()
    {
        global $ci;
        $item = $ci->option_model->get('fav_icon');
        if ($item != null)
            return $item->value;
        return null;
    }
}

if (!function_exists('get_social_url')) {
    /**
     * Lấy đường dẫn trang social
     * @param $social_name
     * @return null
     */
    function get_social_url($social_name)
    {
        global $ci;
        $item = $ci->option_model->get($social_name);
        if ($item != null)
            return $item->value;
        return null;
    }
}

if (!function_exists('site_head')) {
    /**
     * Header của site: tiêu đề, mô tả, ảnh...
     */
    function site_head()
    {
        global $ci, $head_title, $head_des, $head_image, $head_index, $head_custom;
        $data = array(
            'title' => $head_title,
            'des' => $head_des,
            'image' => $head_image,
            'no_index' => $head_index,
            'custom' => $head_custom
        );

        return $ci->blade->render('front/head_meta', $data, true);
    }
}

if (!function_exists('theme_dir')) {
    /**
     * thư mục chứa static của theme
     * @param $file đường dẫn file static
     * @return string
     */
    function theme_dir($file = null)
    {
        global $theme_config;
        $theme = $theme_config['theme'];
        if (!is_dir('./theme/' . $theme))
            $theme = 'default';

        if ($file != null)
            return base_url("theme/$theme/statics/$file");
        return base_url("theme/$theme/statics/");
    }
}

if (!function_exists('view_dir')) {
    /**
     * thư mục chứa theme
     * @return string
     */
    function view_dir()
    {
        global $theme_config;
        $theme = $theme_config['theme'];
        if (!is_dir('./theme/' . $theme))
            $theme = 'default';

        return './theme/' . $theme . '/views/';
    }
}

if (!function_exists('product_url')) {
    function product_url($slug)
    {
        return base_url("/$slug.html");
    }
}
//
//if (!function_exists('article_url')) {
//    function article_url($term_slug, $article_slug)
//    {
//        return base_url("/$term_slug/$article_slug.html");
//    }
//}
if (!function_exists('article_url')) {
    function article_url($article_slug)
    {
        return base_url("/$article_slug.html");
    }
}
if (!function_exists('team_url')) {
    function team_url($slug)
    {
        return base_url('thanh-vien/' . $slug . '.html');
    }
}

if (!function_exists('project_url')) {
    function project_url($slug)
    {
        return base_url('du-an/' . $slug . '.html');
    }
}

if (!function_exists('has_sale_price')) {
    /**
     * Kiểm tra sản phẩm giảm giá hay không
     * @param $prod
     * @return bool
     */
    function has_sale_price($prod)
    {
        if ($prod->sale_price != null) {
            if ($prod->sale_from != null || $prod->sale_to != null) {
                $prod->sale_from = $prod->sale_from == null ? 0 : $prod->sale_from;
                $prod->sale_to = $prod->sale_to == null ? time() + 1 : $prod->sale_to;

                return $prod->sale_from <= time() && $prod->sale_to >= time();
            }

            return true;
        }

        return false;
    }
}

if (!function_exists('sale_percent')) {
    /**
     * Tính % giảm giá
     * @param $price
     * @param $sale_price
     * @return string
     */
    function sale_percent($price, $sale_price)
    {
        return format_number(($price - $sale_price) / $price * 100, 0) . '%';
    }
}

if (!function_exists('send_checked_out_email')) {

    /**
     * Gửi email thông báo mua hàng
     * @param $order_id
     */
    function send_checked_out_email($order_id)
    {
        global $ci;
        $ci->load->model('order_model');
        $ci->load->model('email_model');

        $order = $ci->order_model->get_by_id($order_id);
        $view_data['order'] = $order;
        $view_data['products'] = $ci->order_model->get_detail($order_id);

        $payment = $ci->payment_model->get_by_id($view_data['order']->payment_id);
        $view_data['payment'] = $payment;

        if ($payment->type == 'bank-transfer')
            $view_data['banks'] = $ci->payment_model->get_banks();

        $item = $ci->option_model->get('order_email_footer_content');
        if ($item != null)
            $view_data['order_email_footer'] = $item->value;

        $data = new stdClass();
        $data->to = $order->email;
        $data->subject = "[{$_SERVER['HTTP_HOST']}] Xác nhận đơn hàng #$order_id";
        $data->message = $ci->blade->render('order/invoice', $view_data, true);

        //  Danh sách nhận thông báo
        $item = $ci->option_model->get('order_send_list');
        if ($item != null)
            $data->bcc = $item->value;

        $ci->email_model->insert($data->subject, $data->message, $data->to, $data->bcc);
    }
}

if (!function_exists('tracking_scripts')) {
    /**
     * mã javascript trong thẻ HEAD
     * @param $position
     * @return string
     */
    function tracking_scripts($position = 'head')
    {
        global $ci, $current_term;
        $controller = strtolower($ci->router->fetch_class());
        $action = strtolower($ci->router->fetch_method());
		//Kiem tra trang chu
         $result = '';
		if ($controller == 'front' && $action == 'index') {
            $scripts = $ci->script_model->get($position);

            foreach ($scripts as $script) {
                if ($script->note != null) {
					//Check nhung script hien thi trang chu va tat ca trang
                    if ($script->note == 'home' || $script->note == 'all') {
                        $result .= $script->code;
                    }
                }
            }
        }

        $page_id = null;
        if (isset($current_term))
            $page_id = $current_term->id;

		//get nhung script theo trang
        $scripts = $ci->script_model->get($position, $page_id);
        foreach ($scripts as $script){
            if ($script->note == null){
                $result .= $script->code;
			}
		}

		//get nhung script hien thi theo tat ca trang
		$scripts = $ci->script_model->get($position);
		foreach ($scripts as $script) {
			if ($script->note != null) {
				if ($script->note == 'all') {
					$result .= $script->code;
				}
			}
		}

        return $result;
    }
}

if (!function_exists('contact_form')) {
    /**
     * @param null $data
     * @return mixed
     */
    function contact_form($data = null)
    {
        global $ci;

        $form_data = [
            'form_title' => null,
            'form_description' => null,
            'name_label' => 'Họ tên',
            'name_holder' => null,
            'email_label' => 'Địa chỉ email',
            'email_holder' => null,
            'phone_label' => 'Số điện thoại',
            'phone_holder' => null,
            'message_label' => 'Nội dung liên hệ',
            'message_holder' => null,
            'button_label' => 'Gửi đi',
            'button_class' => 'btn btn-primary'
        ];

        if (isset($data)) {
            if ($data != null) {
                if (is_array($data)) {
                    foreach ($form_data as $key => $value) {
                        if (isset($data[$key]))
                            $form_data[$key] = $data[$key];
                    }
                }
            }
        }

        return $ci->blade->render('front/contact_form', ['data' => $form_data], true);
    }
}

if (!function_exists('get_terms')) {
    /**
     * @return array
     */
    function get_terms()
    {
        global $theme_config;
        if ($theme_config == null) return [];
        if (!isset($theme_config['term'])) return [];

        $terms = array_filter($theme_config['term'], function ($item) {
            return $item['active'];
        });
        return array_column($terms, 'id');
    }
}

if (!function_exists('get_lang')) {
    function get_lang()
    {
        global $theme_config;
        if (isset($theme_config['languages'])) {
            if (is_array($theme_config['languages'])) {
                if (count($theme_config['languages']) > 1) {
                    $lang_cookie = get_cookie('language');
                    $current_lang = null;

                    if (isset($lang_cookie)) {
                        if ($lang_cookie != null) {
                            $current_lang = $lang_cookie;
                        }
                    }

                    if ($current_lang == null) {
                        $current_lang = array_keys($theme_config['languages'])[0];
                    }

                    return $current_lang;
                }
            }
        }

        return null;
    }
}
