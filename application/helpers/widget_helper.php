<?php if (!defined('BASEPATH')) exit('No direct access script allowed');
if (!function_exists('display_widget')) {
    /**
     * @param string $area_id
     * @param string|null $widget_type
     * @param string|null $view_file
     */
    function display_widget($area_id, $widget_type = null, $view_file = null)
    {
        global $ci;

        $widgets = $ci->widget_model->get($area_id, $widget_type);
        if (count($widgets) == 0) return;

        foreach ($widgets as $widget)
            show_widget($widget, $view_file);
    }
}

if (!function_exists('get_widgets')) {
    function get_widgets($area, $widget_type = null, $limit = 0)
    {
        global $ci;
        $widgets = $ci->widget_model->get($area, $widget_type, $limit);
        return $widgets;
    }
}

if (!function_exists('show_widget')) {
    function show_widget($widget, $view_file = null)
    {
        if (!isset($widget)) return;
        if ($widget == null) return;

        $widget->data = json_decode($widget->options, false);
        $widget->show_title = $widget->show_title == 1;

        $_view = 'widgets/' . $widget->type;
        if ($view_file != null) {
            if (is_file(view_dir() . 'widget/' . $view_file . '.blade.php'))
                $_view = 'widget/' . $view_file;
        } else {
            if (is_file(view_dir() . 'widget/' . $widget->type . '.blade.php'))
                $_view = 'widget/' . $widget->type;
        }

        global $ci;
        $ci->blade->set('widget', $widget);
        switch ($widget->type) {
            case 'product_terms':
                $terms = $ci->term_model->get_by_type('product', $widget->data->order, $widget->data->show_count == 1);
                $ci->blade->set('terms', $terms);
                break;

            case 'article_terms':
                $terms = $ci->term_model->get_by_type('post', $widget->data->order, $widget->data->show_count == 1);
                $ci->blade->set('terms', $terms);
                break;

            case 'products':
                $products = $ci->product_model->get_widget($widget->data->limit, $widget->data->order);
                for ($i = 0; $i < count($products); $i++)
                    $products[$i]->images = $ci->product_model->get_images($products[$i]->id);

                $ci->blade->set('products', $products);
                break;

            case 'articles':
                $widget->show_description = $widget->data->show_des == 1;
                $articles = $ci->article_model->get_widget($widget->data->terms,
                    $widget->data->order,
                    $widget->data->limit);

                $ci->blade->set('articles', $articles);
                break;

            case 'member':
                if ($widget->data->type == 'all')
                    $members = $ci->team_model->get(null, $widget->data->limit);
                else {
                    $list = [];
                    foreach (explode(',', $widget->data->list) as $id) {
                        $id = trim($id);
                        if (is_numeric($id))
                            array_push($list, $id);
                    }
                    $members = $ci->team_model->get($list);
                }
                $ci->blade->set('members', $members);
                break;
        }

        echo $ci->blade->render($_view, null, true);
    }
}