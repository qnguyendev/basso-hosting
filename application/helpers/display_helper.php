<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

if (!function_exists('display_categories')) {
    /**
     * Hiển thị danh mục đa cấp
     * @param $parent_id
     * @param $categories
     * @param null $data
     * @return bool|string
     */
    function display_categories($parent_id, $categories, $data = null)
    {
        if (!is_array($categories) or empty($categories)) return;

        $active_class = null;   //  css class highlight danh mục hiện tại
        $anchor_attributes = [];
        $nav_item_attributes = [];
        $top_child_attributes = [];   //  css class thẻ li đầu khi có menu con
        $dropdown_attributes = [];   //
        $top_anchor_attributes = [];  //
        $nav_attr = [];
        if ($data != null) {
            if (is_array($data)) {
                if (isset($data['active_class']))
                    $active_class = $data['active_class'];
                if (isset($data['top_child_attributes']))
                    $top_child_attributes = $data['top_child_attributes'];
                if (isset($data['dropdown_attributes']))
                    $dropdown_attributes = $data['dropdown_attributes'];
                if (isset($data['top_anchor_attributes']))
                    $top_anchor_attributes = $data['top_anchor_attributes'];
                if (isset($data['nav_attributes']))
                    $nav_attr = $data['nav_attributes'];
                if (isset($data['anchor_attributes']))
                    $anchor_attributes = $data['anchor_attributes'];
                if (isset($data['nav_item_attributes']))
                    $nav_item_attributes = $data['nav_item_attributes'];
            }
        }

        #region Kiểm tra nếu tìm theo parent id không có thằng con nào
        $parent = array_filter($categories, function ($item) use ($parent_id) {
            return $parent_id == $item->id;
        });

        $childs = array_filter($categories, function ($item) use ($parent_id) {
            return $parent_id == $item->parent_id;
        });

        if (count($parent) == 0 && count($childs) == 0) {
            foreach ($categories as $cat) {
                $parent = array_filter($categories, function ($item) use ($cat) {
                    return $cat->parent_id == $item->id;
                });

                $childs = array_filter($categories, function ($item) use ($cat) {
                    return $cat->parent_id == $item->parent_id;
                });

                if (count($parent) == 0 && count($childs) > 0) {
                    $parent_id = $cat->parent_id;
                    break;
                }
            }
        }
        #endregion

        $output = '<ul';
        if (count($nav_attr) > 0) {
            foreach ($nav_attr as $key => $value)
                $output .= " $key='$value'";
        }
        $output .= '>';

        if ($parent_id != 0) {
            $output = "<ul id='menu-$parent_id'";
            if (count($dropdown_attributes) > 0) {
                foreach ($dropdown_attributes as $key => $value) {
                    if ($key != 'id')
                        $output .= " $key='$value'";
                }
            }
            $output .= '>';
        }

        foreach ($categories as $cat) {
            if ($cat->parent_id == $parent_id) {

                $childs = array_filter($categories, function ($item) use ($cat) {
                    return $cat->id == $item->parent_id;
                });

                $output .= '<li';
                if ($cat->parent_id == 0 || !empty($parent_li_class)) {
                    if (count($childs) > 0) {
                        if (count($top_child_attributes) > 0) {
                            foreach ($top_child_attributes as $key => $value) {
                                $output .= " $key='$value'";
                            }
                        }
                    } else {
                        if (count($nav_item_attributes) > 0) {
                            foreach ($nav_item_attributes as $key => $value) {
                                $output .= " $key='$value'";
                            }
                        }
                    }
                } else {
                    if (count($nav_item_attributes) > 0) {
                        foreach ($nav_item_attributes as $key => $value) {
                            $output .= " $key='$value'";
                        }
                    }
                }
                $output .= '>';

                $output .= display_term_anchor($cat, $active_class, $top_anchor_attributes, $anchor_attributes);
                if (count($childs) > 0)
                    $output .= display_categories($cat->id, $categories, $data);

                $output .= '</li>';
            }
        }

        $output .= '</ul>';
        return $output;
    }
}

if (!function_exists('display_term_anchor')) {
    /**
     * Tạo link danh mục
     * @param $term
     * @param $active_class css class khi đang ở danh mục hiện tại
     * @param array|null $data
     * @param array|null $anchor_attributes
     * @return string
     */
    function check_no_follows($term_slug)
    {
        $check = array("ve-basso", "dich-vu", "tin-tuc", "lien-he");
        if (in_array($term_slug, $check)) {
            return true;
        }
        return false;
    }

    function display_term_anchor($term, $active_class = null, array $data = null, array $anchor_attributes = null)
    {
        global $current_term, $ci;
        $css_class = '';
        //hanhcode
//        echo "<pre style='display: none'>";
//        print_r($term);
//        echo "</pre>";
        $link = "";
        if ($term->slug == "dich-vu"):
            $link='<span class="nav-link">Dịch vụ</span>';
        else:
            $link = "<a ";
            switch ($term->type) {
                case 'home':
                    $data = [];
                    $anchor = '/';
                    break;
                case 'external_link':
                    $anchor = $term->external_url;
//                $link .= " rel='external_link'";
                    $link .= " rel='dofollow'";
                    break;
                default:
                    $data = [];
                    $anchor = base_url($term->slug);
                    break;
            }

            $link .= " href='$anchor'";
            if ($term->new_tab)
                $link .= " target='_blank' ";
            if (check_no_follows($term->slug)) $link .= " rel='nofollow'";
            if ($data != null) {
                if (is_array($data)) {
                    foreach ($data as $key => $value) {
                        if ($key != 'class')
                            $link .= "$key='$value' ";
                        else
                            $css_class = $value;
                    }
                }
            }

            if ($anchor_attributes != null) {
                if (is_array($anchor_attributes)) {
                    foreach ($anchor_attributes as $key => $value) {
                        if ($key != 'class')
                            $link .= "$key='$value' ";
                        else
                            $css_class = $value;
                    }
                }
            }

            if (isset($current_term)) {
                if ($current_term->id == $term->id || $current_term->parent_id == $term->id) {
                    if (!empty($css_class))
                        $css_class .= " $active_class";
                    else
                        $css_class = $active_class;
                }
            } else if ($term->type == 'Dashboard') {
                $controller = strtolower($ci->router->fetch_class());
                if ($controller == 'Dashboard')
                    $css_class = $active_class;
            }

            if (!empty($css_class))
                $link .= "class='$css_class' ";
            if (isset($term->count))
                $link .= ">$term->name ($term->count)</a>";
            else
                $link .= ">$term->name</a>";
        endif;
        return $link;
    }
}
