<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Migration_alter_inventory_packages * @property CI_DB_forge $dbforge
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 */
class Migration_alter_inventory_packages extends CI_Migration
{
    protected $_table_name = "inventory_packages";

    public function up()
    {
        $this->dbforge->add_column($this->_table_name, [
            'branch' => ['type' => 'varchar', 'constraint' => 32, 'null' => true]
        ]);
    }

    public function down()
    {
        $this->dbforge->drop_table($this->_table_name, TRUE);
    }
}