<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Migration_add_order_term_list * @property CI_DB_forge $dbforge
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 */
class Migration_add_order_term_list extends CI_Migration
{
    protected $_table_name = "order_term_list";

    public function up()
    {
        $this->dbforge->add_field([
            'id' => ['type' => 'int', 'auto_increment' => true],
            'order_term_id' => ['type' => 'int'],
            'category' => ['type' => 'varchar', 'constraint' => 1024]
        ]);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->_table_name, TRUE);
    }

    public function down()
    {
        $this->dbforge->drop_table($this->_table_name, TRUE);
    }
}