<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Migration_add_vouchers * @property CI_DB_forge $dbforge
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 */
class Migration_add_vouchers extends CI_Migration
{
    protected $_table_name = "vouchers";

    public function up()
    {
        $this->dbforge->add_field([
            'id' => ['type' => 'int', 'auto_increment' => true],
            'created_time' => ['type' => 'int'],
            'active' => ['type' => 'tinyint', 'default' => 1],
            'code' => ['type' => 'varchar', 'constraint' => 256],
            'valid_from' => ['type' => 'int'],
            'valid_to' => ['type' => 'int'],
            'discount_type' => ['type' => 'varchar', 'constraint' => 256],
            'discount_value' => ['type' => 'int'],
            'max_value' => ['type' => 'int', 'default' => 0],
            'limit' => ['type' => 'int', 'null' => true],
            'remaining' => ['type' => 'int', 'null' => true],
            'note' => ['type' => 'text', 'null' => true]
        ]);
        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table($this->_table_name, TRUE);
    }

    public function down()
    {
        $this->dbforge->drop_table($this->_table_name, TRUE);
    }
}