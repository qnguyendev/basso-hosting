<?php 
require_once('Zalo/autoload.php');
use Zalo\Zalo;
class Zalo {
	protected $CI;

	public function __construct(){
		$this->CI =& get_instance();
        $this->CI->load->library('session');
        $this->CI->config->load('config');
		
		$config = array(
			'app_id' => $this->CI->config->item('zalo_app_id'),
			'app_secret' => $this->CI->config->item('zalo_app_secret'),
			'callback_url' => $this->CI->config->item('zalo_callback_url')
		);
		$this->client = new Zalo($config);

	}


	public function validate(){		
		if (isset($_GET['code'])) {
		  $this->client->authenticate($_GET['code']);
		  $_SESSION['access_token'] = $this->client->getAccessToken();

		}
		if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
		  $this->client->setAccessToken($_SESSION['access_token']);
		  $plus = new Google_Service_Oauth2($this->client);
			$person = $plus->userinfo_v2_me->get();
			$info['id']=$person['id'];
			$info['email']=$person['email'];
			$info['name']=$person['name'];
			$info['link']=$person['link'];
			$info['profile_pic']= $person['picture'];

		   return  $info;
		}


	}

}