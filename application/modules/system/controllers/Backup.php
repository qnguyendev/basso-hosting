<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Backup
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Option_model $option_model
 * @property Backup_model $backup_model
 * @property CI_DB_utility $dbutil
 */
class Backup extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('basso/contact_form_model');

        global $theme_config;

        $this->load->helper('file');
        $this->load->model('backup_model');
    }

    public function index()
    {
        $breadcrumb = [['text' => 'Backup Dropbox']];
        $this->blade
            ->set('breadcrumbs', $breadcrumb)
            ->render();
    }

    public function GET_index()
    {
        $type = $this->input->get('type');
        if ($type != null) {
            if ($type == 'init') {

                $result['is_authorized'] = true;
                $result['result'] = [];

                $backups = $this->backup_model->get();
                foreach ($backups as $item) {
                    array_push($result['result'], [
                        'created_time' => date('H:i d/m/Y', $item->created_time),
                        'file_name' => basename($item->file_name),
                        'size' => format_number(intval($item->file_size) / 1024, 1) . ' KB',
                        'id' => $item->id
                    ]);
                }

                json_success(null, $result);
            }

            if ($type == 'download') {
                $id = $this->input->get('id');
                if ($id != null) {
                    $backup = $this->backup_model->get($id);
                    if ($backup != null) {

                        $file_path = "./backups/" . $backup->file_name;
                        if (!is_file($file_path))
                            json_error('Không tìm thấy file');

                        $host_url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'];
                        $file = $host_url . '/backups/' . $backup->file_name;
                        json_success(null, ['file' => $file]);
                    }

                    json_error('Không tìm thấy file backup');
                }

                json_error('Không tìm thấy ID');
            }
        }

        json_error('');
    }

    public function POST_index()
    {
        $type = $this->input->post('type');
        if ($type != null) {
            if ($type == 'backup_now') {

                $this->load->dbutil();
                $prefs = [
                    'format' => 'zip',                       // gzip, zip, txt
                    'filename' => 'backup_db.sql'              // File name - NEEDED ONLY WITH ZIP FILES
                ];
                $backup = $this->dbutil->backup($prefs);

                $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $random_string_name = substr(str_shuffle($permitted_chars), 0, 8);

                $file_name = 'backup-' . date('Y-m-d-H-i-s') . "-$random_string_name" . '.zip';
                $file_path = "./backups/$file_name";
                $is_written = write_file($file_path, $backup);

                if ($is_written === true) {
                    $id = $this->backup_model->save_history($file_name, filesize($file_path));

                    json_success(null, ['data' => [
                        'id' => $id,
                        'file_name' => $file_name,
                        'size' => format_number(filesize($file_path) / 1024, 1) . ' KB',
                        'time' => time()
                    ]]);
                }

                json_error('Không thể lưu file backup');
            }
        }

        json_error('');
    }

    public function DELETE_index()
    {
        $id = $this->input->input_stream('id');
        $backup = $this->backup_model->get($id);

        if ($backup != null) {

            $file_path = './backups/' . $backup->file_name;
            $this->backup_model->delete($id);

            if (!is_file($file_path)) {
                json_error('Không tìm thấy file');
            }

            unlink($file_path);
            json_success('Xóa file backup thành công');
        }
        json_error('Không tìm thấy file backup');
    }
}
