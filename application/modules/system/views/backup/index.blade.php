<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-12 col-sm-5">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">Cấu hình Dropbox</div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label>Trạng thái Dropbox Token</label>
                        <!--ko if: is_authorized()-->
                        <span class="badge-success badge">
                            Đã xác thực với Dropbox
                        </span>
                        <!--/ko-->
                        <!--ko if: is_authorized() === false-->
                        <span class="badge badge-danger">
                            Chưa xác thực Dropbox
                        </span>
                        <!--/ko-->
                    </div>
                    <br>
                    <div class="form-group" data-bind="visible: !is_authorized()">
                        <label>
                            Dropbox Access Token
                            <span class="text-danger">*</span>
                            <span class="pull-right validationMessage"
                                  data-bind="validationMessage: access_token"></span>
                        </label>
                        <div class="input-group">
                            <input type="text" class="form-control" data-bind="value: access_token"/>
                            <div class="input-group-append">
                                <button data-bind="click: $root.save_token" class="btn btn-primary">
                                    <i class="fa fa-save"></i>
                                    Cập nhật
                                </button>
                            </div>
                            <div class="input-group-append">
                                <button class="btn btn-info" data-bind="click: $root.get_token">
                                    <i class="fa fa-external-link-alt"></i>
                                    Lấy token
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" data-bind="visible: is_authorized">
                        <button data-bind="click: $root.backup_now" class="btn btn-primary">
                            <i class="fa fa-upload"></i>
                            Backup ngay
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12 col-sm-7">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">Danh sách backup</div>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-bordered text-center">
                        <thead>
                        <tr>
                            <th style="width: 60px">STT</th>
                            <th>Thời gian</th>
                            <th>Tên file</th>
                            <th style="width: 95px">Size</th>
                            <th style="width: 115px"></th>
                        </tr>
                        </thead>
                        <tbody data-bind="foreach: backups">
                        <tr>
                            <th data-bind="text: $index() + 1"></th>
                            <td data-bind="text: created_time"></td>
                            <td data-bind="text: file_name"></td>
                            <td data-bind="text: size"></td>
                            <td>
                                <div class="dropdown d-inline">
                                    <button class="btn btn-secondary btn-xs btn-round dropdown-toggle btn-xs"
                                            id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false">
                                        Lựa chọn
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                        <a class="dropdown-item" href="javascript:" data-bind="click: $root.download">
                                            Tải về
                                        </a>
                                        <a class="dropdown-item" href="javascript:" data-bind="click: $root.delete">
                                            Xóa backup
                                        </a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{load_js('index')}}"></script>
@endsection