function viewModel() {
    let self = this;

    self.is_authorized = ko.observable(false);
    self.access_token = ko.observable().extend({
        required: {
            message: LABEL.required,
            onlyIf: function () {
                return self.is_authorized() == false;
            }
        }
    });
    self.backups = ko.mapping.fromJS([]);

    self.init = function () {
        AJAX.get(window.location, {type: 'init'}, true, (res) => {
            self.is_authorized(res.is_authorized);
            ko.mapping.fromJS(res.result, self.backups);
        });
    };

    self.save_token = function () {
        if (!self.isValid())
            self.errors.showAllMessages();
        else {
            AJAX.post(window.location, {token_code: self.access_token, type: 'save_token'}, true, (res) => {
                if (res.error)
                    ALERT.error(res.message);
                else {
                    self.access_token(undefined);
                    self.is_authorized(true);
                    ALERT.success(res.message);
                }
            });
        }
    };

    self.get_token = function () {
        AJAX.get(window.location, {type: 'get_token'}, true, (res) => {
            let win = window.open(res.redirect_url, '_blank');
            win.focus();
        });
    };

    self.download = function (item) {
        AJAX.get(window.location, {id: item.id(), type: 'download'}, true, (res) => {
            if (res.error)
                ALERT.error(res.message);
            else
                window.open(res.file, '_blank');
        });
    };

    self.delete = function (item) {
        ALERT.confirm('Bạn muốn xóa backup', null, () => {
            AJAX.delete(window.location, {id: item.id()}, true, (res) => {
                self.backups.remove(item);
                if (res.error)
                    ALERT.error(res.message);
                else
                    ALERT.success(res.message);
            });
        });
    };

    self.backup_now = function () {
        AJAX.post(window.location, {type: 'backup_now'}, true, (res) => {
            self.backups.unshift({
                id: ko.observable(res.data.id),
                created_time: ko.observable(moment.unix(res.data.time).format('HH:mm DD/MM/YYYY')),
                file_name: ko.observable(res.data.file_name),
                size: ko.observable(res.data.size)
            });
            ALERT.success('Backup dữ liệu thành công');
        });
    }
}

let model = new viewModel();
model.init();
ko.validatedObservable(model);
ko.applyBindings(model, document.getElementById('main-content'));