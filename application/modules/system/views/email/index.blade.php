<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3">
            <form method="post">
                <div class="card card-default">
                    <div class="card-header">
                        <div class="card-title">Cài đặt Email</div>
                    </div>
                    <div class="card-body">
                       <div class="row">
                           <div class="form-group col-md-6 col-sm-12">
                               <label>Địa chỉ máy chủ SMTP</label>
                               <input type="text" class="form-control" placeholder="smtp.gmail.com"
                                      name="smtp_host" value="{{$smtp_host}}"/>
                           </div>
                           <div class="form-group col-md-6 col-sm-12">
                               <label>Cổng máy chủ SMTP</label>
                               <input type="number" class="form-control" placeholder="587"
                                      name="smtp_port" value="{{$smtp_port}}"/>
                           </div>
                           <div class="form-group col-md-6 col-sm-12">
                               <label>Tài khoản đăng nhập</label>
                               <input type="text" class="form-control" name="smtp_user"
                                      value="{{$smtp_user}}"/>
                           </div>
                           <div class="form-group col-md-6 col-sm-12">
                               <label>Mật khẩu</label>
                               <input type="password" class="form-control" name="smtp_pass"
                                      value="{{$smtp_pass}}"/>
                           </div>
                           <div class="form-group col-md-6 col-sm-12">
                               <label>Email hiển thị</label>
                               <input type="text" class="form-control" name="email_display_email"
                                      value="{{$email_display_email}}"/>
                           </div>
                           <div class="form-group col-md-6 col-sm-12">
                               <label>Tên hiển thị</label>
                               <input type="text" class="form-control" name="email_display_name"
                                      value="{{$email_display_name}}"/>
                           </div>
						   <!--
						   <div class="form-group col-md-6 col-sm-12">
                               <label>Email hiển thị Asale</label>
                               <input type="text" class="form-control" name="email_display_email_asale"
                                      value="{{$email_display_email_asale}}"/>
                           </div>
						   -->
                           <div class="form-group col-md-6 col-sm-12">
                               <label>Tên hiển thị Asale</label>
                               <input type="text" class="form-control" name="email_display_name_asale"
                                      value="{{$email_display_name_asale}}"/>
                           </div>
						   
                       </div>
                    </div>
                    <div class="card-footer text-center">
                        <button class="btn btn-success" type="submit">
                            <i class="fa fa-check"></i>
                            Cập nhật
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection