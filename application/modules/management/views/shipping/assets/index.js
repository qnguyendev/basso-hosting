function viewModel() {
    let self = this;
    self.id = ko.observable();
    self.name = ko.observable().extend({
        required: {message: LABEL.required}
    });
    self.website = ko.observable();
    self.phone = ko.observable();
    self.result = ko.mapping.fromJS([]);

    self.add_shipping = function () {
        self.id(0);
        self.name(undefined);
        self.website(undefined);
        self.phone(undefined);
        self.errors.showAllMessages(false);
        MODAL.show('#editor-modal');
    };

    self.init = function () {
        AJAX.get(window.location, null, true, (res) => {
            ko.mapping.fromJS(res.data, self.result);
        });
    };

    self.edit = function (item) {
        self.id(item.id());
        self.name(item.name());
        self.website(item.website());
        self.phone(item.phone());

        MODAL.show('#editor-modal');
    };

    self.delete = function (item) {
        ALERT.confirm('Bạn muốn xóa đơn vị vận chuyển?', null, function () {
            AJAX.delete(window.location, {id: item.id()}, true, (res) => {
                if (res.error)
                    ALERT.error(res.message);
                else {
                    self.init();
                    ALERT.success(res.message);
                }
            });
        });
    };

    self.save = function () {
        if (!self.isValid())
            self.errors.showAllMessages();
        else {
            let data = {
                id: self.id(),
                name: self.name(),
                phone: self.phone(),
                website: self.website()
            };

            AJAX.post(window.location, data, true, (res) => {
                if (res.error)
                    ALERT.error(res.message);
                else {
                    ALERT.success(res.message);
                    MODAL.hide('#editor-modal');
                    self.init();
                }
            });
        }
    };
}

let model = new viewModel();
ko.validatedObservable(model);
model.init();
ko.applyBindings(model, document.getElementById('main-content'));