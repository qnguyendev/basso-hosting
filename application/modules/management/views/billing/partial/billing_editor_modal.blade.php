<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
$action = strtolower($this->router->fetch_method());
?>
<div class="modal fade" id="billing-editor-modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-notice">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{$action == 'in' ? 'Tạo phiếu thu' : 'Tạo phiếu chi'}}</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>
                        {{$action == 'in' ? 'Người nộp' : 'Người nhận tiền'}}
                        <span class="text-danger">*</span>
                        <span class="validationMessage" data-bind="validationMessage: modal.name"></span>
                    </label>
                    <input type="text" class="form-control" data-bind="value: modal.name"/>
                </div>
                <div class="form-group">
                    <label>Lý do</label>
                    <input type="text" class="form-control" data-bind="value: modal.des"/>
                </div>
                <div class="row">
                    <div class="form-group col-sm-12 col-lg-6">
                        <label>Đối tượng</label>
                        <select class="form-control" data-bind="value: modal.group" name="group">
                            @foreach(BillingGroup::LIST_STATE as $key=>$value)
                                <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-12 col-lg-6">
                        <label>PTTT</label>
                        <select class="form-control" data-bind="value: modal.payment_method" name="payment_method">
                            @foreach(BillingPaymentMethod::LIST_STATE as $key=>$value)
                                <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <label>Điện thoại (nếu có)</label>
                        <input type="text" class="form-control" data-bind="value: modal.phone"/>
                    </div>
                    <div class="form-group col-12 col-md-6">
                        <label>
                            Loại phiếu
                            <span class="text-danger">*</span>
                            <span class="validationMessage" data-bind="validationMessage: modal.term_id"></span>
                        </label>
                        <select class="form-control"
                                data-bind="value: modal.term_id, options: terms, optionsValue: 'id', optionsText: 'name'"></select>
                    </div>

                    <div class="col-12 col-md-6 form-group">
                        <label>Thời gian</label>
                        <input type="text" class="form-control text-center" data-bind="dateTimePicker: modal.time"/>
                    </div>
                    <div class="col-12 col-md-6 form-group">
                        <label>
                            Số tiền
                            <span class="text-danger">*</span>
                            <span class="validationMessage" data-bind="validationMessage: modal.amount"></span>
                        </label>
                        <div class="input-group">
                            <input type="text" class="form-control text-right" data-bind="moneyMask: modal.amount"/>
                            <div class="input-group-append">
                                <span class="input-group-text">đ</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center">
                <button class="btn btn-success" data-bind="click: modal.save">
                    <i class="fa fa-check"></i>
                    TẠO PHIẾU
                </button>
            </div>
        </div>
    </div>
</div>