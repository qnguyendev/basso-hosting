function viewModel() {
    let self = this;
    self.result = ko.mapping.fromJS([]);
    self.pagination = ko.observable();
    self.terms = ko.mapping.fromJS([]);
    self.total = ko.observable();

    self.filter = {
        time: ko.observable('all'),
        start: ko.observable(moment(new Date()).format('DD-MM-YYYY')),
        end: ko.observable(moment(new Date()).format('DD-MM-YYYY')),
        payment_method: ko.observable('all'),
        key: ko.observable()
    };

    self.modal = {
        term_id: ko.observable(),
        time: ko.observable().extend({required: {message: LABEL.required}}),
        name: ko.observable().extend({required: {message: LABEL.required}}),
        amount: ko.observable().extend({required: {message: LABEL.required}}),
        phone: ko.observable(),
        des: ko.observable(),
        group: ko.observable($('select[name=group] option:first').val()),
        payment_method: ko.observable($('select[name=payment_method] option:first').val()),
        show: function () {
            self.modal.time(moment(new Date()).format('YYYY-MM-DD HH:mm'));
            self.modal.name(undefined);
            self.modal.amount(undefined);
            self.modal.phone(undefined);
            self.modal.des(undefined);
            self.modal.errors.showAllMessages(false);

            MODAL.show('#billing-editor-modal');
        },
        save: function () {
            if (!self.modal.isValid())
                self.modal.errors.showAllMessages();
            else {
                let data = {
                    time: self.modal.time(),
                    name: self.modal.name(),
                    amount: toNumber(self.modal.amount()),
                    phone: self.modal.phone(),
                    des: self.modal.des(),
                    term_id: self.modal.term_id(),
                    group: self.modal.group(),
                    payment_method: self.modal.payment_method()
                };

                AJAX.post(window.location, data, true, (res) => {
                    if (res.error)
                        ALERT.error(res.message);
                    else {
                        MODAL.hide('#billing-editor-modal');
                        ALERT.success(res.message);
                        self.search(1);
                    }
                });
            }
        }
    };

    self.search = function (page) {
        let data = {
            page: page,
            payment_method: self.filter.payment_method(),
            time: self.filter.time(),
            start: self.filter.start(),
            end: self.filter.end(),
            key: self.filter.key()
        };

        AJAX.get(window.location, data, true, (res) => {
            ko.mapping.fromJS(res.terms, self.terms);
            ko.mapping.fromJS(res.data, self.result);

            self.pagination(res.pagination);
            self.total(res.total);
        });
    };

    self.export_excel = function () {
        var url = `/management/billing/export?type=out&time=${self.filter.time()}&start=${self.filter.start()}&end=${self.filter.end()}&payment_method=${self.filter.payment_method()}`;
        if (self.filter.key() !== undefined) {
            if (self.filter.key() !== null) {
                if (self.filter.key().length > 0)
                    url += `&key=${self.filter.key()}`;
            }
        }

        window.location = url;
    };
}

let model = new viewModel();
model.search(1);
ko.validatedObservable(model.modal);
ko.applyBindings(model, document.getElementById('main-content'));