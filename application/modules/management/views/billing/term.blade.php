<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">
                        Phân loại thu chi
                        <a href="#" class="float-right" data-bind="click: add_modal">
                            <i class="fa fa-plus"></i>
                            Thêm phân loại
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Phân loại</th>
                            <th>Mô tả</th>
                            <th width="180"></th>
                        </tr>
                        </thead>
                        <tbody data-bind="foreach: result" class="text-center">
                        <tr>
                            <th data-bind="text: $index() + 1"></th>
                            <td data-bind="text: name"></td>
                            <td data-bind="text: description"></td>
                            <td>
                                <button class="btn btn-xs btn-link" data-bind="click: $root.edit, enable: editable() == 1">
                                    <i class="fa fa-edit"></i>
                                    Cập nhật
                                </button>
                                <button class="btn btn-xs btn-danger ml-1"
                                        data-bind="click: $root.delete, enable: editable() == 1">
                                    <i class="fa fa-trash"></i>
                                    Xóa
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    @include('partial/term_editor_modal')
@endsection
@section('script')
    <script src="{{load_js('term')}}"></script>
@endsection