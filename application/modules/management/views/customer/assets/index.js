function viewModel() {
    let self = this;
    self.result = ko.mapping.fromJS([]);
    self.pagination = ko.observable();
    self.data = {
        cities: ko.mapping.fromJS([]),
        districts: ko.mapping.fromJS([]),
        groups: ko.mapping.fromJS([]),
        init: function () {
            AJAX.get(window.location, {action: 'init'}, false, (res) => {
                ko.mapping.fromJS(res.cities, self.data.cities);
                ko.mapping.fromJS(res.groups, self.data.groups);

                self.modal.city_id(res.cities[0].id);
                self.search(1);
            });
        }
    };

    self.filter = {
        total_paid: ko.observable(0),
        group_id: ko.observable(0),
        key: ko.observable(),
        start: ko.observable(),
        end: ko.observable()
    };

    self.search = function (page) {
        let data = {
            total_paid: toNumber(self.filter.total_paid()),
            page: page,
            group_id: self.filter.group_id(),
            key: self.filter.key(),
			start: self.filter.start(),
			end: self.filter.end(),
            action: 'search'
        };

        AJAX.get(window.location, data, true, (res) => {
            ko.mapping.fromJS(res.data, self.result);
            self.pagination(res.pagination);
        });
    };

	self.exportCustomers = function () {
		 let data = {
            total_paid: toNumber(self.filter.total_paid()),
            page: self.pagination().current_page,
            group_id: self.filter.group_id(),
            key: self.filter.key(),
			start: self.filter.start(),
			end: self.filter.end(),
            action: 'export'
        };

		AJAX.post(window.location, data, true, (res) => {
            if (res.error)
                NOTI.danger(res.message);
            else {
                if (res.redirect_url != null)
                    window.location = res.redirect_url;
            }
        });
    };
	
	self.not_exportCustomers = function () {
		NOTI.danger("Bạn không được quyền xuất file này");
	}

    self.modal = {
        name: ko.observable().extend({required: {message: LABEL.required}}),
        email: ko.observable(),
        phone: ko.observable(),
        address: ko.observable(),
        city_id: ko.observable(),
        district_id: ko.observable(),
        group_id: ko.observable(),
        gender: ko.observable('male'),
        note: ko.observable(),
        show: function () {
            self.modal.name(undefined);
            self.modal.email(undefined);
            self.modal.phone(undefined);
            self.modal.address(undefined);
            self.modal.note(undefined);
            self.modal.group_id(undefined);
            self.modal.errors.showAllMessages(false);
            MODAL.show('#editor-modal');
        },
        save: function () {
            if (!self.modal.isValid())
                self.modal.errors.showAllMessages();
            else {
                let data = {
                    name: self.modal.name(),
                    phone: self.modal.phone(),
                    email: self.modal.email(),
                    address: self.modal.address(),
                    city_id: self.modal.city_id(),
                    district_id: self.modal.district_id(),
                    gender: self.modal.gender(),
                    note: self.modal.note(),
                    group_id: self.modal.group_id(),
                    action: 'add'
                };

                AJAX.post(window.location, data, true, (res) => {
                    if (res.error)
                        NOTI.danger(res.message);
                    else {
                        MODAL.hide('#editor-modal');
                        ALERT.success(res.message);
                        self.search(1);
                    }
                });
            }
        },
        change_city: function () {
            AJAX.get(window.location, {action: 'change-city', id: self.modal.city_id()}, false, (res) => {
                ko.mapping.fromJS(res.data, self.data.districts);
                self.modal.district_id(res.data[0].id);
            });
        }
    };

    self.import_modal = {
        data: ko.observable().extend({required: {message: LABEL.required}}),
        log: ko.observable(),
        list: ko.observableArray([]),
        show: function () {
            self.import_modal.data(undefined);
            self.import_modal.log(undefined);
            self.import_modal.errors.showAllMessages(false);
            MODAL.show('#import-modal');
        },
        save: function () {
            if (!self.import_modal.isValid()) {
                NOTI.danger('Vui lòng nhập thông tin khách hàng');
                return;
            }

            self.import_modal.log(undefined)
            self.import_modal.list(self.import_modal.data().split('\n'));
            if (self.import_modal.list().length > 0) {
                $('#loader').show();
                self.import_modal.send(0);
            } else {
                NOTI.danger('Vui lòng nhập thông tin khách hàng');
            }
        },
        send: function (index) {
            if (index < self.import_modal.list().length) {
                let data = self.import_modal.list()[index].split('\t');
                AJAX.post(window.location, {action: 'import', data: data}, false, (res) => {
                    let log = self.import_modal.log();
                    self.import_modal.log(`${log}\n${res.message}`);

                    if (index < self.import_modal.list().length - 1) {
                        index++;
                        self.import_modal.send(index);
                    } else {
                        $('#loader').fadeOut();
                        self.import_modal.data(undefined);
                        self.import_modal.errors.showAllMessages(false);
                        ALERT.success('Hoàn tất nhập khách hàng');
                        self.search(1);
                    }
                });
            }
        }
    };
}

let model = new viewModel();
model.data.init();
ko.validatedObservable(model.modal);
ko.validatedObservable(model.import_modal);
ko.applyBindings(model, document.getElementById('main-content'));