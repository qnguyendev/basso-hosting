<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="card card-default">
        <div class="card-header">
            <div class="card-title">
                Danh sách khách hàng
                <a href="#" class="float-right text-success ml-3" data-bind="click: import_modal.show">
                    <i class="fa fa-file-import"></i>
                    Nhập khách cũ
                </a>

                <a href="#" class="float-right" data-bind="click: modal.show">
                    <i class="fa fa-plus"></i>
                    Thêm khách hàng
                </a>
				<?php
					/* fixcode chỉ có nhan vien cham soc khach hang moi cap nhat note dc */
					global $user_roles;
					if(in_array("admin", $user_roles)){
				?>
				<a class="btn btn-success btn-xs float-right mr-2" href="#" data-bind="click: $root.exportCustomers">
					<i class="fa fa-file-export"></i>
					Xuất Excel
				</a>
				<?php } else { ?>
				<a class="btn btn-success btn-xs float-right mr-2" href="#" data-bind="click: $root.not_exportCustomers">
					<i class="fa fa-file-export"></i>
					Xuất Excel
				</a>
				<?php }  ?>
            </div>
        </div>
        <div class="card-body">
            <form autocomplete="off" class="row">
                <div class="form-group col-sm-12 col-md-6 col-lg-2">
					<label>Tổng chi</label>
					<div class="input-group">
						<input type="text" class="form-control" data-bind="moneyMask: filter.total_paid"/>
						<div class="input-group-append">
							<span class="input-group-text">đ</span>
						</div>
					</div>
                </div>
                <div class="form-group col-sm-12 col-md-6 col-lg-2">
					<label>Nhóm khách</label>
					<select class="form-control" data-bind="value: filter.group_id">
						<option value="0">Tất cả</option>
						<!--ko foreach: data.groups-->
						<option data-bind="value: id, text: name"></option>
						<!--/ko-->
					</select>
                </div>
				<div class="form-group col-md-6 col-lg-3 col-sm-12 col-xl-2">
					<label class="label_right_calendar"><em class="icon-calendar"></em> Từ ngày</label>
					<input type="text" class="form-control"
						   data-bind="datePicker: filter.start"/>
				</div>
				<div class="col-md-6 form-group col-lg-3 col-sm-12 col-xl-2">
					<label class="label_right_calendar"><em class="icon-calendar"></em> Đến ngày</label>
					<input type="text" class="form-control"
						   data-bind="datePicker: filter.end"/>
				</div>
                <div class="form-group col-sm-12 col-md-6 col-xl-3">
					<label>&nbsp;</label>
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Tìm theo tên, mã, số điện thoại"
                               data-bind="value: filter.key"/>
                        <div class="input-group-append">
                            <button class="btn btn-sm btn-primary" data-bind="click: function(){search(1);}">
                                <i class="fa fa-search"></i>
                                Tìm
                            </button>
                        </div>
                    </div>
                </div>
            </form>

            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Khách hàng</th>
                        <th>Điện thoại</th>
                        <th>Tiền hàng</th>
                        <th>Địa chỉ</th>
						<!--
                        <th>Đã thanh toán</th>
                        <th>Còn thiếu</th>
						-->
                        <th>Nhóm khách</th>
                    </tr>
                    </thead>
                    <tbody class="text-center" data-bind="foreach: result">
                    <tr>
                        <td>
                            <a class="text-info font-weight-bold"
                               data-bind="attr: {href: `/management/customer/detail/${id()}`}, text: name">
                                <i class="icon-pencil"></i>
                                Chi tiết
                            </a>
                        </td>
                        <td data-bind="text: phone"></td>
						
                        <td data-bind="text: parseInt(sub_total()).toMoney(0)"></td>
						<td data-bind="text: address"></td>
						<!--
                        <td data-bind="text: parseInt(total_paid()).toMoney(0)"></td>
                        <td data-bind="text: parseInt(sub_total() - total_paid()).toMoney(0)"></td>
                        -->
						<td data-bind="text: group"></td>
                    </tr>
                    </tbody>
                </table>
            </div>

            @include('pagination')
        </div>
    </div>
@endsection
@section('script')
    <script src="{{load_js('index')}}"></script>
@endsection
@section('modal')
    @include('partial/index/editor_modal')
    @include('partial/index/import_modal')
@endsection