<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="import-modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Nhập khách hàng
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form autocomplete="off" class="modal-body">
                <textarea class="form-control" data-bind="value: import_modal.data" rows="10" cols="10"
                          placeholder="Paste dữ liệu khách hàng ở đây"></textarea>
                <textarea class="form-control mt-3" data-bind="value: import_modal.log" rows="10" cols="10"
                          readonly></textarea>
            </form>
            <div class="modal-footer text-center">
                <button class="btn btn-success" data-bind="click: import_modal.save">
                    <i class="fa fa-check"></i>
                    Lưu lại
                </button> 
            </div>
        </div>
    </div>
</div>