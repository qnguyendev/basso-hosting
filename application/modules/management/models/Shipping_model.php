<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Shipping_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Shipping_model extends CI_Model
{
    /**
     * Thêm hình thức vận chuyển
     * @param int $id
     * @param string $name
     * @param string|null $phone
     * @param string|null $website
     * @return bool
     */
    public function insert(int $id = 0, string $name, string $phone = null, string $website = null)
    {
        $this->db->trans_begin();
        $data = [
            'name' => $name,
            'phone' => $phone,
            'website' => $website
        ];

        if ($id == 0)
            $this->db->insert('shippings', $data);
        else {
            $this->db->where('id', $id);
            $this->db->update('shippings', $data);
        }

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function get(int $id = null)
    {
        if ($id == null) {
            $this->db->order_by('name', 'asc');
            return $this->db->get('shippings')->result();
        }

        $this->db->where('id', $id);
        return $this->db->get('shippings')->row();
    }

    /**
     * Xóa hình thức vận chuyển
     * @param int $id
     * @return bool
     */
    public function delete(int $id)
    {
        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->delete('shippings');
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }
}