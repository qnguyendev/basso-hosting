<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-10 offset-lg-1 col-xl-8 offset-xl-2">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">
                        <a href="{{base_url('admin/tracking_code/create')}}" class="float-right">
                            <i class="fa fa-plus"></i>
                            Thêm mã
                        </a>
                        Danh sách mã javascript
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th width="60">#</th>
                            <th>Tên</th>
                            <th>Kích hoạt</th>
                            <th>Vị trí hiển thị</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody data-bind="foreach: result" class="text-center">
                            <tr>
                                <th data-bind="text: $index() + 1"></th>
                                <td>
                                    <a data-bind="attr: {href: '/admin/tracking_code/edit/' + id()}, text: name"
                                       class="font-weight-bold"></a></td>
                                <td>
                                    <!--ko if: active() == 1-->
                                    <span class="badge badge-success">Có</span>
                                    <!--/ko-->
                                    <!--ko if: active() == 0-->
                                    <span class="badge badge-danger">Không</span>
                                    <!--/ko-->
                                </td>
                                <td>
                                    <!--ko if: position() == 'head'-->
                                    Trong thẻ HEAD
                                    <!--/ko-->
                                    <!--ko if: position() == 'body'-->
                                    Trong thẻ BODY
                                    <!--/ko-->
                                    <!--ko if: position() == 'footer'-->
                                    Cuối trang web
                                    <!--/ko-->
                                </td>
                                <td>
                                    <a data-bind="attr: {href: '{{base_url("admin/tracking_code/edit/")}}' + id()}"
                                       class="btn btn-xs btn-info">
                                        <i class="fa fa-edit"></i>
                                        Sửa
                                    </a>
                                    <a href="javascript:" class="btn btn-xs btn-danger" data-bind="click: $root.delete">
                                        <i class="fa fa-trash"></i>
                                        Xóa
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    @include('pagination')
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{load_js('index')}}"></script>
@endsection