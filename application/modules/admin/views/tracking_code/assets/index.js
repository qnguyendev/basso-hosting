function viewModel() {
    let self = this;
    self.pagination = ko.observable();
    self.result = ko.mapping.fromJS([]);

    self.search = function (page) {
        AJAX.get(window.location, {page: page}, true, (res) => {
            self.pagination(res.pagination);
            ko.mapping.fromJS(res.data, self.result);
        });
    };

    self.delete = function (item) {
        ALERT.confirm('Xác nhận xóa', 'Bạn muốn xóa mã ' + item.name(), function () {
            AJAX.delete(window.location, {id: item.id()}, true, (res) => {
                if (res.error)
                    NOTI.danger(res.message);
                else {
                    NOTI.success(res.message);
                    self.result.remove(item);
                }
            });
        });
    }
}

let model = new viewModel();
model.search(1);
ko.applyBindings(model, document.getElementById('main-content'));