function viewModel() {
    let self = this;
    self.id = ko.observable(0);
    self.name = ko.observable().extend({
        required: {message: LABEL.required}
    });
    self.code = ko.observable().extend({
        required: {message: LABEL.required}
    });

    self.active = ko.observable(1);
    self.page = ko.observableArray([]);
    self.position = ko.observable('footer');

    self.init = function () {
        if (window.location.toString().indexOf('/edit/') > 0) {
            AJAX.get(window.location, null, true, (res) => {
                if (res.error) {
                    NOTI.danger(res.message, function () {
                        window.location = res.url;
                    });
                } else {
                    self.id(res.data.id);
                    self.name(res.data.name);
                    self.code(res.data.code);
                    self.position(res.data.position);
                    self.active(res.data.active);
                    self.page(res.data.page);
                    $('select.select2').val(res.data.page).trigger('change');
                }
            });
        }
    };

    self.save = function () {
        if (!self.isValid())
            self.errors().showAllMessages();
        else {
            let data = {
                id: self.id(),
                active: self.active(),
                name: self.name(),
                code: self.code(),
                position: self.position(),
                page: self.page()
            };

            AJAX.post('/admin/tracking_code/save', data, true, (res) => {
                if (res.error)
                    NOTI.danger(res.message);
                else {
                    window.location = '/admin/tracking_code/';
                }
            });
        }
    };
}

let model = new viewModel();
model.init();
ko.validatedObservable(model);
ko.applyBindings(model, document.getElementById('main-content'));