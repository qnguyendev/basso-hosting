<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-7">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">
                        Đặc tính sản phẩm
                        <a href="javascript:" class="float-right"
                           data-bind="click: attributes().create">
                            <i class="fa fa-plus"></i>
                            Thêm mới
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th class="text-left">Tên</th>
                            <th>Kiểu giá trị</th>
                            <th>Trạng thái</th>
                            <th style="width: 200px"></th>
                        </tr>
                        </thead>
                        <tbody data-bind="foreach: attributes().result" class="text-center">
                        <tr>
                            <td class="text-left" data-bind="text: name"></td>
                            <td>
                                <!--ko if: type() == 'text'-->
                                Giá trị
                                <!--/ko-->
                                <!--ko if: type() == 'color'-->
                                Màu sắc
                                <!--/ko-->
                                <!--ko if: type() == 'image'-->
                                Có ảnh
                                <!--/ko-->
                            </td>
                            <td data-bind="text: active() == 1 ? 'Hiện' : 'Ẩn'"></td>
                            <td>
                                <button class="btn btn-xs btn-behance" data-bind="click: $root.attributes().view">
                                    <i class="fa fa-eye"></i>
                                    Giá trị
                                </button>
                                <button class="btn btn-xs btn-info" data-bind="click: $root.attributes().edit">
                                    <i class="fa fa-edit"></i>
                                    Sửa
                                </button>
                                <button class="btn btn-xs btn-danger" data-bind="click: $root.attributes().delete">
                                    <i class="fa fa-trash"></i>
                                    Xóa
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    @include('attributes_pagination')
                </div>
            </div>
        </div>

        <div class="col-md-12 col-lg-5" data-bind="visible: values().attribute_name() !== undefined">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">
                        <span data-bind="text: values().attribute_name"></span>
                        <a href="javascript:" class="float-right"
                           data-bind="click: values().create">
                            <i class="fa fa-plus"></i>
                            Thêm giá trị
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Giá trị</th>
                            <th data-bind="text: values().type() == 'color' ? 'Màu sắc' : (values().type() == 'image' ? 'Ảnh' : '-')"></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody data-bind="foreach: values().result" class="text-center">
                        <tr>
                            <td data-bind="text: name"></td>
                            <td style="padding: 3px">
                                <!--ko if: type() == 'color' && value() !== null-->
                                <div data-bind="style: {background: value}" class="attribute-value-color"></div>
                                <!--/ko-->
                                <!--ko if: type() == 'image' && value() != null-->
                                <img height="40" data-bind="attr: {src: value}"/>
                                <!--/ko-->
                                <!--ko if: type() == 'text'-->
                                -
                                <!--/ko-->
                            </td>
                            <td>
                                <button class="btn btn-info btn-xs" data-bind="click: $root.values().edit">
                                    <i class="fa fa-edit"></i>
                                    Sửa
                                </button>
                                <button class="btn btn-danger btn-xs" data-bind="click: $root.values().delete">
                                    <i class="fa fa-trash"></i>
                                    Xóa
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <br/>
                    @include('values_pagination')
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="/assets/vendor/colorpicker/js/colorpicker.js"></script>
    <script src="{{load_js('index')}}"></script>
@endsection

@section('css')
    <link rel="stylesheet" href="/assets/vendor/colorpicker/css/colorpicker.css"/>
@endsection

@section('modal')
    @include('modal/attribute_editor')
    @include('modal/value_editor')
@endsection