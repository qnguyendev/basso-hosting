<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="payment-detail-modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Cập nhật hình thức thanh toán
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>
                        Tên hiển thị
                        <span class="text-danger">*</span>
                        <span class="validationMessage" data-bind="validationMessage: name"></span>
                    </label>
                    <input type="text" class="form-control" data-bind="value: name"/>
                </div>
                <div class="form-group">
                    <label>Mô tả cho khách hàng</label>
                    <input type="text" class="form-control" data-bind="value: des"/>
                </div>
                <div class="form-group" data-bind="visible: type() == 'banking' || type() == 'visa'">
                    <label>
                        TrueMoney App key
                        <span class="text-danger">*</span>
                        <span class="validationMessage" data-bind="validationMessage: app_key"></span>
                    </label>
                    <input type="text" class="form-control" data-bind="value: app_key"/>
                </div>
                <div class="form-group" data-bind="visible: type() == 'banking' || type() == 'visa'">
                    <label>
                        TrueMoney App secret
                        <span class="text-danger">*</span>
                        <span class="validationMessage" data-bind="validationMessage: app_secret"></span>
                    </label>
                    <input type="text" class="form-control" data-bind="value: app_secret"/>
                </div>
            </div>
            <div class="modal-footer text-center">
                <button class="btn btn-success text-center" data-bind="click: $root.save">
                    <i class="fa fa-check"></i>
                    Lưu lại
                </button>
            </div>
        </div>
    </div>
</div>