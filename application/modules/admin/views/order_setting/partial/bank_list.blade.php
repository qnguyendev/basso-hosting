<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="bank_list_modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Cập nhật hình thức thanh toán
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-12 col-sm-6">
                        <label>
                            Tên hiển thị
                            <span class="text-danger">*</span>
                            <span class="validationMessage" data-bind="validationMessage: name"></span>
                        </label>
                        <input type="text" class="form-control" data-bind="value: name"/>
                    </div>
                    <div class="form-group col-12 col-sm-6">
                        <label>Mô tả cho khách hàng</label>
                        <input type="text" class="form-control" data-bind="value: des"/>
                    </div>
                </div>

                <br/>
                <h6 class="card-title">Danh sách tài khoản ngân hàng</h6>
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Ngân hàng</th>
                        <th>Số tài khoản</th>
                        <th>Chủ tài khoản</th>
                        <th>Chi nhánh</th>
                        <th width="70"></th>
                    </tr>
                    </thead>
                    <tbody data-bind="foreach: banks" class="text-center">
                    <tr>
                        <td>
                            <input type="text" class="text-center form-control input-sm"
                                   placeholder="Vietcombank" data-bind="value: bank_name"/>
                        </td>
                        <td>
                            <input type="text" class="text-center form-control input-sm"
                                   placeholder="0531000123041" data-bind="value: bank_number"/>
                        </td>
                        <td>
                            <input type="text" class="text-center form-control input-sm"
                                   placeholder="Nguyễn Văn A" data-bind="value: bank_account"/>
                        </td>
                        <td>
                            <input type="text" class="text-center form-control input-sm"
                                   placeholder="Chương Dương" data-bind="value: bank_branch"/>
                        </td>
                        <td>
                            <button class="btn btn-xs btn-danger" data-bind="click: $root.remove_bank">
                                <i class="fa fa-trash"></i>
                                Xóa
                            </button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer text-center">
                <button class="btn btn-primary" data-bind="click: $root.add_bank">
                    <i class="fa fa-plus"></i>
                    Thêm tài khoản
                </button>
                <button class="btn btn-success" data-bind="click: $root.save">
                    <i class="fa fa-check"></i>
                    Lưu lại
                </button>
            </div>
        </div>
    </div>
</div>