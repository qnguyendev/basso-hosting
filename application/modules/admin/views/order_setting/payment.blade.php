<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-12 col-sm-8 offset-sm-2">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">Danh sách hình thức thanh toán</div>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-bordered text-center">
                        <thead>
                        <tr>
                            <th width="60">#</th>
                            <th>Hình thức</th>
                            <th width="140">Trạng thái</th>
                            <th width="160"></th>
                        </tr>
                        </thead>
                        <tbody data-bind="foreach: result">
                        <tr>
                            <th data-bind="text: $index() + 1"></th>
                            <td class="text-left" data-bind="text: name"></td>
                            <td>
                                <!--ko if: active() == 1-->
                                <span class="badge badge-success" style="cursor: pointer"
                                      data-bind="click: $root.change_active">
                                    <i class="fa fa-check"></i>
                                    Áp dụng
                                </span>
                                <!--/ko-->
                                <!--ko if: active() == 0-->
                                <span class="badge badge-danger" style="cursor: pointer"
                                      data-bind="click: $root.change_active">
                                    <i class="fa fa-ban"></i>
                                    Ẩn
                                </span>
                                <!--/ko-->
                            </td>
                            <td>
                                <button class="btn btn-xs btn-info" data-bind="click: $root.edit">
                                    <i class="fa fa-edit"></i>
                                    Cập nhật
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    @include('partial/payment_detail')
    @include('partial/bank_list')
@endsection
@section('script')
    <script src="{{load_js('payment')}}"></script>
@endsection
