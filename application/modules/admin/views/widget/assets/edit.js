function viewModel() {
    let self = this;
    ko.utils.extend(self, new imageUploadModel());

    self.id = ko.observable(0);
    self.type = ko.observable();
    self.area = ko.observableArray([]).extend({
        required: {message: LABEL.required}
    });
    self.title = ko.observable().extend({
        required: {message: LABEL.required}
    });
    self.order = ko.observable(0);
    self.show_title = ko.observable(1);
    self.description = ko.observable();
    self.active = ko.observable(1);
    self.content = ko.observable();

    self.links = ko.observable({
        list: ko.observableArray([]),
        add: function () {
            self.links().list.push({
                text: ko.observable(),
                link: ko.observable(),
                new_tab: ko.observable(),
                follow: ko.observable('follow')
            });
        },
        delete: function (item) {
            self.links().list.remove(item);
        }
    });

    self.product_terms = ko.observable({
        order: ko.observable('asc'),
        show_count: ko.observable(1)
    });

    self.article_terms = ko.observable({
        order: ko.observable('asc'),
        show_count: ko.observable(1)
    });

    self.photos = ko.observable({
        image_id: ko.observableArray([]),
        list: ko.observableArray([]),
        create: function () {
            self.photos().list.push({
                id: ko.observable(guid()),
                title: ko.observable('Tiêu đề banner').extend({
                    required: {
                        message: LABEL.required,
                        onlyIf: function () {
                            return self.type() === 'photos';
                        }
                    }
                }),
                des: ko.observable(),
                link: ko.observable(),
                image_id: ko.observable(),
                image_path: ko.observable()
            });
        },
        upload: function (item) {
            self.image_uploader.show();
            self.image_uploader.save = function () {
                self.photos().image_id.push(self.image_uploader.selected_images[0].id);
                self.photos().list.each((x) => {
                    if (x.id() == item.id()) {
                        x.image_path(self.image_uploader.selected_images[0].path);
                        x.image_id(self.image_uploader.selected_images[0].id);
                    }
                });

                self.image_uploader.close();
            };
        },
        delete: function (item) {
            self.photos().list.remove(item);
        }
    });

    self.products = ko.observable({
        des: ko.observable(),
        order: ko.observable('newest'),
        limit: ko.observable(6)
    });

    self.articles = ko.observable({
        order: ko.observable('newest'),
        terms: ko.observableArray([]),
        limit: ko.observable(6),
        show_des: ko.observable(0),
        des: ko.observable()
    });

    self.member = ko.observable({
        type: ko.observable('all'),
        limit: ko.observable(3),
        list: ko.observable()
    });

    self.testimonial = {
        image_id: ko.observableArray([]),
        list: ko.observableArray([]),
        upload: function (item) {
            self.image_uploader.show();
            self.image_uploader.save = function () {
                self.testimonial.image_id.push(self.image_uploader.selected_images[0].id);
                self.testimonial.list.each((x) => {
                    if (x.id() == item.id()) {
                        x.image_path(self.image_uploader.selected_images[0].path);
                        x.image_id(self.image_uploader.selected_images[0].id);
                    }
                });

                self.image_uploader.close();
            };
        },
        delete: function (item) {
            self.testimonial.list.remove(item);
        },
        add: function () {
            self.testimonial.list.push({
                id: ko.observable(guid()),
                name: ko.observable('Tên khách hàng').extend({
                    required: {
                        message: LABEL.required,
                        onlyIf: function () {
                            return self.type() === 'testimonial';
                        }
                    }
                }),
                content: ko.observable().extend({
                    required: {
                        message: LABEL.required,
                        onlyIf: function () {
                            return self.type() === 'testimonial';
                        }
                    }
                }),
                position: ko.observable(),
                image_id: ko.observable(),
                image_path: ko.observable()
            });
        }
    };

    self.block_text = {
        list: ko.observableArray([]),
        add: function () {
            self.block_text.list.push({
                id: ko.observable(guid()),
                title: ko.observable('Tiêu đề').extend({
                    required: {
                        message: LABEL.required,
                        onlyIf: function () {
                            return self.type() === 'testimonial';
                        }
                    }
                }),
                content: ko.observable()
            });
        }
    };

    self.init = function () {
        if (window.location.toString().indexOf('/edit/') > 0) {
            if (widget == null)
                widget = {};

            self.id(widget.id);
            self.title(widget.title);
            self.show_title(widget.show_title);
            self.order(widget.order);
            self.area(widget.area);
            self.content(widget.options);
            self.description(widget.description);
            self.active(widget.active);

            if (widget.type === 'links')
                self.links().list(widget.options);
            else if (widget.type === 'product_terms') {
                self.product_terms().order(widget.options.order);
                self.product_terms().show_count(widget.options.show_count);
            } else if (widget.type == 'article_terms') {
                self.article_terms().order(widget.options.order);
                self.article_terms().show_count(widget.options.show_count);
            } else if (widget.type == 'photos') {
                if (widget.options != null) {
                    for (let i = 0; i < widget.options.length; i++) {
                        let item = widget.options[i];
                        self.photos().image_id.push(item.image_id);
                        self.photos().list.push({
                            id: ko.observable(item.id),
                            title: ko.observable(item.title),
                            des: ko.observable(item.des),
                            link: ko.observable(item.link),
                            image_path: ko.observable(item.image_path),
                            image_id: ko.observable(item.image_id)
                        })
                    }
                }
            } else if (widget.type == 'testimonial') {
                if (widget.options != null) {
                    for (let i = 0; i < widget.options.length; i++) {
                        let item = widget.options[i];
                        self.testimonial.image_id.push(item.image_id);
                        self.testimonial.list.push({
                            id: ko.observable(item.id),
                            name: ko.observable(item.name),
                            content: ko.observable(item.content),
                            position: ko.observable(item.position),
                            image_id: ko.observable(item.image_id),
                            image_path: ko.observable(item.image_path)
                        });
                    }
                }
            } else if (widget.type == 'products') {
                self.products().des(widget.options.des);
                self.products().order(widget.options.order);
                self.products().limit(widget.options.limit);
            } else if (widget.type == 'articles') {
                self.articles().order(widget.options.order);
                self.articles().limit(widget.options.limit);
                self.articles().terms(widget.options.terms);
                self.articles().des(widget.options.des);
                self.articles().show_des(widget.options.show_des);
                $('select[multiple]').val(widget.options.terms).trigger('change');
            } else if (widget.type == 'member') {
                self.member().type(widget.options.type);
                self.member().list(widget.options.list);
                self.member().limit(widget.options.limit);
            } else if (widget.type == 'block_text') {
                for (let i = 0; i < widget.options.length; i++) {
                    let item = widget.options[i];
                    self.block_text.list.push({
                        id: ko.observable(item.id),
                        title: ko.observable(item.title),
                        content: ko.observable(item.content)
                    });
                }
            }
        }
    };

    self.change_type = function () {
        AJAX.post(window.location, {set_type: self.type()}, false, () => {
            window.location.reload();
        });
    };

    self.save = function () {
        if (!self.isValid())
            self.errors.showAllMessages();
        else {
            let data = {
                id: self.id(),
                title: self.title(),
                type: self.type(),
                show_title: self.show_title(),
                order: self.order(),
                area: self.area(),
                options: self.content(),
                description: self.description(),
                active: self.active()
            };

            if (self.type() == 'links')
                data.options = self.links().list();
            else if (self.type() == 'product_terms')
                data.options = self.product_terms();
            else if (self.type() == 'article_terms')
                data.options = self.article_terms();
            else if (self.type() == 'photos') {
                data.options = self.photos().list();
                data.image_id = self.photos().image_id();
            } else if (self.type() == 'testimonial') {
                data.options = self.testimonial.list();
                data.image_id = self.testimonial.image_id();
            } else if (self.type() == 'products')
                data.options = self.products()
            else if (self.type() == 'articles')
                data.options = self.articles();
            else if (self.type() == 'member')
                data.options = self.member();
            else if (self.type() == 'block_text')
                data.options = self.block_text.list();

            AJAX.post('/admin/widget/save', data, true, (res) => {
                if (res.error)
                    ALERT.error(res.message);
                else {
                    window.location = '/admin/widget';
                }
            });
        }
    };
}

let model = new viewModel();
model.init();
ko.validatedObservable(model);
ko.validatedObservable(model.photos());
ko.validatedObservable(model.testimonial);
ko.validatedObservable(model.block_text);
ko.applyBindings(model, document.getElementById('main-content'));