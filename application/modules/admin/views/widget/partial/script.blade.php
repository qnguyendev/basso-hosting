<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="form-group">
    <label>Nội dung</label>
    <textarea data-bind="value: content" class="form-control" style="resize: none; height: 280px; max-height: none"
              rows="25"
              cols="7"></textarea>
</div>