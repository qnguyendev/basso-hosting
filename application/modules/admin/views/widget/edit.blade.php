<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
usort($positions, function ($a, $b) {
    return strcmp($a['name'], $b['name']);
});

$system_widgets = $system_widgets;
?>
@layout('cpanel_layout')
@section('content')
    <div class="card card-default" id="widget-edit">
        <div class="card-header">
            <div class="card-title">
                <a href="#" class="float-right" data-bind="click: $root.save">
                    <i class="fa fa-check"></i>
                    Lưu lại
                </a>
                <span data-bind="text: id() == 0 ? 'Thêm widget' : 'Cập nhật widget'"></span>
            </div>
        </div>
        <form autocomplete="off" class="card-body">
            <div class="row">
                <div class="col-md-12 col-lg-4">
                    <div class="form-group">
                        <label>
                            Vị trí hiển thị
                            <span class="text-danger">*</span>
                            <span class="validationMessage" data-bind="validationMessage: area"></span>
                        </label>
                        @foreach($positions as $item)
                            <div class="checkbox c-checkbox mt-2 mb-2">
                                <label>
                                    <input class="form-check-input" type="checkbox"
                                           value="{{$item['id']}}" data-bind="checked: area"/>
                                    <span class="fa fa-check"></span>
                                    {{$item['name']}}
                                    <?php if (isset($item['description'])) : ?>
                                    <p style="color: #999;margin: 0">{{$item['description']}}</p>
                                    <?php endif; ?>
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="col-md-12 col-lg-8">
                    <div class="row">
                        <div class="form-group col-12 col-sm-6">
                            <label>Thể loại widget</label>
                            <select class="form-control"
                                    data-bind="value: type, event: {change: change_type}, disable: id() > 0">
                                @foreach($system_widgets as $item)
                                    <option value="{{$item['id']}}"
                                            {{$item['id'] == $widget_view ? 'selected': ''}}>
                                        {{$item['name']}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-12 col-sm-6">
                            <label>
                                Tiêu đều widget
                                <span class="text-danger">*</span>
                                <span class="validationMessage" data-bind="validationMessage: title"></span>
                            </label>
                            <input type="text" class="form-control" data-bind="value: title"/>
                        </div>
                        <div class="form-group col-12">
                            <label>Mô tả widget</label>
                            <textarea type="text" class="form-control"
                                      data-bind="ckeditor: description"></textarea>
                        </div>
                        <div class="form-group col-12 col-sm-4">
                            <label>Hiển thị tiêu đề</label>
                            <select class="form-control" data-bind="value: show_title">
                                <option value="0">Không</option>
                                <option value="1">Có</option>
                            </select>
                        </div>
                        <div class="form-group col-12 col-sm-4">
                            <label>Kích hoạt</label>
                            <select class="form-control" data-bind="value: active">
                                <option value="0">Không</option>
                                <option value="1">Có</option>
                            </select>
                        </div>
                        <div class="form-group col-12 col-sm-4">
                            <label>Thứ tự hiển thị</label>
                            <input type="number" class="form-control" data-bind="value: order"/>
                        </div>
                    </div>
                    @if(is_file(APPPATH.'/modules/admin/views/widget/partial/'.$widget_view.'.blade.php'))
                        @include("partial/$widget_view")
                    @else
                        @include('partial/text')
                    @endif
                </div>
            </div>
        </form>
    </div>
@endsection
@section('script')
    <?php if (isset($widget)) : ?>
    <script>
        let widget = {{json_encode($widget)}};
        widget.area = {{json_encode($area)}};
    </script>
    <?php endif; ?>
    {{ckeditor()}}
    @if($widget_view == 'block_text')
        {{ckeditor()}}
    @endif
    <script src="{{load_js('edit')}}"></script>
@endsection