function viewModel() {
    let self = this;
    self.result = ko.mapping.fromJS([]);
    self.filter_type = ko.observable();
    self.filter_key = ko.observable();
    self.pagination = ko.observable();

    self.search = function (page) {
        let data = {
            page: page,
            type: self.filter_type(),
            key: self.filter_key()
        };
        AJAX.get(window.location, data, true, (res) => {
            ko.mapping.fromJS(res.data, self.result);
            self.pagination(res.pagination);
        });
    };

    self.change_active = function (item) {
        let active = item.active() == 1 ? 0 : 1;
        AJAX.post('/admin/term/change_active', {id: item.id(), active: active}, false, () => {
            item.active(active);
            NOTI.success('Cập nhật trạng thái thành công');
        });
    };

    self.change_order = function (item) {
        AJAX.post('/admin/term/change_order', {id: item.id(), order: item.order()}, false, () => {
            NOTI.success('Cập nhật thứ tự sắp xếp thành công');
        });
    };

    self.delete = function (item) {
        ALERT.confirm('Xác nhận xóa danh mục', null, function () {
            AJAX.delete(window.location, {id: item.id()}, true, () => {
                NOTI.success('Xóa thành công danh mục');
                self.search(self.pagination().current_page);
            });
        });
    }
}

let model = new viewModel();
model.search(1);
ko.applyBindings(model, document.getElementById('main-content'))