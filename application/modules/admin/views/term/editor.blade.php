<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
global $theme_config;
$system_terms = array_filter($theme_config['term'], function ($item) {
    return $item['active'];
});
?>
@layout('cpanel_layout')
@section('content')
    <form class="row" autocomplete="off">
        <div class="col-md-12 col-lg-8">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">Thông tin danh mục</div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label>{{lang('terms_parent_label')}}</label>
                        <select class="form-control" data-bind="value: parent_id">
                            <option value="0">Phân loại chính</option>
                            {{display_select_tree($terms)}}
                        </select>
                    </div>
                    <div class="form-group">
                        <label>
                            Tên danh mục
                            <span class="text-danger">*</span>
                            <span class="validationMessage" data-bind="validationMessage: name"></span>
                        </label>
                        <input type="text" class="form-control"
                               data-bind="value: name"/>
                    </div>
                    <div class="form-group">
                        <label>Đường dẫn tĩnh</label>
                        <input type="text" class="form-control"
                               data-bind="value: slug, event: {change: $root.create_slug}"/>
                    </div>

                    <div class="row">
                        <div class="form-group col-12 col-sm-8">
                            <label>{{lang('terms_type_label')}}</label>
                            <select class="form-control" data-bind="value: type, disable: id() > 0">
                                @foreach($system_terms as $term)
                                    <?php if ($term['active']) : ?>
                                    <option value="{{$term['id']}}">{{$term['name']}}</option>
                                    <?php endif; ?>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-12 col-sm-4">
                            <label>Thứ tự hiển thị</label>
                            <input type="number" data-bind="value: order" class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group" data-bind="visible: type() == 'external_link'">
                        <label>Liên kết yêu cầu</label>
                        <input type="text" class="form-control" data-bind="value: external_url"/>
                    </div>
                    <div class="form-group">
                        <label>Nội dung</label>
                        <textarea data-bind="ckeditor: content"></textarea>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-lg-4">
            <div class="card card-default">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="checkbox c-checkbox w-100 mt-2">
                                <label>
                                    <input class="form-check-input" type="checkbox" data-bind="checked: new_tab"/>
                                    <span class="fa fa-check"></span>
                                    Mở trong tab mới
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="checkbox c-checkbox w-100 mt-2">
                                <label>
                                    <input class="form-check-input" type="checkbox" data-bind="checked: active"/>
                                    <span class="fa fa-check"></span>
                                    Hiển thị
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer text-center">
                    <a class="btn btn-secondary" href="{{base_url('admin/term')}}">
                        <i class="fa fa-arrow-left"></i>
                        Hủy
                    </a>
                    <button class="btn btn-success btn-sm" data-bind="click: save">
                        <i class="fa fa-check"></i>
                        Lưu lại
                    </button>
                </div>
            </div>

            @include('partial/single_image_upload')
            @include('partial/seo_editor')
        </div>
    </form>
@endsection

@section('script')
    {{ckeditor()}}
    <script src="{{load_js('edit')}}"></script>
@endsection