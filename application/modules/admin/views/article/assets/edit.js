function viewModel() {
    let self = this;
    ko.utils.extend(self, new imageUploadModel());

    self.id = ko.observable(0);
    self.name = ko.observable().extend({
        required: {message: LABEL.required}
    });

    self.slug = ko.observable();
    self.des = ko.observable();
    self.content = ko.observable();

    self.created_time = ko.observable(moment(new Date()).format('YYYY-MM-DD HH:mm'));
    self.terms = ko.observableArray([]);

    self.image_id = ko.observable();
    self.image_path = ko.observable();

    self.featured = ko.observable(false);
    self.active = ko.observable(true);

    self.seo_title = ko.observable();
    self.seo_keywords = ko.observable();
    self.seo_description = ko.observable();
    self.seo_index = ko.observable(1);
    self.seo_preview = {
        title: ko.observable('Tiêu đề page website không vượt quá 70 kí tự (tốt nhất từ 60-70 kí tự)'),
        des: ko.observable('Mô tả page website không vượt quá 155 kí tự (tốt nhất từ 100-155 kí tự). Là những đoạn mô tả ngắn gọn về website, bài viết...'),
        slug: ko.observable(undefined),
        change: function () {
            let title = LABEL.default_seo_title, des = LABEL.default_seo_des;

            if (self.name() !== undefined) {
                if (self.name() !== null)
                    if (self.name().length > 0)
                        title = self.name();
            }

            if (self.seo_title() !== undefined) {
                if (self.seo_title() !== null)
                    if (self.seo_title().length > 0)
                        title = self.seo_title();
            }

            if (self.seo_description() !== undefined) {
                if (self.seo_description() !== null)
                    if (self.seo_description().length > 0)
                        des = self.seo_description();
            }

            self.seo_preview.title(title);
            self.seo_preview.slug(self.slug());
            self.seo_preview.des(des);

            if (self.slug() === undefined && self.name() !== undefined) {
                if (self.name() !== null)
                    if (self.name().length > 0)
                        self.slug(create_slug(self.name()));
            }
        }
    };

    self.init = function () {
        if (window.location.toString().indexOf('/edit/') > 0) {
            AJAX.get(window.location, null, true, (res) => {
                if (res.error) {
                    ALERT.error(res.message, function () {
                        window.location = res.url;
                    });
                } else {
                    self.id(res.data.id);
                    self.name(res.data.name);
                    self.slug(res.data.slug);
                    if (res.data.short_content != null)
                        self.des(res.data.short_content);
                    if (res.data.content != null)
                        self.content(res.data.content);
                    if (res.data.seo_title != null)
                        self.seo_title(res.data.seo_title);
                    if (res.data.seo_description != null)
                        self.seo_description(res.data.seo_description);

                    self.seo_preview.change();

                    self.seo_index(res.data.seo_index);
                    self.active(res.data.active == 1);
                    self.featured(res.data.featured == 1);

                    if (res.data.image_path != null)
                        self.image_path(res.data.image_path);
                    if (res.data.image_id != null)
                        self.image_id(res.data.image_id);

                    self.terms(res.terms);
                    self.created_time(moment.unix(res.data.created_time).format('YYYY-MM-DD HH:mm'));
                }
            });
        }
    };

    //#region Xem trước ảnh upload
    self.image_upload = function () {
        self.image_uploader.show(true);
        self.image_uploader.save = function () {
            self.image_id(self.image_uploader.selected_images[0].id);
            self.image_path(self.image_uploader.selected_images[0].path);
            self.image_uploader.close();
        };
    };

    self.delete_image = function () {
        self.image_id(undefined);
        self.image_path(undefined);
    };
    //#endregion

    self.create_slug = function () {
        if (self.slug() !== undefined)
            if (self.slug().length > 0)
                self.slug(create_slug(self.slug()));
    };

    self.save = function () {
        if (!self.isValid())
            self.errors.showAllMessages();
        else {
            let data = {
                name: self.name(),
                slug: self.slug(),
                id: self.id(),
                created_time: self.created_time(),
                active: self.active() ? 1 : 0,
                featured: self.featured() ? 1 : 0,
                seo_index: self.seo_index()
            };

            if (self.image_id() !== undefined)
                data.image = self.image_id();

            if (self.terms().length > 0)
                data.terms = self.terms();

            if (self.des() !== undefined)
                if (self.des().length > 0)
                    data.des = self.des();

            if (self.content() !== undefined)
                if (self.content().length > 0)
                    data.content = self.content();

            if (self.seo_title() !== undefined)
                if (self.seo_title().length > 0)
                    data.seo_title = self.seo_title();

            if (self.seo_description() !== undefined)
                if (self.seo_description().length > 0)
                    data.seo_des = self.seo_description();

            AJAX.post('/admin/article/save', data, true, (res) => {
                if (res.error)
                    ALERT.error(res.message);
                else
                    window.location = '/admin/article';
            });
        }
    }
}

let model = new viewModel();
model.init();
ko.validatedObservable(model);
ko.applyBindings(model, document.getElementById('main-content'))