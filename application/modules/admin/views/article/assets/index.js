function viewModel() {
    let self = this;
    self.result = ko.mapping.fromJS([]);
    self.pagination = ko.observable();
    self.filter_key = ko.observable();
    self.filter_term = ko.observable(0);

    self.search = function (page) {
        let data = {
            page: page,
            key: self.filter_key(),
            term_id: self.filter_term()
        };

        AJAX.get(window.location, data, true, (res) => {
            ko.mapping.fromJS(res.data, self.result);
            self.pagination(res.pagination);
        });
    }

    self.change_active = function (item) {
        let active = item.active() == 1 ? 0 : 1;
        AJAX.post('/admin/article/change_active', {id: item.id(), active: active}, false, () => {
            item.active(active);
            NOTI.success('Cập nhật trạng thái thành công');
        });
    };

    self.change_featured = function (item) {
        let featured = item.featured() == 1 ? 0 : 1;
        AJAX.post('/admin/article/change_featured', {id: item.id(), featured: featured}, false, () => {
            item.featured(featured);
            NOTI.success('Thay đổi thành công');
        });
    };

    self.delete = function (item) {
        ALERT.confirm('Xác nhận xóa danh mục', null, function () {
            AJAX.delete(window.location, {id: item.id()}, true, () => {
                NOTI.success('Xóa thành công bài viết');
                self.search(self.pagination().current_page);
            });
        });
    }
}

let model = new viewModel();
model.search(1);
ko.applyBindings(model, document.getElementById('main-content'))