<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')

@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-7 col-xl-6 offset-xl-1">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">Thông tin thành viên</div>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label>
                            Họ tên
                            <span class="text-danger">*</span>
                            <span class="validationMessage" data-bind="validationMessage: name"></span>
                        </label>
                        <input type="text" class="form-control" data-bind="value: name"/>
                    </div>
                    <div class="form-group">
                        <label>Tóm tắt</label>
                        <textarea class="form-control no-resize" data-bind="value: description"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Nội dung về thành viên</label>
                        <textarea data-bind="ckeditor: content"></textarea>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-lg-5 col-xl-4">
            <div class="card card-default">
                <div class="card-body text-center">
                    <a href="{{base_url('admin/member')}}" class="btn btn-secondary">
                        <i class="fa fa-arrow-left"></i>
                        Quay lại
                    </a>
                    <button class="btn btn-success" data-bind="click: $root.save">
                        <i class="fa fa-check"></i>
                        Lưu lại
                    </button>
                </div>
            </div>
            @include('partial/single_image_upload')

            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <label>
                            Vị trí làm việc
                            <span class="text-danger">*</span>
                            <span class="validationMessage" data-bind="validationMessage: position"></span>
                        </label>
                        <input type="text" class="form-control" data-bind="value: position"/>
                    </div>
                    <div class="form-group">
                        <label>Địa chỉ email</label>
                        <input type="email" class="form-control" data-bind="value: email"/>
                    </div>
                    <div class="form-group">
                        <label>Số điện thoại</label>
                        <input type="text" class="form-control" data-bind="value: phone"/>
                    </div>
                    <div class="form-group">
                        <label>Facebook</label>
                        <input type="text" class="form-control" data-bind="value: facebook"/>
                    </div>
                    <div class="form-group">
                        <label>Twitter</label>
                        <input type="text" class="form-control" data-bind="value: twitter"/>
                    </div>
                    <div class="form-group">
                        <label>Google Plus</label>
                        <input type="text" class="form-control" data-bind="value: google_plus"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    {{ckeditor()}}
    <?php if (isset($member)) : ?>
    <script>
        let data = {{json_encode($member)}};
    </script>
    <?php endif; ?>
    <script src="{{load_js('edit')}}"></script>
@endsection