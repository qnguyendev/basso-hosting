<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <div class="card">
                <div class="card-header">
                    <h6 class="card-title">Bài viết mới</h6>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Bài viết</th>
                            <th>Thời gian</th>
                            <th>Danh mục</th>
                            <th>Ẩn/hiện</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($posts as $item)
                            <tr>
                                <td class="text-left">
                                    <a href="{{base_url('admin/article/edit/'.$item->id)}}">{{$item->name}}</a>
                                </td>
                                <td>{{date('H:i d/m/Y', $item->created_time)}}</td>
                                <td>{{$item->terms}}</td>
                                <td>{{$item->active == 1 ? 'Hiện' : 'Ẩn'}}</td>
                                <td>
                                    <a href="{{base_url('admin/article/edit/'.$item->id)}}"
                                       class="btn btn-info btn-xs">
                                        <i class="now-ui-icons design-2_ruler-pencil"></i>
                                        Cập nhật
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection