<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="card">
        <div class="card-header text-center">
            <h1 class="page-title">Error 404</h1>
        </div>
        <div class="card-body">
            <div class="error-body text-center">
                <h3 class="text-uppercase">Không tìm thấy trang</h3>
                <p class="text-muted m-t-30 m-b-30">Vui lòng thử lại</p>
                <a class="btn btn-danger" href="/">
                    <i class="now-ui-icons loader_refresh spin"></i>
                    TRANG CHỦ
                </a>
            </div>
        </div>
    </div>
@endsection