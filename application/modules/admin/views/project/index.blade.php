<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('top')
    <a href="{{base_url('admin/project/create')}}" class="btn btn-primary">
        <i class="now-ui-icons ui-1_simple-add"></i>
        Thêm dự án
    </a>
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-10 offset-sm-1">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">Danh sách dự án</div>
                </div>
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th width="80">ID</th>
                            <th width="80"></th>
                            <th>Dự án</th>
                            <th>Khách hàng</th>
                            <th>Danh mục</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody data-bind="foreach: result">
                        <tr>
                            <th data-bind="text: id"></th>
                            <td>
                                <!--ko if: image() != null -->
                                <img data-bind="attr: {src: '/timthumb.php?src=' + image() + '&w=70&h=70'}, visible: image() != null"/>
                                <!--/ko-->
                                <!--ko if: image() == null-->
                                <img src="/timthumb.php?src=/assets/img/no-image-available.jpg&w=50&h=50"/>
                                <!--/ko-->
                            </td>
                            <td>
                                <a data-bind="attr: {href: '{{base_url('admin/project/edit/')}}' + id()}, text: name"
                                   class="font-weight-bold">
                                </a>
                            </td>
                            <td data-bind="text: client"></td>
                            <td data-bind="text: terms"></td>
                            <td>
                                <a data-bind="attr: {href: '{{base_url('admin/project/edit/')}}' + id()}"
                                   class="btn btn-xs btn-info">
                                    <i class="now-ui-icons design-2_ruler-pencil"></i>
                                    Sửa
                                </a>
                                <a href="javascript:" class="btn btn-xs btn-danger" data-bind="click: $root.delete">
                                    <i class="now-ui-icons ui-1_simple-remove"></i>
                                    Xóa
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    @include('pagination')
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{load_js('index')}}"></script>
@endsection