<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('top')
    <a href="{{base_url('admin/project')}}" class="btn btn-default">
        <i class="now-ui-icons arrows-1_minimal-left"></i>
        Quay lại
    </a>
    <button class="btn btn-success" data-bind="click: $root.save">
        <i class="now-ui-icons ui-1_check"></i>
        Lưu lại
    </button>
@endsection
@section('content')
    <div class="row">
        <div class="col-sm-6 offset-sm-1">
            <div class="card card-default">
                <div class="card-body">
                    <div class="form-group">
                        <label>
                            Tên dự án
                            <span class="text-danger">*</span>
                            <span class="validationMessage" data-bind="validationMessag: name"></span>
                        </label>
                        <input type="text" class="form-control" data-bind="value: name"/>
                    </div>
                    <div class="form-group">
                        <label>Nội dung/thông tin dự án</label>
                        <textarea data-bind="ckeditor: content"></textarea>
                    </div>
                </div>
            </div>

            <!-- Hình ảnh -->
            <div class="card">
                <div class="card-header">
                    <h6 class="card-title">
                        Hình ảnh
                        <label class="pull-right text-link" for="productImages">
                            Chọn hình
                            <input type="file" data-bind="event: {change: $root.images_preview}"
                                   accept="image/jpg, image/jpeg, image/png"
                                   style="display: none;visibility: hidden" multiple id="productImages"/>
                        </label>
                    </h6>
                </div>
                <div class="card-body row" id="preview_image">
                    <div class="text-center" data-bind="visible: images().length === 0"
                         style="margin: 0 auto; color: #777">
                        <svg class="next-icon-product next-icon-product-80 next-upload-product-dropzone__icon"
                             style="fill: #d3dbe2">
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-photos-80">
                                <svg id="next-photos-80" class="icon-symbol--loaded" width="100%" height="100%">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 80 80">
                                        <path d="M80 57.6l-4-18.7v-23.9c0-1.1-.9-2-2-2h-3.5l-1.1-5.4c-.3-1.1-1.4-1.8-2.4-1.6l-32.6 7h-27.4c-1.1 0-2 .9-2 2v4.3l-3.4.7c-1.1.2-1.8 1.3-1.5 2.4l5 23.4v20.2c0 1.1.9 2 2 2h2.7l.9 4.4c.2.9 1 1.6 2 1.6h.4l27.9-6h33c1.1 0 2-.9 2-2v-5.5l2.4-.5c1.1-.2 1.8-1.3 1.6-2.4zm-75-21.5l-3-14.1 3-.6v14.7zm62.4-28.1l1.1 5h-24.5l23.4-5zm-54.8 64l-.8-4h19.6l-18.8 4zm37.7-6h-43.3v-51h67v51h-23.7zm25.7-7.5v-9.9l2 9.4-2 .5zm-52-21.5c-2.8 0-5-2.2-5-5s2.2-5 5-5 5 2.2 5 5-2.2 5-5 5zm0-8c-1.7 0-3 1.3-3 3s1.3 3 3 3 3-1.3 3-3-1.3-3-3-3zm-13-10v43h59v-43h-59zm57 2v24.1l-12.8-12.8c-3-3-7.9-3-11 0l-13.3 13.2-.1-.1c-1.1-1.1-2.5-1.7-4.1-1.7-1.5 0-3 .6-4.1 1.7l-9.6 9.8v-34.2h55zm-55 39v-2l11.1-11.2c1.4-1.4 3.9-1.4 5.3 0l9.7 9.7c-5.2 1.3-9 2.4-9.4 2.5l-3.7 1h-13zm55 0h-34.2c7.1-2 23.2-5.9 33-5.9l1.2-.1v6zm-1.3-7.9c-7.2 0-17.4 2-25.3 3.9l-9.1-9.1 13.3-13.3c2.2-2.2 5.9-2.2 8.1 0l14.3 14.3v4.1l-1.3.1z"></path>
                                    </svg>
                                </svg>
                            </use>
                            <p style="color:#c3cfd8">Sử dụng nút <strong>Chọn hình</strong> để thêm hình.</p>
                    </div>
                    <!--ko foreach: images-->
                    <div class="col-12 col-sm-3">
                        <div class="card card-product">
                            <div class="card-img">
                                <button type="button" class="btn-close btn btn-danger"
                                        data-bind="click: $root.delete_image">
                                    x
                                </button>
                                <img data-bind="attr: {src: '/timthumb.php?src=' + path + '&w=100&h=100'}" class=""/>
                            </div>
                        </div>
                    </div>
                    <!--/ko-->
                </div>
            </div>
        </div>

        <div class="col-sm-4">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <label>Khách hàng</label>
                        <input type="text" class="form-control" data-bind="value: client"/>
                    </div>
                    <div class="form-group">
                        <label>Địa điểm</label>
                        <input type="text" class="form-control" data-bind="value: location"/>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <h6 class="card-title">Danh mục dự án</h6>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        @foreach($terms as $term)
                            <div class="form-check mt-3">
                                <label class="form-check-label font-weight-normal">
                                    <input class="form-check-input" type="checkbox" value="{{$term->id}}"
                                           data-bind="checked: terms"/>
                                    <span class="form-check-sign"></span>
                                    {{$term->name}}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    {{ckeditor()}}
    <?php if(isset($project)) : ?>
    <script>
        let data = {{json_encode($project)}};
    </script>
    <?php endif; ?>
    <script src="/assets/js/underscore.min.js"></script>
    <script src="{{load_js('edit')}}"></script>
@endsection