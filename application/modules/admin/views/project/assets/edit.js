function viewModel() {
    let self = this;
    self.id = ko.observable(0);
    self.name = ko.observable().extend({
        required: {message: LABEL.required}
    });
    self.content = ko.observable();
    self.location = ko.observable();
    self.client = ko.observable();

    self.images = ko.observableArray([]);
    self.images_id = ko.observableArray([]);
    self.terms = ko.observableArray([]);

    self.init = function () {
        if (window.location.toString().indexOf('/edit/') >= 0) {
            self.id(data.id);
            self.name(data.name);
            self.content(data.content);
            self.client(data.client);
            self.location(data.location);
            self.images(data.images);
            self.images_id(data.images_id);
            self.terms(data.terms);
        }
    };

    //#region Ảnh/upload
    self.images_preview = function (data, event) {
        if (event.target.files.length === 0)
            return;

        if (event.target.files.length > 8) {
            ALERT.error('Bạn chỉ được chọn tối đa 8 ảnh');
            return;
        }

        let formData = new FormData();
        _.each(event.target.files, function (file) {
            if (file.type == "image/jpeg" || file.type == "image/jpg" || file.type == "image/png") {
                if (file.size < 2048 * 1024)
                    formData.append('files[]', file);
            }
        });

        AJAX.upload(URL.upload_image, formData, true, (res) => {
            ko.utils.arrayPushAll(self.images, res.data);
            ko.utils.arrayPushAll(self.images_id, res.id);
        });
    };

    self.delete_image = function (item) {
        self.images_id.remove(item.id);
        self.images.remove(item);
    };
    //#endregion

    self.save = function () {
        if (!self.isValid())
            self.errors.showAllMessages();
        else {
            let data = {
                id: self.id(),
                name: self.name(),
                content: self.content(),
                location: self.location(),
                client: self.client(),
                terms: self.terms()
            };

            if (self.images_id().length > 0)
                data.images_id = self.images_id();

            AJAX.post(window.location, data, true, (res) => {
                if (res.error)
                    ALERT.error(res.error);
                else {
                    window.location = '/admin/project';
                }
            });
        }
    }
}

let model = new viewModel();
model.init();
ko.validatedObservable(model);
ko.applyBindings(model, document.getElementById('main-content'));