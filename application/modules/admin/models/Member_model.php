<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class content/Member_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property Option_model $option_model
 * @property CI_Config $config
 */
class Member_model extends \CI_Model
{
    /**
     * @param int $id
     * @param string $name
     * @param string $position
     * @param array $data
     * @return int
     */
    public function insert(int $id, string $name, string $position, array $data)
    {
        $data['name'] = $name;
        $data['position'] = $position;

        if ($id == 0) {
            $data['slug'] = $this->create_slug($id, url_title($name, '-', true));
            $this->db->insert('members', $data);
            $id = $this->db->insert_id();
        } else {
            $this->db->where('id', $id);
            $this->db->update('members', $data);
        }

        return $id;
    }

    public function get_by_id(int $id)
    {
        $this->db->where('id', $id);
        return $this->db->get('members')->row();
    }

    public function get(int $page = 1)
    {
        $this->db->limit(PAGING_SIZE);
        $this->db->offset(($page - 1) * PAGING_SIZE);
        $this->db->order_by('name', 'asc');
        return $this->db->get('members')->result();
    }

    public function count()
    {
        return $this->db->count_all_results('members');
    }

    public function delete(int $id)
    {
        $this->db->where('id', $id);
        $this->db->delete('members');
    }

    private function create_slug(int $id, string $slug)
    {
        $count = 0;
        $_slug = $slug;             // Create temp name
        while (true) {
            $this->db->where('id !=', $id);
            $this->db->where('slug', $_slug);
            if ($this->db->count_all_results('members') == 0) break;
            $_slug = $slug . '-' . (++$count);  // Recreate new temp name
        }
        return $_slug;      // Return temp name
    }
}