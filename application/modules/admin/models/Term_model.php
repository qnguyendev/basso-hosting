<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Term_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_Config $config
 */
class Term_model extends CI_Model
{
    public function insert($id, $data)
    {
        if ($id == 0) {
            $data['created_time'] = time();
            $this->db->insert('terms', $data);
        } else {
            $this->db->where('id', $id);
            $this->db->update('terms', $data);
        }
    }

    public function get_sitemap($type = null, $key = null)
    {
        global $theme_config;
        $system_terms = array_filter($theme_config['term'], function ($item) {
            return $item['active'];
        });

        $this->db->select('id, name, slug, active, order, type, seo_index');
        $this->db->from('terms');
        $this->db->where_in('type', array_column($system_terms, 'id'));

        if ($type != null) {
            if (!is_array($type))
                $this->db->where('type !=', $type);
            else
                $this->db->where_in('type', $type);
        }

        if ($key != null)
            $this->db->like('name', $key, 'both');

        $this->db->order_by('order', 'asc');
        return $this->db->get()->result();
    }

    public function get_by_id($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('terms')->row();
    }

    public function get_by_slug($slug)
    {
        $this->db->where('slug', $slug);
        return $this->db->get('terms')->result();
    }

    public function create_slug($id, $slug)
    {
        $count = 0;
        $_slug = $slug;             // Create temp name
        while (true) {
            $this->db->where('id !=', $id);
            $this->db->where('slug', $_slug);
            if ($this->db->count_all_results('terms') == 0) break;
            $_slug = $slug . '-' . (++$count);  // Recreate new temp name
        }
        return $_slug;      // Return temp name
    }

    public function get($page = 0, $type = null, $key = null)
    {
        global $theme_config;
        $system_terms = array_filter($theme_config['term'], function ($item) {
            return $item['active'];
        });

        $this->db->select('id, name, slug, active, order, image_path, parent_id, type, seo_index');
        $this->db->from('terms');
        $this->db->where_in('type', array_column($system_terms, 'id'));

        if ($type != null) {
            if (!is_array($type))
                $this->db->where('type', $type);
            else
                $this->db->where_in('type', $type);
        }

        if ($key != null)
            $this->db->like('name', $key, 'both');

        if ($page >= 1) {
            $this->db->offset(($page - 1) * PAGING_SIZE);
            $this->db->limit(PAGING_SIZE);
        }

        $this->db->order_by('order', 'asc');
        return $this->db->get()->result();
    }

    public function count($type = null, $key = null)
    {
        $this->db->from('terms');
        if ($type != null) {
            if (!is_array($type))
                $this->db->where('type', $type);
            else
                $this->db->where_in('type', $type);
        }

        if ($key != null)
            $this->db->like('name', $key, 'both');

        return $this->db->count_all_results();
    }

    public function change_active($id, $active)
    {
        $this->db->where('id', $id);
        $this->db->update('terms', ['active' => $active]);
    }

    public function change_order($id, $order)
    {
        $this->db->where('id', $id);
        $this->db->update('terms', ['order' => $order]);
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('terms');
    }
}
