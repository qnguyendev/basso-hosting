<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Article_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 */
class Article_model extends CI_Model
{
    public function insert($user_id, $id, $data, $terms = null)
    {
        if ($id == 0) {
            $data['author_id'] = $user_id;
            $this->db->insert('articles', $data);
            $id = $this->db->insert_id();
        } else {
            $data['updated_time'] = time();
            $this->db->where('id', $id);
            $this->db->update('articles', $data);
        }

        $this->db->where('article_id', $id);
        $this->db->delete('articles_terms');

        if ($terms != null) {
            if (is_array($terms)) {
                $art_terms = array();
                foreach ($terms as $t)
                    array_push($art_terms, array('article_id' => $id, 'term_id' => $t));

                $this->db->insert_batch('articles_terms', $art_terms);
            }
        }
    }

    public function get_terms($id, $full_info = false)
    {
        if ($full_info) {
            $this->db->select('d.name, d.id');
            $this->db->from('articles_terms t');
            $this->db->join('terms d', 'd.id = t.term_id', 'left');
            $this->db->where('t.article_id', $id);

            return $this->db->get()->result();
        }

        $this->db->where('article_id', $id);
        return array_column($this->db->get('articles_terms')->result(), 'term_id');
    }

    public function get_by_slug($slug)
    {
        $this->db->where('slug', $slug);
        return $this->db->get('articles')->row();
    }

    public function create_slug($id, $slug)
    {
        $count = 0;
        $_slug = $slug;             // Create temp name
        while (true) {
            $this->db->where('id !=', $id);
            $this->db->where('slug', $_slug);
            if ($this->db->count_all_results('articles') == 0) break;
            $_slug = $slug . '-' . (++$count);  // Recreate new temp name
        }
        return $_slug;      // Return temp name
    }

    public function get_by_id($id)
    {
        if (!is_array($id)) {
            $this->db->where('id', $id);
            return $this->db->get('articles')->row();
        }

        $this->db->where_in('id', $id);
        return $this->db->get('articles')->result();
    }

    /**
     * Thay đổi trạng thái ẩn/hiện bài viết
     * @param $id
     * @param $active
     */
    public function change_active($id, $active)
    {
        if (!is_array($id))
            $this->db->where('id', $id);
        else
            $this->db->where_in('id', $id);
        $this->db->update('articles', array('active' => $active));
    }

    /**
     * Thay đổi bài nội bật
     * @param $id
     * @param $featured
     */
    public function change_featured($id, $featured)
    {
        if (!is_array($id))
            $this->db->where('id', $id);
        else
            $this->db->where_in('id', $id);
        $this->db->update('articles', array('featured' => $featured));
    }

    /**
     * Xóa bài viết
     * @param $id
     */
    public function delete($id)
    {
        if (!is_array($id))
            $this->db->where('id', $id);
        else
            $this->db->where_in('id', $id);
        $this->db->delete('articles');
    }

    public function get($page = 1, $key = null, $term_id = 0, $excerpt_id = null)
    {
        $this->db->select('t.id, t.image_path, t.name, t.slug, t.active, t.views');
        $this->db->select('t.featured, t.created_time, GROUP_CONCAT(" ", o.name) as terms');
        $this->db->from('articles t');

        if ($key != null)
            $this->db->like('t.name', $key, 'both');

        if ($excerpt_id != null) {
            if (is_array($excerpt_id))
                $this->db->where_not_in('t.id', $excerpt_id);
            else
                $this->db->where('t.id !=', $excerpt_id);
        }

        $this->db->join('articles_terms d', 'd.article_id = t.id', 'left');
        $this->db->join('terms o', 'd.term_id = o.id', 'left');

        if ($term_id > 0)
            $this->db->where('d.term_id', $term_id);

        $this->db->offset(($page - 1) * PAGING_SIZE);
        $this->db->limit(PAGING_SIZE);
        $this->db->order_by('t.created_time', 'desc');
        $this->db->group_by('t.id');

        return $this->db->get()->result();
    }

    public function count($key = null, $term_id = 0, $excerpt_id = null)
    {
        $this->db->from('articles t');
        $this->db->join('users o', 't.author_id = o.id', 'left');

        if ($key != null)
            $this->db->like('t.name', $key, 'both');

        if ($excerpt_id != null) {
            if (is_array($excerpt_id))
                $this->db->where_not_in('t.id', $excerpt_id);
            else
                $this->db->where('t.id !=', $excerpt_id);
        }

        if ($term_id > 0) {
            $this->db->join('articles_terms d', 'd.article_id = t.id', 'left');
            $this->db->where('d.term_id', $term_id);
        }

        return $this->db->count_all_results();
    }
}