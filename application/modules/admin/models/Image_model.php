<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Image_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 */
class Image_model extends CI_Model
{
    public function insert($path)
    {
        $data = array(
            'created_time' => time(),
            'active' => 0,
            'path' => $path
        );

        $this->db->insert('images', $data);
        return $this->db->insert_id();
    }

    public function get($id)
    {
        if (!is_array($id)) {
            $this->db->where('id', $id);
            return $this->db->get('images')->row();
        }

        $this->db->where_in('id', $id);
        return $this->db->get('images')->result();
    }

    public function get_older($time, $active = 0)
    {
        $this->db->where('created_time <=', $time);
        $this->db->where('active', $active);

        return $this->db->get('images')->result();
    }

    public function set_active($id, $active = 1)
    {
        if (!is_array($id))
            $this->db->where('id', $id);
        else
            $this->db->where_in('id', $id);

        $this->db->update('images', array('active' => $active));
    }

    public function delete($id)
    {
        if (!is_array($id))
            $this->db->where('id', $id);
        else
            $this->db->where_in('id', $id);

        $this->db->delete('images');
    }
}