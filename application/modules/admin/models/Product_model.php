<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Product_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 */
class Product_model extends CI_Model
{
    public function get($page = 1, $filter = null, $excerpt_id = null)
    {
        $select = 't.id, t.name, t.price, t.sale_price, t.sale_from, t.sale_to, t.sku, GROUP_CONCAT(d.name) as tags, 
            (select path from products_images where product_id = t.id limit 1) as image, t.available';
        $this->db->select($select);
        $this->db->from('products t');
        $this->db->join('products_tags o', 't.id = o.product_id', 'left');
        $this->db->join('tags d', 'd.id = o.tag_id', 'left');

        if ($filter != null)
            $this->db->where("t.sku = '$filter' or t.name like '%$filter%'");

        if ($excerpt_id != null) {
            if (is_array($excerpt_id))
                $this->db->where_not_in('t.id', $excerpt_id);
            else
                $this->db->where('t.id !=', $excerpt_id);
        }


        $this->db->order_by('created_time', 'desc');
        $this->db->group_by('t.id');
        $this->db->offset(($page - 1) * $this->config->item('result_per_page'));
        $this->db->limit($this->config->item('result_per_page'));

        return $this->db->get()->result();
    }

    public function count($filter = null, $excerpt_id = null)
    {
        if ($filter != null)
            $this->db->where("sku = '$filter' or name like '%$filter%'");

        if ($excerpt_id != null) {
            if (is_array($excerpt_id))
                $this->db->where_not_in('id', $excerpt_id);
            else
                $this->db->where('id !=', $excerpt_id);
        }
        return $this->db->count_all_results('products');
    }

    /**
     * Lấy sản phẩm theo ID
     * @param $id
     * @return mixed
     */
    public function get_by_id($id)
    {
        if (!is_array($id)) {
            $this->db->where('id', $id);
            return $this->db->get('products')->row();
        }

        $select = 't.id, t.name, t.price, t.sale_price, t.sale_from, t.sale_to, t.sku, GROUP_CONCAT(d.name) as tags, 
            (select path from products_images where product_id = t.id limit 1) as image, t.available';
        $this->db->select($select);
        $this->db->from('products t');
        $this->db->join('products_tags o', 't.id = o.product_id', 'left');
        $this->db->join('tags d', 'd.id = o.tag_id', 'left');
        $this->db->where_in('t.id', $id);

        return $this->db->get()->result();
    }

    /**
     * Lấy sản phẩm theo mã SKU
     * @param $sku
     * @return mixed
     */
    public function get_by_sku($sku)
    {
        $sku = strtoupper($sku);
        $this->db->where('sku', $sku);
        return $this->db->get('products')->row();
    }

    /**
     * Thêm mới/cập nhật sản  phẩm
     * @param $id
     * @param $data
     * @param null $terms_id
     * @param null $images
     * @param null $tags_id
     * @param null $attributes
     * @param null $related_products
     * @param null $related_articles
     * @return bool|int
     */
    public function insert($id, $data, $terms_id = null, $images = null, $tags_id = null, $attributes = null,
                           $related_products = null, $related_articles = null)
    {
        $this->db->trans_begin();
        if ($id == 0) {
            $data['created_time'] = time();
            $this->db->insert('products', $data);
            $id = $this->db->insert_id();
        } else {
            $data['updated_time'] = time();
            $this->db->where('id', $id);
            $this->db->update('products', $data);
        }

        if ($this->db->trans_status()) {
            if ($this->_insert_images($id, $images)) {
                if ($this->_insert_attributes($id, $attributes)) {
                    if ($this->_insert_terms($id, $terms_id)) {
                        if ($this->_insert_tags($id, $tags_id)) {
                            if ($this->_insert_related_articles($id, $related_articles)) {
                                if ($this->_insert_related_products($id, $related_products)) {
                                    $this->db->trans_commit();
                                    return $id;
                                }
                            }
                        }
                    }
                }
            }
        }

        $this->db->trans_rollback();
        return false;
    }

    private function _insert_images(int $product_id, $images)
    {
        $this->db->trans_begin();
        $this->db->where('product_id', $product_id);
        $this->db->delete('products_images');

        if ($this->db->trans_status()) {
            if ($images != null) {
                if (is_array($images)) {
                    if (count($images) > 0) {
                        for ($i = 0; $i < count($images); $i++) {
                            $images[$i]['product_id'] = $product_id;
                        }

                        $this->db->insert_batch('products_images', $images);
                        if ($this->db->trans_status()) {
                            $this->db->trans_commit();
                            return true;
                        }

                        $this->db->trans_rollback();
                        return false;
                    }
                }
            }

            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    private function _insert_terms(int $product_id, $terms_id)
    {
        $this->db->trans_begin();
        $this->db->where('product_id', $product_id);
        $this->db->delete('products_terms');

        if ($this->db->trans_status()) {
            if ($terms_id != null) {
                if (is_array($terms_id)) {
                    if (count($terms_id) > 0) {
                        $terms = [];
                        foreach ($terms_id as $term)
                            array_push($terms, ['term_id' => $term, 'product_id' => $product_id]);

                        $this->db->insert_batch('products_terms', $terms);
                        if ($this->db->trans_status()) {
                            $this->db->trans_commit();
                            return true;
                        }

                        $this->db->trans_rollback();
                        return false;
                    }
                }
            }

            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    private function _insert_tags(int $product_id, $tags_id)
    {
        $this->db->trans_begin();
        $this->db->where('product_id', $product_id);
        $this->db->delete('products_tags');

        if ($this->db->trans_status()) {
            if ($tags_id != null) {
                if (is_array($tags_id)) {
                    if (count($tags_id) > 0) {
                        $insert = [];
                        foreach ($tags_id as $tag_id)
                            array_push($insert, ['product_id' => $product_id, 'tag_id' => $tag_id]);

                        $this->db->insert_batch('products_tags', $insert);
                        if ($this->db->trans_status()) {
                            $this->db->trans_commit();
                            return true;
                        }

                        $this->db->trans_rollback();
                        return false;
                    }
                }
            }

            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    private function _insert_attributes(int $product_id, $attributes)
    {
        $this->db->trans_begin();
        $this->db->where('product_id', $product_id);
        $this->db->delete('products_attribute_values');

        if ($this->db->trans_status()) {
            if ($attributes != null) {
                if (is_array($attributes)) {
                    $data = [];
                    $this->db->select('attribute_id, id');
                    $this->db->where_in('id', $attributes);
                    $attributes = $this->db->get('attribute_values')->result();

                    foreach ($attributes as $item) {
                        array_push($data,
                            [
                                'product_id' => $product_id,
                                'attribute_id' => $item->attribute_id,
                                'attribute_value_id' => $item->id
                            ]);
                    }

                    if (count($data) > 0) {
                        $this->db->insert_batch('products_attribute_values', $data);
                        if ($this->db->trans_status()) {
                            $this->db->trans_commit();
                            return true;
                        }

                        $this->db->trans_rollback();
                        return false;
                    }
                }
            }

            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    private function _insert_related_products(int $product_id, $related)
    {
        $this->db->trans_begin();
        $this->db->where('product_id', $product_id);
        $this->db->delete('products_upsell');
        if ($this->db->trans_status()) {
            if (isset($related)) {
                if (is_array($related)) {
                    $data = [];
                    foreach ($related as $id)
                        array_push($data, ['product_id' => $product_id, 'related_id' => $id]);

                    $this->db->insert_batch('products_upsell', $data);
                    if ($this->db->trans_status()) {
                        $this->db->trans_commit();
                        return true;
                    }

                    $this->db->trans_rollback();
                    return false;
                }
            }

            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    private function _insert_related_articles(int $product_id, $related)
    {
        $this->db->trans_begin();
        $this->db->where('product_id', $product_id);
        $this->db->delete('products_articles');
        if ($this->db->trans_status()) {
            if (isset($related)) {
                if (is_array($related)) {
                    $data = [];
                    foreach ($related as $id)
                        array_push($data, ['product_id' => $product_id, 'article_id' => $id]);

                    $this->db->insert_batch('products_articles', $data);
                    if ($this->db->trans_status()) {
                        $this->db->trans_commit();
                        return true;
                    }

                    $this->db->trans_rollback();
                    return false;
                }
            }

            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    /**
     * Lấy sản phẩm theo slug
     * @param $slug
     * @return array
     */
    public function get_by_slug($slug)
    {
        $this->db->select('id, slug');
        $this->db->where('slug', $slug);
        return $this->db->get('products')->row();
    }

    /**
     * Tạo slug duy nhất cho sản phẩm
     * @param $id
     * @param $slug
     * @return string
     */
    public function create_slug($id, $slug)
    {
        $count = 0;
        $_slug = $slug;             // Create temp name
        while (true) {
            $this->db->where('id !=', $id);
            $this->db->where('slug', $_slug);
            if ($this->db->count_all_results('products') == 0) break;
            $_slug = $slug . '-' . (++$count);  // Recreate new temp name
        }
        return $_slug;      // Return temp name
    }

    /**
     * Lấy ảnh sản phẩm
     * @param $id
     * @return array
     */
    public function get_images($id)
    {
        $this->db->select('image_id as id, path');
        $this->db->where('product_id', $id);
        return $this->db->get('products_images')->result();
    }

    /**
     * Lấy tags theo id sản phẩm
     * @param $id
     * @return array
     */
    public function get_tags($id)
    {
        $this->db->select('o.*');
        $this->db->from('products_tags t');
        $this->db->join('tags o', 't.tag_id = o.id', 'right');
        $this->db->where('t.product_id', $id);
        return $this->db->get()->result();
    }

    /**
     * Lấy ID danh mục của sản phẩm
     * @param $id
     * @return array
     */
    public function get_terms($id)
    {
        $this->db->where('product_id', $id);
        return $this->db->get('products_terms')->result();
    }

    /**
     * Xóa sản phẩm
     * @param $id
     */
    public function delete($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('products');
    }

    /**
     * Danh sách thuộc tính sản phẩm
     * @param $product_id
     * @return array
     */
    public function get_attributes($product_id)
    {
        $this->db->where('product_id', $product_id);
        return $this->db->get('products_attribute_values')->result();
    }

    public function get_upsell_product(int $product_id)
    {
        $select = 't.id, t.name, t.price, t.sale_price, t.sale_from, t.sale_to, t.sku,
            (select path from products_images where product_id = t.id limit 1) as image';
        $this->db->select($select);
        $this->db->from('products t');
        $this->db->join('products_upsell o', 't.id = o.related_id', 'left');
        $this->db->where_in('o.product_id', $product_id);
        $this->db->group_by('t.id');
        return $this->db->get()->result();
    }

    public function get_related_articles(int $product_id)
    {
        $this->db->select('t.*');
        $this->db->from('articles t');
        $this->db->join('products_articles o', 't.id = o.article_id', 'left');
        $this->db->where('o.product_id', $product_id);
        $this->db->group_by('t.id');
        return $this->db->get()->result();
    }
}