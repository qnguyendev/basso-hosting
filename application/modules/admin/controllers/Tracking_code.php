<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Tracking_code
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Tracking_model $tracking_model
 * @property Term_model $term_model
 * @property CI_Form_validation $form_validation
 */
class Tracking_code extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('tracking_model');
        $this->load->model('term_model');
		$this->load->model('basso/contact_form_model');
    }

    public function index()
    {
        $breadcrumb = [['text' => 'Quản lý mã javascript']];
        return $this->blade
            ->set('breadcrumbs', $breadcrumb)
            ->render();
    }

    public function GET_index()
    {
        $page = $this->input->get('page');
        $pagination = Pagination::calc($this->tracking_model->count(), $page, $this->config->item('result_per_page'));
        $result = $this->tracking_model->get($page);
        json_success(null, ['data' => $result, 'pagination' => $pagination]);
    }

    public function DELETE_index()
    {
        $this->form_validation->set_data($this->input->input_stream());
        $this->form_validation->set_rules('id', null, 'required|numeric');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $id = $this->input->input_stream('id');
        $code = $this->tracking_model->get_by_id($id);
        if ($code == null)
            json_error('Không tìm thấy mã javascrip');

        $this->tracking_model->delete($id);
        json_success('Xóa mã javascript thành công');
    }

    public function create()
    {
        $breadcrumb = [['text' => 'Quản lý mã javascript', 'url' => base_url('admin/tracking_code')],
            ['text' => 'Thêm mã javascript']];
        $terms = $this->term_model->get(0, ['post', 'product', 'page', 'Dashboard']);

        return $this->blade
            ->set('terms', $terms)
            ->set('breadcrumbs', $breadcrumb)
            ->render('editor');
    }

    public function edit($id)
    {
        $code = $this->tracking_model->get_by_id($id);
        if ($code == null) {
            $this->session->set_flashdata('error', 'Không tìm thấy mã javascript');
            return redirect(base_url('admin/tracking_code'));
        }

        $breadcrumb = [['text' => 'Quản lý mã javascript', 'url' => base_url('admin/tracking_code')],
            ['text' => 'Cập nhật mã javascript']];

        $terms = $this->term_model->get(0, ['post', 'product', 'page', 'Dashboard']);
        return $this->blade
            ->set('terms', $terms)
            ->set('breadcrumbs', $breadcrumb)
            ->render('editor');
    }

    public function GET_edit($id)
    {
        $code = $this->tracking_model->get_by_id($id);
        if ($code == null) {
            $this->session->set_flashdata('error', 'Không tìm thấy mã javascript');
            json_error('Không tìm thấy mã javascript', ['url' => base_url('admin/tracking_code')]);
        }

        $code->page = $this->tracking_model->get_pages($id);
        if ($code->note != null) {
            if ($code->note == 'home')
                array_push($code->page, -1);
        }

        json_success(null, ['data' => $code]);
    }

    public function POST_save()
    {
        $this->form_validation->set_rules('id', null, 'required|numeric');
        $this->form_validation->set_rules('active', null, 'required|numeric');
        $this->form_validation->set_rules('position', null, 'required');
        $this->form_validation->set_rules('code', null, 'required');
        $this->form_validation->set_rules('name', null, 'required');
        if (!$this->form_validation->run())
            json_error('Vui lòng nhập đủ thông tin yêu cầu');

        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $code = $this->input->post('code', false);
        $active = $this->input->post('active');
        $position = $this->input->post('position');
        $page = $this->input->post('page');

        $this->tracking_model->insert($id, $name, $active, $code, $position, $page);
        $this->session->set_flashdata('success', $id == 0
            ? 'Thêm mã javascript thành công'
            : 'Cập nhật thành công');
        json_success(null);
    }
}