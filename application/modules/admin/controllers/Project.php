<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Project
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Term_model $term_model
 * @property Project_model $project_model
 * @property Image_model $image_model
 */
class Project extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('term_model');
        $this->load->model('image_model');
        $this->load->model('admin/project_model');
    }

    public function index()
    {
        $breadcrumbs = [['text' => 'Danh sách dự án']];
        return $this->blade
            ->set('breadcrumbs', $breadcrumbs)
            ->render();
    }

    public function GET_index()
    {
        $page = $this->input->get('page');
        if (!is_numeric($page))
            $page = 1;
        $page = $page < 1 ? 1 : $page;
        $result = $this->project_model->get($page);
        $pagination = Pagination::calc($this->project_model->count(), $page, PAGING_SIZE);
        json_success(null, ['data' => $result, 'pagination' => $pagination]);
    }

    public function DELETE_index()
    {
        $id = $this->input->input_stream('id');
        if ($id == null)
            json_error('Không tìm thấy ID dự án');

        $project = $this->project_model->get_by_id(intval($id));
        if ($project == null)
            json_error('Không tìm thấy dự án');

        $this->project_model->delete($id);

        $images = $this->project_model->get_images($id);
        if (count($images) > 0)
            $this->image_model->set_active(array_column($images, 'id'), 0);

        success_msg('Xóa dự án thành công');
        json_success('Xóa dự án thành công');
    }

    public function create()
    {
        $breadcrumbs = [
            ['text' => 'Danh sách dự án', 'url' => base_url('admin/project')],
            ['text' => 'Thêm dự án']
        ];

        $terms = $this->term_model->get(0, 'project');
        return $this->blade
            ->set('terms', $terms)
            ->set('breadcrumbs', $breadcrumbs)
            ->render('edit');
    }

    public function edit($id)
    {
        if (!isset($id)) {
            error_msg('Đường dẫn không tồn tại');
            return redirect(base_url('admin/project'));
        }

        $project = $this->project_model->get_by_id($id);
        if ($project == null) {
            error_msg('Không tìm thấy dự án');
            return redirect(base_url('admin/project'));
        }

        $breadcrumbs = [
            ['text' => 'Danh sách dự án', 'url' => base_url('admin/project')],
            ['text' => 'Cập nhật dự án']
        ];

        $terms = $this->term_model->get(0, 'project');
        $project->images = $this->project_model->get_images($id);
        $project->images_id = array_column($project->images, 'id');
        $project->terms = array_column($this->project_model->get_terms($id), 'term_id');

        return $this->blade
            ->set('terms', $terms)
            ->set('project', $project)
            ->set('breadcrumbs', $breadcrumbs)
            ->render();
    }

    public function POST_save()
    {
        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $content = $this->input->post('content');
        $location = $this->input->post('location');
        $client = $this->input->post('client');
        $terms = $this->input->post('terms');
        $images_id = $this->input->post('images_id');

        if ($id == null || $name == null)
            json_error('Vui lòng nhập đủ thông tin');

        //  Set các ảnh cũ chưa active
        if ($id > 0) {
            $images = $this->project_model->get_images($id);
            if (count($images) > 0)
                $this->image_model->set_active(array_column($images, 'id'), 0);
        }

        //  Lấy ảnh mới
        $images = [];
        if ($images_id != null) {
            if (is_array($images_id)) {
                if (count($images_id) > 0) {

                    $this->image_model->set_active($images_id);     //  Đánh dấu ảnh được sử dụng
                    $img = $this->image_model->get($images_id);     //  Lấy thông tin ảnh

                    $images = array();
                    foreach ($img as $item) {
                        array_push($images,
                            [
                                'project_id' => null,
                                'image_id' => $item->id,
                                'image_path' => $item->path
                            ]);
                    }
                }
            }
        }

        $data = [
            'content' => $content,
            'location' => $location,
            'client' => $client
        ];

        $this->project_model->insert($id, $name, $data, $images, $terms);

        success_msg($id == 0 ? 'Thêm dự án thành công' : 'Cập nhật dự án thành công');
        json_success($id == 0 ? 'Thêm dự án thành công' : 'Cập nhật dự án thành công');
        #endregion
    }
}