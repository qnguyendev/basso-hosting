<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Widget
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Option_model $option_model
 * @property Widget_model $widget_model
 * @property Image_model $image_model
 * @property Term_model $term_model
 * @property CI_Form_validation $form_validation
 */
class Widget extends Cpanel_Controller
{
    private $widget_positions;
    private $widget_type;

    public function __construct()
    {
        parent::__construct();
		$this->load->model('basso/contact_form_model');
        require './theme/cpanel.php';
        if (!isset($widget))
            $widget = [];

        $this->widget_type = $widget['type'];
        $this->widget_positions = $widget['position'];

        $this->widget_type = array_filter($this->widget_type, function ($item) {
            return $item['active'];
        });

        $this->load->helper('file');
        $this->load->model('widget_model');
        $this->load->model('term_model');
        
    }

    public function index()
    {
        $breadcrumb = [['text' => 'Quản lý widget']];
        return $this->blade
            ->set('positions', $this->widget_positions)
            ->set('types', $this->widget_type)
            ->set('breadcrumbs', $breadcrumb)
            ->render();
    }

    public function GET_index()
    {
        $page = $this->input->get('page');
        $position = $this->input->get('position');
        $type = $this->input->get('type');
        $active = $this->input->get('active');
        $key = $this->input->get('key');

        $widgets = $this->widget_model->get($page, $position, $type, $active, $key);
        $total_result = $this->widget_model->count($position, $type, $active, $key);


        for ($i = 0; $i < count($widgets); $i++) {
            $t = $widgets[$i];

            $search_result = array_filter($this->widget_positions, function ($item) use ($t) {
                return in_array($item['id'], explode(',', $t->area));
            });

            $widgets[$i]->area = join(', ', array_column($search_result, 'name'));

            foreach ($this->widget_type as $item) {
                if ($item['id'] == $widgets[$i]->type) {
                    $widgets[$i]->type = $item['name'];
                    break;
                }
            }
        }

        json_success(null, ['data' => $widgets, 'pagination' => Pagination::calc($total_result, $page)]);
    }

    public function DELETE_index()
    {
        $id = $this->input->input_stream('id');
        $widget = $this->widget_model->get_by_id($id);
        if ($widget == null)
            json_error('Không tìm thấy widget');

        //  Nếu widget là ảnh
        if ($widget->type == 'photos') {
            $data = json_decode($widget->options);
            $image_id = array_column($data, 'image_id');
            $this->image_model->set_active($image_id, 0);
        }

        $this->widget_model->delete($id);
        json_success('Xóa widget thành công');
    }

    public function create()
    {
        $breadcrumb = [['text' => 'Quản lý widget', 'url' => '/admin/widget'],
            ['text' => 'Thêm widget']];

        $widget_type = get_cookie('widget_type') == null ? 'text' : get_cookie('widget_type');
        if ($widget_type == 'articles')
            $this->blade->set('terms', $this->term_model->get(0, 'post'));

        //  Lấy view tương ứng widget
        $widget_view = 'text';
        foreach ($this->widget_type as $item) {
            if ($item['id'] == $widget_type) {
                $widget_view = $item['view'];
                break;
            }
        }

        $this->blade->set('image_uploader', true);

        return $this->blade
            ->set('positions', $this->widget_positions)
            ->set('system_widgets', $this->widget_type)
            ->set('widget_view', $widget_view)
            ->set('breadcrumbs', $breadcrumb)->render('edit');
    }

    public function edit($id)
    {
        $widget = $this->widget_model->get_by_id($id);
        if ($widget == null) {
            $this->session->set_flashdata('error', 'Không tìm thấy widget');
            return redirect('/admin/widget');
        }

        $breadcrumb = [['text' => 'Quản lý widget', 'url' => '/admin/widget'],
            ['text' => 'Cập nhật widget']];

        if ($widget->options != null)
            $widget->options = json_decode($widget->options, false);
        else $widget->options = [];
        $areas = array_column($this->widget_model->get_area($id), 'area_id');

        if ($widget->type == 'articles')
            $this->blade->set('terms', $this->term_model->get(0, 'post'));

        //  Lấy view tương ứng widget
        $widget_view = 'text';
        foreach ($this->widget_type as $item) {
            if ($item['id'] == $widget->type) {
                $widget_view = $item['view'];
                break;
            }
        }

        $this->blade->set('image_uploader', true);
        return $this->blade
            ->set('positions', $this->widget_positions)
            ->set('system_widgets', $this->widget_type)
            ->set('widget', $widget)
            ->set('widget_view', $widget_view)
            ->set('area', $areas)
            ->set('breadcrumbs', $breadcrumb)->render();
    }

    public function POST_save()
    {
        $set_type = $this->input->post('set_type');
        if ($set_type != null) {
            set_cookie('widget_type', $set_type, 3600, null, '/admin');
            die;
        }

        $this->form_validation->set_rules('id', null, 'numeric|required');
        $this->form_validation->set_rules('title', null, 'required');
        $this->form_validation->set_rules('show_title', null, 'required');
        $this->form_validation->set_rules('order', null, 'required');
        if (!$this->form_validation->run())
            json_error('Vui lòng nhập đủ thông tin');

        $id = $this->input->post('id');
        $title = $this->input->post('title');
        $show_title = $this->input->post('show_title');
        $order = $this->input->post('order');
        $area = $this->input->post('area');
        $type = $this->input->post('type');
        $description = $this->input->post('description', false);
        $active = $this->input->post('active');
        $options = $this->input->post('options', false);

        //  Nếu widget là nhiều ảnh, update các id ảnh sử dụng
        if (in_array($type, ['photos', 'testimonial'])) {
            $this->load->model('image_model');
            $image_id = $this->input->post('image_id');
            $this->image_model->set_active($image_id, 0);
            $image_id = array_column($options, 'image_id');
            $this->image_model->set_active($image_id, 1);
        }

        $this->widget_model->save($id, $type, $title, $show_title, $active,
            $description, $order, $options == null ? null : json_encode($options), $area);
        json_success(null);
    }
}