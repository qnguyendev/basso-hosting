<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Order
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Order_model $order_model
 * @property Order_Label_model $order_label_model
 * @property Product_model $product_model
 * @property Attribute_model $attribute_model
 * @property Payment_model $payment_model
 * @property Common_model $common_model
 */
class Order extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('order_model');
        $this->load->model('order_label_model');
        $this->load->model('payment_model');
        $this->load->model('product_model');
        $this->load->model('attribute_model');
        $this->load->helper('truemoney');
        $this->load->helper('status');
    }

    public function index()
    {
        $breadcrumb = [['text' => 'Quản lý đơn hàng']];

        return $this->blade
            ->set('payment_method', $this->payment_model->get_active())
            ->set('cities', $this->common_model->get_cities())
            ->set('breadcrumbs', $breadcrumb)
            ->set('labels', $this->order_label_model->get())
            ->render();
    }

    public function GET_index()
    {
        $key = $this->input->get('filter_key');
        if ($key != null) {
            $products = $this->order_model->search_products($key);
            for ($i = 0; $i < count($products); $i++) {
                if (has_sale_price($products[$i]))
                    $products[$i]->price = $products[$i]->sale_price;

                $products[$i]->attributes = [];

                //  Danh sách thuộc tính của sản phẩm
                $product_attributes = $this->product_model->get_attributes($products[$i]->id);
                $product_attributes_id = array_unique(array_column($product_attributes, 'attribute_id'));
                foreach ($product_attributes_id as $key => $id) {
                    $attribute = $this->attribute_model->get_by_id($id);

                    //  Các giá trị của thuộc tính tương ứng
                    $values = $this->attribute_model->get_values($id, 0);
                    for ($j = 0; $j < count($values); $j++) {
                        unset($values[$j]->attribute_id);
                        unset($values[$j]->value);
                        unset($values[$j]->type);
                    }

                    $item = ['name' => $attribute->name,
                        'values' => $values,
                        'value_id' => null,
                        'attribute_id' => $id
                    ];
                    array_push($products[$i]->attributes, $item);
                }
            }

            json_success(null, ['data' => $products]);
        }

        $page = $this->input->get('page');
        if ($page != null) {
            $page = !is_numeric($page) ? 1 : ($page < 1 ? 1 : $page);
            $time = $this->input->get('time');
            $date = $this->input->get('date');
            $status = $this->input->get('status');
            $key = $this->input->get('key');
            $labels = $this->input->get('labels');
            $start = strtotime($date . ' 00:00:00');
            $end = strtotime($date . ' 23:59:59');

            switch ($time) {
                case 'all':
                    $start = 0;
                    $end = time();
                    break;
                case 'today':
                    $end = time();
                    $start = strtotime(date('Y-m-d 00:00:00'));
                    break;
                case 'yesterday':
                    $end = strtotime(date('Y-m-d 00:00:00')) - 1;
                    $start = $end + 1 - 24 * 3600;
                    break;
                case 'month':
                    $end = time();
                    $start = strtotime(date('Y-m-1 00:00:00'));
                    break;
            }

            $data = $this->order_model->get($page, $start, $end, $status, $key, $labels);
            $total_items = $this->order_model->count($start, $end, $status, $key, $labels);

            for ($i = 0; $i < count($data); $i++)
                $data[$i]->labels = $this->order_model->get_labels($data[$i]->id);

            $result = [
                'data' => $data,
                'pagination' => Pagination::calc($total_items, $page)
            ];

            json_success(null, $result);
        }

        $city_id = $this->input->get('city_id');
        if ($city_id != null)
            json_success(null, ['data' => $this->common_model->get_districts($city_id)]);

        die;
    }

    public function POST_index()
    {
        $products = $this->input->post('products');
        $customer = $this->input->post('customer');
        $discount = $this->input->post('discount');

        if ($products == null || $customer == null || $discount == null)
            json_error('Vui lòng nhập đủ thông tin');

        if (!is_array($products))
            json_error('Vui lòng chọn sản phẩm');

        if (count($products) == 0)
            json_error('Vui lòng chọn sản phẩm');

        $payment = $this->payment_model->get_by_id($customer['payment_id']);
        $this->order_model->insert($customer, $discount, $payment->type, $products);
        json_success('Thêm đơn hàng thành công');
    }

    /**
     * Chi tiết đơn hàng
     * @param $id
     * @return string|void
     */
    public function detail($id)
    {
        if ($id == null)
            return redirect('admin/order');

        if (!is_numeric($id))
            return redirect('admin/order');

        $order = $this->order_model->get_by_id($id);
        if ($order == null) {
            $this->session->set_flashdata('error', 'Đơn hàng không tồn tại');
            return redirect('admin/order');
        }

        $breadcrumb = [['url' => '/admin/order', 'text' => 'Quản lý đơn hàng'],
            ['text' => 'Chi tiết đơn hàng #' . $id]];

        $products = $this->order_model->get_products($id);
        for ($i = 0; $i < count($products); $i++) {
            if (has_sale_price($products[$i]))
                $products[$i]->price = $products[$i]->sale_price;

            #region Lấy thuộc tính sản phẩm
            $products[$i]->attributes = []; //  Chứa thuộc tính của sản phẩm
            $products[$i]->data = [];   //  Chứa thuộc tính sản phẩm khi ấn update

            if ($order->shipping_status == 'waiting') {

                //  Danh sách thuộc tính của sản phẩm
                $product_attributes = $this->product_model->get_attributes($products[$i]->id);
                $product_attributes_id = array_unique(array_column($product_attributes, 'attribute_id'));
                $order_attributes = $this->order_model->get_attributes($products[$i]->order_detail_id);

                foreach ($product_attributes_id as $key => $id) {
                    $attribute = $this->attribute_model->get_by_id($id);

                    //  Các giá trị của thuộc tính tương ứng
                    $values = $this->attribute_model->get_values($id, 0);
                    for ($j = 0; $j < count($values); $j++) {
                        unset($values[$j]->attribute_id);
                        unset($values[$j]->value);
                        unset($values[$j]->type);
                    }

                    //  Giá trị được lựa chọn trong đơn hàng
                    $index = array_search($id, array_column($order_attributes, 'attribute_id'));
                    $item = ['name' => $attribute->name,
                        'values' => $values,
                        'value_id' => isset($order_attributes[$index]) ? $order_attributes[$index]->id : null,
                        'attribute_id' => $id
                    ];

                    array_push($products[$i]->attributes, $item);
                }
            } else {
                $products[$i]->attributes = $this->order_model->get_attributes($order->id, $products[$i]->id);
            }
            #endregion

            unset($products[$i]->sale_price);
            unset($products[$i]->sale_from);
            unset($products[$i]->sale_to);
        }

        $data = ['order' => $order,
            'products' => $products,
            'labels' => $this->order_label_model->get(),
            'labels_id' => $this->order_model->get_labels($id),
            'shipping_methods' => $this->order_model->shipping_method(),
            'cities' => $this->common_model->get_cities()
        ];

        return $this->blade
            ->set('breadcrumbs', $breadcrumb)
            ->render($data);
    }

    public function GET_detail()
    {
        $city_id = $this->input->get('city_id');
        if ($city_id != null)
            json_success(null, ['data' => $this->common_model->get_districts($city_id)]);

        $key = $this->input->get('filter_key');
        if ($key != null) {
            $products = $this->order_model->search_products($key);
            for ($i = 0; $i < count($products); $i++) {
                if (has_sale_price($products[$i]))
                    $products[$i]->price = $products[$i]->sale_price;

                $products[$i]->attributes = [];

                //  Danh sách thuộc tính của sản phẩm
                $product_attributes = $this->product_model->get_attributes($products[$i]->id);
                $product_attributes_id = array_unique(array_column($product_attributes, 'attribute_id'));
                foreach ($product_attributes_id as $key => $id) {
                    $attribute = $this->attribute_model->get_by_id($id);

                    //  Các giá trị của thuộc tính tương ứng
                    $values = $this->attribute_model->get_values($id, 0);
                    for ($j = 0; $j < count($values); $j++) {
                        unset($values[$j]->attribute_id);
                        unset($values[$j]->value);
                        unset($values[$j]->type);
                    }

                    $item = ['name' => $attribute->name,
                        'values' => $values,
                        'value_id' => null,
                        'attribute_id' => $id
                    ];
                    array_push($products[$i]->attributes, $item);
                }
            }

            json_success(null, ['data' => $products]);
        }

        die;
    }

    public function POST_detail($id)
    {
        $products = $this->input->post('products');
        if ($products == null)
            json_error('Vui lòng nhập sản phẩm cho đơn hàng');

        if (count($products) == 0)
            json_error('Vui lòng nhập sản phẩm cho đơn hàng');

        $order = $this->input->post('order');
        $labels_id = $this->input->post('labels_id');

        $data = ['name' => $order['name'],
            'phone' => $order['phone'],
            'email' => $order['email'],
            'address' => $order['address'],
            'district_id' => $order['district_id'],
            'city_id' => $order['city_id'],
            'shipping_status' => $order['shipping_status'],
            'tracking_code' => $order['tracking_code'],
            'shipping_id' => $order['shipping_id'],
            'cancel_note' => $order['cancel_note']];

        $this->order_model->update($id, $data);
        if ($labels_id != null)
            $this->order_model->update_labels($id, $labels_id);

        #region cập nhật chi tiết đơn hàng
        $data = [];
        foreach ($products as $prod) {
            $item = ['product_id' => $prod['id'],
                'order_id' => $id,
                'quantity' => $prod['quantity'],
                'sub_total' => $prod['sub_total'],
                'total' => $prod['sub_total'] + $prod['discount_total'],
                'discount_total' => $prod['discount_total'],
                'data' => $prod['data']
            ];

            //  Kiểm tra sản phẩm đã có trong mảng chưa
            $filter_index = array_search($prod['id'], array_column($data, 'product_id'));
            if (isset($data[$filter_index])) {
                $new_prod = array_column($prod['data'], 'attribute_value_id');
                $exists_prod = array_column($data[$filter_index]['data'], 'attribute_value_id');
                $filter_result = array_intersect($new_prod, $exists_prod);

                //  Nếu đã có sp cùng thuộc tính trong mảng -> tăng số lượng và giá lên
                if (count($filter_result) == count($prod['data'])) {
                    $data[$filter_index]['quantity'] += $prod['quantity'];
                    $data[$filter_index]['sub_total'] += $prod['sub_total'];
                    $data[$filter_index]['total'] += $prod['total'];
                    $data[$filter_index]['discount_total'] += $prod['discount_total'];
                } else
                    array_push($data, $item);
            } else
                array_push($data, $item);
        }

        $this->order_model->update_detail($id, $data);
        #endregion

        if ($this->input->is_ajax_request())
            json_success('Cập nhật đơn hàng thành công');

        return redirect(current_url());
    }

    /**
     * Kiểm tra thanh toán online/xác nhận khách đã chuyển khoản
     */
    public function POST_payment()
    {
        $id = $this->input->post('id');
        $type = $this->input->post('type');

        if ($id != null && $type != null) {
            if (in_array($type, ['confirm_bank_transfer', 'online'])) {
                $order = $this->order_model->get_by_id($id);
                if ($order != null) {
                    switch ($type) {
                        case 'confirm_bank_transfer':
                            $this->order_model->confirm_payment($id, 'success');
                            json_success('Xác nhận chuyển khoản thành công');
                            break;

                        case 'online':
                            $payment = $this->payment_model->get_by_type($this->input->post('opt'));
                            if ($payment->type == 'banking') {
                                $result = check_bank_transaction($order->transaction_id, $payment->app_key,
                                    $payment->app_secret);

                                if ($result->response_code != '00') {
                                    $this->order_model->confirm_payment($id, 'error');
                                    json_error($result->response_message);
                                }

                                $this->order_model->confirm_payment($id, 'success');
                                json_success('Đơn hàng đã được thanh toán');
                            }
                            break;
                    }
                }
                json_error('Không tìm thấy đơn hàng');
            }
            json_error('Thao tác không hợp lệ');
        }
        json_error('Vui lòng nhập đủ thông tin');
    }
}