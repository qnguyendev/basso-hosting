<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

use \DTS\eBaySDK\Shopping\Services;
use \DTS\eBaySDK\Shopping\Types;

/**
 * Class Crawled_product
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Option_model $option_model
 * @property Image_model $image_model
 * @property Crawled_product_model $crawled_product_model
 * @property Amazon_product_model $amazon_product_model
 * @property Order_term_model $order_term_model
 */
class Crawled_product extends Site_Controller
{
	private $_rate_policy;
	private $_rate;
	private $_shipping_fee;
	
    public function __construct()
    {
        parent::__construct();
        $this->load->model('basso/crawled_product_model');
        $this->load->model('basso/amazon_product_model');
        $this->load->model('basso/order_term_model');
        $this->load->model('basso/amazon_crawler_model');
        $this->load->helper('basso/crawler');
		$this->_rate_policy = get_country_policy();
		$this->_rate = $this->_rate_policy['rate']>0? $this->_rate_policy['rate']: 26000;
		$this->_shipping_fee = $this->_rate_policy['shipping_fee']>0? $this->_rate_policy['shipping_fee']: 26000;
    }

    public function index($slug)
    {			
        if (!isset($slug))
            return redirect(base_url());

        if (empty($slug))
            return redirect(base_url());

        $slug = explode('-', $slug);
        $id = count($slug) == 1 ? $slug[0] : end($slug);
        set_head_index(false);

        $product = $this->crawled_product_model->get_by_id($id);
        #region ebay
        if ($product != null) {
            $product->images = json_decode($product->images, true);
            $product->attributes = json_decode($product->attributes, true);
            $product->variations = json_decode($product->variations, true);
            $product->variation_images = json_decode($product->variation_images, true);
            $product->seller_url = "https://www.ebay.com/usr/{$product->seller}";
            $product->categories = json_decode($product->categories, true);
            $product->variations_price = json_decode($product->variations_price, true);
            $cat = [];
            foreach ($product->categories as $c)
                $cat = array_merge($cat, explode(':', $c));

            #region variantions
            $default_variant = [];
            foreach ($product->variations as $key => $value) {
                array_push($default_variant, $value[0]);
            }

            sort($default_variant);
            $default_variant = join('|', $default_variant);
            if (isset($product->variations_price[md5($default_variant)]))
                $product->origin_price = $product->variations_price[md5($default_variant)];
            #endregion

            $product->term_fee = $this->_calc_term_fee($product->origin_price, $cat);
            // $product->price = $product->web_shipping_fee * ($product->origin_price >= 500 ? 26000 : 26000);
            // $product->price += $product->origin_price * ($product->origin_price >= 500 ? 26000 : 26000);
            $product->price = $product->web_shipping_fee * $this->_rate;
            $product->price += $product->origin_price * $this->_rate;
			
			$product->price += $product->term_fee;

            $this->blade->set('website', 'ebay');
            $this->blade->set('product', $product);

            set_head_title($product->title);
            set_head_image($product->images[0]);

            return $this->blade->render();
        }
        #endregion
	
        $product = $this->amazon_product_model->get_parent_product($id);
        if ($product != null) {
			/* $product->title = $this->amazon_crawler_model->_crawl_product_title($id); */
			/* fixcode */
			$pdata = $this->amazon_crawler_model->_get_product_data_from_API($id);
			if($pdata["product_name"] != "") $product->title = $pdata["product_name"];
			/* end fixcode */
			
            $product->variations = json_decode($product->variations);
            $product->color_images = json_decode($product->color_images, true);
            $product->description = json_decode($product->description);

            if ($id != $product->id) {
                $child = $this->amazon_product_model->get_variant($id);
            } else {
                $child = $this->amazon_product_model->get_child($id, 1);
            }

            if ($child->price != null) {
                if ($child->sale_price != null) {
                    if ($child->sale_price > 0)
                        $child->price = $child->sale_price;
                }
				//Tính phí dịch vụ và chuyển giá ra tiền việt.
                $child->origin_price = number_format($child->price, 2, '.', ',');
                $child->shipping_fee = calc_product_shipping_fee_v2($child->weight,$this->_rate_policy);

                $price = calc_product_price_v2($child->price,$this->_rate_policy);
                $term_fee = $this->_calc_term_fee($child->price, json_decode($product->categories, true));
                $child->term_fee = $term_fee;
                $child->price = $price + $term_fee + $child->shipping_fee;
                $child->shipping_fee = $child->shipping_fee;
				/* print_r($child->shipping_fee); */
            }

            $images = json_decode($child->images);
            $child->images = [];
            foreach ($images as $item) {
                array_push($child->images, [
                    'thumbnail' => $item->thumbnail,
                    'large' => $item->large
                ]);
            }

            array_reverse($child->images);

            $att = json_decode($child->attributes, true);
            $child->attributes = [];
            foreach ($att as $key => $val) {
                array_push($child->attributes, [
                    'name' => $key,
                    'value' => $val
                ]);
            }

            $variations = [];
            foreach ($product->variations->list as $key => $value) {
                $item = [
                    'name' => $key,
                    'selected' => null,
                    'values' => []
                ];

                foreach ($product->variations->detail as $variant) {
                    if ($variant->asin == $child->id) {
                        foreach ($variant->value as $val) {
							if(!is_array($val) && is_array($value)) { /* fixcode sometime it not get product from product link */
								if (in_array($val, $value))
									$item['selected'] = $val;
							}
                        }
                    }
                }

                foreach ($value as $k => $v) {
                    $_item = [
                        'value' => $v,
                        'enable' => true,
                        'name' => $key,
                        'image' => null
                    ];

                    if (array_key_exists($v, $product->color_images))
                        $_item['image'] = $product->color_images[$v];

                    array_push($item['values'], $_item);
                }

                if ($item['selected'] == null)
                    $item['selected'] = $item['values'][0]['value'];
                array_push($variations, $item);
            }

            set_head_title($product->title);
            if (count($child->images) > 0) {
                if (isset($child->images[0]['large']))
                    set_head_image($child->images[0]['large']);
            }

            $child->weight += 0.2;
            $this->blade->set('website', 'amazon');
            $this->blade->set('variations', $variations);
            $this->blade->set('product', $product);
            $this->blade->set('child', $child);
            return $this->blade->render();
        }

        set_head_title('Item not found | Basso');
        $this->blade->set('product_id', $id);

        $product_url = 'https://';
        if (is_numeric($id))
            $product_url .= 'ebay.com/itm/product/' . $id;
        else
            $product_url .= 'www.amazon.com/dp/' . $id;

        $this->blade->set('product_url', $product_url);
        return $this->blade->render();
    }

    public function GET_index($slug)
    {
        $item_id = $this->input->get('item_id');
        if ($item_id != null) {
            $variant = $this->input->get('variant');
            $product = $this->crawled_product_model->get_by_id($item_id);
            $product->categories = json_decode($product->categories, true);
            $cat = [];
            foreach ($product->categories as $c)
                $cat = array_merge($cat, explode(':', $c));

            sort($variant);
            $variant = join('|', $variant);
            $product->variations_price = json_decode($product->variations_price, true);
            if (isset($product->variations_price[md5($variant)]))
                $product->origin_price = $product->variations_price[md5($variant)];

            $product->term_fee = $this->_calc_term_fee($product->origin_price, $cat);
            // $product->price = $product->web_shipping_fee * ($product->origin_price >= 500 ? 26000 : 26000);
            // $product->price += $product->origin_price * ($product->origin_price >= 500 ? 26000 : 26000);
			
            $product->price = $product->web_shipping_fee * $this->_rate;
            $product->price += $product->origin_price *  $this->_rate;
			
            $product->price += $product->term_fee;

            json_success(null, [
                'price' => format_money($product->price),
                'origin_price' => '$ ' . round($product->origin_price, 2)
            ]);
        }

        $asin = $this->input->get('asin');
        if ($asin == null)
            json_error(null);

        $parent = $this->amazon_product_model->get_parent_product($asin);
        if ($asin != $parent->id) {
            $child = $this->amazon_product_model->get_variant($asin);
        } else {
            $child = $this->amazon_product_model->get_child($asin, 1);
        }
        if ($child != null) {

            if ($child->sale_price != null) {
                if ($child->sale_price > 0)
                    $child->price = $child->sale_price;
            }

            $images = json_decode($child->images);
            $child->images = [];
            foreach ($images as $item) {
                array_push($child->images, [
                    'thumbnail' => $item->thumbnail,
                    'large' => $item->large
                ]);
            }

            $att = json_decode($child->attributes);
            $child->attributes = [];
            foreach ($att as $key => $val) {
                array_push($child->attributes, [
                    'name' => $key,
                    'value' => $val
                ]);
            }

            $data = [
                'images' => array_reverse($child->images),
                'weight' => round($child->weight, 2) + 0.2,
                'attributes' => $child->attributes
            ];

            if ($child->price != null) {
				/* fixcode */
                /* $shipping_fee = $this->_shipping_fee * (round($child->weight, 2) + 0.2); */		
                $shipping_fee = calc_product_shipping_fee_v2($child->weight,$this->_rate_policy);
				/* end fixcode */
				
                $data['origin_price'] = "$ " . number_format(round($child->price, 2), 2, '.', ',');

                $price = calc_product_price($child->price);
                $term_fee = $this->_calc_term_fee($child->price, json_decode($parent->categories, true));
                $data['price'] = format_money($price + $term_fee + $shipping_fee);
                $data['term_fee'] = $term_fee;
                $data['shipping_fee'] = round($shipping_fee, 2);
                $data['condition'] = $child->condition;
            }
		
            json_success(null, $data);
        }

        json_error(null);
    }

    /**
     * Thêm sp vào giỏ hàng
     * @param $slug
     */
    public function POST_index($slug)
    {
        $this->form_validation->set_rules('quantity', null, 'required|numeric');
        $this->form_validation->set_rules('id', null, 'required');
        $this->form_validation->set_rules('image', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $quantity = $this->input->post('quantity');
        $id = $this->input->post('id');
        $image = $this->input->post('image');
        $term_id = null; $shipping_fee = 0;

        $product = $this->crawled_product_model->get_by_id($id);
        if ($product == null) {
            $product = $this->amazon_product_model->get_variant($id);
            if ($product == null)
                json_error('Không tìm thấy sản phẩm');

            if (isset($product->sale_price)) {
                if ($product->sale_price != null) {
                    if ($product->sale_price > 0)
                        $product->price = $product->sale_price;
                }
            }

            $parent = $this->amazon_product_model->get_parent_product($id);
			
			/* fixcode */
			$pdata = $this->amazon_crawler_model->_get_product_data_from_API($id);
            if($pdata["product_name"] != "") $product->title = $pdata["product_name"];
			else 
				$product->title = $parent->title;
			/* end fixcode */
			
			/* get current product name with options */
            /* $product->title = $this->amazon_crawler_model->_crawl_product_title($id); */

            $product->origin_price = $product->price;
            $product->web_shipping_fee = 0;

            $term_fee = $this->_calc_term_fee($product->price, json_decode($parent->categories, true), true);
            $term_origin_fee = floatval($term_fee['origin_fee']);
            $term_fee = intval($term_fee['fee']);

			/* fixcode */
            /* $shipping_fee = $this->_shipping_fee * (round($product->weight, 2) + 0.2); */
			$shipping_fee = calc_product_shipping_fee_v2($product->weight,$this->_rate_policy);
			
			
            $price = calc_product_price_v2($product->price,$this->_rate_policy);

            $product->price = $term_fee + $shipping_fee + $price;
            $product->url = 'https://amazon.com/dp/' . $product->id;
            $product->website = 'amazon.com';
            $product->images = json_decode($product->images);
            $image = end($product->images)->large;

            $term_id = $this->_get_term_fee_id(json_decode($parent->categories, true));
            if ($product->origin_price < 20)
                $term_id = 8;
			
			/* update shipping fee to crawled_amz_products table */
			$this->amazon_product_model->updateShippingFee($id, $shipping_fee);
			
        } else {
            $cat = [];
            $product->categories = json_decode($product->categories, true);
            foreach ($product->categories as $c)
                $cat = array_merge($cat, explode(':', $c));

            $variant = [];

            $default_keys = ['quantity', 'id', 'image'];
            foreach ($this->input->post() as $key => $value) {
                if (!in_array($key, $default_keys)) {
                    array_push($variant, $value);
                }
            }

            sort($variant);
            $variant = join('|', $variant);
            $product->variations_price = json_decode($product->variations_price, true);
            if (isset($product->variations_price[md5($variant)]))
                $product->origin_price = $product->variations_price[md5($variant)];

            $term_fee = $this->_calc_term_fee($product->origin_price, $cat, true);
            $term_origin_fee = floatval($term_fee['origin_fee']);
            $term_fee = intval($term_fee['fee']);
            $term_id = $this->_get_term_fee_id($cat);
            if ($product->origin_price < 20)
                $term_id = 8;
        }

        global $cart;
        if (count($cart) > 0) {
            if ($cart[0]->website != $product->website)
                json_error("Bạn chỉ được thêm sản phẩm từ website <b>{$cart[0]->website}</b>");
        }

        $data = [
            'qty' => intval($quantity),
            'image' => $image,
            'price' => floatval($product->price),
            'origin_price' => round(floatval($product->origin_price), 2),
            'weight' => round($product->weight, 2),
            'website' => $product->website,
            '_name' => $product->title,
            'name' => $product->id,
            'link' => $product->url,
            'note' => null,
            'variations' => [],
            'term_id' => $term_id,
            'term_fee' => $term_fee,
            'origin_term_fee' => $term_origin_fee,
            'web_shipping_fee' => floatval($product->web_shipping_fee),
            'total' => floatval($product->origin_price) * intval($quantity),
            'sub_total' => 0,
            'product_id' => $id,
            'shipping_fee' => $shipping_fee,
        ];

        if ($data['weight'] > 0)
            $data['weight'] += 0.2;

        $default_keys = ['quantity', 'id', 'image'];
        foreach ($this->input->post() as $key => $value) {
            if (!in_array($key, $default_keys)) {
                array_push($data['variations'], [
                    'name' => str_replace('_', ' ', $key),
                    'value' => $value
                ]);
            }
        }

        $data['id'] = md5($id . implode('|', array_column($data['variations'], 'value')));
		
		if ($this->cart->insert($data))
            json_success('Đã thêm sản phẩm vào giỏ hàng');
        json_error('Lỗi thêm sp vào giỏ hàng');
    }

    public function description($id)
    {
        $product = $this->crawled_product_model->get_by_id($id);
        $this->blade->set('product', $product);
        return $this->blade->render();
    }

    private function _calc_term_fee($origin_price, array $categories = null, bool $get_orgin_fee = false)
    {
        $origin_price = floatval($origin_price);
        if ($categories == null && $origin_price >= 20)
            return 0;

        if ($origin_price < 20) {
            $order_term = $this->order_term_model->get(8);
            $steps = $this->order_term_model->get_steps(8);
        } else {
            $steps = $this->order_term_model->get_steps_by_category($categories);
            if (count($steps) == 0)
                return 0;

            usort($steps, function ($a, $b) {
                return $a->step >= $b->step;
            });
            $order_term = $this->order_term_model->get($steps[0]->order_term_id);
        }

        $currency_rate = $this->_rate;
        if ($origin_price > 500)
            $currency_rate = $this->_rate;

        $_price = 0;
        $origin_fee = 0;

        for ($i = 0; $i < count($steps); $i++) {
            if (floatval($origin_price) >= $steps[$i]->step) {
                if ($steps[$i]->type == OrderTermType::AMOUNT)
                    $origin_fee = $_price = $steps[$i]->amount;
                elseif ($steps[$i]->type == OrderTermType::PERCENT) {
                    $fee = $origin_price * intval($steps[$i]->amount) / 100;
                    if ($fee < $order_term->min_fee) {
                        $origin_fee = $fee = $order_term->min_fee;
                        $_price = $fee * $currency_rate;
                    } else {
                        $origin_fee = $origin_price * intval($steps[$i]->amount) / 100;
                        $_price = $currency_rate * $origin_price * intval($steps[$i]->amount) / 100;
                    }
                }
            }
        }

        if ($get_orgin_fee)
            return ['fee' => $_price, 'origin_fee' => $origin_fee];

        return $_price;
    }

    private function _get_term_fee_id(array $categories = null)
    {
        if ($categories != null) {
            if (count($categories) > 0) {
                $steps = $this->order_term_model->get_steps_by_category($categories);
                if (count($steps) > 0) {
                    return $steps[0]->order_term_id;
                }
            }
        }

        return null;
    }
}