<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Account
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Option_model $option_model
 * @property Image_model $image_model
 * @property Common_model $common_model
 * @property Customer_model $customer_model
 * @property Customer_order_model $customer_order_model
 * @property Payment_history_model $payment_history_model
 * @property Country_model $country_model
 * @property Order_shipping_model $order_shipping_model
 */
class Account extends Site_Controller
{
    private $_user;
	
	private $_curency_rate;
	private $_shipping_fee;

    public function __construct()
    {
        parent::__construct();
        if (!$this->ion_auth->logged_in())
            return redirect(base_url());

        $this->_user = $this->ion_auth->user()->row();
        $this->load->model('common_model');
        $this->load->model('management/customer_model');
        $this->load->model('basso/customer_order_model');
        $this->load->model('basso/payment_history_model');
        $this->load->model('basso/country_model');
        $this->load->model('basso/order_shipping_model');
		$this->load->helper('basso/crawler');
		$this->load->model('basso/order_term_model');
		
		$this->_rate_policy = get_country_policy();
		$this->_curency_rate = $this->_rate_policy["rate"]; 
		$this->_shipping_fee = $this->_rate_policy["shipping_fee"];
    }

    #region Thông tin tài khoản
    public function index()
    {
        set_head_title('Thông tin tài khoản');
        return $this->blade->render();
    }

    public function GET_index()
    {
        $this->form_validation->set_data($this->input->get());
        $this->form_validation->set_rules('action', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        switch ($this->input->get('action')) {
            case 'init':
                $user = $this->customer_model->get_info_by_id($this->_user->id);
                $cities = $this->common_model->get_cities();
                $districts = [];
                if ($user->city_id != null)
                    $districts = $this->common_model->get_districts($user->city_id);

                if (count($districts) == 0)
                    $districts = $this->common_model->get_districts($cities[0]->id);

				//Get tổng số tiền các đơn hàng của khách hàng
				$total_revenue = $this->customer_order_model->total_revenue_by_customer($this->_user->id,CustomerAccountOrderTab::PROCESSING);
				$total_total_paid = $this->customer_order_model->total_paid_by_customer($this->_user->id,CustomerAccountOrderTab::PROCESSING);
				$debit = $total_revenue->sub_total - $total_total_paid->total_paid;
				json_success(null, ['user' => $user, 'cities' => $cities, 'districts' => $districts,'debit'=>$debit]);
                break;
            case 'change-city':
                $id = $this->input->get('id');
                json_success(null, ['data' => $this->common_model->get_districts($id)]);
                break;
            default:
                json_error('Yêu cầu không hợp lệ');
                break;
        }
    }

    public function POST_index()
    {
        $this->form_validation->set_rules('name', null, 'required');
        $this->form_validation->set_rules('phone', null, 'required');
        $this->form_validation->set_rules('address', null, 'required');
        $this->form_validation->set_rules('gender', null, 'required');
        $this->form_validation->set_rules('city_id', null, 'required');
        $this->form_validation->set_rules('district_id', null, 'required');
        if (!$this->form_validation->run())
            json_error('Vui lòng nhập thông tin bắt buộc');

        $name = $this->input->post('name');
        $phone = $this->input->post('phone');
        $address = $this->input->post('address');
        $city_id = $this->input->post('city_id');
        $district_id = $this->input->post('district_id');
        $gender = $this->input->post('gender');
        $birthday = $this->input->post('birthday');
        $new_pass = $this->input->post('new_pass');
        $re_pass = $this->input->post('re_pass');

        $new_pass = is_enull($new_pass, null);
        $re_pass = is_enull($re_pass, null);
        if ($new_pass != null) {
            if ($new_pass != $re_pass)
                json_error('Xác nhận mật khẩu không khớp');
        }

        $update_data = [
            'first_name' => $name,
            'phone' => $phone
        ];

        if ($new_pass != null)
            $update_data['password'] = $new_pass;

        if ($this->ion_auth->update($this->_user->id, $update_data)) {
            $update_data = [
                'address' => $address,
                'city_id' => $city_id,
                'district_id' => $district_id,
                'gender' => $gender
            ];

            if (is_enull($birthday, null) != null)
                $update_data['birthday'] = strtoupper($birthday);

            if ($this->customer_model->update_info($this->_user->id, $update_data))
                json_success('Cập nhật thành công');

            json_error('Có lỗi, vui lòng F5 thử lại');
        }

        json_error('Có lỗi, vui lòng F5 thử lại');
    }
    #endregion

    #region Lịch sử đơn hàng
    public function orders($page = 1)
    {
        set_head_title('Quản lý đơn hàng');
        $id = $this->input->get('id');
        if ($id != null) {
			
            #region Chi tiết đơn hàng
			if(strpos($id, "BS") !== false || strpos($id, "AS") !== false){
				$order = $this->customer_order_model->get_by_code($id);
			}else{
				$order = $this->customer_order_model->get_by_id($id);
			}
            if ($order == null) {
                $this->session->set_flashdata('error_msg', "Không tìm thấy đơn hàng <b>#$id</b>");
                return redirect(base_url('tai-khoan/don-hang'));
            }

            if ($this->_user->id != $order->customer_id) {
                $this->session->set_flashdata('error_msg', "Không tìm thấy đơn hàng <b>#$id</b>");
                return redirect(base_url('tai-khoan/don-hang'));
            }

            $items = $this->customer_order_model->get_items($order->id);

			
			/* fixcode update items if qty >= 2 in frontend 
			$new_items = [];
			for($t1=0; $t1<count($items); $t1++) {
				$qty = 0;
				for($t2=0; $t2<count($new_items); $t2++){
					if(trim(strtolower($items[$t1]->name)) == trim(strtolower($new_items[$t2]->name))
						&& trim(strtolower($items[$t1]->order_id)) == trim(strtolower($new_items[$t2]->order_id))
						&& trim(strtolower($items[$t1]->price)) == trim(strtolower($new_items[$t2]->price))
						&& trim(strtolower($items[$t1]->status)) == trim(strtolower($new_items[$t2]->status))
						&& trim(strtolower($items[$t1]->term_id)) == trim(strtolower($new_items[$t2]->term_id))
						&& trim(strtolower($items[$t1]->term_fee)) == trim(strtolower($new_items[$t2]->term_fee))
						&& trim(strtolower($items[$t1]->weight)) == trim(strtolower($new_items[$t2]->weight))
						&& trim(strtolower($items[$t1]->website)) == trim(strtolower($new_items[$t2]->website))
					){
						$qty++;
					}
				}
				if($qty <= 0) array_push($new_items, $items[$t1]);
			}
			//recount qty
			for($j1=0; $j1<count($new_items); $j1++){
				$qty = 0;
				for($j2=0; $j2<count($items); $j2++){
					if(trim(strtolower($items[$j2]->name)) == trim(strtolower($new_items[$j1]->name))
						&& trim(strtolower($items[$j2]->order_id)) == trim(strtolower($new_items[$j1]->order_id))
						&& trim(strtolower($items[$j2]->price)) == trim(strtolower($new_items[$j1]->price))
						&& trim(strtolower($items[$j2]->status)) == trim(strtolower($new_items[$j1]->status))
						&& trim(strtolower($items[$j2]->term_id)) == trim(strtolower($new_items[$j1]->term_id))
						&& trim(strtolower($items[$j2]->term_fee)) == trim(strtolower($new_items[$j1]->term_fee))
						&& trim(strtolower($items[$j2]->weight)) == trim(strtolower($new_items[$j1]->weight))
						&& trim(strtolower($items[$j2]->website)) == trim(strtolower($new_items[$j1]->website))
					){
						$qty++;
					}
				}
				$new_items[$j1]->quantity = $qty;
			}		
			
			$items = $new_items;	
*/
			/* end fixcode update items if qty >= 2 in frontend */
			
			
			/* fixcode Tổng giá trị đơn hàng và Tổng giá SP khác nhau khi nhập cân nặng sản phẩm ( ở cả font end và backend) */
			$currency_rate = $this->_curency_rate;
			$shipping_fee = $this->_shipping_fee;
			$world_shipping_fee = 0; $total_weight = 0; 
			$web_shipping_fee = 0; $sub_total = 0;
				
			$customer_order_total_weight = 0;
			$customer_order_extra_fee = 0;
			$customer_order_item_total = 0; 
			
			for($k2=0; $k2<count($items); $k2++){
				if ($items[$k2]->weight > 0.2) {
					$world_shipping_fee += $shipping_fee * $items[$k2]->weight * $items[$k2]->quantity;
					$total_weight += $items[$k2]->weight * $items[$k2]->quantity;
					$customer_order_total_weight += $items[$k2]->weight * $items[$k2]->quantity;
				} else
					$items[$k2]->weight = 0;

				$web_shipping_fee += $items[$k2]->web_shipping_fee;

				$items[$k2]->item_world_shipping_fee = $order->world_shipping_fee * is_enull($items[$k2]->weight, 0);
				#region Danh mục phụ thu và số tiền phụ thu
				if ($items[$k2]->term_id != null) {
					$order_term = $this->order_term_model->get($items[$k2]->term_id);
					if ($order_term != null) {
						$steps = $this->order_term_model->get_steps($items[$k2]->term_id);
						usort($steps, function ($a, $b) {
							return $a->step >= $b->step;
						});

						for ($i = 0; $i < count($steps); $i++) {
							if (floatval($items[$k2]->price) >= $steps[$i]->step) {
								if ($steps[$i]->type == OrderTermType::AMOUNT)
									$items[$k2]->term_fee = $steps[$i]->amount;
								elseif ($steps[$i]->type == OrderTermType::PERCENT) {
									$fee = $items[$k2]->price * intval($steps[$i]->amount) / 100;
									if ($fee < $order_term->min_fee) {
										$fee = $order_term->min_fee;
										$items[$k2]->term_fee = $fee * $currency_rate;
									} else {
										$items[$k2]->term_fee = $currency_rate * $items[$k2]->price * intval($steps[$i]->amount) / 100;
									}
								}
							}
						}
					}
				}
				#endregion

				$sub_total += $items[$k2]->term_fee * $items[$k2]->quantity;
				
				$sub_total += ($web_shipping_fee + $order->total) * $currency_rate + $world_shipping_fee;
				$sub_total = $sub_total * ($order->fee_percent + 100) / 100;
				
				$items[$k2]->sub_total = $sub_total;
			}
			/* end fixcode tong gia tri */
			
			$order->total_weight = $customer_order_total_weight;
			$order->world_shipping_fee_total = $customer_order_total_weight * $order->world_shipping_fee;
			
            $country = $this->country_model->get($order->country_id);
            if ($country != null) {
                $order->world_shipping_fee = $country->shipping_fee * $order->total_weight;
            }
            $order_info = [
                'payment_date' => null,
                'in_warehouse_date' => null,
                'in_inventory_date' => null,
                'shipped_time' => null
            ];

            #region Ngày về kho Mỹ
            $in_warehouse_items = array_filter($items, function ($t) {
                return $t->delivered_date != NULL;
            });

            if (count($in_warehouse_items) == count($items)) {
                usort($in_warehouse_items, function ($a, $b) {
                    return $a->delivered_date < $b->delivered_date;
                });

                $order_info['in_warehouse_date'] = date('d/m/Y', $in_warehouse_items[0]->delivered_date);
            }
            #endregion

        #region Ngày về VN
        $in_inventory_items = array_filter($items, function ($t) {
            return $t->status == 'in_inventory' || $t->status == 'shipped' ;
        });

        if (count($in_inventory_items) == count($items)) {
            usort($in_inventory_items, function ($a, $b) {
                return $a->imported_time < $b->imported_time;
            });

            $order_info['in_inventory_date'] = date('d/m/Y', $in_inventory_items[0]->imported_time);
        }
        #endregion

        #region ngày giao hàng
        $shipped_items = array_filter($items, functioN ($t) {
            return $t->status == 'shipped';
            #return $t->shipped == 1;
        });

        if (count($shipped_items) == count($items)) {
            $shipping_histories = $this->order_shipping_model->get_by_order($order->id);
            if (count($shipping_histories) > 0) {
                $shipping_histories = array_filter($shipping_histories, function ($t) {
                    return $t->shipped_time != null;
                });
                usort($shipping_histories, function ($a, $b) {
                    return $a->shipped_time < $b->shipped_time;
                });

                if (isset($shipping_histories[0]))
                    $order_info['shipped_time'] = date('d/m/Y', $shipping_histories[0]->shipped_time);
            }
        }
        #endregion
			

            $this->blade->set('order_info', $order_info);
            $this->blade->set('order', $order);
            $this->blade->set('items', $items);
            $this->blade->set('payments', $this->payment_history_model->get_by_order($order->id));
            return $this->blade->render();
            #endregion
        }

        $page = $page < 1 ? 1 : $page;
        $status = null;
        if ($this->input->get('s') != null) {
            $_s = $this->input->get('s');
            if (in_array($_s, array_keys(CustomerAccountOrderTab::LIST_STATE)))
                $status = $_s;
        }

        $query = null;
        if ($this->input->get('q') != null) {
            $query = $this->input->get('q');
        }

        $orders = $this->customer_order_model->get_orders_by_customer_frontend($page, $this->_user->id, $status, $query);
        $total_items = $this->customer_order_model->count_orders_by_customer_frontend($this->_user->id, $status, $query);
        $pagination = Pagination::calc($total_items, $page, PAGING_SIZE);

        $total_revenue = $this->customer_order_model->total_revenue_by_customer_frontend($this->_user->id, $status, $query);
        $this->blade->set('pagination', $pagination);
        $this->blade->set('total_revenue', $total_revenue);
        $this->blade->set('orders', $orders);
        return $this->blade->render();
    }
    #endregion

    #region Lịch sử thanh toán
    public function payments($page = 1)
    {
        set_head_title('Lịch sử thanh toán');
        $page = $page < 1 ? 1 : $page;

        $payments = $this->payment_history_model->get_by_customer($page, $this->_user->id);
        $total_items = $this->payment_history_model->count_by_customer($this->_user->id);
        $pagination = Pagination::calc($total_items, $page, PAGING_SIZE);
        $orders = $this->customer_order_model->get_orders_by_customer(0, $this->_user->id);

        $this->blade->set('orders', $orders);
        $this->blade->set('payments', $payments);
        $this->blade->set('pagination', $pagination);
        return $this->blade->render();
    }
    #endregion
}