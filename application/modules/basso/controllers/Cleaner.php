<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Cleaner
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Option_model $option_model
 * @property Image_model $image_model
 * @property Amazon_product_model $amazon_product_model
 * @property Crawled_product_model $crawled_product_model
 * @property Customer_order_model $customer_order_model
 * @property Order_shipping_model $order_shipping_model
 */
class Cleaner extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('amazon_product_model');
        $this->load->model('crawled_product_model');
        $this->load->model('customer_order_model');
        $this->load->model('order_shipping_model');
        $this->load->helper('status');
    }

    public function run()
    {
        $this->amazon_product_model->clean();
        $this->crawled_product_model->clean();
    }

    public function check_completed()
    {
        $orders = $this->customer_order_model->get_pending_orders();
        foreach ($orders as $order) {
            if (abs($order->total_paid - $order->sub_total) <= 1000) {
                //echo $order->order_code . '\n';
                $shipped_order = $this->customer_order_model->get_shipped_order($order->id);
                if ($shipped_order != null) {
                    if ($shipped_order->total_items == $order->total_item) {
                        //echo $order->order_code . '\n';
                        $this->customer_order_model->update($order->id, ['status' => CustomerOrderStatus::COMPLETED]);
                    }
                }
            }
        }
    }
}