<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Web_order
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Option_model $option_model
 * @property Image_model $image_model
 * @property Customer_order_model $customer_order_model
 * @property Country_model $country_model
 * @property Web_order_model $web_order_model
 * @property Billing_model $billing_model
 * @property Warehouse_model $warehouse_model
 * @property Buyer_model $buyer_model
 * @property Web_payment_model $web_payment_model
 * @property Excel $excel
 */
class Web_order extends Cpanel_Controller
{
    private $_user;

    public function __construct()
    {
        parent::__construct();
        global $current_user;
        $this->_user = $current_user;

        $this->load->model('customer_order_model');
        $this->load->model('warehouse_model');
        $this->load->model('country_model');
        $this->load->model('web_order_model');
        $this->load->model('management/billing_model');
        $this->load->model('buyer_model');
        $this->load->model('web_payment_model');
        $this->load->model('inventory_model');
    }

    public function index()
    {
        $this->blade->set('breadcrumbs', [['text' => 'Danh sách mua hàng website']]);
        return $this->blade->render();
    }

    public function GET_index()
    {
        $this->form_validation->set_data($this->input->get());
        $this->form_validation->set_rules('action', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        switch ($this->input->get('action')) {
            case 'search':
                $this->form_validation->set_rules('page', null, 'required|numeric');
                $this->form_validation->set_rules('warehouse_id', null, 'required|numeric');
                $this->form_validation->set_rules('payment_id', null, 'required|numeric');
                $this->form_validation->set_rules('buyer_id', null, 'required|numeric');
                $this->form_validation->set_rules('branch', null, 'required');
                $this->form_validation->set_rules('status', null, 'required');
                if (!$this->form_validation->run())
                    json_error('Yêu cầu không hợp lệ');

                $page = $this->input->get('page');
                $branch = $this->input->get('branch');
                $status = $this->input->get('status');
                $key = $this->input->get('key');
                $warehouse_id = $this->input->get('warehouse_id');
                $payment_id = $this->input->get('payment_id');
                $buyer_id = $this->input->get('buyer_id');
				$website = $this->input->get('websites');
				
                $status = $status == 'all' ? null : $status;
                $date = $this->input->get('date');
                $to_date = $this->input->get('to_date');

                $result = $this->web_order_model->get($page, $date, $warehouse_id, $payment_id, $buyer_id, $branch, $status, $key, $website, $to_date);
                if (count($result) == 0)
                    json_error('Không tìm thấy đơn hàng');

                for ($i = 0; $i < count($result); $i++) {
                    $result[$i]->items = [];
                    $_web = explode('.', $result[$i]->website);
                    if (count($_web) > 1)
                        unset($_web[count($_web) - 1]);

                    $result[$i]->website = implode('.', $_web);
                }

                $items = $this->web_order_model->get_items(array_column($result, 'id'), $key);
                for ($i = 0; $i < count($items); $i++) {
                    if ($items[$i]->delivered_date != null) {
                        if (intval($items[$i]->delivered_date) > 0)
                            $items[$i]->delivered_date = date('d-m-Y', $items[$i]->delivered_date);
                        else
                            $items[$i]->delivered_date = null;
                    }

                    if ($items[$i]->tracking_date != null)
                        $items[$i]->tracking_date = date('d-m-Y', $items[$i]->tracking_date);

                }

                $total_item = $this->web_order_model->count($date, $warehouse_id, $payment_id, $buyer_id, $branch, $status, $key, $website, $to_date);
                $pagination = Pagination::calc($total_item, $page);
                $websites = $this->customer_order_model->get_websites();
				json_success(null, ['orders' => $result, 'items' => $items, 'pagination' => $pagination, 'websites' => $websites]);
                break;

            case 'tracking':
                $data = $this->web_order_model->get_tracking();
                json_success(null, ['data' => array_column($data, 'tracking_code')]);
                break;

            case 'init':
                $payments = $this->web_payment_model->get_active();
				
                $warehouse = $this->warehouse_model->get();
				$websites = $this->customer_order_model->get_websites();
				
				/* print_r("<pre>");
				print_r($websites);
				print_r("</pre>"); */
				
                json_success(null, [
                    'payments' => $payments,
                    'warehouses' => $warehouse,
                    'buyers' => $this->buyer_model->get(),
                    'websites' => $websites,
                ]);
                break;

            default:
                json_error('Yêu cầu không hợp lệ');
                break;
        }
    }

    public function POST_index()
    {
		
		$this->form_validation->set_rules('action', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        switch ($this->input->post('action')) {
            case 'update-item':
                $this->form_validation->set_rules('id', null, 'required|numeric');
                $this->form_validation->set_rules('order_id', null, 'required|numeric');
                if (!$this->form_validation->run())
                    json_error('Yêu cầu không hợp lệ');

                $id = $this->input->post('id');
                $order_id = $this->input->post('order_id');
                $tracking_code = $this->input->post('tracking_code');
                $delivered_date = $this->input->post('delivered_date');
                $buy_price = $this->input->post('buy_price');
				
				/* fixcode */
				if($delivered_date == "") $delivered_date = null;
				/* end fixcode */
				
                //if ($tracking_code != null || $delivered_date != null) {
                    if ($delivered_date != null)
                        $delivered_date = strtotime(trim($delivered_date));
					
					/******* fixcode update web_tracking from đơn admin to kho inventory *******/
					$web_order_item = $this->web_order_model->get_web_order_item_by_id($id);
					if($web_order_item[0]->tracking_code) {
						$inventory_package_web_tracking_obj = $this->inventory_model->get_inventory_package_by_web_tracking($web_order_item[0]->tracking_code);
						if($inventory_package_web_tracking_obj != null){
							$inventory_package_item_id = $inventory_package_web_tracking_obj->id;
							$this->inventory_model->update_package($inventory_package_item_id, ["web_tracking" => trim($tracking_code)]);
						}
					}
					/******* end fixcode *******/

                    $data = ['tracking_code' => trim($tracking_code), 'delivered_date' => $delivered_date, 'buy_price' => $buy_price];
                    if ($this->web_order_model->update_item($order_id, $id, $data)){
						
						/* fixcode update order info */
						if($buy_price) {
							$re_total = 0; $re_subtotal = 0;
							$items = $this->web_order_model->get_items($order_id);
							for ($i = 0; $i < count($items); $i++) {
								$re_total += $items[$i]->quantity * floatval($items[$i]->buy_price);
							}
							
							$order_info = $this->web_order_model->get_by_id($order_id);
							$re_subtotal = (floatval($order_info->ship_fee) + $re_total) * $order_info->buy_rate;
							$order_data['subtotal'] = $re_subtotal;
							$order_data['total'] = $re_total;
							$this->web_order_model->update_order($order_id, $order_info->status, $order_data);
						}
						/* fixcode update order info */
						
						
						
						//Sau khi cap nhật thành công tracking code ở inventory_package thì cập nhật tracking_code cho web_order_item và customer_order_item
						if($buy_price>0)
							$data_item = ['tracking_code' => trim($tracking_code), 'price' => $buy_price];
						else
							$data_item = ['tracking_code' => trim($tracking_code)]; /* fixcode neu gia >0 */
						
						$customer_order_item_id = $web_order_item[0]->item_id;
						$customer_order_id = $web_order_item[0]->customer_order_id;
						//Sau khi cập nhật giá ở đơn mua sẽ không cập nhật giá sản phẩm đơn hàng của khách.
						//$this->customer_order_model->update_item($customer_order_id, $customer_order_item_id, $data_item);
                        json_success('Cập nhật thành công');
					}

                    json_error('Có lỗi, vui lòng F5 thử lại');
               // }

                json_error('Yêu cầu không hợp lệ');
                break;

            case 'update-tracking':
                $data = $this->input->post('data');
                if (!is_array($data))
                    json_error('Không tìm thấy tracking hợp lệ');

                foreach ($data as $item) {
                    if (!$this->web_order_model->update_delivered_date($item['tracking'], strtotime($item['date'])))
                        json_error('Có lỗi, vui lòng F5 thử lại');
                }

                $items = $this->web_order_model->get_by_tracking(array_column($data, 'tracking'));
                if (count($items) == 0)
                    json_error('Không tìm thấy đơn hàng tương ứng');

                foreach (array_column($items, 'web_order_id') as $order_id)
                    $this->web_order_model->check_order_items_delivered($order_id);

                json_success('Cập nhật thành công');
                break;

            default:
                json_error('Yêu cầu không hợp lệ');
                break;
        }
    }
	
	public function POST_change_active()
    {
        $id = $this->input->post('id');
        $active = $this->input->post('active');
        $this->web_payment_model->change_active($id, $active);
        die;
    }

    #region Tạo đơn hàng
    public function create()
    {
        $this->blade->set('breadcrumbs', [
            ['text' => 'Danh sách mua hàng web', 'url' => base_url('basso/web_order')],
            ['text' => 'Tạo đơn']
        ]);
        $this->blade->set('buyers', $this->buyer_model->get());
        return $this->blade->render();
    }

    public function GET_create()
    {
        $this->form_validation->set_data($this->input->get());
        $this->form_validation->set_rules('action', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');
		
		
        switch ($this->input->get('action')) {
            case 'search':
                $this->form_validation->set_rules('site', null, 'required');
                if (!$this->form_validation->run())
                    json_error('Yêu cầu không hợp lệ');

                $web = $this->input->get('site');
                $key = $this->input->get('key');
                $web = $web == 'all' ? null : $web;
                $page = $this->input->get('page');
                $page = $page < 1 ? 1 : $page;

                $orders = $this->customer_order_model->get_web_orders($page, $web, $key);
                $total_item = $this->customer_order_model->count_web_orders($web, $key);
                if (count($orders) == 0)
                    json_error('Không tìm thấy đơn hàng');

                for ($i = 0; $i < count($orders); $i++) {
                    $orders[$i]->items = [];
                    $_web = explode('.', $orders[$i]->website);
                    if (count($_web) > 1)
                        unset($_web[count($_web) - 1]);

                    $orders[$i]->website = implode('.', $_web);
                    if (empty($orders[$i]->branch))
                        $orders[$i]->branch = 'ha-noi';
                }

                $items = $this->customer_order_model->get_items(array_column($orders, 'id'),
                    CustomerOrderItemStatus::PENDING, CustomerOrderItemRequirement::BUY_NOW);
                for ($i = 0; $i < count($items); $i++) {
                    if ($items[$i]->variations == null)
                        $items[$i]->variations = [];
                    else if (!empty($items[$i]->variations))
                        $items[$i]->variations = json_decode($items[$i]->variations);
                    else
                        $items[$i]->variations = [];
                }

                json_success(null, [
                    'orders' => $orders,
                    'items' => $items,
                    'pagination' => Pagination::calc($total_item, $page)
                ]);
                break;
            case 'init':
                $websites = $this->customer_order_model->get_websites();
                $countries = $this->country_model->get();
                $payments = $this->web_payment_model->get_active();
                $warehouse = $this->warehouse_model->get();
                json_success(null, [
                    'websites' => $websites,
                    'countries' => $countries,
                    'payments' => $payments,
                    'warehouses' => $warehouse
                ]);
                break;
				
			case 'save-note':
				$order_id = $this->input->get('order_id');
                $note = $this->input->get('admin_note');
				$data = ['admin_note' => $note];
				$this->customer_order_model->update_note($order_id, $data);
				json_success('Cập nhật thành công');
				break;
        }

        json_error('Yêu cầu không hợp lệ');
    }

    public function POST_save()
    {
        $this->form_validation->set_rules('branch', null, 'required');
        $this->form_validation->set_rules('country_id', null, 'required|numeric');
        $this->form_validation->set_rules('buyer_id', null, 'required|numeric');
        $this->form_validation->set_rules('payment_id', null, 'required|numeric');
        $this->form_validation->set_rules('created_time', null, 'required');
        $this->form_validation->set_rules('create_billing', null, 'required');
        $this->form_validation->set_rules('ship_fee', null, 'required');
        $this->form_validation->set_rules('buy_rate', null, 'required');
        $this->form_validation->set_rules('order_number', null, 'required');
        if (!$this->form_validation->run())
            json_error('Vui lòng nhập các thông tin bắt buộc');

        $items = $this->input->post('items');
        if ($items == null)
            json_error('Bạn chưa chọn sản phẩm');

        if (!is_array($items))
            json_error('Bạn chưa chọn sản phẩm');

        if (count($items) == 0)
            json_error('Bạn chưa chọn sản phẩm');

        $branch = $this->input->post('branch');
        $country_id = $this->input->post('country_id');
        $buyer_id = $this->input->post('buyer_id');
        $payment_id = $this->input->post('payment_id');
        $created_time = $this->input->post('created_time');
        $create_billing = $this->input->post('create_billing');
        $ship_fee = $this->input->post('ship_fee');
        $buy_rate = $this->input->post('buy_rate');
        $order_number = $this->input->post('order_number');
        $note = $this->input->post('note');
        $website = $this->input->post('website');
        $warehouse_id = $this->input->post('warehouse_id');

        $country = $this->country_model->get($country_id);
        if ($country == null)
            json_error('Không tìm thấy quốc gia');

        $order_info = [
            'created_time' => strtotime($created_time),
            'order_number' => $order_number,
            'note' => is_enull($note, null),
            'user_id' => $this->_user->id,
            'branch' => $branch,
            'country_id' => $country_id,
            'ship_fee' => is_enull($ship_fee, 0.0),
            'currency_symbol' => $country->currency_symbol,
            'buy_rate' => intval($buy_rate),
            'total' => 0.0,
            'sub_total' => 0,
            'website' => $website,
            'buyer_id' => $buyer_id,
            'payment_id' => $payment_id,
            'warehouse_id' => is_enull($warehouse_id, null, 'numeric')
        ];

        $_items = [];
        foreach ($items as $item) {
            array_push($_items, [
                'customer_order_id' => $item['order_id'],
                'item_id' => $item['id'],
                'name' => $item['name'],
                'link' => $item['link'],
                'variations' => $item['variations'],
                'note' => $item['note'],
                'quantity' => $item['quantity'],
                'buy_price' => $item['price']
            ]);

            $order_info['total'] += $item['quantity'] * floatval($item['price']);
        }

        $order_info['sub_total'] = (floatval($order_info['ship_fee']) + $order_info['total']) * $order_info['buy_rate'];
        $order_id = $this->web_order_model->insert($order_info, $_items);

        if (!$order_id)
            json_error('Có lỗi, vui lòng F5 thử lại');

        //  Tạo phiếu chi
        if ($create_billing) {
            $content = "TT đơn hàng $order_number";
            $buyer = $this->buyer_model->get($buyer_id);
            $name = $buyer == null ? null : $buyer->name;
            $payment_method = $this->web_payment_model->get($payment_id);
            $payment_method = $payment_method == null ? null : $payment_method->name;

            $this->billing_model->insert(strtotime($created_time), BillingType::OUT, AUTO_BILLING_TERM,
                $name, $order_info['sub_total'], null, $content, null, null, $payment_method,
                null, $order_id);
        }

        success_msg('Tạo đơn hàng thành công');
        json_success(null, ['redirect_url' => base_url('basso/web_order/detail/' . $order_id)]);
    }
    #endregion

    #region Chi tiết đơn hàng
    public function detail($id)
    {		
        if (!isset($id)) {
            error_msg('Yêu cầu không hợp lệ');
            return redirect(base_url('basso/web_order'));
        }

        if (!is_numeric($id)) {
            error_msg('Yêu cầu không hợp lệ');
            return redirect(base_url('basso/web_order'));
        }

        $order = $this->web_order_model->get_by_id($id);
        if ($order == null) {
            error_msg('Đơn hàng không tồn tại');
            return redirect(base_url('basso/web_order'));
        }

        $this->blade->set('breadcrumbs', [
            ['text' => 'Danh sách mua hàng website', 'url' => base_url('basso/web_order')],
            ['text' => 'Đơn hàng ' . $order->order_number]
        ]);
        return $this->blade->render();
    }

    public function GET_detail($id)
    {
        $order = $this->web_order_model->get_by_id($id);
        if ($order == null)
            json_error('Đơn hàng không tồn tại');

        $items = $this->web_order_model->get_items($id);
        for ($i = 0; $i < count($items); $i++) {
            $items[$i]->_tracking_code = null;
            $items[$i]->_delivered_date = null;
            $items[$i]->variations = json_decode($items[$i]->variations);
        }
	
		$branchs = array();
		$branchsArr = array();
		foreach(Branch::LIST_STATE as $key => $value) {
			$branchs["branch"] = $key;
			$branchs["name"] = $value;
			$branchsArr[] = $branchs;
		}

        json_success(null, [
            'order' => $order,
            'items' => $items,
            'buyers' => $this->buyer_model->get(),
            'warehouses' => $this->warehouse_model->get(),
            'countries' => $this->country_model->get(),
			'payments' => $this->web_payment_model->get_active(),
			'branchs' => $branchsArr,
        ]);
    }

    public function POST_detail($id)
    {
        $this->form_validation->set_rules('status', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $status = $this->input->post('status');
        $tracking_code = $this->input->post('tracking_code');
        $delivered_date = $this->input->post('delivered_date');
        $order = $this->input->post('order');
		
		
		$created_time = $this->input->post('created_time');
		if ($created_time != null)
            $created_time = strtotime($created_time);
		

        if ($delivered_date != null)
            $delivered_date = strtotime($delivered_date);

        $this->web_order_model->update_order($id, $status, $order);
		
		if($created_time != "" && $created_time != null) 
			$this->web_order_model->update_order_create_time($id, $created_time);

        if ($status == WebOrderStatus::CANCELLED) {
            $this->billing_model->delete_by_web_order($id);
            $items = $this->web_order_model->get_items($id);

            foreach ($items as $item) {
                $this->customer_order_model->update_item($item->customer_order_id, $item->item_id, [
                    'status' => CustomerOrderItemStatus::PENDING,
                    'ordre_number' => null
                ]);
            }
        } else {
            $this->billing_model->update_by_web_order($id, BillingType::OUT, $order['sub_total']);
        }

        if ($tracking_code != null || $delivered_date != null) {
			
            //  Cập nhật đơn đã về kho VC
            if ($delivered_date != null)
                $this->web_order_model->update_order($id, WebOrderStatus::IN_WAREHOUSE);

            $update_data = ['tracking_code' => $tracking_code, 'delivered_date' => $delivered_date];
            $this->web_order_model->update_item($id, null, $update_data);
        }

        json_success('Cập nhật thành công');
    }

    #endregion

    #region Người mua hàng
    public function buyer()
    {
        $this->blade->set('breadcrumbs', [['text' => 'Người mua hàng website']]);
        return $this->blade->render();
    }

    public function GET_buyer()
    {
        json_success(null, ['data' => $this->buyer_model->get()]);
    }

    public function POST_buyer()
    {
        $this->form_validation->set_rules('id', null, 'required|numeric');
        $this->form_validation->set_rules('name', null, 'required');
        if (!$this->form_validation->run())
            json_success('Vui lòng nhập đủ thông tin');

        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $note = $this->input->post('note');

        if ($this->buyer_model->insert($id, $name, $note))
            json_success($id == 0 ? 'Thêm người mua thành công' : 'Cập nhật thành công');

        json_error('Có lỗi, vui lòng F5 thử lại');
    }

    public function DELETE_buyer()
    {
        $this->form_validation->set_data($this->input->input_stream());
        $this->form_validation->set_rules('id', null, 'required|numeric');
        if ($this->form_validation->run() == false)
            json_error('Yêu cầu không hợp lệ');

        $id = $this->input->input_stream('id');
        if ($this->buyer_model->delete($id))
            json_success('Đã xóa người mua hàng');

        json_error('Có lỗi, vui lòng F5 thử lại');
    }

    #endregion

    #region Phương thức thanh toán
    public function payment()
    {
        $this->blade->set('breadcrumbs', [['text' => 'Phương thức thanh toán']]);
        return $this->blade->render();
    }

    public function GET_payment()
    {
        json_success(null, ['data' => $this->web_payment_model->get()]);
    }

    public function DELETE_payment()
    {
        $this->form_validation->set_data($this->input->input_stream());
        $this->form_validation->set_rules('id', null, 'required|numeric');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $id = $this->input->input_stream('id');
        if ($this->web_payment_model->delete($id))
            json_success('Xóa PTTT thành công');

        json_error('Có lỗi, vui lòng F5 thử lại');
    }

    public function POST_payment()
    {
        $this->form_validation->set_rules('id', null, 'required|numeric');
        $this->form_validation->set_rules('rate', null, 'required|numeric');
        $this->form_validation->set_rules('name', null, 'required');
        if (!$this->form_validation->run())
            json_error('Vui lòng nhập đủ thông tin');

        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $rate = $this->input->post('rate');
        $note = $this->input->post('note');

        $result = $this->web_payment_model->insert($id, $name, $rate, $note);
        if ($result)
            json_success('Lưu PTTT thành công');

        json_error('Có lỗi, vui lòng F5 thử lại');
    }

    #endregion

    public function export()
    {
        $this->load->library('excel');
        $type = $this->input->get('type');
		/* fixcode 404 when export excel */
		/*
        if ($type == null)
            return redirect(base_url('basso/admin'));
		*/
		if ($type == null) $type = "create";
        if ($type == 'create') {
            $web = $this->input->get('site');
            $key = $this->input->get('key');
            $web = $web == 'all' ? null : $web;
            $key = $key == 'undefined' ? null : $key;

            $orders = $this->customer_order_model->get_web_orders(0, $web, $key);
            $list_items = $this->customer_order_model->get_items(array_column($orders, 'id'),
                CustomerOrderItemStatus::PENDING, CustomerOrderItemRequirement::BUY_NOW, true);

            $row_count = 1;
            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle('Sheet1');

            #region khởi tạo header
            $this->excel->getActiveSheet()->setCellValue("A{$row_count}", 'Ngày tạo đơn');
            $this->excel->getActiveSheet()->setCellValue("B{$row_count}", 'Mã ĐH');
            $this->excel->getActiveSheet()->setCellValue("C{$row_count}", 'Chi nhánh');
            $this->excel->getActiveSheet()->setCellValue("D{$row_count}", 'Warehouse');
            $this->excel->getActiveSheet()->setCellValue("E{$row_count}", 'Website');
            $this->excel->getActiveSheet()->setCellValue("F{$row_count}", 'Link Sp');
            $this->excel->getActiveSheet()->setCellValue("G{$row_count}", 'Tên sp');
            $this->excel->getActiveSheet()->setCellValue("H{$row_count}", 'Giá sản phẩm');
            $this->excel->getActiveSheet()->setCellValue("I{$row_count}", 'S.Lượng');
            $this->excel->getActiveSheet()->setCellValue("J{$row_count}", 'Ship web');
            $this->excel->getActiveSheet()->setCellValue("K{$row_count}", 'Tổng tiền');
            $this->excel->getActiveSheet()->setCellValue("L{$row_count}", 'Ghi chú');
            $this->excel->getActiveSheet()->getStyle("A1:L1")->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('0a48ad');
            $this->excel->getActiveSheet()->getStyle("A1:L1")->applyFromArray([
                'font' => [
                    'bold' => true,
                    'color' => ['rgb' => 'ffffff'],
                    'size' => 12
                ]
            ]);
            $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
            #endregion

            #region Fill sản phẩm
            $order_count = 1;
            $row_count = 2;

            foreach ($orders as $order) {
                $order->created_time = date('d-M-Y', $order->created_time);
                $items = array_filter($list_items, function ($t) use ($order) {
                    return $t->order_id == $order->id;
                });

                foreach ($items as $item) {
                    $this->excel->getActiveSheet()->setCellValue("A{$row_count}", $order->created_time);
                    $this->excel->getActiveSheet()->setCellValue("B{$row_count}", $order->order_code);

                    if (in_array($order->branch, array_keys(Branch::LIST_STATE)))
                        $order->branch = Branch::LIST_STATE[$order->branch];

                    $this->excel->getActiveSheet()->setCellValue("C{$row_count}", $order->branch);
                    $this->excel->getActiveSheet()->setCellValue("D{$row_count}", null);
                    $this->excel->getActiveSheet()->setCellValue("E{$row_count}", $order->website);
                    $this->excel->getActiveSheet()->setCellValue("F{$row_count}", $item->link);
                    $this->excel->getActiveSheet()->setCellValue("G{$row_count}", $item->name);
                    $this->excel->getActiveSheet()->setCellValue("H{$row_count}", "{$order->currency_symbol} {$item->price}");
                    $this->excel->getActiveSheet()->setCellValue("I{$row_count}", $item->quantity);
                    $this->excel->getActiveSheet()->setCellValue("J{$row_count}",
                        "{$order->currency_symbol} " . $order->web_shipping_fee / count($items));
                    $this->excel->getActiveSheet()->setCellValue("K{$row_count}",
                        "{$order->currency_symbol} " . $item->price * $item->quantity);
                    $this->excel->getActiveSheet()->setCellValue("L{$row_count}", $item->note);
                    $this->excel->getActiveSheet()->getRowDimension(strval($row_count))->setRowHeight(20);
                    $row_count++;
                }

                $order_count++;
            }
            #endregion

            #region Sửa style, xuất file
            $this->excel->getActiveSheet()->getStyle("A1:L{$row_count}")->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $this->excel->getActiveSheet()->getStyle("A1:L{$row_count}")->applyFromArray([
                'font' => ['size' => 12]
            ]);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename=Don hang can mua.xls');
            header('Cache-Control: max-age=0');

            $this->session->unset_userdata('export-items');
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            $objWriter->save('php://output');
            #endregion
        } else if ($type == 'index') {
            $branch = $this->input->get('branch');
            $status = $this->input->get('status');
            $key = $this->input->get('key');
            $warehouse_id = $this->input->get('warehouse_id');
            $payment_id = $this->input->get('payment_id');
            $buyer_id = $this->input->get('buyer_id');
            $status = $status == 'all' ? null : $status;
            $date = $this->input->get('date');
			
            $to_date = $this->input->get('to_date');
			$website = $this->input->get('websites');

            $orders = $this->web_order_model->get(0, $date, $warehouse_id, $payment_id, $buyer_id, $branch, $status, $key, $website, $to_date);
            $row_count = 1;
            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle('Sheet1');

            #region khởi tạo header
            /* $this->excel->getActiveSheet()->setCellValue("A{$row_count}", 'Order Number');
            $this->excel->getActiveSheet()->setCellValue("B{$row_count}", 'Website');
            $this->excel->getActiveSheet()->setCellValue("C{$row_count}", 'Chi nhánh');
            $this->excel->getActiveSheet()->setCellValue("D{$row_count}", 'Ngày mua');
            $this->excel->getActiveSheet()->setCellValue("E{$row_count}", 'Trạng thái');
            $this->excel->getActiveSheet()->setCellValue("F{$row_count}", 'Tổng tiền mua');
            $this->excel->getActiveSheet()->setCellValue("G{$row_count}", 'Người mua');
            $this->excel->getActiveSheet()->setCellValue("H{$row_count}", 'Warehouse');
            $this->excel->getActiveSheet()->setCellValue("I{$row_count}", 'PTTT'); */
			
			
			$this->excel->getActiveSheet()->setCellValue("A{$row_count}", 'Ngày mua');
            $this->excel->getActiveSheet()->setCellValue("B{$row_count}", 'Website');
			$this->excel->getActiveSheet()->setCellValue("C{$row_count}", 'Order Number');
            $this->excel->getActiveSheet()->setCellValue("D{$row_count}", 'Tổng tiền mua');
            $this->excel->getActiveSheet()->setCellValue("E{$row_count}", 'Trạng thái');
            $this->excel->getActiveSheet()->setCellValue("F{$row_count}", 'Người mua');
			$this->excel->getActiveSheet()->setCellValue("G{$row_count}", 'Chi nhánh');
            $this->excel->getActiveSheet()->setCellValue("H{$row_count}", 'Warehouse');
            $this->excel->getActiveSheet()->setCellValue("I{$row_count}", 'PTTT');
			
			
            $this->excel->getActiveSheet()->getStyle("A1:I1")->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('0a48ad');
            $this->excel->getActiveSheet()->getStyle("A1:I1")->applyFromArray([
                'font' => [
                    'bold' => true,
                    'color' => ['rgb' => 'ffffff'],
                    'size' => 12
                ]
            ]);
            $this->excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
            #endregion

            #region Fill đơn hàng
            foreach ($orders as $order) {
                $row_count++;
                $this->excel->getActiveSheet()->setCellValueExplicit("C{$row_count}",$order->order_number, PHPExcel_Cell_DataType::TYPE_STRING);
                $this->excel->getActiveSheet()->setCellValue("B{$row_count}", $order->website);
				$this->excel->getActiveSheet()->getCell("B{$row_count}")->getHyperlink(new PHPExcel_Cell_Hyperlink(''))->setUrl("http://".$order->website); 
				$this->excel->getActiveSheet()->getStyle("B{$row_count}")->getFont()->setUnderline(true);

                if (in_array($order->branch, array_keys(Branch::LIST_STATE)))
                    $order->branch = Branch::LIST_STATE[$order->branch];
                $this->excel->getActiveSheet()->setCellValue("G{$row_count}", $order->branch);
                $this->excel->getActiveSheet()->setCellValue("A{$row_count}",date('d/m/Y', $order->created_time));

                if (in_array($order->status, array_keys(WebOrderStatus::LIST_STATE)))
                    $order->status = WebOrderStatus::LIST_STATE[$order->status];

                $this->excel->getActiveSheet()->setCellValue("E{$row_count}", $order->status);
                $this->excel->getActiveSheet()->setCellValue("D{$row_count}",
                    "{$order->currency_symbol} " . ($order->total + $order->ship_fee));
                $this->excel->getActiveSheet()->setCellValue("F{$row_count}", $order->buyer);
                $this->excel->getActiveSheet()->setCellValue("H{$row_count}", $order->warehouse);
                $this->excel->getActiveSheet()->setCellValue("I{$row_count}", $order->payment);
            }
            #endregion

            #region Sửa style, xuất file
            $this->excel->getActiveSheet()->getStyle("A1:I{$row_count}")->getAlignment()
                ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $this->excel->getActiveSheet()->getStyle("A1:I{$row_count}")->applyFromArray([
                'font' => ['size' => 12]
            ]);

            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename=Don mua hang website.xls');
            header('Cache-Control: max-age=0');

            $this->session->unset_userdata('export-items');
            $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
            $objWriter->save('php://output');
            #endregion
        }
    }
}