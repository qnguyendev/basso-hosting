<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Warehouse
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Option_model $option_model
 * @property Image_model $image_model
 * @property Warehouse_model $warehouse_model
 * @property Buyer_model $buyer_model
 */
class Warehouse extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('warehouse_model');
    }

    public function index()
    {
        $this->blade->set('breadcrumbs', [['text' => 'Quản lý warehouse']]);
        return $this->blade->render();
    }

    public function GET_index()
    {
        json_success(null, ['data' => $this->warehouse_model->get()]);
    }

    public function DELETE_index()
    {
        $this->form_validation->set_data($this->input->input_stream());
        $this->form_validation->set_rules('id', null, 'required|numeric');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $id = $this->input->input_stream('id');
        if ($this->warehouse_model->delete($id))
            json_success('Đã xóa warehouse');

        json_error('Có lỗi, vui lòng F5 thử lại');
    }

    public function POST_index()
    {
        $this->form_validation->set_rules('id', null, 'required|numeric');
        $this->form_validation->set_rules('name', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $note = $this->input->post('note');
        if ($this->warehouse_model->insert($id, $name, $note)) {
            $msg = $id == 0 ? 'Thêm warehouse thành công' : 'Cập nhật thành công';
            json_success($msg);
        }

        json_error('Có lỗi, vui lòng F5 thử lại');
    }
}