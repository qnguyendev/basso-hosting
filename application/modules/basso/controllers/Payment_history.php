<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Payment_history
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Option_model $option_model
 * @property Image_model $image_model
 * @property Payment_history_model $payment_history_model
 * @property User_model $user_model
 * @property Customer_order_model $customer_order_model
 * @property Billing_model $billing_model
 * @property Payment_model $payment_model
 * @property Email_model $email_model
 */
class Payment_history extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/user_model');
        $this->load->model('payment_history_model');
        $this->load->model('customer_order_model');
        $this->load->model('management/billing_model');
        $this->load->model('payment_model');
        $this->load->model('email_model');
		$this->load->helper('basso/crawler');
    }

    public function index()
    {
        $this->blade->set('breadcrumbs', [['text' => 'Lịch sử thanh toán']]);
        return $this->blade->render();
    }

    public function GET_index()
    {
        $page = $this->input->get('page');
        $init = $this->input->get('init');
        if ($init != null) {
            $users = $this->user_model->get();
            json_success(null, ['data' => $users]);
        }

        if ($page != null) {
            $page = !is_numeric($page) ? 1 : ($page < 1 ? 1 : $page);
            $time = $this->input->get('time');
            $date = $this->input->get('date');
            $status = $this->input->get('status');
            $user_id = $this->input->get('user_id');
            $start = strtotime($date . ' 00:00:00');
            $end = strtotime($date . ' 23:59:59');

            switch ($time) {
                case 'all':
                    $start = 0;
                    $end = time();
                    break;
                case 'today':
                    $end = time();
                    $start = strtotime(date('Y-m-d 00:00:00'));
                    break;
                case 'yesterday':
                    $end = strtotime(date('Y-m-d 00:00:00')) - 1;
                    $start = $end + 1 - 24 * 3600;
                    break;
                case 'month':
                    $end = time();
                    $start = strtotime(date('Y-m-1 00:00:00'));
                    break;
            }

            $data = $this->payment_history_model->get($page, $start, $end, $status, $user_id);
            $total_item = $this->payment_history_model->count($start, $end, $status, $user_id);
            $pagination = Pagination::calc($total_item, $page, PAGING_SIZE);

            json_success(null, ['data' => $data, 'pagination' => $pagination]);
        }
    }

    public function POST_index()
    {
        $this->form_validation->set_rules('id', null, 'required|numeric');
        $this->form_validation->set_rules('status', null, 'required');
        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $id = $this->input->post('id');
        $status = $this->input->post('status');
        if (!in_array($status, array_keys(PaymentHistoryStatus::LIST_STATE)))
            json_error('Yêu cầu không hợp lệ');

        $payment = $this->payment_history_model->get_by_id($id);
        if ($payment == null)
            json_error('Không tìm thấy thanh toán');

        if ($payment->status != PaymentHistoryStatus::PENDING)
            json_error('Yêu cầu không hợp lệ');

        $order = null;
        if ($payment->order_id != null) {
            $order = $this->customer_order_model->get_by_id($payment->order_id);
            if ($order == null)
                json_error('Không tìm thấy đơn hàng');
        }

        if ($this->payment_history_model->update_status($id, $status, $order)) {
			// Insert log Huy hoac xac nhan thanh toan;
			
			$log_content = ($status == PaymentHistoryStatus::COMPLETED ? 'Đã xác nhận thanh toán' : 'Đã hủy thanh toán');
			$this->log_model->insert($order->id, 'payment_history', 'index', $log_content);
            //  Tạo phiếu thu tự động nếu chấp nhận thanh toán
            if ($status == PaymentHistoryStatus::COMPLETED) {
                $content = 'Thanh toán đơn hàng #' . $order->id;
                $this->billing_model->insert(time(), BillingType::IN, AUTO_BILLING_TERM, $order->name,
                    $payment->amount, null, $content, null, null, null,
                    $payment->order_id, null);

                $items = $this->customer_order_model->get_items($order->id);
                $order = $this->customer_order_model->get_by_id($payment->order_id);
                $order->payment_method = 'Chuyển khoản';

                if (isset($order->payment_method)) {
                    $payment = $this->payment_model->get_by_type($order->payment_method);
                    if ($payment != null)
                        $order->payment_method = $payment->name;
                }
				
				/* ---- fixcode cho sub_total trong email không khớp với chi tiết đơn hàng ---- */
				$customer_order_total_weight = 0;
				$customer_order_extra_fee = 0;
		
				$customer_order_item_total = 0;
				
				/* $items = $this->customer_order_model->get_items($id, null, null, true); */
				for ($i = 0; $i < count($items); $i++) {
					$items[$i]->variations = json_decode($items[$i]->variations);
					$items[$i]->total_weight = $items[$i]->quantity * is_enull($items[$i]->weight, 0);

					$customer_order_total_weight += $items[$i]->total_weight;
					
					if ($items[$i]->delivered_date != null) {
						if (intval($items[$i]->delivered_date) > 0)
							$items[$i]->delivered_date = date('d-m-Y', $items[$i]->delivered_date);
						else
							$items[$i]->delivered_date = null;
					}
					
					$items[$i]->item_world_shipping_fee = $order->world_shipping_fee * is_enull($items[$i]->weight, 0);
					$customer_order_item_total += ($items[$i]->quantity * $items[$i]->price * $order->currency_rate +
					$items[$i]->term_fee * $items[$i]->quantity +
					$items[$i]->item_world_shipping_fee * $items[$i]->quantity);
					
					/* important fixcode: https://prnt.sc/rjg20c, https://prnt.sc/rjg3cf */
					if($items[$i]->new_shipping_fee){
						if($items[$i]->new_shipping_fee > $items[$i]->shipping_fee)
							$customer_order_extra_fee += $items[$i]->new_shipping_fee - $items[$i]->shipping_fee;
						else
							$customer_order_extra_fee += $items[$i]->shipping_fee - $items[$i]->new_shipping_fee;
						$items[$i]->shipping_fee = $items[$i]->new_shipping_fee;
					}
					/* end important fixcode: https://prnt.sc/rjg20c, https://prnt.sc/rjg3cf */
				}
				$_rate_policy_current = get_country_policy($order->customer_id, $order->country_id);
				$order->country_shipping_fee = $_rate_policy_current["shipping_fee"];
				$order->total_weight = $customer_order_total_weight;
				$order->world_shipping_fee_total = $customer_order_total_weight * $order->world_shipping_fee;
				
				$order->sub_total = $customer_order_item_total + ($order->web_shipping_fee* $order->currency_rate) + $order->inter_shipping_fee;
			
				$order->sub_total = $order->sub_total * (100 + $order->fee_percent) / 100;
				$discount_total = $order->discount_total;
				//Tinh tien discount 
				if ($discount_total >= 0) {
					if ($order->discount_type == 'percent' && $discount_total > 100)
						$discount_total = 100;

					if ($discount_total < 0)
						 $discount_total = 0;

					if ($order->discount_type != 'amount') {
						$order->sub_total -= $discount_total;
					} else {
						$discount_amount = $order->sub_total * $discount_total / 100;
						if ($discount_amount > $order->discount_max && $order->discount_max > 0)
							$discount_amount =  $order->discount_max;

						$order->sub_total -= $discount_amount;
					}
				}
				/* important fixcode: https://prnt.sc/rjg20c, https://prnt.sc/rjg3cf */
				$order->total_weight = $customer_order_total_weight;
				/* $order->sub_total = $order->sub_total + $customer_order_extra_fee; */
				$order->first_total_deposit = $order->first_total_deposit + $customer_order_extra_fee;
				/* end important fixcode: https://prnt.sc/rjg20c, https://prnt.sc/rjg3cf */

				/* ---- end fixcode cho sub_total trong email không khớp với chi tiết đơn hàng ---- */
		
				
				/* fixcode: check order status da_dat_coc hoac chu_dat_coc */
				if($order->payment_status == 'no_payment') $order->payment_status = "Chưa đặt cọc";
				if($order->total_paid > 0) {
					$order->payment_method = $order->payment_method . " trước ".round(100*$order->total_paid/$order->sub_total) . "%";
					if($order->payment_status != 'no_payment' && $order->payment_status != 'uncompleted'){
						$order->payment_status = "Đã đặt cọc";
					}
					
					if($order->payment_status == 'completed'){
						$order->payment_status = "Đã thanh toán đủ";
					}
				}
				/* end fixcode */

                if (strpos($order->email, 'no_email.com') == false) {
					/* fixcode */
					$_rate_policy = get_country_policy($order->customer_id, $order->country_id);
					$_shipping_fee = $_rate_policy["shipping_fee"];
		
					if ($order->sub_total - $order->total_paid <= 0) {
						$content = $this->blade->render('confirm_complete_payment', ['order' => $order, 'items' => $items, 'shipping_fee' => $_shipping_fee], true);
						$subject = "[".ucfirst($order->brand)."] - Xác nhận thanh toán - #$order->order_code";
						$this->email_model->insert($subject, $content, $order->email);
					} else { /* end fixcode */
						$content = $this->blade->render('confirm_payment', ['order' => $order, 'items' => $items, 'shipping_fee' => $_shipping_fee], true);
						$subject = "[".ucfirst($order->brand)."] - Xác nhận thanh toán - #$order->order_code";
						$this->email_model->insert($subject, $content, $order->email);
					}
                }
				
				/* $headers = "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
				mail ($order->email, $subject, $content, $headers);
				
				print_r("<pre>Content: ");
				print_r($content);
				print_r("</pre>");
				
				print_r("<pre>Order: ");
				print_r($order);
				print_r("</pre>");
				die; */
            }

            if ($order != null)
                json_success($status == PaymentHistoryStatus::COMPLETED ? 'Đã xác nhận thanh toán đơn hàng #' . $order->order_code :
                    'Đã hủy thanh toán đơn hàng #' . $order->order_code);
            json_success($status == PaymentHistoryStatus::COMPLETED ? 'Đã xác nhận thanh toán' : 'Đã hủy thanh toán');
        }

        json_error('Có lỗi, vui lòng F5 thử lại');
    }
}