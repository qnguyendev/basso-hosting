<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Crawl_history
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property CI_Form_validation $form_validation
 * @property Option_model $option_model
 * @property Image_model $image_model
 * @property Crawl_history_model $crawl_history_model
 */
class Crawl_history extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('crawl_history_model');
    }

    public function index()
    {
        $this->blade->set('breadcrumbs', [['text' => 'Lịch sử báo giá']]);
        $this->blade->render();
    }

    public function GET_index()
    {
        $this->form_validation->set_data($this->input->get());
        $this->form_validation->set_rules('time', null, 'required');
        $this->form_validation->set_rules('start', null, 'required');
        $this->form_validation->set_rules('end', null, 'required');
        $this->form_validation->set_rules('page', null, 'required');

        if (!$this->form_validation->run())
            json_error('Yêu cầu không hợp lệ');

        $time = $this->input->get('time');
        $from = $this->input->get('start');
        $to = $this->input->get('end');
        $page = $this->input->get('page');

        switch ($time) {
            case 'week':
                $day = strtotime('this week', time());
                $start = strtotime(date('Y-m-d 00:00:00', $day));
                $end = time();
                break;
            case 'month':
                $start = strtotime(date('Y-m-1 00:00:00'));
                $end = time();
                break;
            case 'custom':
                $start = strtotime("$from 00:00:00");
                $end = strtotime("$to 23:59:59");
                break;
            case 'all':
                $start = strtotime('2018-10-10');
                $end = time();
                break;
            default:
                $start = strtotime(date('Y-m-d 00:00:00'));
                $end = time();
                break;
        }

        $result = $this->crawl_history_model->get($page, $start, $end);
        $total_item = $this->crawl_history_model->count($start, $end);
        $pagination = Pagination::calc($total_item, $page);
		
		$count_success = $this->crawl_history_model->count_success_fail(1);
        $count_fail = $this->crawl_history_model->count_success_fail(0);
        $count_keyword_weblink = $this->crawl_history_model->count_keyword_weblink();

        json_success(null, ['data' => $result, 'pagination' => $pagination, 'count_success' => $count_success, 'count_fail' => $count_fail, 'count_keyword_weblink' => $count_keyword_weblink]);
    }
}