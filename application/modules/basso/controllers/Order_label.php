<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Order_lable
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Order_label_model $order_label_model
 * @property CI_Form_validation $form_validation
 */
class Order_label extends Cpanel_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('basso/order_label_model');
    }

    public function index()
    {
        $this->blade->set('breadcrumbs', [['text' => 'Hệ thống'], ['text' => 'Nhãn đơn khách hàng']]);
        return $this->blade->render();
    }

    public function GET_index()
    {
        $group_id = $this->input->get('group_id');
        if ($group_id == null)
            json_success(null, ['data' => $this->order_label_model->group_get()]);

        json_success(null, ['data' => $this->order_label_model->label_get(0, $group_id)]);
    }

    public function DELETE_index()
    {
        $group_id = $this->input->input_stream('group_id');
        if ($group_id != null) {
            if (!is_numeric($group_id))
                json_error('Yêu cầu không hợp lệ');

            $group = $this->order_label_model->group_get($group_id);
            if ($group == null)
                json_error('Không tìm thấy nhóm');

            $this->order_label_model->group_delete($group_id);
            json_success('Xóa nhóm thành công');
        }

        $label_id = $this->input->input_stream('label_id');
        if ($label_id != null) {
            if (!is_numeric($label_id))
                json_error('Yêu cầu không hợp lệ');

            $label = $this->order_label_model->label_get($label_id);
            if ($label == null)
                json_error('Không tìm thấy nhãn');

            if ($this->order_label_model->label_delete($label_id))
                json_success('Xóa nhãn thành công');
            json_error('Có lỗi, vui lòng F5 thử lại');
        }
    }

    public function POST_index()
    {
        $this->form_validation->set_rules('id', null, 'required|numeric');
        $this->form_validation->set_rules('name', null, 'required');
        if (!$this->form_validation->run())
            json_error('Vui lòng nhập đủ thông tin');

        $type = $this->input->post('type');
        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $des = $this->input->post('des');

        if ($type == 'group') {
            $this->order_label_model->group_insert($id, $name, $des);
            json_success($id == 0 ? 'Thêm nhóm thành công' : 'Cập nhật nhóm thành công');
        }

        if ($type == 'label') {
            $group_id = $this->input->post('group_id');
            if ($group_id == null)
                json_error('Vui lòng nhập đủ thông tin');

            $group = $this->order_label_model->group_get($group_id);
            if ($group == null)
                json_error('Không tìm thấy nhóm');

            if ($this->order_label_model->label_insert($id, $group_id, $name, $des))
                json_success($id == 0 ? 'Thêm nhãn thành công' : 'Cập nhật nhãn thành công');
            json_error('Có lỗi, vui lòng F5 thử lại');
        }
    }
}