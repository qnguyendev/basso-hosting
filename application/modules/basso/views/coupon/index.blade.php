<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div clas="row">
        <div class="col-lg-12 col-xl-10 offset-xl-1">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">
                        Quản lý mã giảm giá
                        <a href="{{base_url('basso/coupon/create')}}" class="float-right">
                            <i class="fa fa-plus"></i>
                            Thêm mã
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Mã</th>
                            <th>Kiểu giảm</th>
                            <th>Giá trị</th>
                            <th>Từ ngày</th>
                            <th>Đến ngày</th>
                            <th>Trạng thái</th>
                            <th>Thao tác</th>
                        </tr>
                        </thead>
                        <tbody class="text-center">
                        <tr>
                            <th>BLACKFRIDAY</th>
                            <td>Tiền</td>
                            <td>200.000 đ</td>
                            <td>20/11/2018</td>
                            <td>20/12/2018</td>
                            <td><a href="javascript:" class="badge badge-success">Kích hoạt</a></td>
                            <td>
                                <button class="btn btn-link btn-xs">
                                    <i class="fa fa-edit"></i>
                                    Cập nhật
                                </button>
                                <button class="btn btn-xs btn-danger">
                                    <i class="icon-trash"></i>
                                    Xóa
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{load_js('index')}}"></script>
@endsection