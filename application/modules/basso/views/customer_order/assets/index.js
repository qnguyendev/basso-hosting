ko.bindingHandlers.dbClick = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        let handler = valueAccessor(),
            delay = 200,
            clickTimeout = false;

        $(element).click(function () {
            if (clickTimeout !== false) {
                handler.call(viewModel);
                clickTimeout = false;
            } else {
                clickTimeout = setTimeout(function () {
                    clickTimeout = false;
                }, delay);
            }
        });
    }
};

function viewModel() {
    let self = this;
    self.result = ko.mapping.fromJS([]);
    self.items = ko.mapping.fromJS([]);
    self.order_items = ko.mapping.fromJS([]);
    self.pagination = ko.observable();
    self.tracking_code = ko.observable();

    self.filter = {
        time: ko.observable('all'),
        /* from: ko.observable(moment(new Date()).format('DD-MM-YYYY')),
        to: ko.observable(moment(new Date()).format('DD-MM-YYYY')), */
		from: ko.observable(moment().startOf('year').format('DD-MM-YYYY')),
        to: ko.observable(moment().endOf('year').format('DD-MM-YYYY')),
        status: ko.observable('all'),
        staff_id: ko.observable(0),
        key: ko.observable(),
		
		change_time: function () {
			switch(self.filter.time()){
				case 'today':								
					self.filter.from(moment().format('DD-MM-YYYY'));
					self.filter.to(moment().format('DD-MM-YYYY'));
					break;		
				case 'week':
					self.filter.from(moment().startOf('isoWeek').isoWeekday(1).format('DD-MM-YYYY'));
					self.filter.to(moment().endOf('isoWeek').isoWeekday(7).format('DD-MM-YYYY'));
					break;
				case 'month':
					self.filter.from(moment().startOf('month').format('DD-MM-YYYY'));
					self.filter.to(moment().endOf('month').format('DD-MM-YYYY'));
					break;
				case 'all':
					self.filter.from(moment().startOf('year').format('DD-MM-YYYY'));
					self.filter.to(moment().endOf('year').format('DD-MM-YYYY'));
					break;
				case 'custom':
					break;
			}
        }
		
    };

    //  Danh sách bộ lọc
    self.tabs = {
        list: ko.mapping.fromJS([]),
        current: ko.observable({
            id: ko.observable(0)
        }),
        total_item: ko.observable(0),
        delete: function (item) {
            ALERT.confirm('Bạn muốn xóa bộ lọc?', null, function () {
                /* AJAX.delete(window.location, {id: item.id(), action: 'tab'}, false, (res) => { */
                AJAX.post(window.location, {id: item.id(), action: 'tab'}, false, (res) => {
                    if (!res.error) {
                        self.tabs.list.remove(item);
                        NOTI.success(`Đã xóa bộ lọc ${item.name()}`);

                        if (item.id() === self.tabs.current().id() && self.tabs.list().length > 0) {
                            let tab = self.tabs.list()[0];
                            self.tabs.current().id(tab.id());
                            self.tabs.current().order(tab.order());
                            self.tabs.current().name(tab.name());
                            self.tabs.current().status(tab.status());
                            self.tabs.current().payment_status(tab.payment_status());
                            self.tabs.current().item_status(tab.item_status());
                            self.tabs.current().labels(tab.labels());
                            self.tabs.current().status(tab.status());
                            self.tabs.current().users(tab.users());
                            self.tabs.current().total(tab.total());
                            self.tabs.current().item_requirement(tab.item_requirement());

                            self.search(1);
                        } else {
                            self.tabs.current(item);
                            self.pagination(undefined);
                            ko.mapping.fromJS([], self.items);
                            ko.mapping.fromJS([], self.result);
                        }
                    } else
                        ALERT.error(res.message);
                });
            });
        },
        change: function (item) {
            if (item.id() === self.tabs.current().id()) return;
            self.tabs.current(item);
            self.pagination(undefined);
            ko.mapping.fromJS([], self.items);
            ko.mapping.fromJS([], self.result);
            self.search(1);
        }
    };

    //  Tab bộ lọc
    self.tab_modal = {
        users: ko.mapping.fromJS([]),
        labels: ko.mapping.fromJS([]),
        id: ko.observable(0),
        name: ko.observable().extend({required: {message: LABEL.required}}),
        staff_id: ko.observableArray([]),
        payment_status: ko.observableArray([]),
        item_status: ko.observableArray([]),
        label_id: ko.observableArray([]),
        tracking_status: ko.observableArray([]),
        shipping_status: ko.observableArray([]),
        order: ko.observable(1),
        item_requirement: ko.observableArray([]),
        order_status: ko.observableArray([]),
        show: function () {
            self.tab_modal.id(0);
            self.tab_modal.order(1);
            self.tab_modal.name(undefined);
            self.tab_modal.staff_id([]);
            self.tab_modal.label_id([]);
            self.tab_modal.tracking_status([]);
            self.tab_modal.shipping_status([]);
            self.tab_modal.item_requirement([]);
            self.tab_modal.order_status([]);
            self.tab_modal.errors.showAllMessages(false);

            self.tab_modal.labels.each((x) => {
                $(`#tab-modal select.select2[rel=${x.id()}]`).val(self.tab_modal.label_id()).trigger('change');
            });

            $('#tab-modal select.select2[name=payment_status]').val(self.tab_modal.payment_status()).trigger('change');
            $('#tab-modal select.select2[name=item_status]').val(self.tab_modal.item_status()).trigger('change');
            $('#tab-modal select.select2[name=staff_id]').val(self.tab_modal.staff_id()).trigger('change');
            $('#tab-modal select.select2[name=tracking_status]').val(self.tab_modal.tracking_status()).trigger('change');
            $('#tab-modal select.select2[name=shipping_status]').val(self.tab_modal.shipping_status()).trigger('change');
            $('#tab-modal select.select2[name=item_requirement]').val(self.tab_modal.item_requirement()).trigger('change');
            $('#tab-modal select.select2[name=order_status]').val(self.tab_modal.order_status()).trigger('change');

            MODAL.show('#tab-modal');
        },
        edit: function () {
            let tab = self.tabs.current();
            console.log(tab);

            self.tab_modal.id(tab.id());
            self.tab_modal.order(tab.order());
            self.tab_modal.name(tab.name());
            self.tab_modal.payment_status(tab.payment_status());
            self.tab_modal.staff_id(tab.staff_id());
            self.tab_modal.shipping_status(tab.shipping_status());
            self.tab_modal.tracking_status(tab.tracking_status());
            self.tab_modal.label_id(tab.labels());
            self.tab_modal.item_status(tab.item_status());
            self.tab_modal.item_requirement(tab.item_requirement());
            self.tab_modal.order_status(tab.order_status());
            self.tab_modal.labels.each((x) => {
                $(`#tab-modal select.select2[rel=${x.id()}]`).val(self.tab_modal.label_id()).trigger('change');
            });

            $('#tab-modal select.select2[name=payment_status]').val(self.tab_modal.payment_status()).trigger('change');
            $('#tab-modal select.select2[name=item_status]').val(self.tab_modal.item_status()).trigger('change');
            $('#tab-modal select.select2[name=staff_id]').val(self.tab_modal.staff_id()).trigger('change');
            $('#tab-modal select.select2[name=tracking_status]').val(self.tab_modal.tracking_status()).trigger('change');
            $('#tab-modal select.select2[name=shipping_status]').val(self.tab_modal.shipping_status()).trigger('change');
            $('#tab-modal select.select2[name=item_requirement]').val(self.tab_modal.item_requirement()).trigger('change');
            $('#tab-modal select.select2[name=order_status]').val(self.tab_modal.order_status()).trigger('change');

            MODAL.show('#tab-modal');
        },
        save: function () {
            if (!self.tab_modal.isValid())
                self.tab_modal.errors.showAllMessages();
            else {
                let data = {
                    id: self.tab_modal.id(),
                    order: self.tab_modal.order(),
                    name: self.tab_modal.name(),
                    payment_status: self.tab_modal.payment_status(),
                    item_status: self.tab_modal.item_status(),
                    labels: self.tab_modal.label_id(),
                    staff_id: self.tab_modal.staff_id(),
                    shipping_status: self.tab_modal.shipping_status(),
                    tracking_status: self.tab_modal.tracking_status(),
                    item_requirement: self.tab_modal.item_requirement(),
                    order_status: self.tab_modal.order_status(),
                    action: 'save-tab'
                };

                AJAX.post(window.location, data, true, (res) => {
                    if (res.error)
                        ALERT.error(res.message);
                    else {
                        MODAL.hide('#tab-modal');
                        NOTI.success(res.message);
                        data.id = res.id;

                        let new_tab = {
                            id: ko.observable(data.id),
                            order: ko.observable(data.order),
                            name: ko.observable(data.name),
                            payment_status: ko.observableArray(data.payment_status),
                            item_status: ko.observableArray(data.item_status),
                            labels: ko.observableArray(data.labels),
                            staff_id: ko.observableArray(data.staff_id),
                            shipping_status: ko.observableArray(data.shipping_status),
                            tracking_status: ko.observableArray(data.tracking_status),
                            item_requirement: ko.observableArray(data.item_requirement),
                            order_status: ko.observableArray(data.order_status),
                        };

                        let exist_tab = false;
                        //  Kiểm tra tab đã có trong danh sách hiện tại hay chưa
                        for (let i = 0; i < self.tabs.list().length; i++) {
                            if (self.tabs.list()[i].id() == data.id) {
                                self.tabs.list()[i].order(data.order);
                                self.tabs.list()[i].name(data.name);
                                self.tabs.list()[i].payment_status(data.payment_status);
                                self.tabs.list()[i].item_status(data.item_status);
                                self.tabs.list()[i].labels(data.labels);
                                self.tabs.list()[i].staff_id(data.staff_id);
                                self.tabs.list()[i].shipping_status(data.shipping_status);
                                self.tabs.list()[i].tracking_status(data.tracking_status);
                                self.tabs.list()[i].item_requirement(data.item_requirement);
                                self.tabs.list()[i].order_status(data.order_status);
                                self.search(1);

                                exist_tab = true;
                                break;
                            }
                        }

                        if (!exist_tab)
                            self.tabs.list.push(new_tab);

                        if (self.tabs.list().length === 1) {
                            self.tabs.current(new_tab);
                            self.search(1);
                        }

                        self.tabs.list.sortBy_((x) => {
                            return x.order();
                        });
                    }
                });
            }
        }
    };

    //  Nạp dữ liệu cơ sở
    self.init_data = function () {
        $('#export-modal select').select2();

        AJAX.get(window.location, {action: 'init-data'}, false, (res) => {
            if (res.tabs.length > 0) {
                self.tabs.current({
                    id: ko.observable(res.tabs[0].id),
                    order: ko.observable(res.tabs[0].order),
                    name: ko.observable(res.tabs[0].name),
                    payment_status: ko.observableArray(res.tabs[0].payment_status),
                    item_status: ko.observableArray(res.tabs[0].item_status),
                    labels: ko.observableArray(res.tabs[0].labels),
                    staff_id: ko.observableArray(res.tabs[0].staff_id),
                    shipping_status: ko.observableArray(res.tabs[0].shipping_status),
                    tracking_status: ko.observableArray(res.tabs[0].tracking_status),
                    item_requirement: ko.observableArray(res.tabs[0].item_requirement),
                    order_status: ko.observableArray(res.tabs[0].order_status),
                });
            }

            ko.mapping.fromJS(res.users, self.tab_modal.users);
            ko.mapping.fromJS(res.labels, self.tab_modal.labels);
            ko.mapping.fromJS(res.tabs, self.tabs.list);
            $('#tab-modal select.select2').select2();

            if (localStorage.getItem('prev_url') != null) {
                if (localStorage.getItem('prev_url') == 'customer_order_detail') {
                    if (localStorage.getItem('search_data') != null) {
                        var search_data = JSON.parse(localStorage.getItem('search_data'));
                        if (search_data.from != null)
                            self.filter.from(search_data.from);
                        if (search_data.to != null)
                            self.filter.to(search_data.to);
                        if (search_data.time != null)
                            self.filter.time(search_data.time);
                        if (search_data.user_id != null)
                            self.filter.staff_id(parseInt(search_data.user_id));
                        if (search_data.key != null)
                            self.filter.key(search_data.key);

                        self.tabs.current({
                            id: ko.observable(parseInt(search_data.tab_id)),
                            order: ko.observable(search_data.order),
                            payment_status: ko.observableArray(search_data.payment_status),
                            item_status: ko.observableArray(search_data.item_status),
                            labels: ko.observableArray(search_data.labels),
                            staff_id: ko.observableArray(search_data.staff_id),
                            shipping_status: ko.observableArray(search_data.shipping_status),
                            tracking_status: ko.observableArray(search_data.tracking_status),
                            item_requirement: ko.observableArray(search_data.item_requirement),
                            order_status: ko.observableArray(search_data.order_status),
                        });
                        localStorage.removeItem('search_data');
                    }

                    localStorage.removeItem('prev_url');
                }
            }

            model.search(1);
        });
    };

    //  Tìm kiếm sản phẩm theo bộ lọc
    self.search = function (page) {
        let data = {
            page: page,
            time: self.filter.time(),
            from: self.filter.from(),
            to: self.filter.to(),
            user_id: self.filter.staff_id(),
            key: self.filter.key(),
            action: 'search'
        };

        if (self.tabs.current() !== undefined) {
            if (self.tabs.current() !== null) {
                try {
                    data.shipping_status = self.tabs.current().shipping_status();
                    data.staff_id = self.tabs.current().staff_id();
                    data.tracking_status = self.tabs.current().tracking_status();
                    data.item_requirement = self.tabs.current().item_requirement();
                    data.payment_status = self.tabs.current().payment_status();
                    data.item_status = self.tabs.current().item_status();
                    data.labels = self.tabs.current().labels();
                    data.order_status = self.tabs.current().order_status();
                    data.tab_id = self.tabs.current().id();
                } catch (e) {

                }
            }
        }

        localStorage.setItem('search_data', ko.toJSON(data));
        AJAX.get(window.location, data, true, (res) => {
            ko.mapping.fromJS(res.data, self.result);
            ko.mapping.fromJS(res.items, self.items);
            self.pagination(res.pagination);
            self.tabs.total_item(res.pagination.total_item);
        });
    };
	self.calc_subtotal = function (order,order_items){
		let total = 0;
        let sub_total = 0;
        var discount_amount = 0;

        if (order.discount_type() == 'percent')
            order.discount_total(toNumber(order.discount_total()));
        else
            order.discount_total(moneyFormat(order.discount_total()));

        order_items.each((item) => {
            if (item.quantity() !== undefined) {
                if (item.quantity() !== null) {
                    //  Kiểm tra nếu số lượng sản phẩm không phải là số
                    if (isNaN(item.quantity())) {
                        item.quantity(toNumber(item.quantity().toString()));
                    }

                    total += item.quantity() * parseFloat(item.price());
                    sub_total += item.quantity() * parseInt(item.term_fee());
                }
            }
        });
		
        /* self.order.info.total(total); */
		/* fixcode: update TongTien = phi_ship_web + tong_tien */
		/* check neu shipping_fee == empty thi gan bang 0 */
		if(order.web_shipping_fee()==""){
			order.web_shipping_fee(0);
		}
		
		//Tổng tiền đã bao gồm web_shipping_fee (Phí ship web) 
        order.total(total + parseFloat(order.web_shipping_fee()));
        //self.order.info.sub_total(parseFloat(self.order.info.currency_rate()) * total + parseFloat(self.order.info.world_shipping_fee()) * parseFloat(self.order.info.total_weight()));
			
		//Công thức tính world_shipping fee đã có sự thay đổi, nó đc tính trong controller
        /* let world_shipping = parseFloat(self.order.info.total_weight()) * parseInt(self.order.info.country_shipping_fee()); */
		//let world_shipping = parseFloat(order.world_shipping_fee_total());	
		
        //order.world_shipping_fee(world_shipping);
        sub_total += (parseFloat(order.web_shipping_fee()) + total) * toNumber(order.currency_rate())
            + parseInt(order.world_shipping_fee());
        sub_total += parseInt(order.inter_shipping_fee());
        let discount_total = toNumber(order.discount_total());

        if (discount_total >= 0) {
            if (order.discount_type() == 'percent' && discount_total > 100)
                order.discount_total(100);

            if (discount_total < 0)
                order.discount_total(0);

            if (order.discount_type() == 'amount') {
                sub_total -= discount_total;
                discount_amount = discount_total;
            } else {
                discount_amount = sub_total * discount_total / 100;
                if (discount_amount > parseInt(order.discount_max()) && parseInt(order.discount_max()) > 0)
                    discount_amount = parseInt(order.discount_max());

                sub_total -= discount_amount;
            }
        }

        sub_total = sub_total * (100 + parseInt(order.fee_percent())) / 100;
        sub_total = Math.floor(sub_total);
        sub_total = sub_total <= 5 ? 0 : sub_total;

        return sub_total + discount_amount;
	}
    //  Hiển thị các sản phẩm theo đơn hàng
    self.list_items = function (item, event) {
        let nextItems = $(event.currentTarget).parent().parent().next().find('.item-detail');
        if (nextItems.is(':hidden')) {
            let items = self.items.filter((x) => {
                return x.order_id() === item.id();
            });
            ko.mapping.fromJS(items, self.order_items);
            toggle_row(event.currentTarget);
        } else
            nextItems.slideToggle();
    };

    //  Xóa đơn hàng
    self.delete_order = function (item) {
        ALERT.confirm('Bạn chắc chắn muốn xóa đơn hàng?', 'Tất cả thông tin liên quan sẽ bị xóa', () => {
            AJAX.delete(window.location, {id: item.id(), action: 'order'}, true, (res) => {
                if (res.error)
                    ALERT.error(res.message);
                else {
                    self.search(1);
                    NOTI.success(res.message);
                }
            });
        });
    };

    //  Xuất mã tracking
    self.export_tracking = function () {
        AJAX.post(window.location, {action: 'export-tracking'}, true, (res) => {
            self.tracking_code(res.data.join('\n'));
            $('#tracking-code-modal textarea').linenumbers({
                col_width: '25px',
                start: 1,
                digits: 4

            });

            MODAL.show('#tracking-code-modal');
            $('#tracking-code-modal textarea').focus();
        });
    };

    //  Cập nhật tracking
    self.update_tracking = {
        data: ko.observable(),
        show: function () {
            MODAL.show('#update-tracking-modal');
        },
        post: function () {
            if (self.update_tracking.data() == undefined) {
                NOTI.danger('Vui lòng nhập thông tin tracking');
                return;
            }

            let data = self.update_tracking.data().split('\n');
            if (data.length === 0)
                NOTI.danger('Vui lòng nhập thông tin tracking');
            else {
                let valid_data = [];
                for (let i = 0; i < data.length; i++) {
                    let temp = data[i].split('\t');
                    if (temp.length >= 5) {
                        if (temp[4].toLocaleLowerCase() == 'delivered') {
                            let tracking = temp[0];
                            let date = temp[3].split(',')[0];
                            valid_data.push({tracking: tracking, date: date});
                        }
                    }
                }

                if (valid_data.length > 0) {
                    AJAX.post(window.location, {action: 'update-tracking', data: valid_data}, true, (res) => {
                        if (res.error)
                            ALERT.error(res.message);
                        else {
                            self.result.each(function (t) {
                                t.items.each(function (x) {
                                    for (let i = 0; i < valid_data.length; i++) {
                                        if (valid_data[i].tracking == x.tracking_code()) {
                                            x.delivered_date(moment(valid_data[i].date).format('X'));
                                        }
                                    }
                                });
                            });
                            MODAL.hide('#update-tracking-modal');
                            ALERT.success(res.message);
                        }
                    });
                } else {
                    ALERT.error('Không có tracking nào đã được giao');
                }
            }
        }
    };

    self.export_modal = {
        user_id: ko.observableArray([]),
        show: function () {
            self.export_modal.user_id([]);
            $('#export-modal select').val(self.export_modal.user_id()).trigger('change');
            MODAL.show('#export-modal');
        },
        do: function () {
            let data = {
                time: self.filter.time(),
                from: self.filter.from(),
                to: self.filter.to(),
                user_id: self.export_modal.user_id(),
                key: self.filter.key(),
                status: self.filter.status(),
                action: 'export-items',
            };

            AJAX.post(window.location, data, true, (res) => {
                if (!res.error) {
                    MODAL.hide('#export-modal');
                    window.location = '/basso/customer_order/export';
                }
            });
        }
    };
}

let model = new viewModel();
model.init_data();

ko.validatedObservable(model.tab_modal);
ko.applyBindings(model, document.getElementById('main-content'));