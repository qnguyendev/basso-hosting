<table style="width: 100%; margin: 10px 0 30px" cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="4" style="padding: 10px 0">
			<strong style="text-transform: uppercase; font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #f3a23b">
				THÔNG TIN CHUYỂN KHOẢN
			</strong>
		</td>
	</tr>
	<tr>
		<td align="center" width="50%"
			style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE">
			<strong>Ngân hàng Ngoại Thương Vietcombank</strong>
		</td>
		<td align="center" width="50%"
			style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-left: none">
			<strong>Ngân hàng BIDV</strong>
		</td>
	</tr>
	<tr>
		<td style="font-family: Arial, Helvetica, sans-serif; padding: 15px; line-height: 1.6; color: #00354E; border: solid 1px #00354E; border-top: 0">
			<ul style="margin: 0; list-style: none; padding: 0">
				<li>STK: 0011004213524</li>
				<li>Nguyễn Thị Thùy Dương</li>
				<li>Chi nhánh Sở Giao dịch, HN</li>
			</ul>
		</td>
		<td style="font-family: Arial, Helvetica, sans-serif; padding: 15px; line-height: 1.6; color: #00354E; border: solid 1px #00354E; border-top: 0; border-left: 0">
			<ul style="margin: 0; list-style: none; padding: 0">
				<li>STK: 15010000615850</li>
				<li>Nguyễn Thị Thùy Dương</li>
				<li>Chi nhánh Bắc Hà Nội, HN</li>
			</ul>
		</td>
	</tr>
	<tr>
		<td align="center"
			style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-top: 0">
			<strong>Ngân hàng Techcombank</strong>
		</td>
		<td align="center"
			style="font-family: Arial, Helvetica, sans-serif; padding: 7px; color: #00354E; border: solid 1px #00354E; background: #EEE; border-top: 0; border-left: none">
			<strong>Ngân hàng Vietinbank</strong>
		</td>
	</tr>
	<tr>
		<td style="font-family: Arial, Helvetica, sans-serif; padding: 15px; line-height: 1.6; color: #00354E; border: solid 1px #00354E; border-top: 0">
			<ul style="margin: 0; list-style: none; padding: 0">
				<li>STK: 19032994116991</li>
				<li>Nguyễn Thị Thùy Dương</li>
				<li>Chi nhánh Chương Dương, HN</li>
			</ul>
		</td>
		<td style="font-family: Arial, Helvetica, sans-serif; padding: 15px; line-height: 1.6; color: #00354E; border: solid 1px #00354E; border-top: 0; border-left: 0">
			<ul style="margin: 0; list-style: none; padding: 0">
				<li>STK: 103868789731</li>
				<li>Nguyễn Thị Thùy Dương</li>
				<li>Chi nhánh Bắc Hà Nội, HN</li>
			</ul>
		</td>
	</tr>
</table>