<strong style="text-transform: uppercase; font-family: Arial, Helvetica, sans-serif; font-size: 16px; color: #f3a23b; font-size: 14px">
	LƯU Ý:
</strong>

<div style="font-size: 13px; color: #00354E; padding:10px; font-family: Arial, Helvetica, sans-serif; line-height: 1.6">
	<ul style="margin: 5px; padding: 0">
		<li>Thời gian hàng về Việt Nam: Dự kiến 2 – 3 tuần. Để chủ động theo dõi hành trình đơn hàng, Quý khách có
			thể truy cập <a href="https://basso.eprtech.com/don-hang?id={{$order->order_code}}">tại đây</a>.
		</li>
		<li>
			Trường hợp Quý khách có những băn khoăn về đơn hàng, có thể xem ở mục các <a
					href="https://basso.eprtech.com/tro-giup?id=27">câu hỏi thường gặp</a>.
		</li>
		<li>
			Quý khách cần được hỗ trợ ngay? Chỉ cần email <strong><a style="color: #00354E;"
																	 href="mailto:cskh@basso.vn">cskh@basso.vn</a></strong>,
			hoặc gọi số điện thoại <strong><a style="color: #00354E;" href="tel:0965687790">0965687790</a></strong>
			(từ
			9h - 18h, Thứ 2 - Chủ
			Nhật). Đội ngũ Basso luôn sẵn sàng hỗ trợ Quý
			khách bất kì lúc nào.
		</li>
	</ul>
</div>