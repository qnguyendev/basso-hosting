<table style="width: 100%; margin-top: 20px; font-size: 13px" cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td align="center" colspan="2"
			style="background-color: #00354E; padding:20px; color: #FFF; line-height: 1.6; font-family: Arial, Helvetica, sans-serif;">
			CÔNG TY TNHH SẢN XUẤT VÀ THƯƠNG MẠI THIÊN LONG<br/>
			Mã số thuế: 0101474581<br/>
			<a href="https://BASSO.VN" target="_blank" style="color: #FFF">BASSO.VN</a> – dịch vụ mua hàng quốc tế
			hàng đầu Việt Nam.
		</td>
	</tr>
	<tr>
		<td align="center" width="50%"
			style="background-color: #00354E; padding:20px; color: #FFF; line-height: 1.6; font-family: Arial, Helvetica, sans-serif;">
			<strong>Chi nhánh Hà Nội</strong><br/>
			493 Nguyễn Văn Cừ, Long Biên, HN
		</td>
		<td align="center" width="50%"
			style="background-color: #00354E; padding:20px; color: #FFF; line-height: 1.6; font-family: Arial, Helvetica, sans-serif;">
			<strong>Chi nhánh Hồ Chí Minh</strong><br/>
			60 Lê Trung Nghĩa, P12, Q.Tân Bình, HCM
		</td>
	</tr>
</table>