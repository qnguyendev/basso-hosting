<?php if (!defined('BASEPATH')) exit('No direct access script allowed');
global $user_roles, $current_user;
?>
<div role="tabpanel" style="margin: 0 -1px -1px -1px">
    <!-- Nav tabs-->
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item" role="presentation">
            <a class="nav-link active" href="#paymentadd" aria-controls="paymentadd" role="tab"
               data-toggle="tab">
                Thanh toán
            </a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" href="#paymenthistory" aria-controls="orderlabel" role="tab"
               data-toggle="tab">
                Lịch sử
            </a>
        </li>
    </ul>
    <!-- Tab panes-->
    <div class="tab-content">
        <form class="tab-pane active" id="paymentadd" role="tabpanel">
            <table class="table border-vertical">
                <tbody>
                <tr>
                    <th class="text-right" width="80">
                        Thời gian
                        <span class="text-danger">*</span>
                    </th>
                    <td><input type="text" class="form-control" data-bind="datePicker: payment.created_time"/></td>
                </tr>
                <tr>
                    <th class="text-right">
                        Số tiền
                        <span class="text-danger">*</span>
                    </th>
                    <td>
                        <div class="input-group">
                            <input type="text" name="payment_amount" class="form-control" data-bind="moneyMask: payment.amount"/>
                            <div class="input-group-append">
                                <span class="input-group-text">đ</span>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th class="text-right">Ngân hàng</th>
                    <td>
                        <select class="form-control" data-bind="value: payment.bank_id, options: data.banks,
                            optionsText: 'name', optionsValue: 'id'"></select>
                    </td>
                </tr>
                <tr>
                    <th class="text-right">
                        Nội dung
                        <span class="text-danger">*</span>
                    </th>
                    <td>
                        <input type="text" class="form-control" data-bind="value: payment.note" autocomplete="off"/>
                    </td>
                </tr>
                <tr>
                    <th class="text-right">PTTT</th>
                    <td>
                        <select class="form-control" name="payment_type" data-bind="value: payment.type">
                            @foreach(PaymentType::LIST_STATE as $key=>$value)
                                <option value="{{$key}}">{{$value}}</option>
                            @endforeach
                        </select>
                    </td>
                </tr>
                </tbody>
            </table>
            <div class="text-center mt-2">
                <button class="btn btn-primary text-uppercase btn-sm"
                        data-bind="click: payment.add, visible: $root.order.info.status() != '{{CustomerOrderStatus::CANCELLED}}'
                                        && $root.order.info.status() != '{{CustomerOrderStatus::COMPLETED}}'">
                    <i class="fa fa-check"></i>
                    Thêm thanh toán
                </button>
            </div>
        </form>
        <div class="tab-pane" id="paymenthistory" role="tabpanel">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>STT</th>
                    <th>Thời gian</th>
                    <th>Số tiền</th>
                    <th>PTTT</th>
                    @if(in_array('admin', $user_roles))
                        <th></th>
                    @endif
                </tr>
                </thead>
                <tbody class="text-center" data-bind="foreach: payment.histories">
                <tr>
                    <th data-bind="text: $index() + 1"></th>
                    <td>
                        <a href="javascript:"
                           data-bind="text: moment.unix(created_time()).format('DD/MM/YYYY'),
                                   popover: '#payment-' + id()">
                        </a>
                        <div data-bind="attr: {id: 'payment-' + id()}" style="display: none">
                            <div class="popover-heading">
                                Thời gian
                                <span data-bind="text: moment.unix(created_time()).format('DD/MM/YYYY')"></span>
                            </div>
                            <div class="popover-body">
                                <table class="table border-vertical">
                                    <tbody>
                                    <tr>
                                        <th class="text-right" width="110">Phương thức</th>
                                        <td data-bind="text: type_name"></td>
                                    </tr>
                                    <tr>
                                        <th class="text-right">Số tiền</th>
                                        <td data-bind="text: parseInt(amount()).toMoney(0)"></td>
                                    </tr>
                                    <tr>
                                        <th class="text-right">Thời gian</th>
                                        <td data-bind="text: moment.unix(created_time()).format('DD/MM/YYYY')"></td>
                                    </tr>
                                    <tr>
                                        <th class="text-right">Ngân hàng</th>
                                        <td data-bind="text: bank"></td>
                                    </tr>
                                    <tr>
                                        <th class="text-right">Trạng thái</th>
                                        <td>
                                            @foreach(PaymentHistoryStatus::LIST_STATE as $key=>$value)
                                                <span class="badge badge-{{$value['class']}}"
                                                      data-bind="visible: status() == '{{$key}}'">
                                                            {{$value['name']}}
                                                        </span>
                                            @endforeach
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="text-right">Nội dung</th>
                                        <td data-bind="text: note"></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </td>
                    <td data-bind="text: parseInt(amount()).toMoney(0)"></td>
                    <td data-bind="text: type_name"></td>
                    @if(in_array('admin', $user_roles) || in_array('accounting_manager', $user_roles) )
                        <td>
                            <a href="#" data-bind="click: $root.payment.edit">
                                <i class="fa fa-edit"></i>
                                Sửa
                            </a>
                        </td>
                    @endif
                </tr>
                </tbody>
                <tbody>
                <tr>
                    <th colspan="2" class="text-center">TỔNG TIỀN</th>
                    <th class="text-center" data-bind="text: parseInt(order.info.total_paid()).toMoney(0)"></th>
                    <th></th>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
