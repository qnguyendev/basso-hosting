<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<!-- Modal -->
<div class="modal fade" id="shipping-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel">Thêm đơn vận chuyển</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form autocomplete="off" class="modal-body">
                <table class="table table-striped table-bordered mb-3">
                    <thead>
                    <tr>
                        <th width="40">
                            <div class="checkbox c-checkbox w-100 mt-2">
                                <label>
                                    <input type="checkbox"
                                           data-bind="checked: shipping_modal.selected_items().length === shipping_modal.items().length
                                           && shipping_modal.selected_items().length > 0,
                                           event: {change: shipping_modal.check_all}"/>
                                    <span class="fa fa-check"></span>
                                </label>
                            </div>
                        </th>
                        <th colspan="2">Sản phẩm</th>
                        <th>Variants</th>
                        <th>SL</th>
                    </tr>
                    </thead>
                    <tbody data-bind="foreach: shipping_modal.items">
                    <tr data-bind="visible: status() === '{{CustomerOrderItemStatus::IN_INVENTORY}}'">
                        <td>
                            <div class="checkbox c-checkbox w-100 mt-2">
                                <label>
                                    <input type="checkbox"
                                           data-bind="checked: $root.shipping_modal.selected_items, value: id"/>
                                    <span class="fa fa-check"></span>
                                </label>
                            </div>
                        </td>
                        <td width="70">
                            <img data-bind="attr: {src: image_path()}" class="img-fluid ie-fix-flex"/>
                        </td>
                        <td data-bind="text: name"></td>
                        <td>
                            <!--ko foreach: variations-->
                            <b data-bind="text: name"></b>: <span data-bind="text: value"></span><br/>
                            <!--/ko-->
                        </td>
                        <td class="text-center" data-bind="text: quantity"></td>
                    </tr>
                    </tbody>
                </table>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="font-weight-bold">Thông tin vận chuyển</label>
                            <table class="table border-vertical">
                                <tr>
                                    <th class="text-right font-weight-normal">
                                        <span class="text-danger">*</span>
                                        Người nhận
                                    </th>
                                    <td><input type="text" class="form-control input-sm"
                                               data-bind="value: shipping_modal.customer_info.name"/></td>
                                </tr>
                                <tr>
                                    <th class="text-right font-weight-normal">
                                        <span class="text-danger">*</span>
                                        Điện thoại
                                    </th>
                                    <td><input type="text" class="form-control input-sm"
                                               data-bind="value: shipping_modal.customer_info.phone"/></td>
                                </tr>
                                <tr>
                                    <th class="text-right font-weight-normal">
                                        <span class="text-danger">*</span>Địa chỉ
                                    </th>
                                    <td><input type="text" class="form-control input-sm"
                                               data-bind="value: shipping_modal.customer_info.address"/></td>
                                </tr>
                                <tr>
                                    <th class="text-right font-weight-normal">
                                        <span class="text-danger">*</span>
                                        Tỉnh/thành
                                    </th>
                                    <td>
                                        <select class="form-control input-sm"
                                                data-bind="options: data.cities, optionsText: 'name', optionsValue: 'id',
                                                value: shipping_modal.customer_info.city_id, event: {change: data.change_city}"></select>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-right font-weight-normal">
                                        <span class="text-danger">*</span>
                                        Quận/huyện
                                    </th>
                                    <td>
                                        <select class="form-control input-sm"
                                                data-bind="options: data.districts, optionsText: 'name', optionsValue: 'id',
                                                value: shipping_modal.customer_info.district_id">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-right font-weight-normal">Ghi chú</th>
                                    <td>
                                        <textarea class="form-control input-sm no-resize"
                                                  data-bind="value: shipping_modal.customer_info.note"></textarea>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label class="font-weight-bold">Đơn vị vận chuyển</label>
                            <table class="table border-vertical">
                                <tr>
                                    <th class="text-right font-weight-normal font-weight-normal">Đơn vị</th>
                                    <td>
                                        <select class="form-control input-sm"
                                                data-bind="options: data.shipping_agencies, optionsText: 'name',
                                                optionsValue: 'id', value: shipping_modal.shipping_info.id"></select>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-right font-weight-normal">Mã vận đơn</th>
                                    <td>
                                        <input type="text" class="form-control input-sm"
                                               data-bind="value: shipping_modal.shipping_info.code"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-right font-weight-normal">Phí ship trả shipper</th>
                                    <td>
                                        <div class="input-group">
                                            <input type="text" class="form-control input-sm text-right"
                                                   data-bind="moneyMask: shipping_modal.shipping_info.shipping_fee"/>
                                            <div class="input-group-append">
                                                <span class="input-group-text text-sm">đ</span>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-right font-weight-normal">Phí ship khách trả</th>
                                    <td>
                                        <div class="input-group">
                                            <input type="text" class="form-control input-sm text-right"
                                                   data-bind="moneyMask: shipping_modal.shipping_info.fee,
                                                   event: {change: shipping_modal.calc_total}"/>
                                            <div class="input-group-append">
                                                <span class="input-group-text text-sm">đ</span>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-right font-weight-normal">Tiền thu hộ COD</th>
                                    <td>
                                        <div class="input-group">
                                            <input type="text" class="form-control input-sm text-right"
                                                   data-bind="moneyMask: shipping_modal.shipping_info.cod_amount"/>
                                            <div class="input-group-append">
                                                <span class="input-group-text text-sm">đ</span>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td>
                                        <div class="checkbox c-checkbox w-100">
                                            <label>
                                                <input type="checkbox"
                                                       data-bind="event: {change: shipping_modal.calc_total}, checked: shipping_modal.include_fee"/>
                                                <span class="fa fa-check"></span>
                                                Khách trả ship
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="form-group">
                            <label class="font-weight-bold">Thông tin gói hàng</label>
                            <table class="table border-vertical">
                                <tr>
                                    <th class="text-right font-weight-normal">Khối lượng (kg)</th>
                                    <td>
                                        <input type="number" class="form-control input-sm" min="0" step="0.1"
                                               data-bind="value: shipping_modal.shipping_info.weight"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th class="text-right font-weight-normal">Kích thước</th>
                                    <td>
                                        <input type="text" class="form-control input-sm" placeholder="dài, rộng, cao"
                                               data-bind="value: shipping_modal.shipping_info.size"/>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Thoát</button>
                <button type="button" class="btn btn-success" data-bind="click: shipping_modal.save">
                    <em class="fa fa-check"></em>
                    TẠO ĐƠN
                </button>
            </div>
        </div>
    </div>
</div>
