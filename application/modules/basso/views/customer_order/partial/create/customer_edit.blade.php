<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="editor-customer-modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Thêm khách hàng
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form autocomplete="off" class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-lg-6">
                        <div class="form-group">
                            <label>
                                Khách hàng
                                <span class="text-danger">*</span>
                                <span class="validationMessage" data-bind="validationMessage: modal.name"></span>
                            </label>
                            <input type="text" class="form-control" data-bind="value: modal.name"/>
                        </div>
                        <div class="form-group">
                            <label>Giới tính</label>
                            <select class="form-control" data-bind="value: modal.gender">
                                <option value="male">Nam</option>
                                <option value="female">Nữ</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label> Email </label>
                            <input type="text" class="form-control" data-bind="value: modal.email"/>
                        </div>
                        <div class="form-group">
                            <label>
                                Điện thoại
                                <span class="text-danger">*</span>
                                <span class="validationMessage" data-bind="validationMessage: modal.phone"></span>
                            </label>
                            <input type="text" class="form-control" data-bind="value: modal.phone"/>
                        </div>
                        <div class="form-group">
                            <label>Phân nhóm</label>
                            <select class="form-control" data-bind="options: data.groups, value: modal.group_id,
                                optionsText: 'name', optionsValue: 'id'"></select>
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-6">
                        <div class="form-group">
                            <label> Địa chỉ </label>
                            <input type="text" class="form-control" data-bind="value: modal.address"/>
                        </div>
                        <div class="form-group">
                            <label>Tỉnh/thành</label>
                            <select class="form-control"
                                    data-bind="options: data.cities, optionsText: 'name', optionsValue: 'id',
                                    value: modal.city_id, event:{change: modal.change_city}"></select>
                        </div>
                        <div class="form-group">
                            <label>
                                Quận/huyện
                            </label>
                            <select class="form-control"
                                    data-bind="options: data.districts, optionsText: 'name', optionsValue: 'id',
                                    value: modal.district_id"></select>
                        </div>
                        <div class="form-group">
                            <label>Ghi chú</label>
                            <input type="text" class="form-control" data-bind="value: modal.note" autocomplete="of"/>
                        </div>
                    </div>
                </div>
            </form>
            <div class="modal-footer text-center">
                <button class="btn btn-success" data-bind="click: modal.save">
                    <i class="fa fa-check"></i>
                    Lưu lại
                </button>
            </div>
        </div>
    </div>
</div>