<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
global $user_roles, $current_user;
?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-sm-12 col-lg-8">
            @include('partial/detail/customer_info')
            @include('partial/detail/items')
            @include('partial/detail/shipping')
        </div>
        <div class="col-sm-12 col-lg-4">
            @include('partial/detail/label')
            @include('partial/detail/payment_summary')
        </div>
    </div>
@endsection

@section('top')
    <a href="#" class="mr-3" data-bind="click: log_modal.show">
        <i class="fa fa-eye"></i>
        Lịch sử thao tác
    </a>
    <!--ko if: order.info.status() !== '{{CustomerOrderStatus::CANCELLED}}'-->
    <a href="{{base_url('basso/customer_order/invoice/'. $order->id)}}" class="btn btn-info btn-xs" target="_blank">
        <i class="icon-printer"></i>
        In hóa đơn
    </a>
    <div class="btn-group">
        <button class="btn btn-xs btn-success dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
            <i class="icon-cursor"></i>
            Gửi Email
        </button>
        <div class="dropdown-menu" role="menu" x-placement="top-start">
            <a class="dropdown-item" href="#" data-bind="click: function(){ $root.send_email('receive_items') }">Báo hàng về</a>
            <a class="dropdown-item" href="#" data-bind="click: function(){ $root.send_email('confirm_order') }">Xác nhận đơn hàng</a>
            <a class="dropdown-item" href="#" data-bind="click: function(){ $root.send_email('send_invoice') }">Gửi Invoice</a>
        </div>
    </div>
	
    <button class="btn btn-danger btn-xs"
            data-bind="visible: (order.info.status() == '{{CustomerOrderStatus::PENDING}}' ||
            order.info.status() == '{{CustomerOrderStatus::PROCESSING}}' ) && order.info.items_bought() == 0, click: order.cancel">
        <i class="icon-ban"></i>
        Hủy đơn hàng
    </button>
    <a class="btn btn-primary btn-xs" href="/basso/return_order/create/{{$order->id}}"
       data-bind="visible: order.info.status() == '{{CustomerOrdersReturnedStatus::COMPLETED}}'">
        <i class="fa fa-recycle"></i>
        Hoàn đơn
    </a>
                                       
    <!--/ko-->
@endsection

@section('modal')
    @include('partial/detail/add_item_modal');
    @include('partial/detail/edit_customer_info_modal');
    @include('partial/detail/shipping_modal');
    @include('partial/detail/log_modal');
    @include('partial/detail/edit_payment_modal');
@endsection

@section('script')
    <script>
        let in_inventory = '{{CustomerOrderItemStatus::IN_INVENTORY}}';
    </script>
    <script src="{{load_js('detail')}}"></script>
@endsection