<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-7 col-xl-8">

            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">Khách hàng</div>
                </div>
                <div class="card-body">
                    <table class="table border-vertical">
                        <tr>
                            <th>Họ tên</th>
                            <td>{{$order->name}}</td>
                            <th>Điện thoại</th>
                            <td>{{$order->phone}}</td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>{{$order->email}}</td>
                            <th>Địa chỉ</th>
                            <td>
                                {{$order->shipping_address}},
                                {{$order->district}},
                                {{$order->city}}
                            </td>
                        </tr>

                    </table>
                </div>
            </div>

            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">Đơn hàng #{{$order->id}}</div>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th width="50">
                                <div class="checkbox c-checkbox w-100 mt-2">
                                    <label>
                                        <input type="checkbox"
                                               data-bind="checked: selected_items().length === items().length
                                           && selected_items().length > 0, event: {change: check_all}"/>
                                        <span class="fa fa-check"></span>
                                    </label>
                                </div>
                            </th>
                            <th colspan="2">Sản phẩm</th>
                            <th width="140">Giá trả hàng</th>
                            <th width="80">SL</th>
                            <th>Thành tiền</th>
                        </tr>
                        </thead>
                        <tbody data-bind="foreach: items">
                        <tr>
                            <td class="text-center">
                                <div class="checkbox c-checkbox w-100 mt-2">
                                    <label>
                                        <input type="checkbox"
                                               data-bind="checked: $root.selected_items, value: id, event: {change: $root.calc()}"/>
                                        <span class="fa fa-check"></span>
                                    </label>
                                </div>
                            </td>
                            <td width="70">
                                <img data-bind="attr: {src: image_path()}" class="img-fluid ie-fix-flex"/>
                            </td>
                            <td data-bind="text: name"></td>
                            <td>
                                <div class="input-group">
                                    <input type="text" class="form-control text-right"
                                           data-bind="moneyMask: formatted_price, event: {change: $root.calc()}"/>
                                    <div class="input-group-append">
                                        <span class="input-group-text">đ</span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <input type="number" min="0" data-bind="value: quantity, event: {change: $root.calc()}"
                                       class="form-control"/>
                            </td>
                            <td class="text-right pr-3"
                                data-bind="text: parseInt(quantity() * price()).toMoney(0)"></td>
                        </tr>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="4">TỔNG</th>
                            <th data-bind="text: summary.quantity" class="text-left pl-4"></th>
                            <th class="text-right pr-3" data-bind="text: parseInt(summary.total()).toMoney(0)"></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-5 col-xl-4">
            <form autocomplete="off" class="card card-default">
                <div class="card-header">
                    <div class="card-title">Hoàn tiền</div>
                </div>
                <div class="card-body">
                    <table class="table border-vertical">
                        <tbody>
                        <tr>
                            <th class="text-right" width="80">
                                Thời gian
                            </th>
                            <td><input type="text" class="form-control" data-bind="datePicker: payment.created_time"/>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-right">
                                Số tiền
                            </th>
                            <td>
                                <div class="input-group">
                                    <input type="text" class="form-control" data-bind="moneyMask: payment.amount"/>
                                    <div class="input-group-append">
                                        <span class="input-group-text">đ</span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-right">Ngân hàng</th>
                            <td>
                                <select class="form-control" data-bind="value: payment.bank_id">
                                    <option value="0">- Lựa chọn -</option>
                                    @foreach($banks as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-right">
                                Nội dung
                            </th>
                            <td>
                                <input type="text" class="form-control" data-bind="value: payment.note"
                                       autocomplete="off"/>
                            </td>
                        </tr>
                        <tr>
                            <th class="text-right">PTTT</th>
                            <td>
                                <select class="form-control" name="payment_type" data-bind="value: payment.type">
                                    @foreach(PaymentType::LIST_STATE as $key=>$value)
                                        <option value="{{$key}}">{{$value}}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </form>

            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">Ghi chú</div>
                </div>
                <div class="card-body">
                    <textarea class="form-control" data-bind="value: note" cols="5" rows="4"></textarea>
                </div>
                <div class="card-footer text-center">
                    <button class="btn btn-success text-uppercase" data-bind="click: save">
                        <i class="fa fa-check"></i>
                        Trả hàng
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        let order = {{json_encode($order)}};
        let items = {{json_encode($items)}};
    </script>
    <script src="{{load_js('create')}}"></script>
@endsection