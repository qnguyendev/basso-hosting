function viewModel() {
    let self = this;
    self.result = ko.mapping.fromJS([]);
    self.pagination = ko.observable();

    self.search = function (page) {
        AJAX.get(window.location, {page: page}, true, (res) => {
            ko.mapping.fromJS(res.data, self.result);
            self.pagination(res.pagination);
        });
    };

    self.modal = {
        id: ko.observable(0),
        name: ko.observable().extend({required: {message: LABEL.required}}),
        active: ko.observable(1),
        min_fee: ko.observable(0),
        steps: ko.observableArray([]),
        terms: ko.observableArray([]),
        exclude: ko.observableArray([]),
        show: function () {
            self.modal.terms([]);
            self.modal.id(0);
            self.modal.name(undefined);
            self.modal.min_fee(0);
            self.modal.active(1);
            self.modal.steps([]);
            self.modal.errors.showAllMessages(false);
            MODAL.show('#editor-modal');
        },
        add_step: function () {
            self.modal.steps.push({
                step: ko.observable(0).extend({required: {message: LABEL.required}}),
                type: ko.observable($('#editor-modal select option:first').val()),
                amount: ko.observable(0).extend({required: {message: LABEL.required}})
            });
        },
        remove_step: function (step) {
            self.modal.steps.remove(step);
        },
        edit: function (item) {
            self.modal.id(item.id());
            self.modal.name(item.name());
            self.modal.active(item.active());
            self.modal.min_fee(item.min_fee());
            self.modal.steps(item.steps());
            self.modal.steps.each((x) => {
                x.amount(moneyFormat(x.amount()));
            });

            self.modal.terms([]);
            item.categories.each((x) => {
                self.modal.terms.push({
                    name: ko.observable(x.category()),
                    type: ko.observable(x.type())
                });
            });

            MODAL.show('#editor-modal');
        },
        add_term: function () {
            self.modal.terms.push({
                name: ko.observable(),
                type: ko.observable()
            });
        },
        remove_term: function (item) {
            self.modal.terms.remove(item);
        },
        save: function () {
            if (!self.modal.isValid())
                self.modal.errors.showAllMessages();
            else {
                self.modal.steps.each((x) => {
                    x.amount(toNumber(x.amount()));
                });

                let data = {
                    id: self.modal.id(),
                    name: self.modal.name(),
                    active: self.modal.active(),
                    steps: self.modal.steps(),
                    min_fee: self.modal.min_fee(),
                    categories: self.modal.terms()
                };

                AJAX.post(window.location, data, true, (res) => {
                    if (res.error)
                        NOTI.danger(res.message);
                    else {
                        self.search(1);
                        MODAL.hide('#editor-modal');
                        NOTI.success(res.message);
                    }
                });
            }
        }
    };

    self.delete = function (item) {
        ALERT.confirm('Xác nhận xóa danh mục', null, function () {
            AJAX.delete(window.location, {id: item.id()}, true, (res) => {
                if (res.error)
                    NOTI.danger(res.message);
                else {
                    NOTI.success(res.message);
                    self.search(1);
                }
            });
        });
    };
}

let model = new viewModel();
ko.validatedObservable(model.modal);
model.search(1);
ko.applyBindings(model, document.getElementById('main-content'));