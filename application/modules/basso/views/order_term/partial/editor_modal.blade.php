<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="editor-modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"
                    data-bind="text: modal.id() == 0 ? 'Thêm danh mục phụ thu' : 'Cập nhật danh mục phụ thu'">
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form autocomplete="off" class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-lg-6">
                        <div class="form-group">
                            <label>
                                Tên danh mục
                                <span class="text-danger">*</span>
                                <span class="validationMessage" data-bind="validationMessage: modal.name"></span>
                            </label>
                            <input type="text" class="form-control" data-bind="value: modal.name"/>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-12 col-lg-6">
                                <label>Phí tối thiểu (ngoại tệ)</label>
                                <input type="number" class="form-control" data-bind="value: modal.min_fee"/>
                            </div>
                            <div class="form-group col-md-12 col-lg-6">
                                <label>Trạng thái</label>
                                <select class="form-control" data-bind="value: modal.active">
                                    <option value="1">Kích hoạt</option>
                                    <option value="0">Không kích hoạt</option>
                                </select>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th width="140">Giá sản phẩm</th>
                                <th>Kiểu</th>
                                <th width="140">Giá trị</th>
                                <th width="60"></th>
                            </tr>
                            </thead>
                            <tbody data-bind="foreach: modal.steps">
                            <tr>
                                <td>
                                    <input type="number" min="0" class="form-control input-sm" data-bind="value: step"/>
                                </td>
                                <td>
                                    <select class="form-control input-sm" data-bind="value: type">
                                        @foreach(OrderTermType::LIST_STATE as $key=>$value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <input type="text" class="form-control input-sm" data-bind="moneyMask: amount"/>
                                        <div class="input-group-append">
                                    <span class="input-group-text text-sm"
                                          data-bind="text: type() == '{{OrderTermType::PERCENT}}' ? '%' : 'đ'">
                                    </span>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <a href="#" data-bind="click: $root.modal.remove_step"
                                       class="text-danger font-weight-bold">
                                        <i class="fa fa-trash"></i>
                                        Xóa
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Danh mục áp dụng</th>
                                <th>Kiểu</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody data-bind="foreach: modal.terms">
                            <tr>
                                <td>
                                    <input type="text" class="form-control" data-bind="value: name"/>
                                </td>
                                <td>
                                    <select class="form-control" data-bind="value: type">
                                        @foreach(OrderTermCategoryType::LIST_STATE as $key=>$value)
                                            <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td class="text-center">
                                    <a href="#" data-bind="click: $root.modal.remove_term"
                                       class="text-danger text-bold">
                                        <i class="icon-trash"></i>
                                        xóa
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th colspan="3">
                                    <button class="btn btn-primary btn-block" data-bind="click: modal.add_term">
                                        <i class="icon-plus"></i>
                                        Thêm danh mục
                                    </button>
                                </th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </form>
            <div class="modal-footer text-center">
                <a href="#" class="text-primary font-weight-bold" data-bind="click: modal.add_step">
                    <i class="fa fa-plus"></i>
                    Thêm mức phụ thu
                </a>
                <button class="btn btn-success" data-bind="click: modal.save">
                    <i class="fa fa-check"></i>
                    Lưu lại
                </button>
            </div>
        </div>
    </div>
</div>