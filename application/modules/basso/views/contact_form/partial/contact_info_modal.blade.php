<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="contact-info-modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-notice modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    THÔNG TIN LIÊN HỆ
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>
                        Nội dung liên hệ
                        <span class="text-danger"></span>
                        <span class="validationMessage" data-bind="validationMessage: contact_info_modal.data"></span>
                    </label>
                    <textarea data-bind="ckeditor: $root.contact_info_modal.data"></textarea>
                </div>
            </div>
            <div class="modal-footer text-center">
                <button class="btn btn-success" data-bind="click: contact_info_modal.save">
                    <i class="fa fa-check"></i>
                    Lưu lại
                </button>
            </div>
        </div>
    </div>
</div>