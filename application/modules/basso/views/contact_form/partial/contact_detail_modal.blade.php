<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="modal fade" id="detail-modal" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-notice modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" data-bind="text: `Liên hệ từ ${detail_modal.name()}`">
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table border-vertical">
                    <tr>
                        <th class="text-right" width="120px">Người gửi</th>
                        <td data-bind="text: detail_modal.name"></td>
                    </tr>
                    <tr>
                        <th class="text-right">Điện thoại</th>
                        <td data-bind="text: detail_modal.phone"></td>
                    </tr>
                    <tr>
                        <th class="text-right">Thời gian</th>
                        <td data-bind="text: moment.unix(detail_modal.created_time()).format('DD/MM/YYYY HH:mm')"></td>
                    </tr>
                    <tr>
                        <th class="text-right">Nội dung</th>
                        <td data-bind="text: detail_modal.content"></td>
                    </tr>
                    <tr>
                        <th class="text-right">Ghi chú</th>
                        <td>
                            <input type="text" class="form-control" data-bind="value: detail_modal.note"/>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer text-center">
                <button class="btn btn-success" data-bind="click: detail_modal.save">
                    <i class="fa fa-check"></i>
                    Lưu lại
                </button>
            </div>
        </div>
    </div>
</div>