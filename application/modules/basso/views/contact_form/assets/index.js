function viewModel() {
    let self = this;
    self.result = ko.mapping.fromJS([]);
    self.pagination = ko.observable();

    self.search = function (page) {
        AJAX.get(window.location, {page: page, action: 'search'}, true, (res) => {
            ko.mapping.fromJS(res.data, self.result);
            self.pagination(res.pagination);
        });

        AJAX.get(window.location, {action: 'contact_info'}, true, function (res) {
            self.contact_info_modal.data(res.data);
        });
    };

    self.detail_modal = {
        name: ko.observable(),
        phone: ko.observable(),
        note: ko.observable(),
        content: ko.observable(),
        created_time: ko.observable(),
        id: ko.observable(),
        show: function (item) {
            self.detail_modal.id(item.id());
            self.detail_modal.name(item.name());
            self.detail_modal.phone(item.phone());
            self.detail_modal.note(item.note());
            self.detail_modal.content(item.content());
            self.detail_modal.created_time(item.created_time());

            MODAL.show('#detail-modal');
        },
        save: function () {
            AJAX.post(window.location, {
                id: self.detail_modal.id(),
                note: self.detail_modal.note(),
                action: 'update'
            }, true, (res) => {
                if (!res.error) {
                    ALERT.success('Cập nhật thành công');
                    MODAL.hide('#detail-modal');

                    self.result.each((x) => {
                        if (x.id() == self.detail_modal.id())
                            x.note(self.detail_modal.note());
                    });
					$(".contact_count").text("("+res.count_contact_forms+")");
					
					setTimeout(function() {
						location.reload();
					}, 2000);
                }
            });
        }
    };

    self.contact_info_modal = {
        data: ko.observable(),
        show: function () {
            MODAL.show('#contact-info-modal');
        },
        save: function () {
            if (!self.contact_info_modal.isValid())
                self.contact_info_modal.errors.showAllMessages();
            else {
                AJAX.post(window.location, {
                    action: 'contact_info',
                    data: self.contact_info_modal.data()
                }, true, (res) => {
                    if (res.error)
                        NOTI.danger(res.message);
                    else {
                        ALERT.success('Cập nhật thành công');
                        MODAL.hide('#contact-info-modal');
                    }
                });
            }
        }
    };
}

let model = new viewModel();
model.search(1);
ko.validatedObservable(model.contact_info_modal);
ko.applyBindings(model, document.getElementById('main-content'));