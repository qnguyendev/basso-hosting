function viewModel() {
    let self = this;
    self.result = ko.mapping.fromJS([]);
    self.customer_groups = ko.mapping.fromJS([]);

    self.init = function () {
        AJAX.get(window.location, {action: 'init'}, true, (res) => {
            ko.mapping.fromJS(res.data, self.result);
            ko.mapping.fromJS(res.customer_groups, self.customer_groups);
        });
    };

    self.delete = function (item) {
        ALERT.confirm('Xác nhận xóa Warehouse', null, function () {
            AJAX.delete(window.location, {id: item.id()}, true, (res) => {
                if (res.error)
                    ALERT.error(res.message);
                else {
                    ALERT.success(res.message);
                    self.result.remove(item);
                }
            });
        });
    };

    self.modal = {
        id: ko.observable(0),
        name: ko.observable().extend({required: {message: LABEL.required}}),
        currency_id: ko.observable().extend({required: {message: LABEL.required}}),
        customer_rate: ko.observable().extend({required: {message: LABEL.required}}),
        buy_rate: ko.observable().extend({required: {message: LABEL.required}}),
        shipping_fee: ko.observable().extend({required: {message: LABEL.required}}),
        show: function () {
            self.modal.id(0);
            self.modal.name(undefined);
            self.modal.currency_id(undefined);
            self.modal.customer_rate(undefined);
            self.modal.buy_rate(undefined);
            self.modal.shipping_fee(undefined);
            self.modal.errors.showAllMessages(false);
            MODAL.show('#editor-modal');
        },
        edit: function (item) {
            self.modal.id(item.id());
            self.modal.name(item.name());
            self.modal.currency_id(item.currency_id());
            self.modal.customer_rate(moneyFormat(item.customer_rate()));
            self.modal.buy_rate(moneyFormat(item.buy_rate()));
            self.modal.shipping_fee(moneyFormat(item.shipping_fee()));

            MODAL.show('#editor-modal');
        },
        save: function () {
            if (!self.modal.isValid())
                self.modal.errors.showAllMessages();
            else {
                let data = {
                    id: self.modal.id(),
                    name: self.modal.name(),
                    currency_id: self.modal.currency_id(),
                    customer_rate: toNumber(self.modal.customer_rate()),
                    buy_rate: toNumber(self.modal.buy_rate()),
                    shipping_fee: toNumber(self.modal.shipping_fee()),
                    action: 'save-warehouse'
                };

                AJAX.post(window.location, data, true, (res) => {
                    if (res.error)
                        ALERT.error(res.message);
                    else {
                        MODAL.hide('#editor-modal');
                        ALERT.success(res.message);
                        self.init();
                    }
                });
            }
        }
    };

    self.user_group = {
        list: ko.observableArray([]),
        warehouse: ko.observable(),
        show: function (item) {
            self.user_group.list([]);
            AJAX.get(window.location, {action: 'detail', id: item.id()}, true, (res) => {
                if (!res.error) {
                    self.user_group.warehouse(item);
                    self.customer_groups.each((x) => {
                        let t = {
                            name: ko.observable(x.name()),
                            id: ko.observable(x.id()),
                            rate: ko.observable(moneyFormat(item.customer_rate())),
							shipping_fee: ko.observable(moneyFormat(item.shipping_fee())),
                        };

                        for (let i = 0; i < res.data.length; i++) {
                            if (res.data[i].customer_group_id == t.id()){
                                t.rate(moneyFormat(res.data[i].rate));
                                t.shipping_fee(moneyFormat(res.data[i].shipping_fee));
							}
                        }

                        self.user_group.list.push(t);
                    });
                    MODAL.show('#user-group-modal');
                } else {
                    NOTI.danger(res.message);
                }
            });
        },
        save: function () {
            let rates = [];
            self.user_group.list.each((x) => {
                let item = {
                    customer_group_id: x.id(),
                    rate: toNumber(x.rate()),
                    shipping_fee: toNumber(x.shipping_fee())
                };
                if (x.rate() == undefined || x.rate() == null)
                    item.rate = toNumber(self.user_group.warehouse().customer_rate());

                rates.push(item);
            });

            let data = {
                action: 'save-customer-rates',
                warehouse_id: self.user_group.warehouse().id(),
                rates: rates
            };

            AJAX.post(window.location, data, true, (res) => {
                if (res.error)
                    ALERT.error(res.message);
                else {
                    MODAL.hide('#user-group-modal');
                    NOTI.success(res.message);
                }
            });
        }
    };
}

let model = new viewModel();
model.init();
ko.validatedObservable(model.modal);
ko.applyBindings(model, document.getElementById('main-content'));