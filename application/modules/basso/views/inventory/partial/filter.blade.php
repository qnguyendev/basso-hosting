<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<div class="row">
    <div class="form-group col-sm-12 col-md-12 col-lg-3">
        <select class="form-control" data-bind="value: filter.time">
            <option value="all">Tất cả các ngày</option>
            <option value="week">Trong tuần</option>
            <option value="month">Trong tháng</option>
            <option value="custom">Tùy chọn</option>
        </select>
    </div>

    <div class="form-group col-sm-12 col-md-12 col-lg-2">
        <input type="text" class="form-control"
               data-bind="datePicker: filter.date, enable: filter.time() == 'custom'">
    </div>

    <div class="form-group col-sm-12 col-md-5 col-lg-2">
        <select class="form-control" data-bind="value: filter.warehouse_id">
            <option value="0">Tất cả warehouse</option>
            @foreach($warehouses as $item)
                <option value="{{$item->id}}">{{$item->name}}</option>
            @endforeach
        </select>
    </div>
	
	<div class="form-group col-sm-12 col-md-5 col-lg-2">
		<select class="form-control" name="branch" data-bind="value: filter.branch">
			<option value="">- Chọn chi nhánh -</option>
			@foreach(Branch::LIST_STATE as $key=>$value)
				<option value="{{$key}}">{{$value}}</option>
			@endforeach
		</select>
	</div>

    <div class="form-group col-sm-12 col-md-12 col-lg-3">
        <div class="input-group">
            <input type="text" class="form-control" data-bind="value: filter.key"
                   placeholder="Tìm theo mã tracking, tên sản phẩm...">
            <div class="input-group-append">
                <button class="btn btn-primary" data-bind="click: function(){ $root.search(1) }">
                    <i class="fa fa-search"></i>
                    TÌM
                </button>
            </div>
        </div>
    </div>
</div>
