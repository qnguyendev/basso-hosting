<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
<ul class="nav nav-tabs" role="tablist">
    <li class="nav-item" role="presentation">
        <a class="nav-link active" href="#tab1" aria-controls="ordertracking" role="tab"
           data-toggle="tab" data-bind="click: function(){set_tab('all')}">
            Hàng về
        </a>
    </li>
    <li class="nav-item" role="presentation">
        <a class="nav-link" href="#tab2" aria-controls="orderlabel" role="tab" data-toggle="tab"
           data-bind="click: function(){set_tab('{{InventoryItemStatus::NOT_FOUND}}')}">
            Not found
        </a>
    </li>
    <li class="nav-item" role="presentation">
        <a class="nav-link" href="#tab3" aria-controls="orderlabel" role="tab" data-toggle="tab"
           data-bind="click: function(){set_tab('{{InventoryItemStatus::LOST}}')}">
            Hàng thiếu
        </a>
    </li>
</ul>
