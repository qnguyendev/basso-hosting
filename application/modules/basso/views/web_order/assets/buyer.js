function viewModel() {
    let self = this;
    self.result = ko.mapping.fromJS([]);
    self.modal = {
        id: ko.observable(0),
        name: ko.observable().extend({required: {message: LABEL.required}}),
        note: ko.observable(),
        show: function () {
            self.modal.id(0);
            self.modal.name(undefined);
            self.modal.note(undefined);
            self.modal.errors.showAllMessages(false);
            MODAL.show('#editor-modal');
        },
        edit: function (item) {
            self.modal.id(item.id());
            self.modal.name(item.name());
            self.modal.note(item.note());
            MODAL.show('#editor-modal');
        },
        save: function () {
            let data = {
                id: self.modal.id(),
                name: self.modal.name(),
                note: self.modal.note()
            };

            if (!self.modal.isValid())
                self.modal.errors.showAllMessages();
            else {
                AJAX.post(window.location, data, true, (res) => {
                    if (res.error)
                        ALERT.error(res.message);
                    else {
                        MODAL.hide('#editor-modal');
                        ALERT.success(res.message);
                        self.init();
                    }
                });
            }
        }
    };

    self.init = function () {
        AJAX.get(window.location, null, false, (res) => {
            ko.mapping.fromJS(res.data, self.result);
        });
    };

    self.delete = function (item) {
        ALERT.confirm(`Bạn muốn xóa ${item.name()}`, null, () => {
            AJAX.delete(window.location, {id: item.id()}, true, (res) => {
                if (res.error)
                    ALERT.error(res.message);
                else {
                    ALERT.success(res.message);
                    self.result.remove(item);
                }
            });
        });
    };
}

let model = new viewModel();
model.init();
ko.validatedObservable(model.modal);
ko.applyBindings(model, document.getElementById('main-content'));