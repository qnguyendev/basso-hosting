<?php
if (!defined('BASEPATH')) exit('No direct access script allowed');
?>
<div class="mt-3 row" data-bind="with: $root.search_result.pagination">
    <div class="col-12 col-sm-6">
        <ul class="pagination">
            <li data-bind="disable: current_page == 1" class="page-item">
                <a href="javascript:" class="page-link"
                   data-bind="click: function() {$root.filter.search(parseInt(current_page) - 1)}, visible: parseInt(current_page) > 1">
                    <i class="icon-arrow-left"></i>
                    Trước
                </a>
            </li>
            <li class="page-item">
                <a href="javascript:" class="page-link"
                   data-bind="click: function() {$root.filter.search(parseInt(current_page) + 1)}, visible: parseInt(current_page) < total_page">
                    Sau
                    <i class="icon-arrow-right"></i>
                </a>
            </li>
        </ul>
    </div>
    <div class="col-12 col-sm-6 text-right">
        Trang số
        <input type="number" class="text-center" style="width: 60px"
               data-bind="value: current_page, attr: {min: 1, max: total_page}, event: { change: function() { $root.filter.search(current_page) } }"
               min="1" max="460">
        trên <span data-bind="text: total_page"></span> trang. Tổng <span data-bind="text: total_item"></span>
    </div>
</div>