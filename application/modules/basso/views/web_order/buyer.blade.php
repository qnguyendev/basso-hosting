<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-md-12 col-lg-8 offset-lg-2 col-xl-6 offset-xl-3">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">
                        Người mua hàng
                        <a href="#" class="float-right" data-bind="click: modal.show">
                            <i class="fa fa-plus"></i>
                            Thêm
                        </a>
                    </div>
                </div>

                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Người mua</th>
                            <th>Ghi chú</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody data-bind="foreach: result" class="text-center">
                        <tr>
                            <th data-bind="text: $index() + 1"></th>
                            <td data-bind="text: name"></td>
                            <td data-bind="text: note"></td>
                            <td>
                                <a href="#" class="text-primary font-weight-bold" data-bind="click: $root.modal.edit">
                                    <i class="icon-pencil"></i>
                                    Cập nhật
                                </a>
                                <a href="#" class="text-danger font-weight-bold ml-1" data-bind="click: $root.delete">
                                    <i class="icon-trash"></i>
                                    Xóa
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    @include('partial/buyer/editor_modal')
@endsection
@section('script')
    <script src="{{load_js('buyer')}}"></script>
@endsection