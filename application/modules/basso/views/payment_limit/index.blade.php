<?php if (!defined('BASEPATH')) exit('No direct access script allowed'); ?>
@layout('cpanel_layout')
@section('content')
    <div class="row">
        <div class="col-sm-12 col-md-6 offset-md-3">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-title">
                        <a href="javascript:" class="float-right" data-bind="click: add_limit">
                            <i class="fa fa-plus"></i>
                            Thêm hạn mức
                        </a>
                        Hạn mức thanh toán
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Hạn mức</th>
                            <th>Phụ thu</th>
                            <th>Ghi chú</th>
                            <th>Thao tác</th>
                        </tr>
                        </thead>
                        <tbody class="text-center" data-bind="foreach: result">
                        <tr>
                            <td data-bind="text: `${limit()}%`"></td>
                            <td data-bind="text: `${fee()}%`"></td>
                            <td data-bind="text: description"></td>
                            <td>
                                <button class="btn btn-xs btn-info" data-bind="click: $root.edit">
                                    <i class="fa fa-edit"></i>
                                    Sửa
                                </button>
                                <button class="btn btn-xs btn-danger" data-bind="click: $root.delete">
                                    <i class="icon-trash"></i>
                                    Xóa
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('modal')
    @include('partial/editor_modal');
@endsection
@section('script')
    <script src="{{load_js('limit')}}"></script>
@endsection