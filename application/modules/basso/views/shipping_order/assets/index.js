function viewModel() {
    let self = this;
    self.result = ko.mapping.fromJS([]);
    self.pagination = ko.observable();
    self.filter = {
        shipping_id: ko.observable(0),
        status: ko.observable('all'),
        key: ko.observable()
    };

    self.current_tab = ko.observable();
    self.set_current = function (tab) {
        if (tab != self.current_tab()) {
            self.current_tab(tab);
            self.search(1);
        }
    };

    self.selected_items = ko.observableArray([]);

    self.check_all = function (data, event) {
        if ($(event.currentTarget).is(':checked')) {
            self.result.each((x) => {
                if (!self.selected_items().includes(x.id()))
                    self.selected_items.push(x.id());
            });
        } else
            self.selected_items([]);
    };

    self.search = function (page) {
        self.selected_items([]);
        let data = {
            page: page,
            shipping_id: self.filter.shipping_id(),
            status: self.filter.status(),
            key: self.filter.key(),
            branch: self.current_tab()
        };

        AJAX.get(window.location, data, true, (res) => {
            ko.mapping.fromJS(res.data, self.result);
            self.pagination(res.pagination);
        });
    };

    self.print_invoice = function () {
        if (self.selected_items().length > 0) {
            let id = self.selected_items().join(',');
            window.open(`/basso/invoice?id=${id}`, '_blank');
        }
    };

	self.shipping = {
		id: ko.observable(),
        code: ko.observable(),
        cod_amount: ko.observable().extend({required: {message: LABEL.required}}),
        fee: ko.observable().extend({required: {message: LABEL.required}}),
        shipping_id: ko.observable(),
		edit: function (item) {
			self.shipping.id(item.id());
			self.shipping.code(item.code());
			self.shipping.cod_amount(moneyFormat(item.cod_amount()));
			self.shipping.shipping_id(item.shipping_id());
			self.shipping.fee(moneyFormat(item.fee()));
			MODAL.show('#shipping-edit-modal');
		},
        save: function () {
            if (!self.shipping.isValid()) {
                self.shipping.errors.showAllMessages();
            }else {
				//Cập nhật thông tin shipping
                let data = {
                    id: self.shipping.id(),
                    shipping_id: self.shipping.shipping_id(),
                    fee: toNumber(self.shipping.fee()),
                    cod_amount: toNumber(self.shipping.cod_amount()),
                    code: self.shipping.code(),
                    action: 'update-shipping'
                };

                ALERT.confirm('Xác nhận cập nhật đơn vận chuyển?', null, () => {
                    AJAX.post(window.location, data, true, (res) => {
                        if (!res.error) {
                            ALERT.success(res.message);
                            MODAL.hide('#shipping-edit-modal');
							let shipping_name = $("#ddl-shippings option:selected").text();
							self.result.each((x) => {
								if (x.id()==data.id){
									x.fee(data.fee);
									x.shipping_id(data.shipping_id);
									x.cod_amount(data.cod_amount);
									x.code(data.code);
									x.shipping(shipping_name);
								}
							});
                        } else
                            ALERT.error(res.message);
                    });
                });
            }
        }
	}

    self.mark_shipped = function (item) { 
        ALERT.confirm('Xác nhận đơn đã được giao', null, function () {
            AJAX.post(window.location, {action: 'shipped',id: item.id()}, true, (res) => {
                ALERT.success(res.message);
                item.status(res.status);
            });
        });
    };
	
	self.mark_exported = function (item) { 
        ALERT.confirm('Xác nhận đơn đã được giao', null, function () {
            AJAX.post(window.location, {action: 'exported',id: item.id()}, true, (res) => {
                ALERT.success(res.message);
                item.status(res.status);
            });
        });
    };

    self.multi_shipped = function () {
        ALERT.confirm('Xác nhận đơn đã được giao', null, function () {
            AJAX.post(window.location, {action: 'shipped',id: self.selected_items()}, true, (res) => {
                ALERT.success(res.message);
                self.result.each((x) => {
                    if (self.selected_items().includes(x.id()))
                        x.status(res.status);
                });

                self.selected_items([]);
            });
        });
    };

    self.toggle_row = function (data, event) {
        toggle_row(event.currentTarget);
    };
}

let model = new viewModel();
model.search(1);
ko.validatedObservable(model.shipping);
ko.applyBindings(model, document.getElementById('main-content'));