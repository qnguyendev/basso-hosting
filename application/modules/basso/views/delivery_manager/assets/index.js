ko.bindingHandlers.popover = {
    init: function (element) {
        let $element = $(element);
        ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
            if ($element.data('bs.popover')) {
                try {
                    $element.popover('destroy');
                } catch (err) {
                }
            }
        });
    },
    update: function (element, valueAccessor) {
        let $element = $(element), value = ko.unwrap(valueAccessor());
        $element.popover({
            html: true,
            animation: true,
            placement: 'left',
            trigger: 'hover',
            content: function () {
                return $(value).find(".popover-body").html();
            },
            title: function () {
                return $(value).find(".popover-heading").html();
            }
        });
    }
};

/* fixcode for keyenter note */
ko.bindingHandlers.enterkey = {
    init: function (element, valueAccessor, allBindings, viewModel) {
        /* $(element).keypress(function (event) {
            var keyCode = (event.which ? event.which : event.keyCode);
            if (keyCode === 13) {
				$(element).parents().eq(0).next().children(".btn-primary").trigger("click");
                return false;
            }
            return true;
        }); */
    },
	update: function (element, item) {
		$(element).keypress(function (event) {
            var keyCode = (event.which ? event.which : event.keyCode);
			self.note = $(element).val();
            if (keyCode === 13) {
				$(element).parents().eq(0).next().children(".btn-primary").trigger("click");
                return false;
            }
            return true;
        });
	}
};
/* end fixcode for keyenter note */

function viewModel() {
    let self = this;
    self.result = ko.mapping.fromJS([]);
    self.pagination = ko.observable();
    self.current_tab = ko.observable($('.nav-tabs a:first').attr('rel'));
	self.tracking_code = ko.observable();
	self.export_tracking = ko.observable();
	
    self.filter = {
        warehouse_id: ko.observable(0),
        key: ko.observable()
    };

    self.set_current = function (t) {
        self.current_tab(t);
        self.search(1);
    };

    self.search = function (page, e) {
        let data = {
            page: page,
            action: self.current_tab(),
            warehouse_id: self.filter.warehouse_id(),
            key: self.filter.key()
        };

        localStorage.setItem('search_data', ko.toJSON(data));
        AJAX.get(window.location, data, true, (res) => {
            if (res.error)
                NOTI.danger(res.message);
            else {
                ko.mapping.fromJS(res.data, self.result);
                /*self.result.each((x) => {
                    if (x.delivered_date() != null) {
                        if (x.delivered_date() != '') {
                            x.delivered_date(moment.unix(x.delivered_date()).format('DD-MM-YYYY'));
                        }
                    }
                });*/

                self.pagination(res.pagination);
				return false;
            }
        });
		return false;
    };


    self.update = function (item) {
        let data = {
            order_id: item.id(),
            note: item.note(),
            item_id: item.item_id(),
            delivered_date: item.delivered_date()
        };

        AJAX.post(window.location, data, true, (res) => {
            if (!res.error)
                NOTI.success(res.message);
            else
                NOTI.danger(res.message);
        });
    };
	
	//  Xuất mã tracking
    self.export_tracking = function () {
        AJAX.post(window.location, {action: 'export-tracking'}, true, (res) => {
            self.tracking_code(res.data.join('\n'));
            $('#tracking-code-modal textarea').linenumbers({
                col_width: '25px',
                start: 1,
                digits: 4

            });

            MODAL.show('#tracking-code-modal');
            $('#tracking-code-modal textarea').focus();
        });
    };
	
    //  Cập nhật tracking
    self.update_tracking = {
        data: ko.observable(),
        show: function () {
            MODAL.show('#update-tracking-modal');
        },
        post: function () {
            if (self.update_tracking.data() == undefined) {
                NOTI.danger('Vui lòng nhập thông tin tracking');
                return;
            }

            let data = self.update_tracking.data().split('\n');
            if (data.length === 0)
                NOTI.danger('Vui lòng nhập thông tin tracking');
            else {
                let valid_data = [];
                for (let i = 0; i < data.length; i++) {
                    let temp = data[i].split('\t');
                    if (temp.length >= 5) {
                        if (temp[4].toLocaleLowerCase() == 'delivered') {
                            let tracking = temp[0];
                            let date = temp[3].split(',')[0];
                            valid_data.push({tracking: tracking, date: date});
                        }
                    }
                }

                if (valid_data.length > 0) {
                    AJAX.post('https://basso.vn/basso/customer_order', {action: 'update-tracking', data: valid_data}, true, (res) => {
                        if (res.error)
                            ALERT.error(res.message);
                        else {
                            self.result.each(function (t) {
                                    for (let i = 0; i < valid_data.length; i++) {
                                        if (valid_data[i].tracking == t.tracking_code()) {
                                            t.delivered_date(moment(valid_data[i].date).format('X'));
                                        }
                                    }
                            });
                            MODAL.hide('#update-tracking-modal');
                            ALERT.success(res.message);
                        }
                    });
                } else {
                    ALERT.error('Không có tracking nào đã được giao');
                }
            }
        }
    };
}

let model = new viewModel();
model.search(1);
ko.applyBindings(model, document.getElementById('main-content'));