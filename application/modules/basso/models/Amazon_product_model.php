<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Amazon_product_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Amazon_product_model extends CI_Model
{
    private $_table = 'crawled_amz_products';
    private $_variations_table = 'crawled_amz_variations';

    public function insert($product, $variations)
    {
        $this->db->trans_begin();

        $product['created_time'] = time();
        if ($this->get_parent_product($product['id']) == null)
            $this->db->insert($this->_table, $product);

        if ($this->db->trans_status()) {
            $this->_insert_child($variations);
            if ($this->db->trans_status()) {
                $this->db->trans_commit();
                return true;
            }
        }

        $this->db->trans_rollback();
        return false;
    }

    private function _insert_child($variantions = null)
    {
        foreach ($variantions as $variant) {
            $this->db->where('id', $variant['id']);
            $child = $this->db->get($this->_variations_table)->row();

            if ($child == null)
                $this->db->insert($this->_variations_table, $variant);
        }
    }

    public function delete_parent($asin)
    {
        $child = $this->get_child($asin);
        if ($child != null) {
            $this->db->where('id', $child->parent_id);
            $this->db->delete($this->_table);
        }
    }

    public function get_parent_product($id)
    {
        $this->db->select('t.*');
        $this->db->from("$this->_table t");
        $this->db->join("$this->_variations_table o", 't.id = o.parent_id', 'left');
        $this->db->where('t.id', $id);
        $this->db->or_where('o.id', $id);

        return $this->db->get()->row();
    }

    public function get_child($parent_id, $limit = 1)
    {
        $this->db->where('parent_id', $parent_id);
        $this->db->limit($limit);

        if ($limit == 1)
            return $this->db->get($this->_variations_table)->row();

        return $this->db->get($this->_variations_table)->result();
    }

    public function get_variant($id)
    {
        $this->db->where('id', $id);
        return $this->db->get($this->_variations_table)->row();
    }

    public function clean()
    {
        $this->db->where('created_time <=', time() - 3600 * 12);
        $this->db->delete($this->_table);
    }
	
	public function updateShippingFee($id, $shipping_fee)
    {
		$this->db->trans_begin();
		$this->db->where('id', $id);
		$data["shipping_fee"] = $shipping_fee;
        $this->db->update($this->_variations_table, $data);
		if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
	}
}