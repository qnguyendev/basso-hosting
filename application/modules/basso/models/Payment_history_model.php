<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Payment_history_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Payment_history_model extends CI_Model
{
    private $_table = 'payment_histories';
    private $_bank_table = 'banks';

    public function insert(int $created_time, int $amount, string $type, string $note = null, int $order_id = null,
                           string $status = null, int $user_id = null, int $bank_id = null)
    {
        $this->db->trans_begin();
        $data = [
            'created_time' => $created_time,
            'user_id' => $user_id,
            'amount' => $amount,
            'status' => $status,
            'note' => $note,
            'type' => $type,
            'order_id' => $order_id,
            'bank_id' => $bank_id
        ];

        $this->db->insert($this->_table, $data);
        $id = $this->db->insert_id();
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return $id;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function get(int $page = 0, int $start = 0, int $end = 0, string $status = 'all', int $user_id = 0)
    {
        $this->db->select('t.*, o.first_name as user, d.sub_total, k.name as bank, d.order_code');
        $this->db->from("{$this->_table} t");
        $this->db->join('users o', 't.user_id = o.id', 'left');
        $this->db->join('customer_orders d', 'd.id = t.order_id', 'left');
        $this->db->join("$this->_bank_table k", 'k.id = t.bank_id', 'left');
        $this->db->where('t.created_time >=', $start);
        $this->db->where('t.created_time <=', $end);
        if ($status != 'all')
            $this->db->where('t.status', $status);

        if ($user_id != 0)
            $this->db->where('t.user_id', $user_id);

        if ($page > 0) {
            $this->db->limit(PAGING_SIZE);
            $this->db->offset(($page - 1) * PAGING_SIZE);
        }

        $this->db->order_by('t.created_time', 'desc');
        $this->db->group_by('t.id', 'desc');
        return $this->db->get()->result();
    }

    public function count(int $start = 0, int $end = 0, string $status = 'all', int $user_id = 0)
    {
        $this->db->select('t.*, o.first_name as name');
        $this->db->from("{$this->_table} t");
        $this->db->join('users o', 't.user_id = o.id', 'left');
        $this->db->where('t.created_time >=', $start);
        $this->db->where('t.created_time <=', $end);
        if ($status != 'all')
            $this->db->where('t.status', $status);

        if ($user_id != 0)
            $this->db->where('t.user_id', $user_id);

        $this->db->group_by('t.id');
        return $this->db->count_all_results();
    }

    public function get_by_order(int $order_id)
    {
        $this->db->select('t.*, k.name as bank');
        $this->db->from("{$this->_table} t");
        $this->db->join("$this->_bank_table k", 'k.id = t.bank_id', 'left');
        $this->db->where('order_id', $order_id);
        $this->db->order_by('created_time', 'desc');
        return $this->db->get()->result();
    }

    /**
     * Cập nhật trạng thái thanh toán
     * @param int $id
     * @param string $status
     * @param stdClass $order
     * @return bool
     */
    public function update_status(int $id, string $status, $order = null)
    {
        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->update($this->_table, ['status' => $status]);
        if ($this->db->affected_rows()) {
            $this->db->trans_commit();

            if ($order != null) {
                if ($status == PaymentHistoryStatus::COMPLETED) {
                    $this->_update_order_items_status(intval($order->id));

                    $this->db->where('order_id', $order->id);
                    $this->db->where('status', PaymentHistoryStatus::COMPLETED);
                    $total = $this->db->count_all_results($this->_table);
                    $this->_update_order_payment_status($order->id, $total);
                }
            }

            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function update_trans_ref(int $id, string $trans_ref)
    {
        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->update($this->_table, ['trans_ref' => $trans_ref]);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    /**
     * Cập nhật trạng thái thanh toán đơn hàng của khách
     * @param int $order_id
     * @param int $total_paid
     * @return bool
     */
    private function _update_order_payment_status(int $order_id, int $total_paid)
    {
        $this->db->trans_begin();
        $this->db->where('id', $order_id);
        $this->db->update('customer_orders', [
            'payment_status' => $total_paid == 1 ? OrderPaymentStatus::FIRST_PAYMENT : OrderPaymentStatus::SECOND_PAYMENT
        ]);

        $this->update_order_total_paid($order_id);
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function update_amount(int $payment_id, int $amount)
    {
        $this->db->trans_begin();
        $this->db->where('id', $payment_id);
        $this->db->update($this->_table, ['amount' => $amount]);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    /**
     * Cập nhật tổng thanh toán đơn hàng
     * @param int $order_id
     * @return bool
     */
    public function update_order_total_paid(int $order_id)
    {
        $this->db->trans_begin();
        $this->db->query("update customer_orders l,
                        (select sum(amount) as total_paid, id
                        from payment_histories
                        where status = '" . PaymentHistoryStatus::COMPLETED . "' and order_id = $order_id) m 
                            set l.total_paid = m.total_paid
                        where l.id = $order_id");
        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }
	
	/* fixcode custom function */
	/* public function get_order_total_paid(int $order_id)
    {
		$this->db->select_sum('amount');
		$this->db->from('payment_histories');
		$this->db->where('status = "' . PaymentHistoryStatus::COMPLETED . '" and order_id = '.$order_id.'');
		$query = $this->db->get();
		return $query->row()->amount;
    } */

    /**
     * Chuyển trạng thái sản phẩm từ chờ duyệt -> mua ngay
     * @param int $order_id
     */
    private function _update_order_items_status(int $order_id)
    {
        $this->db->where('order_id', $order_id);
        $this->db->where('status', CustomerOrderItemStatus::PENDING);
        $this->db->update('customer_order_items', ['requirement' => CustomerOrderItemRequirement::BUY_NOW]);
    }

    public function get_by_id($id)
    {
        $this->db->where('id', $id);
        return $this->db->get($this->_table)->row();
    }

    public function get_by_trans_ref($trans_ref)
    {
        $this->db->where('trans_ref', $trans_ref);
        return $this->db->get($this->_table)->row();
    }

    /**
     * Lịch sử thanh toán của khách hàng
     * @param int $page
     * @param int $customer_id
     * @return array
     */
    public function get_by_customer(int $page = 1, int $customer_id)
    {
        $this->db->select('t.*, o.order_code');
        $this->db->from("$this->_table t");
        $this->db->join("customer_orders o", 't.order_id = o.id', 'left');
        $this->db->where('o.customer_id', $customer_id);
        $this->db->order_by('t.created_time', 'desc');

        if ($page > 0) {
            $this->db->limit(PAGING_SIZE);
            $this->db->offset(($page - 1) * PAGING_SIZE);
        }
        return $this->db->get()->result();
    }

    public function count_by_customer(int $customer_id)
    {
        $this->db->select('t.id');
        $this->db->from("$this->_table t");
        $this->db->join("customer_orders o", 't.order_id = o.id', 'left');
        $this->db->where('o.customer_id', $customer_id);

        return $this->db->count_all_results();
    }
	
	
	
	public function get_by_customer_nonuser(int $page = 1)
    {
        $this->db->select('t.*, o.order_code');
        $this->db->from("$this->_table t");
        $this->db->join("customer_orders o", 't.order_id = o.id', 'left');
        /* $this->db->where('o.customer_id', $customer_id); */
        $this->db->order_by('t.created_time', 'desc');

        if ($page > 0) {
            $this->db->limit(PAGING_SIZE);
            $this->db->offset(($page - 1) * PAGING_SIZE);
        }
        return $this->db->get()->result();
    }

    public function count_by_customer_nonuser()
    {
        $this->db->select('t.id');
        $this->db->from("$this->_table t");
        $this->db->join("customer_orders o", 't.order_id = o.id', 'left');
        /* $this->db->where('o.customer_id', $customer_id); */

        return $this->db->count_all_results();
    }
}
