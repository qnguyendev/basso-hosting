<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Level_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Level_model extends CI_Model
{
    private $_levels_table = 'levels';
	
    public function insert(int $id = 0, string $name)
    {
        $this->db->trans_begin();
         if ($id == 0) {
            $this->db->insert($this->_levels_table, ['name' => $name]);
        } else {
            $this->db->where('id', $id);
            $this->db->update($this->_levels_table, ['name' => $name]);
        }


        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function get($id = null)
    {
        $this->db->select('t.*');
        $this->db->from("$this->_levels_table t");

        if ($id == null)
            return $this->db->get()->result();

        if (is_array($id)) {
            $this->db->where_in('t.id', $id);
            return $this->db->get($this->_levels_table)->result();
        }

        $this->db->where('t.id', $id);
        return $this->db->get($this->_levels_table)->row();
    }

    public function delete(int $id)
    {
        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->delete($this->_levels_table);

        if ($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }
   

   
}