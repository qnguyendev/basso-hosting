<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Subscribe_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Subscribe_model extends CI_Model
{
    private $_table = 'subscribe_emails';

    public function insert(string $email)
    {
        $email = strtolower(trim($email));

        $this->db->where('email', $email);
        $row = $this->db->get($this->_table)->row();
        if ($row == null)
            $this->db->insert($this->_table, ['email' => $email, 'created_time' => time()]);
    }

    public function get($page = 1, $from, $to)
    {
        $this->db->order_by('created_time', 'desc');
        $this->db->where('created_time >=', $from);
        $this->db->where('created_time <=', $to);

        if ($page > 0) {
            $this->db->limit(PAGING_SIZE);
            $this->db->offset(($page - 1) * PAGING_SIZE);
        }

        return $this->db->get($this->_table)->result();
    }

    public function count(int $from, int $to)
    {
        $this->db->where('created_time >=', $from);
        $this->db->where('created_time <=', $to);
        return $this->db->count_all_results($this->_table);
    }
}