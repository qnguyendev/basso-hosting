<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Order_place_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Order_place_model extends CI_Model
{
    /**
     * Thêm nơi chốt đơn
     * @param int $id
     * @param string $name
     * @return bool
     */
    public function insert(int $id = 0, string $name)
    {
        $this->db->trans_begin();
        if ($id == 0)
            $this->db->insert('order_places', ['name' => $name]);
        else {
            $this->db->where('id', $id);
            $this->db->update('order_places', ['name' => $name]);
        }
        if($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }

    public function get(int $id = null)
    {
        if ($id == null)
            return $this->db->get('order_places')->result();

        $this->db->where('id', $id);
        return $this->db->get('order_places')->row();
    }

    /**
     * Xóa nơi chốt đơn
     * @param int $id
     * @return bool
     */
    public function delete(int $id)
    {
        $this->db->trans_begin();
        $this->db->where('id', $id);
        $this->db->delete('order_places');
        if($this->db->trans_status()) {
            $this->db->trans_commit();
            return true;
        }

        $this->db->trans_rollback();
        return false;
    }
}