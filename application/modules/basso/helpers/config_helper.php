<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

abstract class BassoBrand
{
    const LIST_STATE = [
        'asale' => 'Asale',
        'basso' => 'Basso'
    ];
}

abstract class Branch
{
    const LIST_STATE = [
        'ha-noi' => 'Hà Nội',
        'ho-chi-minh' => 'Hồ Chí Minh'
    ];
}

abstract class BillingGroup
{
    const LIST_STATE = [
        'customer' => 'Khách hàng',
        'shipping' => 'Đối tác vận chuyển'
    ];

    const CUSTOMER = 'customer';
    const SHIPPING = 'shipping';
}

abstract class BillingPaymentMethod
{
    const LIST_STATE = [
        'cod' => 'COD',
        'bank-transfer' => 'Chuyển khoản',
        'cash' => 'Tiền mặt'
    ];

    const COD = 'cod';
    const BANK_TRANSFER = 'bank-transfer';
    const CASH = 'cash';
}

abstract class DiscountType
{
    const PERCENT = 'percent';
    const AMOUNT = 'amount';
}

function get_website_name(string $website)
{
    $_web = explode('.', $website);
    for ($i = 0; $i < count($_web); $i++) {
        if ($_web[$i] == 'wwww' || $_web[$i] == 'www2' || $_web[$i] == 'www1' || $_web[$i] == 'm')
            unset($_web[$i]);
    }

    for ($i = 0; $i < count($_web); $i++)
        if ($i > 0)
            unset($_web[$i]);

    return implode('', $_web);
}