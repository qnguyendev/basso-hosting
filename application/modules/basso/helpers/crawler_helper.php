<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

use \DTS\eBaySDK\Shopping\Services;
use \DTS\eBaySDK\Shopping\Types;
$CI = get_instance();
$CI->load->model('basso/country_model');
$CI->load->model('management/customer_model');
if (!function_exists('crawl_ebay')) {
    function crawl_ebay(string $id)
    {
        global $theme_config;
        $service = new Services\ShoppingService([ 
            'credentials' => $theme_config['ebay']['credentials']
        ]);

        /**
         * Create the request object.
         */
        $request = new Types\GetSingleItemRequestType();
        $request->ItemID = $id;
        $request->IncludeSelector = 'Description,Details,ItemSpecifics,Variations';
        /**
         * Send the request.
         */
        $response = $service->getSingleItem($request);

        if ($response->Ack !== 'Failure') {
            $product = new stdClass();
            $product->images = [];
            $product->categories = [];

            $item = $response->Item;
            foreach ($item->PictureURL as $key => $value)
                array_push($product->images, $value);

            $product->title = $item->Title;
            $product->description = $item->Description;
            $product->price = calc_product_price($item->ConvertedCurrentPrice->value);
            $product->origin_price = round($item->ConvertedCurrentPrice->value, 2);
            $product->seller = $item->Seller->UserID;
            $product->url = $item->ViewItemURLForNaturalSearch;
            $product->website = parse_url(str_replace('www.', '', $product->url))['host'];
            $product->weight = 0;
            $product->attributes = [];
            $product->variations = [];
            $product->variation_images = [];
            $product->category = $item->PrimaryCategoryName;
            $product->sold_quantity = $item->QuantitySold;
            $product->avaiable_quantity = $item->Quantity - $item->QuantitySold;
            $product->rating = $item->Seller->PositiveFeedbackPercent;
            $product->total_reviews = $item->Seller->FeedbackScore;
            $product->variations_price = [];

            if (isset($item->PrimaryCategoryName))
                array_push($product->categories, $item->PrimaryCategoryName);
            if (isset($item->SecondaryCategoryName))
                array_push($product->categories, $item->SecondaryCategoryName);

            $product->categories = json_encode($product->categories);

            #region Chi tiết sản phẩm
            if (isset($item->ItemSpecifics)) {
                foreach ($item->ItemSpecifics->NameValueList as $att) {
                    $product->attributes[$att->Name] = implode(', ', iterator_to_array($att->Value));
                }
            }
            #endregion

            #region Biến thể
            if (isset($item->Variations)) {
                foreach ($item->Variations->Variation as $variation) {

                    $variation_price = $variation->StartPrice->value;
                    if ($variation->StartPrice->currencyID == 'GBP')
                        $variation_price *= 1.34;

                    $variation_list = [];

                    foreach ($variation->VariationSpecifics as $specific) {
                        foreach ($specific->NameValueList as $nameValues) {
                            if (!isset($product->variations[$nameValues->Name]))
                                $product->variations[$nameValues->Name] = iterator_to_array($nameValues->Value);
                            else {
                                foreach (iterator_to_array($nameValues->Value) as $t)
                                    array_push($product->variations[$nameValues->Name], $t);
                            }

                            $product->variations[$nameValues->Name] = array_unique($product->variations[$nameValues->Name]);
                            foreach (iterator_to_array($nameValues->Value) as $t)
                                array_push($variation_list, $t);
                        }
                    }

                    sort($variation_list);
                    $variation_list = join('|', $variation_list);
                    $product->variations_price[md5($variation_list)] = $variation_price;
                }

                foreach ($item->Variations->Pictures as $picture) {
                    foreach ($picture->VariationSpecificPictureSet as $specific) {
                        if (!isset($product->variation_images[$specific->VariationSpecificValue]))
                            $product->variation_images[$specific->VariationSpecificValue] = [];

                        $product->variation_images[$specific->VariationSpecificValue] = array_merge(
                            $product->variation_images[$specific->VariationSpecificValue],
                            iterator_to_array($specific->PictureURL)
                        );

                        $product->images = array_merge($product->images, $product->variation_images[$specific->VariationSpecificValue]);
                    }
                }
            }
            #endregion

            #region lấy phí ship
            $request = new Types\GetShippingCostsRequestType();
            $request->ItemID = $id;
            $request->QuantitySold = 1;
            $request->DestinationCountryCode = $item->Country == 'GB' ? 'GB' : 'US';
            $request->DestinationPostalCode = $item->Country == 'GB' ? 'SE92SX' : '97225';
            $response = $service->getShippingCosts($request);
            $shipping = $response->ShippingCostSummary;

            $product->web_shipping_fee = $shipping->ListedShippingServiceCost->value;
            if ($item->Country == 'GB' || strtoupper($shipping->ListedShippingServiceCost->currencyID) == 'GBP')
                $product->web_shipping_fee *= 1.34;

            $product->web_shipping_fee = round($product->web_shipping_fee, 2);
            #endregion

            return $product;
        }

        return null;
    }
}

if (!function_exists('calc_product_price')) {
    function calc_product_price($price)
    {
        $currency_rate = 26000;
        if ($price > 500)
            $currency_rate = 25500;

        $price = $currency_rate * $price;
        return $price;
    }
}

if (!function_exists('calc_product_price_v2')) {
    function calc_product_price_v2($price, $rate_policy)
    {
        $currency_rate = 26000;
		if($rate_policy['rate']>0){
			$currency_rate = $rate_policy['rate'];
		}
        // if ($price > 500)
            // $currency_rate = 25500;
		
        $price = $price * $currency_rate;
        return $price;
    }
}

if (!function_exists('calc_product_shipping_fee_v2')) {
    function calc_product_shipping_fee_v2($weight, $rate_policy)
    {
		//(round($weight, 2) + 0.2) * 260000
		$shipping_fee = 260000;
		if($rate_policy['shipping_fee']>0){
		$shipping_fee = $rate_policy['shipping_fee'];
		}
        return (round($weight, 2) + 0.2) * $shipping_fee;
    }
}


if (!function_exists('calc_product_shipping_fee_v2_when_change_weight_in_inventory')) {
    function calc_product_shipping_fee_v2_when_change_weight_in_inventory($weight, $rate_policy)
    {
		//(round($weight, 2) + 0.2) * 260000
		$shipping_fee = 260000;
		if($rate_policy['shipping_fee']>0){
		$shipping_fee = $rate_policy['shipping_fee'];
		}
        /* return (round($weight, 2) + 0.2) * $shipping_fee; */ /* fixcode Vậy a sửa giúp em ở phần back end bỏ 0.2 kg với ạ basso/inventory. */
        return round($weight, 2) * $shipping_fee;
    }
}



if (!function_exists('get_country_policy')) {
    function get_country_policy($cus_id=0, $country_id=1)
    {
		$customer_model = new Customer_model();
		$country_model = new Country_model();
		$data = array(
			"rate"=>26000,
			"shipping_fee"=>0
		);
		
		$country_default_id = 1;
		if($country_id>1){
			$country_default_id = $country_id;
		}
		$group_default_id = 0;
		
		$customer_id = 0;
		if($cus_id>0){
			$customer_id = $cus_id;
		}else{
			if(isset($_SESSION['user_id'])){
				$customer_id = $_SESSION['user_id'];
			}
		}
		if($customer_id>0){
			$customer = $customer_model->get_info_by_id($customer_id);
			if($customer->group_id>0){
				$group_default_id = $customer->group_id;
				$country = $country_model->get_customer_group_rates($country_default_id,$group_default_id);
				if(count($country)) {
					if(isset($country[0]->rate)) {
						$data["rate"] = $country[0]->rate;
					} else {
						$data["rate"] = 0;
					}
					
					if(isset($country[0]->shipping_fee)) {
						$data["shipping_fee"] = $country[0]->shipping_fee;
					} else {
						$data["shipping_fee"] = 0;
					}
				}
			}else{
				$country = $country_model->get($country_default_id);
				$data['rate'] = $country->customer_rate;
				$data["shipping_fee"] = $country->shipping_fee;
			}				
		}else{
			$country = $country_model->get($country_default_id);
			$data['rate'] = $country->customer_rate;
			$data["shipping_fee"] = $country->shipping_fee;
		}
		return $data;
    }
}



if (!function_exists('object_to_array')) {
    function object_to_array(stdClass $obj)
    {
        $obj = (array)$obj;
        foreach ($obj as $key => $value) {
            if (is_array($value))
                $obj[$key] = json_encode($value);
        }

        return $obj;
    }
}