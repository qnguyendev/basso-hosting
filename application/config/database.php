<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$active_group = 'default';
$query_builder = TRUE;

if (!file_exists(APPPATH . '../env.php')) {
    echo '<h1 style="text-align: center">Can not found env.php</h1>';
    die;
}

include APPPATH . '../env.php';
$db['default'] = array(
    'dsn' => '',
    'hostname' => $database['hostname'],   //42.112.20.22
    'username' => $database['username'],
    'password' => $database['password'], //2#$,S(Tgc#s.
    'database' => $database['database'],
    'dbdriver' => 'mysqli',
    'dbprefix' => '',
    'pconnect' => TRUE,
    'db_debug' => (ENVIRONMENT !== 'production'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => true,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);
