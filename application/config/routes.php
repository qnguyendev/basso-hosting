<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = 'home';
$route['404_override'] = 'errors';
$route['404'] = 'errors';
$route['cpanel'] = 'admin/home';
$route['translate_uri_dashes'] = false;
$route['robots.txt'] = 'seo/robots';
$route['sitemap.xml'] = 'seo/sitemap';
$route['post-sitemap.xml'] = 'seo/sitemap_articles';
$route['products.xml'] = 'seo/sitemap_products';
$route['page-sitemap.xml'] = 'seo/sitemap_pages';
$route['category-sitemap.xml'] = 'seo/category_sitemap';


function get_redirect_content()
{
    $file_redirect = fopen('./redirect.txt', "r") or exit("Unable to open file!");
    $redirect_content = [];
    while (!feof($file_redirect)) {
        array_push($redirect_content, fgets($file_redirect));
    }
    fclose($file_redirect);
    $redirect_content = array_filter($redirect_content);
    $redirect_content = array_map(
        function($value) { return json_decode($value); },
        $redirect_content
    );
    return $redirect_content;
}
 
$admin_redirect = get_redirect_content();
foreach ($admin_redirect as $value){
    $route[$value[0]] = 'Redirect_controller/goto';
}


if (is_file('./theme/config.php')) {
    require './theme/config.php';
    if (isset($theme_config)) {
        if (is_array($theme_config)) {
            if (isset($theme_config['route'])) {
                foreach ($theme_config['route'] as $key => $value) {
                    $route[$key] = $value;
                }
            }
        }
    }
}

