<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Article
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 * @property Article_model $article_model
 * @property Option_model $option_model
 */
class Article extends Site_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function category($term, $page = 1)
    {
        if (!is_numeric($page))
            $page = 1;

        if ($term->type == 'post_page')
            $term->id = 0;

        //  Tổng số sản phẩm tìm được
        $total = $this->article_model->count_by_term($term->id);

        //  Số bài viết trên mỗi trang
        $limit = 15;
        $config = $this->option_model->get('articles_per_page');
        if ($config != null)
            $limit = intval($config->value);

        //  Phân trang
        $pagination = Pagination::calc($total, $page, $limit);
        if ($pagination->total_page > 0) {
            if ($page > $pagination->total_page) {
                $page = $pagination->total_page;
                if ($page == 1)
                    return redirect(base_url($term->slug), 'location', 301);
                return redirect(base_url($term->slug . '/' . $page), 'location', 301);
            }
        }

        $articles = $this->article_model->get_by_term($term->id, $page, $limit);
        $breadcrumbs = array(
            array(
                "title" => $term->name,
                "url" => current_url(),
            ),
        );
//        echo "<pre>";
//        print_r($term);
//        echo "</pre>";
        return $this->blade
            ->set('term', $term)
            ->set('articles', $articles)
            ->set('breadcrumbs_front', $breadcrumbs)
            ->set('pagination', $pagination)->render('article/category');
    }

    public function detail($term, $slug)
    {
        $article = $this->article_model->get_by_slug($slug);
        if ($article == null)
            return $this->blade->render('errors/index');

        set_head_title($article->seo_title == null ? $article->name : $article->seo_title);
        set_head_des($article->seo_description == null ? $article->short_content : $article->seo_description);
        set_head_index($article->seo_index);
        set_head_image($article->image_path == null ? null : base_url($article->image_path));

        $term = $this->term_model->get_by_slug($term);
        if ($term == null) {
            $term = new stdClass();
            $term->id = 0;
            $term->slug = 'blog';
            $term->name = 'Blog';
        }

        $limit = 5;
        $option = $this->option_model->get('related_articles');
        if ($option != null)
            $limit = intval($option->value);

        $this->_update_views($article->id);
//        $related_articles = $this->article_model->get_related($term->id, $article->id, $limit);
        //hanhcode
        $breadcrumbs = array(
            array(
                "title" => "Tin Tức",
                "url" => "https://basso.vn/tin-tuc",
            ),
            array(
                "title" => $article->name,
                "url" => current_url(),
            ),
        );
        $related_articles = $this->article_model->get_by_term(0, 1, 4);;
        return $this->blade
            ->set('term', $term)
            ->set('article', $article)
            ->set('breadcrumbs_front', $breadcrumbs)
            ->set('related_articles', $related_articles)
            ->render();
    }

    private function _update_views(int $article_id)
    {
        if (get_cookie("view_$article_id") == null) {
            $this->article_model->update_view($article_id);
            set_cookie("view_$article_id", "view_$article_id", 6 * 3600, null, '/');
        }
    }
}