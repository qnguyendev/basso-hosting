<?php
/**
 * Class Redirect_controller
 * https://www.tutorialspoint.com/codeigniter/codeigniter_page_redirection.htm
 */
class Redirect_controller extends CI_Model {

    public function index($slug='') {
        /*Load the URL helper*/
        $this->load->helper('url');

        /*Redirect the user to some site*/
        redirect('https://basso.vn/lien-he');
    }

    public function goto() {
        $admin_redirect = get_redirect_content();
        $this->load->helper('url');
        foreach ($admin_redirect as $value){
            if ($value[0] == $this->uri->uri_string())
                redirect($value[1],'auto',$value[2]);
            //redirect(site_url($value[1]),'auto',$value[2]);
        }
    }
}