<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Option_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 */
class Option_model extends CI_Model
{
    public function get($name)
    {
        $this->db->where('name', $name);
        return $this->db->get('options')->row();
    }

    public function save($name, $value)
    {
        if ($this->get($name) == null) {
            $this->db->insert('options',
                array(
                    'name' => $name,
                    'value' => $value
                ));
        } else {
            $this->db->where('name', $name);
            $this->db->update('options', array('value' => $value));
        }
    }
}