<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Member_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Team_model extends CI_Model
{
    public function get_by_slug($slug)
    {
        $this->db->where('slug', $slug);
        return $this->db->get('members')->row();
    }

    public function get($id = null, $limit = 0)
    {
        if ($id == null) {
            if ($limit > 0)
                $this->db->limit($limit);
            return $this->db->get('members')->result();
        }

        if (!is_array($id)) {
            $this->db->where('id', $id);
            return $this->db->get('members')->row();
        }

        $this->db->where_in('id', $id);
        return $this->db->get('members')->result();
    }
}