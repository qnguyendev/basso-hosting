<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Cart_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 */
class Payment_model extends CI_Model
{
    /**
     * Các phương thức thanh toán có hiệu lực
     * @return array
     */
    public function get_active()
    {
        $this->db->where('active', 1);
        return $this->db->get("payments")->result();
    }

    public function get_by_id($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('payments')->row();
    }

    public function get_by_type($type)
    {
        $this->db->where('type', $type);
        return $this->db->get('payments')->row();
    }

    /**
     * Danh sách tài khoản ngân hàng cho chuyển khoản
     * @return array
     */
    public function get_banks()
    {
        $this->db->select('t.*');
        $this->db->from('payment_detail t');
        $this->db->join('payments o', 'o.id = t.payment_id', 'left');
        $this->db->where('o.type', 'bank-transfer');

        return $this->db->get()->result();
    }
}