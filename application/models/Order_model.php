<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Order_model
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 */
class Order_model extends CI_Model
{
    /**
     * Thêm đơn hàng
     * @param $name
     * @param $phone
     * @param $email
     * @param $note
     * @param $address
     * @param $city_id
     * @param $district_id
     * @param $payment_id
     * @param $payment_type
     * @param $cart biến giỏ hàng của CI
     * @return int
     */
    public function create($name, $phone, $email, $note, $address, $city_id,
                           $district_id, $payment_id, $payment_type, $cart)
    {
        $data = [
            'created_time' => time(),
            'name' => $name,
            'phone' => $phone,
            'email' => $email,
            'address' => $address,
            'city_id' => $city_id,
            'district_id' => $district_id,
            'payment_id' => $payment_id,
            'total' => 0,
            'sub_total' => 0,
            'shipping_status' => 'waiting',
            'payment_status' => $payment_type != 'cod' ? 'waiting' : 'success'
        ];

        if ($note != null) {
            if (!empty($note))
                $data['note'] = $note;
        }

        foreach ($cart as $item) {
            $data['sub_total'] += $item->sale_price == null
                ? $item->price * $item->qty
                : $item->qty * $item->sale_price;

            $data['total'] += $item->price * $item->qty;
        }

        $this->db->insert('orders', $data);
        $order_id = $this->db->insert_id();

        foreach ($cart as $item) {
            $product_id = explode('-', $item->id)[0];
            $order_detail = [
                'order_id' => $order_id,
                'product_id' => $product_id,
                'quantity' => $item->qty,
                'total' => $item->qty * $item->price,
                'sub_total' => $item->sale_price == null
                    ? $item->qty * $item->price
                    : $item->qty * $item->sale_price
            ];

            $this->db->insert('order_detail', $order_detail);
            $order_detail_id = $this->db->insert_id();

            #region Thuộc tính sản phẩm của đơn hàng
            $this->db->where('order_detail_id', $order_detail_id);
            $this->db->delete('order_detail_product');

            if (isset($item->attributes)) {

                foreach ($item->attributes as $att) {
                    $product_att = [
                        'order_detail_id' => $order_detail_id,
                        'attribute_value_id' => $att->id,
                        'attribute_id' => $att->attribute_id,
                        'order_id' => $order_id
                    ];

                    $this->db->insert('order_detail_product', $product_att);
                }
            }
            #endregion
        }

        return $order_id;
    }

    /**
     * Cập nhật mã utm
     * @param $order_id
     * @param $utm
     */
    public function set_utm($order_id, $utm)
    {
        $this->db->where('id', $order_id);
        $this->db->update('orders', $utm);
    }

    /**
     * Lưu transaction id
     * @param $id
     * @param $trans_ref
     */
    public function update_trans_ref($id, $trans_ref)
    {
        $this->db->where('id', $id);
        $this->db->update('orders', array('transaction_id' => $trans_ref));
    }

    /**
     * Cập nhật kết quả giao dịch
     * @param $trans_ref
     * @param $status
     * @param $res_code
     */
    public function update_transaction_result($trans_ref, $status, $res_code)
    {
        $this->db->where('transaction_id', $trans_ref);
        $this->db->update('orders', array('payment_status' => $status, 'transaction_code' => $res_code));
    }

    public function get_by_id($id)
    {
        $this->db->select('t.*, o.name as payment, o.type as payment_type, d.name as city, k.name as district');
        $this->db->from('orders t');
        $this->db->join('payments o', 'o.id = t.payment_id', 'left');
        $this->db->join('cities d', 'd.id = t.city_id', 'left');
        $this->db->join('districts k', 'k.id = t.district_id', 'left');
        $this->db->where('t.id', $id);
        return $this->db->get()->row();
    }

    /**
     * Lấy chi tiết đơn hàng
     * @param $id
     * @return array
     */
    public function get_detail($id)
    {
        $this->db->select('o.slug, o.name, t.quantity, t.total, t.sub_total, t.discount_total, t.discount_code');
        $this->db->from('order_detail t');
        $this->db->join('products o', 't.product_id = o.id', 'right');
        $this->db->where('t.order_id', $id);
        return $this->db->get()->result();
    }
}