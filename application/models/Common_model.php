<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Common
 * @property CI_DB_mysql_driver|CI_DB_query_builder $db
 * @property CI_config $config
 * @property Option_model $option_model
 */
class Common_model extends CI_Model
{
    /**
     * Danh sách tỉnh thành
     * @return array
     */
    public function get_cities()
    {
        $this->db->order_by('name', 'asc');
        $this->db->where_not_in('id', [1, 2]);
        $result = $this->db->get('cities')->result();

        $item = new stdClass();
        $item->id = 2;
        $item->name = 'Hồ Chí Minh';
        array_unshift($result, $item);

        $item = new stdClass();
        $item->id = 1;
        $item->name = 'Hà Nội';
        array_unshift($result, $item);

        return $result;
    }

    /**
     * Danh sách quận huyện theo tỉnh thành
     * @param int|null $city_id
     * @return array
     */
    public function get_districts(int $city_id = null)
    {
        if ($city_id != null) {
            $this->db->where('city_id', $city_id);
            return $this->db->get('districts')->result();
        }

        return [];
    }

    public function update_city(int $id, string $name)
    {
        $this->db->where('id', $id);
        $this->db->update('cities', ['name' => $name]);
    }
}