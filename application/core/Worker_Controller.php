<?php if (!defined('BASEPATH')) exit('No direct access script allowed');

/**
 * Class Worker_Controller
 * @property Ion_auth|Ion_auth_model $ion_auth
 * @property Blade $blade
 * @property CI_Input $input
 * @property CI_Session $session
 * @property CI_Loader $load
 * @property CI_Config $config
 */

use Pheanstalk\Pheanstalk;

class Worker_Controller extends MY_Controller
{
    private $pheanstalk;
    private $tube_prefix;
    protected $worker_status;
    protected $worker_enable = false;

    public function __construct()
    {
        parent::__construct();

        require './theme/config.php';
        if (!isset($theme_config))
            $theme_config = [];

        $server = null;
        $port = 0;

        if (isset($theme_config['pheanstalk'])) {
            $pheanstalk = $theme_config['pheanstalk'];
            $server = $pheanstalk['server'];
            $port = $pheanstalk['port'];

            if (isset($pheanstalk['tube_prefix']))
                $this->tube_prefix = $pheanstalk['tube_prefix'];

            if (isset($pheanstalk['enable'])) {
                if (is_bool($pheanstalk['enable']))
                    $this->worker_enable = $pheanstalk['enable'];
            }
        }

        try {
            $this->pheanstalk = new Pheanstalk($server, $port);
            $this->worker_status = $this->pheanstalk->getConnection()->isServiceListening();
        } catch (Exception $e) {
            $this->worker_enable = false;
            log_message('error', $e->getMessage());
        }
    }

    protected function reverse_job($tube, $assoc = true)
    {
        if ($this->worker_enable) {
            $this->pheanstalk->watch($this->tube_prefix . $tube)->ignore('default');
            if ($this->worker_status) {
                $job = $this->pheanstalk->reserve();
                $data = json_decode($job->getData(), $assoc);
                $this->delete_job($job);

                return $data;
            }
        }
    }

    protected function delete_job($job)
    {
        if ($this->worker_enable) {
            if ($this->worker_status)
                $this->pheanstalk->delete($job);
        }
    }
}